webpackJsonpac__name_([2],{

/***/ 1000:
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-content\">\n  <div class=\"modal-header\">\n    <h4 class=\"modal-title\">Booking({{activeBooking._id}})</h4>\n    <button class=\"close\" aria-label=\"Close\" (click)=\"closeModal()\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n  </div>\n  <div class=\"modal-body\">\n    <div *ngIf=\"activeBooking\" class=\"col-xs-12\">\n      <div class=\"row\">\n        <div class=\"col-md-6 col-sm-6 col-xs-12\">Key</div>\n        <div class=\"col-md-6 col-sm-6 col-xs-12\">Value</div>\n      </div>\n\n      <div class=\"row\" *ngFor=\"let key of activeBooking | pairs \">\n        <div class=\"col-md-6 col-sm-6 col-xs-12\">{{key[0]}}</div>\n        <div class=\"col-md-6 col-sm-6 col-xs-12\">{{key[1]}}</div>\n      </div>\n\n     </div>\n  </div>\n  <div class=\"modal-footer\">\n    <button class=\"btn btn-primary confirm-btn\" (click)=\"closeModal()\">Close</button>\n  </div>\n</div>\n"

/***/ }),

/***/ 1001:
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-content\">\n  <div class=\"modal-header\">\n    <h4 class=\"modal-title\">Edit Details</h4>\n    <button class=\"close\" aria-label=\"Close\" (click)=\"closeModal()\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n  </div>\n  <div class=\"modal-body\">\n    <div class=\"container fluid\">\n    <form [formGroup]=\"form\" (ngSubmit)=\"onSubmit(form.value,$event)\" class=\"form-horizontal\">\n      <div class=\"form-group row\">\n        <div class=\"col-md-2\"></div>\n        <div class=\"col-md-4\"><h4>User Name</h4></div>\n        <input type=\"text\" formControlName=\"name\" [(ngModel)]=\"name\" value={{activeCustomer.userName}}>\n        </div>\n         <div class=\"form-group row\">\n        <div class=\"col-md-2\"></div>\n        <div class=\"col-md-4\"><h4>Bank Name</h4></div>\n        <input type=\"text\" formControlName=\"bank\" [(ngModel)]=\"bank\" value={{activeCustomer.bankDetails.bankName}}>\n        </div>\n         <div class=\"form-group row\">\n        <div class=\"col-md-2\"></div>\n        <div class=\"col-md-4\"><h4>Bank Country</h4></div>\n        <input type=\"text\" formControlName=\"country\" [(ngModel)]=\"country\" value={{activeCustomer.bankDetails.bankCountry}}>\n        </div>\n         <div class=\"form-group row\">\n        <div class=\"col-md-2\"></div>\n        <div class=\"col-md-4\"><h4>Beneficiary Name</h4></div>\n        <input type=\"text\" formControlName=\"beneficiary\" [(ngModel)]=\"beneficiary\" value={{activeCustomer.bankDetails.beneficiaryName}}>\n        </div>\n         <div class=\"form-group row\">\n        <div class=\"col-md-2\"></div>\n        <div class=\"col-md-4\"><h4>iban Number</h4></div>\n        <input type=\"text\" formControlName=\"iban\" [(ngModel)]=\"iban\" value={{activeCustomer.bankDetails.ibanNumber}}>\n        </div>\n  \n\n  <div class=\"form-group\">\n  <div class=\"modal-footer\">\n    <button class=\"btn create-btn \" type=\"submit\">Update</button>\n  </div>\n    </div>\n  \n      </form>\n    <button class=\"btn cancel-btn \" (click)=\"closeModal()\">Cancel</button>\n\n   \n  </div>\n  </div>\n  </div>\n    \n \n"

/***/ }),

/***/ 1002:
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-content\">\n  <div class=\"modal-header\">\n    <h3 class=\"modal-title\">Add Category</h3>\n    <button class=\"close\" aria-label=\"Close\" (click)=\"closeModal()\">\n      <span aria-hidden=\"true\" style=\"color:white\">&times;</span>\n    </button>\n  </div>\n  <div class=\"modal-body\">\n    <div class=\"container fluid\">\n       <form [formGroup]=\"form\"  class=\"form-horizontal\">\n      <div class=\"row\">\n        <div class=\"col-md-2\"></div>\n        <div class=\"col-md-4\"><h4>Category Name</h4></div>\n         <input formControlName=\"name\" placeholder=\"Category Name*\" [(ngModel)] = \"name\" autocomplete=\"off\">\n               <small [hidden]=\"form.controls.name.valid || (form.controls.name.pristine && !submitted)\" class=\"text-danger\">\n                 Name is required.\n              </small>\n        <!--<div class=\"col-md-6\"> {{activeCustomer.bankName}}</div>-->\n      </div>\n      <!--<div class=\"text-danger\">Name is Required*</div>-->\n      <div class=\"row\">\n        <div class=\"col-md-2\"></div>\n        <div class=\"col-md-4\"><h4>Image</h4></div>\n        <!--<div class=\"col-md-6\"> {{activeCustomer.bankCountry}}</div>-->\n           <input type=\"file\" imageUpload\n       (change)=\"selected($event)\" formControlName=\"image\" [(ngModel)]=\"image\" accept=\"image/*\"  >\n\n         <!--<small [hidden]=\"form.controls.image.valid || (form.controls.name.pristine && !submitted)\" class=\"text-danger\">\n                 Image is required.\n              </small>-->\n              <small [hidden]=\"form.controls.image.valid || (form.controls.image.pristine && !submitted)\" class=\"text-danger\">\n                 Image is required.\n              </small>\n      </div>\n      <!--<div class=\"text-danger\">Image is Required*</div>-->\n       <!--<div class=\"row\">\n        <div class=\"col-md-2\"></div>\n        <div class=\"col-md-4\"><h4>Beneficiary Name</h4></div>\n      \n      </div>-->\n       <!--<div class=\"row\">\n        <div class=\"col-md-2\"></div>\n        <div class=\"col-md-4\"><h4>iban Number</h4></div>\n      \n      </div>-->\n       </form>\n    </div>\n  </div>\n  \n  <div class=\"modal-footer\">\n      <button   class=\"btn btn-primary\" (click)=\"onSubmit(form.value)\">Add </button>\n    <button class=\"btn btn-danger\" (click)=\"closeModal()\">Close</button>\n  </div>\n</div>\n\n"

/***/ }),

/***/ 1003:
/***/ (function(module, exports) {

module.exports = "<!--<div class=\"modal-content\">\n  <form [formGroup]=\"form\" (ngSubmit)=\"onSubmit(form.value)\" class=\"form-horizontal\">\n    <div class=\"modal-header\">\n\n      <h4 class=\"modal-title\">Edit Customer</h4>\n      <button class=\"close\" aria-label=\"Close\" (click)=\"closeModal()\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n    </div>\n    <div class=\"modal-body\" style=\"background-color: lightskyblue;\">\n\n<div class=\"form-group row\" [ngClass]=\"{'has-error': (!name.valid && name.touched), 'has-success': (name.valid && name.touched)}\">\n  <label for=\"inputName3\" class=\"col-sm-3 control-label\">customerName</label>\n\n  <div class=\"col-sm-9\">\n    <input [formControl]=\"name\" type=\"text\" class=\"form-control\" id=\"inputName3\" placeholder=\"Full Name\">\n  </div>\n</div>\n<div class=\"form-group row\" [ngClass]=\"{'has-error': (!countryCode.valid && countryCode.touched), 'has-success': (countryCode.valid && countryCode.touched)}\">\n  <label for=\"countryCode\" class=\"col-sm-3 control-label\">customerCountryCode</label>\n\n  <div class=\"col-sm-9\">\n    <input [formControl]=\"countryCode\" type=\"text\" class=\"form-control\" id=\"countryCode\" placeholder=\"Country Code\">\n  </div>\n</div>\n\n<div class=\"form-group row\" [ngClass]=\"{'has-error': (!mobile.valid && mobile.touched), 'has-success': (mobile.valid && mobile.touched)}\">\n  <label for=\"mobile\" class=\"col-sm-3 control-label\">customerPhone</label>\n\n  <div class=\"col-sm-9\">\n    <input [formControl]=\"mobile\" type=\"text\" class=\"form-control\" id=\"mobile\" placeholder=\"Mobile\">\n  </div>\n</div>\n\n<div class=\"form-group row\" [ngClass]=\"{'has-error': (!companyName.valid && companyName.touched), 'has-success': (companyName.valid && companyName.touched)}\">\n  <label for=\"companyName\" class=\"col-sm-3 control-label\">companyName</label>\n\n  <div class=\"col-sm-9\">\n    <input [formControl]=\"companyName\" type=\"text\" class=\"form-control\" id=\"companyName\" placeholder=\"Company Name\">\n  </div>\n</div>\n\n\n<div class=\"form-group row\" [ngClass]=\"{'has-error': (!companyAddress.valid && companyAddress.touched), 'has-success': (companyAddress.valid && companyAddress.touched)}\">\n  <label for=\"companyAddress\" class=\"col-sm-3 control-label\">companyAddress</label>\n\n  <div class=\"col-sm-9\">\n    <input [formControl]=\"companyAddress\" type=\"text\" class=\"form-control\" id=\"companyAddress\" placeholder=\"Company Address\">\n  </div>\n</div>\n</div>\n<div class=\"modal-footer\">\n  \n  <button [disabled]=\"!form.valid\" type=\"submit\" class=\"btn btn-primary\">Update</button>\n \n  <button class=\"btn btn-danger \" (click)=\"closeModal()\">customerClose</button>\n</div>\n</form>\n</div>-->\n\n<div class=\"modal-content commanmodel\">\n  <div class=\"modal-header\">\n    <h2 class=\"modal-title\">Edit Details</h2>\n    <button class=\"close\" aria-label=\"Close\" (click)=\"closeModal()\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n  </div>\n  <div class=\"modal-body\">\n    <div class=\"container fluid\">\n      <form [formGroup]=\"form\" (ngSubmit)=\"onSubmit(form.value,$event)\" class=\"form-horizontal\" >\n       \n       \n       \n        <div class=\"form-group \">\n      <div class=\"row\">\n          <div class=\"col-sm-4\">\n            <h4 class=\"modal-title\">User Name</h4>\n          </div>\n            <div class=\"col-sm-7\">\n       \n            <input type=\"text\" class=\"form-control\" formControlName=\"name\" [(ngModel)]=\"name\" value=\"{{activeCustomer.name}}\">\n       \n            </div>\n        </div>\n        </div>\n        \n\n<!--<div class=\"form-group \">\n\n<div class=\"row\">\n     <div class=\"col-sm-4 \"><h4 class=\"modal-title\">Profile Picture</h4></div>\n\n\n\n     \n      <div class=\"col-sm-7 \">\n     \n     \n<input class=\"form-control\"  type=\"file\" imageUpload\n       (change)=\"selected($event)\" style=\"color:transparent\" accept=\"image/x-png,image/gif,image/jpeg\" formControlName=\"image\" [(ngModel)]=\"image\">\n      \n      </div>\n\n</div>\n\n\n\n</div>\n<div class=\"form-group row\">\n          <div class=\"col-md-6\" ></div>\n          <img width=\"200\" height=\"200\"[src]=\"src\" />\n        \n</div>-->\n        <div class=\"form-group\">\n          <div class=\"modal-footer\">\n            <button class=\"btn btn-primary confirm-btn\" [disabled]=\"!form.valid\" type=\"submit\">Update</button>\n\n <button type=\"button\" class=\"btn btn-danger\" (click)=\"closeModal()\">Cancel</button>\n\n          </div>\n        </div>\n      </form>\n     \n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ 1015:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(985);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(10)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../../../node_modules/raw-loader/index.js!../../../../../../node_modules/sass-loader/lib/loader.js!./all-bookings.scss", function() {
			var newContent = require("!!../../../../../../node_modules/raw-loader/index.js!../../../../../../node_modules/sass-loader/lib/loader.js!./all-bookings.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1016:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(851);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(10)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../../../node_modules/raw-loader/index.js!../../../../../../node_modules/sass-loader/lib/loader.js!./delete-item-modal.scss", function() {
			var newContent = require("!!../../../../../../node_modules/raw-loader/index.js!../../../../../../node_modules/sass-loader/lib/loader.js!./delete-item-modal.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1017:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(988);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(10)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../../../node_modules/raw-loader/index.js!../../../../../../node_modules/sass-loader/lib/loader.js!./edit-item-modal.scss", function() {
			var newContent = require("!!../../../../../../node_modules/raw-loader/index.js!../../../../../../node_modules/sass-loader/lib/loader.js!./edit-item-modal.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 710:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_pipes__ = __webpack_require__(816);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_add_item_modal_add_item_modal_component__ = __webpack_require__(828);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__theme_nga_module__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__bookings_routing__ = __webpack_require__(853);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__bookings_component__ = __webpack_require__(852);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_AllBookings_all_bookings_component__ = __webpack_require__(827);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ngx_pagination__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_CancelBookingModal_cancel_booking_modal_component__ = __webpack_require__(855);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_delete_item_modal_delete_item_modal_component__ = __webpack_require__(829);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_BookingModal_booking_modal_component__ = __webpack_require__(854);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_edit_item_modal_edit_item_modal_component__ = __webpack_require__(830);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BookingsModule", function() { return BookingsModule; });


//import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

//pipes






//import { PastBookings } from './components/PastBookings/past-bookings.component';






var BookingsModule = (function () {
    function BookingsModule() {
    }
    return BookingsModule;
}());
BookingsModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_6__angular_forms__["FormsModule"],
            __WEBPACK_IMPORTED_MODULE_6__angular_forms__["ReactiveFormsModule"],
            __WEBPACK_IMPORTED_MODULE_5__theme_nga_module__["a" /* NgaModule */],
            __WEBPACK_IMPORTED_MODULE_10_ngx_pagination__["a" /* NgxPaginationModule */],
            __WEBPACK_IMPORTED_MODULE_7__bookings_routing__["a" /* routing */],
            __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["b" /* NgbModalModule */],
            __WEBPACK_IMPORTED_MODULE_3_ngx_pipes__["a" /* NgPipesModule */]
        ],
        declarations: [
            __WEBPACK_IMPORTED_MODULE_8__bookings_component__["a" /* Bookings */],
            //PastBookings,
            __WEBPACK_IMPORTED_MODULE_9__components_AllBookings_all_bookings_component__["a" /* AllBookings */],
            //OnBookings,
            __WEBPACK_IMPORTED_MODULE_13__components_BookingModal_booking_modal_component__["a" /* BookingModal */],
            __WEBPACK_IMPORTED_MODULE_11__components_CancelBookingModal_cancel_booking_modal_component__["a" /* CancelBookingModal */],
            __WEBPACK_IMPORTED_MODULE_12__components_delete_item_modal_delete_item_modal_component__["a" /* DeleteCustomerModal */],
            __WEBPACK_IMPORTED_MODULE_4__components_add_item_modal_add_item_modal_component__["a" /* AddItemModal */],
            __WEBPACK_IMPORTED_MODULE_14__components_edit_item_modal_edit_item_modal_component__["a" /* EditItemModal */]
        ],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_11__components_CancelBookingModal_cancel_booking_modal_component__["a" /* CancelBookingModal */],
            __WEBPACK_IMPORTED_MODULE_13__components_BookingModal_booking_modal_component__["a" /* BookingModal */],
            __WEBPACK_IMPORTED_MODULE_12__components_delete_item_modal_delete_item_modal_component__["a" /* DeleteCustomerModal */],
            __WEBPACK_IMPORTED_MODULE_4__components_add_item_modal_add_item_modal_component__["a" /* AddItemModal */],
            __WEBPACK_IMPORTED_MODULE_14__components_edit_item_modal_edit_item_modal_component__["a" /* EditItemModal */]
        ],
    })
], BookingsModule);



/***/ }),

/***/ 713:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["g"] = isUndefined;
/* harmony export (immutable) */ __webpack_exports__["e"] = isFunction;
/* harmony export (immutable) */ __webpack_exports__["d"] = isNumber;
/* harmony export (immutable) */ __webpack_exports__["f"] = isString;
/* harmony export (immutable) */ __webpack_exports__["k"] = isBoolean;
/* harmony export (immutable) */ __webpack_exports__["a"] = isObject;
/* harmony export (immutable) */ __webpack_exports__["h"] = isNumberFinite;
/* harmony export (immutable) */ __webpack_exports__["i"] = applyPrecision;
/* harmony export (immutable) */ __webpack_exports__["j"] = extractDeepPropertyByMapKey;
/* harmony export (immutable) */ __webpack_exports__["b"] = getKeysTwoObjects;
/* harmony export (immutable) */ __webpack_exports__["c"] = isDeepEqual;
function isUndefined(value) {
    return typeof value === 'undefined';
}
function isFunction(value) {
    return typeof value === 'function';
}
function isNumber(value) {
    return typeof value === 'number';
}
function isString(value) {
    return typeof value === 'string';
}
function isBoolean(value) {
    return typeof value === 'boolean';
}
function isObject(value) {
    return value !== null && typeof value === 'object';
}
function isNumberFinite(value) {
    return isNumber(value) && isFinite(value);
}
function applyPrecision(num, precision) {
    if (precision <= 0) {
        return Math.round(num);
    }
    var tho = Math.pow(10, precision);
    return Math.round(num * tho) / tho;
}
function extractDeepPropertyByMapKey(obj, map) {
    var keys = map.split('.');
    var key = keys.shift();
    return keys.reduce(function (prop, key) {
        return !isUndefined(prop) && !isUndefined(prop[key])
            ? prop[key]
            : undefined;
    }, obj[key || '']);
}
function getKeysTwoObjects(obj, other) {
    return Object.keys(obj).concat(Object.keys(other)).filter(function (key, index, array) { return array.indexOf(key) === index; });
}
function isDeepEqual(obj, other) {
    if (!isObject(obj) || !isObject(other)) {
        return obj === other;
    }
    return getKeysTwoObjects(obj, other).every(function (key) {
        if (!isObject(obj[key]) && !isObject(other[key])) {
            return obj[key] === other[key];
        }
        if (!isObject(obj[key]) || !isObject(other[key])) {
            return false;
        }
        return isDeepEqual(obj[key], other[key]);
    });
}
//# sourceMappingURL=helpers.js.map

/***/ }),

/***/ 725:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DiffPipe; });

var DiffPipe = (function () {
    function DiffPipe() {
    }
    DiffPipe.prototype.transform = function (input) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (!Array.isArray(input)) {
            return input;
        }
        return args.reduce(function (d, c) { return d.filter(function (e) { return !~c.indexOf(e); }); }, input);
    };
    return DiffPipe;
}());

DiffPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'diff' },] },
];
/** @nocollapse */
DiffPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=diff.js.map

/***/ }),

/***/ 726:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EveryPipe; });

var EveryPipe = (function () {
    function EveryPipe() {
    }
    EveryPipe.prototype.transform = function (input, predicate) {
        return Array.isArray(input) ? input.every(predicate) : false;
    };
    return EveryPipe;
}());

EveryPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'every' },] },
];
/** @nocollapse */
EveryPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=every.js.map

/***/ }),

/***/ 727:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterByPipe; });


var FilterByPipe = (function () {
    function FilterByPipe() {
    }
    FilterByPipe.prototype.transform = function (input, props, search, strict) {
        if (search === void 0) { search = ''; }
        if (strict === void 0) { strict = false; }
        if (!Array.isArray(input) || (!__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(search) && !__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["h" /* isNumberFinite */])(search) && !__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["k" /* isBoolean */])(search))) {
            return input;
        }
        var term = String(search).toLowerCase();
        return input.filter(function (obj) {
            return props.some(function (prop) {
                var value = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["j" /* extractDeepPropertyByMapKey */])(obj, prop), strValue = String(value).toLowerCase();
                if (__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["g" /* isUndefined */])(value)) {
                    return false;
                }
                return strict
                    ? term === strValue
                    : !!~strValue.indexOf(term);
            });
        });
    };
    return FilterByPipe;
}());

FilterByPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'filterBy', pure: false },] },
];
/** @nocollapse */
FilterByPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=filter-by.js.map

/***/ }),

/***/ 728:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FlattenPipe; });

var FlattenPipe = (function () {
    function FlattenPipe() {
    }
    FlattenPipe.prototype.transform = function (input, shallow) {
        if (shallow === void 0) { shallow = false; }
        if (!Array.isArray(input)) {
            return input;
        }
        return shallow
            ? [].concat.apply([], input)
            : this.flatten(input);
    };
    FlattenPipe.prototype.flatten = function (array) {
        var _this = this;
        return array.reduce(function (arr, elm) {
            if (Array.isArray(elm)) {
                return arr.concat(_this.flatten(elm));
            }
            return arr.concat(elm);
        }, []);
    };
    return FlattenPipe;
}());

FlattenPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'flatten' },] },
];
/** @nocollapse */
FlattenPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=flatten.js.map

/***/ }),

/***/ 729:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupByPipe; });


var GroupByPipe = (function () {
    function GroupByPipe() {
    }
    GroupByPipe.prototype.transform = function (input, discriminator, delimiter) {
        if (discriminator === void 0) { discriminator = []; }
        if (delimiter === void 0) { delimiter = '|'; }
        if (!Array.isArray(input)) {
            return input;
        }
        return this.groupBy(input, discriminator, delimiter);
    };
    GroupByPipe.prototype.groupBy = function (list, discriminator, delimiter) {
        var _this = this;
        return list.reduce(function (acc, payload) {
            var key = _this.extractKeyByDiscriminator(discriminator, payload, delimiter);
            acc[key] = Array.isArray(acc[key])
                ? acc[key].concat([payload])
                : [payload];
            return acc;
        }, {});
    };
    GroupByPipe.prototype.extractKeyByDiscriminator = function (discriminator, payload, delimiter) {
        if (__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["e" /* isFunction */])(discriminator)) {
            return discriminator(payload);
        }
        if (Array.isArray(discriminator)) {
            return discriminator.map(function (k) { return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["j" /* extractDeepPropertyByMapKey */])(payload, k); }).join(delimiter);
        }
        return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["j" /* extractDeepPropertyByMapKey */])(payload, discriminator);
    };
    return GroupByPipe;
}());

GroupByPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'groupBy', pure: false },] },
];
/** @nocollapse */
GroupByPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=group-by.js.map

/***/ }),

/***/ 730:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__diff__ = __webpack_require__(725);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__initial__ = __webpack_require__(731);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__flatten__ = __webpack_require__(728);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__intersection__ = __webpack_require__(732);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__reverse__ = __webpack_require__(735);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__tail__ = __webpack_require__(739);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__truthify__ = __webpack_require__(740);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__union__ = __webpack_require__(741);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__unique__ = __webpack_require__(742);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__without__ = __webpack_require__(743);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pluck__ = __webpack_require__(734);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__shuffle__ = __webpack_require__(737);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__every__ = __webpack_require__(726);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__some__ = __webpack_require__(738);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__sample__ = __webpack_require__(736);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__group_by__ = __webpack_require__(729);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__filter_by__ = __webpack_require__(727);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__order_by__ = __webpack_require__(733);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__angular_core__ = __webpack_require__(0);
/* unused harmony reexport DiffPipe */
/* unused harmony reexport InitialPipe */
/* unused harmony reexport FlattenPipe */
/* unused harmony reexport IntersectionPipe */
/* unused harmony reexport ReversePipe */
/* unused harmony reexport TailPipe */
/* unused harmony reexport TrurthifyPipe */
/* unused harmony reexport UnionPipe */
/* unused harmony reexport UniquePipe */
/* unused harmony reexport WithoutPipe */
/* unused harmony reexport PluckPipe */
/* unused harmony reexport ShufflePipe */
/* unused harmony reexport EveryPipe */
/* unused harmony reexport SomePipe */
/* unused harmony reexport SamplePipe */
/* unused harmony reexport GroupByPipe */
/* unused harmony reexport FilterByPipe */
/* unused harmony reexport OrderByPipe */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NgArrayPipesModule; });



















var ARRAY_PIPES = [
    __WEBPACK_IMPORTED_MODULE_0__diff__["a" /* DiffPipe */], __WEBPACK_IMPORTED_MODULE_2__flatten__["a" /* FlattenPipe */], __WEBPACK_IMPORTED_MODULE_1__initial__["a" /* InitialPipe */], __WEBPACK_IMPORTED_MODULE_3__intersection__["a" /* IntersectionPipe */], __WEBPACK_IMPORTED_MODULE_4__reverse__["a" /* ReversePipe */], __WEBPACK_IMPORTED_MODULE_5__tail__["a" /* TailPipe */],
    __WEBPACK_IMPORTED_MODULE_6__truthify__["a" /* TrurthifyPipe */], __WEBPACK_IMPORTED_MODULE_7__union__["a" /* UnionPipe */], __WEBPACK_IMPORTED_MODULE_8__unique__["a" /* UniquePipe */], __WEBPACK_IMPORTED_MODULE_9__without__["a" /* WithoutPipe */], __WEBPACK_IMPORTED_MODULE_10__pluck__["a" /* PluckPipe */], __WEBPACK_IMPORTED_MODULE_11__shuffle__["a" /* ShufflePipe */],
    __WEBPACK_IMPORTED_MODULE_12__every__["a" /* EveryPipe */], __WEBPACK_IMPORTED_MODULE_13__some__["a" /* SomePipe */], __WEBPACK_IMPORTED_MODULE_14__sample__["a" /* SamplePipe */], __WEBPACK_IMPORTED_MODULE_15__group_by__["a" /* GroupByPipe */], __WEBPACK_IMPORTED_MODULE_16__filter_by__["a" /* FilterByPipe */], __WEBPACK_IMPORTED_MODULE_17__order_by__["a" /* OrderByPipe */]
];
var NgArrayPipesModule = (function () {
    function NgArrayPipesModule() {
    }
    return NgArrayPipesModule;
}());

NgArrayPipesModule.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_18__angular_core__["NgModule"], args: [{
                declarations: ARRAY_PIPES,
                imports: [],
                exports: ARRAY_PIPES
            },] },
];
/** @nocollapse */
NgArrayPipesModule.ctorParameters = function () { return []; };


















//# sourceMappingURL=index.js.map

/***/ }),

/***/ 731:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InitialPipe; });

var InitialPipe = (function () {
    function InitialPipe() {
    }
    InitialPipe.prototype.transform = function (input, num) {
        if (num === void 0) { num = 0; }
        return Array.isArray(input)
            ? input.slice(0, input.length - num)
            : input;
    };
    return InitialPipe;
}());

InitialPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'initial' },] },
];
/** @nocollapse */
InitialPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=initial.js.map

/***/ }),

/***/ 732:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IntersectionPipe; });

var IntersectionPipe = (function () {
    function IntersectionPipe() {
    }
    IntersectionPipe.prototype.transform = function (input) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (!Array.isArray(input)) {
            return input;
        }
        return args.reduce(function (n, c) { return n.filter(function (e) { return !!~c.indexOf(e); }); }, input);
    };
    return IntersectionPipe;
}());

IntersectionPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'intersection' },] },
];
/** @nocollapse */
IntersectionPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=intersection.js.map

/***/ }),

/***/ 733:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderByPipe; });


var OrderByPipe = (function () {
    function OrderByPipe() {
    }
    OrderByPipe.prototype.transform = function (input, config) {
        if (!Array.isArray(input)) {
            return input;
        }
        var out = input.slice();
        // sort by multiple properties
        if (Array.isArray(config)) {
            return out.sort(function (a, b) {
                for (var i = 0, l = config.length; i < l; ++i) {
                    var _a = OrderByPipe.extractFromConfig(config[i]), prop = _a[0], asc = _a[1];
                    var pos = OrderByPipe.orderCompare(prop, asc, a, b);
                    if (pos !== 0) {
                        return pos;
                    }
                }
                return 0;
            });
        }
        // sort by a single property value
        if (__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(config)) {
            var _a = OrderByPipe.extractFromConfig(config), prop = _a[0], asc = _a[1], sign = _a[2];
            if (config.length === 1) {
                switch (sign) {
                    case '+': return out.sort(OrderByPipe.simpleSort.bind(this));
                    case '-': return out.sort(OrderByPipe.simpleSort.bind(this)).reverse();
                }
            }
            return out.sort(OrderByPipe.orderCompare.bind(this, prop, asc));
        }
        // default sort by value
        return out.sort(OrderByPipe.simpleSort.bind(this));
    };
    OrderByPipe.simpleSort = function (a, b) {
        return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(a) && __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(b)
            ? a.toLowerCase().localeCompare(b.toLowerCase())
            : a - b;
    };
    OrderByPipe.orderCompare = function (prop, asc, a, b) {
        var first = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["j" /* extractDeepPropertyByMapKey */])(a, prop), second = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["j" /* extractDeepPropertyByMapKey */])(b, prop);
        if (first === second) {
            return 0;
        }
        if (__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["g" /* isUndefined */])(first) || first === '') {
            return 1;
        }
        if (__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["g" /* isUndefined */])(second) || second === '') {
            return -1;
        }
        if (__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(first) && __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(second)) {
            var pos = first.toLowerCase().localeCompare(second.toLowerCase());
            return asc ? pos : -pos;
        }
        return asc
            ? first - second
            : second - first;
    };
    OrderByPipe.extractFromConfig = function (config) {
        var sign = config.substr(0, 1);
        var prop = config.replace(/^[-+]/, '');
        var asc = sign !== '-';
        return [prop, asc, sign];
    };
    return OrderByPipe;
}());

OrderByPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'orderBy', pure: false },] },
];
/** @nocollapse */
OrderByPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=order-by.js.map

/***/ }),

/***/ 734:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PluckPipe; });


var PluckPipe = (function () {
    function PluckPipe() {
    }
    PluckPipe.prototype.transform = function (input, map) {
        return Array.isArray(input)
            ? input.map(function (e) { return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["j" /* extractDeepPropertyByMapKey */])(e, map); })
            : input;
    };
    return PluckPipe;
}());

PluckPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'pluck', pure: false },] },
];
/** @nocollapse */
PluckPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=pluck.js.map

/***/ }),

/***/ 735:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReversePipe; });


var ReversePipe = (function () {
    function ReversePipe() {
    }
    ReversePipe.prototype.transform = function (input) {
        if (__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(input)) {
            return input.split('').reverse().join('');
        }
        return Array.isArray(input)
            ? input.reverse()
            : input;
    };
    return ReversePipe;
}());

ReversePipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'reverse' },] },
];
/** @nocollapse */
ReversePipe.ctorParameters = function () { return []; };
//# sourceMappingURL=reverse.js.map

/***/ }),

/***/ 736:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SamplePipe; });

var SamplePipe = (function () {
    function SamplePipe() {
    }
    SamplePipe.prototype.transform = function (input, len) {
        if (len === void 0) { len = 1; }
        if (!Array.isArray(input)) {
            return input;
        }
        var sample = [];
        for (var i = 0, tmp = input.slice(), l = len < tmp.length ? len : tmp.length; i < l; ++i) {
            sample = sample.concat(tmp.splice(Math.floor(Math.random() * tmp.length), 1));
        }
        return sample;
    };
    return SamplePipe;
}());

SamplePipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'sample' },] },
];
/** @nocollapse */
SamplePipe.ctorParameters = function () { return []; };
//# sourceMappingURL=sample.js.map

/***/ }),

/***/ 737:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShufflePipe; });

var ShufflePipe = (function () {
    function ShufflePipe() {
    }
    // Using a version of the Fisher-Yates shuffle algorithm
    // https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
    ShufflePipe.prototype.transform = function (input) {
        if (!Array.isArray(input)) {
            return input;
        }
        var shuffled = input.slice();
        for (var i = 0, n = input.length - 1, l = n - 1; i < l; ++i) {
            var j = Math.floor(Math.random() * (n - i + 1)) + i;
            _a = [shuffled[j], shuffled[i]], shuffled[i] = _a[0], shuffled[j] = _a[1];
        }
        return shuffled;
        var _a;
    };
    return ShufflePipe;
}());

ShufflePipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'shuffle' },] },
];
/** @nocollapse */
ShufflePipe.ctorParameters = function () { return []; };
//# sourceMappingURL=shuffle.js.map

/***/ }),

/***/ 738:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SomePipe; });

var SomePipe = (function () {
    function SomePipe() {
    }
    SomePipe.prototype.transform = function (input, predicate) {
        return Array.isArray(input) ? input.some(predicate) : input;
    };
    return SomePipe;
}());

SomePipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'some' },] },
];
/** @nocollapse */
SomePipe.ctorParameters = function () { return []; };
//# sourceMappingURL=some.js.map

/***/ }),

/***/ 739:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TailPipe; });

var TailPipe = (function () {
    function TailPipe() {
    }
    TailPipe.prototype.transform = function (input, num) {
        if (num === void 0) { num = 0; }
        return Array.isArray(input) ? input.slice(num) : input;
    };
    return TailPipe;
}());

TailPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'tail' },] },
];
/** @nocollapse */
TailPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=tail.js.map

/***/ }),

/***/ 740:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrurthifyPipe; });

var TrurthifyPipe = (function () {
    function TrurthifyPipe() {
    }
    TrurthifyPipe.prototype.transform = function (input) {
        return Array.isArray(input)
            ? input.filter(function (e) { return !!e; })
            : input;
    };
    return TrurthifyPipe;
}());

TrurthifyPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'truthify' },] },
];
/** @nocollapse */
TrurthifyPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=truthify.js.map

/***/ }),

/***/ 741:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UnionPipe; });

var UnionPipe = (function () {
    function UnionPipe() {
    }
    UnionPipe.prototype.transform = function (input, args) {
        if (args === void 0) { args = []; }
        if (!Array.isArray(input) || !Array.isArray(args)) {
            return input;
        }
        return args.reduce(function (newArr, currArr) {
            return newArr.concat(currArr.reduce(function (noDupArr, curr) {
                return (!~noDupArr.indexOf(curr) && !~newArr.indexOf(curr))
                    ? noDupArr.concat([curr])
                    : noDupArr;
            }, []));
        }, input);
    };
    return UnionPipe;
}());

UnionPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'union' },] },
];
/** @nocollapse */
UnionPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=union.js.map

/***/ }),

/***/ 742:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UniquePipe; });


var UniquePipe = (function () {
    function UniquePipe() {
    }
    UniquePipe.prototype.transform = function (input, propertyName) {
        var uniques = [];
        return Array.isArray(input) ?
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["g" /* isUndefined */])(propertyName) ?
                input.filter(function (e, i) { return input.indexOf(e) === i; }) :
                input.filter(function (e, i) {
                    var value = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["j" /* extractDeepPropertyByMapKey */])(e, propertyName);
                    value = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["a" /* isObject */])(value) ? JSON.stringify(value) : value;
                    if (__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["g" /* isUndefined */])(value) || uniques[value]) {
                        return false;
                    }
                    uniques[value] = true;
                    return true;
                }) : input;
    };
    return UniquePipe;
}());

UniquePipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'unique' },] },
];
/** @nocollapse */
UniquePipe.ctorParameters = function () { return []; };
//# sourceMappingURL=unique.js.map

/***/ }),

/***/ 743:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WithoutPipe; });

var WithoutPipe = (function () {
    function WithoutPipe() {
    }
    WithoutPipe.prototype.transform = function (input, args) {
        if (args === void 0) { args = []; }
        return Array.isArray(input)
            ? input.filter(function (e) { return !~args.indexOf(e); })
            : input;
    };
    return WithoutPipe;
}());

WithoutPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'without' },] },
];
/** @nocollapse */
WithoutPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=without.js.map

/***/ }),

/***/ 744:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__is_defined__ = __webpack_require__(746);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__is_null__ = __webpack_require__(756);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__is_undefined__ = __webpack_require__(760);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__is_string__ = __webpack_require__(759);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__is_function__ = __webpack_require__(748);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__is_number__ = __webpack_require__(757);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__is_array__ = __webpack_require__(745);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__is_object__ = __webpack_require__(758);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__is_greater_equal_than__ = __webpack_require__(749);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__is_greater_than__ = __webpack_require__(750);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__is_less_equal_than__ = __webpack_require__(752);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__is_equal_to__ = __webpack_require__(747);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__is_not_equal_to__ = __webpack_require__(754);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__is_identical_to__ = __webpack_require__(751);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__is_not_identical_to__ = __webpack_require__(755);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__is_less_than__ = __webpack_require__(753);
/* unused harmony reexport IsDefinedPipe */
/* unused harmony reexport IsNullPipe */
/* unused harmony reexport IsUndefinedPipe */
/* unused harmony reexport IsStringPipe */
/* unused harmony reexport IsFunctionPipe */
/* unused harmony reexport IsNumberPipe */
/* unused harmony reexport IsArrayPipe */
/* unused harmony reexport IsObjectPipe */
/* unused harmony reexport IsGreaterEqualThanPipe */
/* unused harmony reexport IsGreaterThanPipe */
/* unused harmony reexport IsLessEqualThanPipe */
/* unused harmony reexport IsEqualToPipe */
/* unused harmony reexport IsNotEqualToPipe */
/* unused harmony reexport IsIdenticalToPipe */
/* unused harmony reexport IsNotIdenticalToPipe */
/* unused harmony reexport IsLessThanPipe */
/* unused harmony export BOOLEAN_PIPES */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NgBooleanPipesModule; });

















var BOOLEAN_PIPES = [
    __WEBPACK_IMPORTED_MODULE_1__is_defined__["a" /* IsDefinedPipe */], __WEBPACK_IMPORTED_MODULE_2__is_null__["a" /* IsNullPipe */], __WEBPACK_IMPORTED_MODULE_3__is_undefined__["a" /* IsUndefinedPipe */], __WEBPACK_IMPORTED_MODULE_4__is_string__["a" /* IsStringPipe */], __WEBPACK_IMPORTED_MODULE_5__is_function__["a" /* IsFunctionPipe */], __WEBPACK_IMPORTED_MODULE_6__is_number__["a" /* IsNumberPipe */],
    __WEBPACK_IMPORTED_MODULE_7__is_array__["a" /* IsArrayPipe */], __WEBPACK_IMPORTED_MODULE_8__is_object__["a" /* IsObjectPipe */], __WEBPACK_IMPORTED_MODULE_9__is_greater_equal_than__["a" /* IsGreaterEqualThanPipe */], __WEBPACK_IMPORTED_MODULE_10__is_greater_than__["a" /* IsGreaterThanPipe */], __WEBPACK_IMPORTED_MODULE_11__is_less_equal_than__["a" /* IsLessEqualThanPipe */],
    __WEBPACK_IMPORTED_MODULE_11__is_less_equal_than__["a" /* IsLessEqualThanPipe */], __WEBPACK_IMPORTED_MODULE_12__is_equal_to__["a" /* IsEqualToPipe */], __WEBPACK_IMPORTED_MODULE_13__is_not_equal_to__["a" /* IsNotEqualToPipe */], __WEBPACK_IMPORTED_MODULE_14__is_identical_to__["a" /* IsIdenticalToPipe */], __WEBPACK_IMPORTED_MODULE_15__is_not_identical_to__["a" /* IsNotIdenticalToPipe */],
    __WEBPACK_IMPORTED_MODULE_16__is_less_than__["a" /* IsLessThanPipe */]
];
var NgBooleanPipesModule = (function () {
    function NgBooleanPipesModule() {
    }
    return NgBooleanPipesModule;
}());

NgBooleanPipesModule.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"], args: [{
                declarations: BOOLEAN_PIPES,
                imports: [],
                exports: BOOLEAN_PIPES
            },] },
];
/** @nocollapse */
NgBooleanPipesModule.ctorParameters = function () { return []; };
















//# sourceMappingURL=index.js.map

/***/ }),

/***/ 745:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IsArrayPipe; });

var IsArrayPipe = (function () {
    function IsArrayPipe() {
    }
    IsArrayPipe.prototype.transform = function (input) {
        return Array.isArray(input);
    };
    return IsArrayPipe;
}());

IsArrayPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'isArray' },] },
];
/** @nocollapse */
IsArrayPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=is-array.js.map

/***/ }),

/***/ 746:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IsDefinedPipe; });


var IsDefinedPipe = (function () {
    function IsDefinedPipe() {
    }
    IsDefinedPipe.prototype.transform = function (input) {
        return !__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["g" /* isUndefined */])(input);
    };
    return IsDefinedPipe;
}());

IsDefinedPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'isDefined' },] },
];
/** @nocollapse */
IsDefinedPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=is-defined.js.map

/***/ }),

/***/ 747:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IsEqualToPipe; });

var IsEqualToPipe = (function () {
    function IsEqualToPipe() {
    }
    IsEqualToPipe.prototype.transform = function (input, other) {
        return input == other;
    };
    return IsEqualToPipe;
}());

IsEqualToPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'isEqualTo' },] },
];
/** @nocollapse */
IsEqualToPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=is-equal-to.js.map

/***/ }),

/***/ 748:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IsFunctionPipe; });


var IsFunctionPipe = (function () {
    function IsFunctionPipe() {
    }
    IsFunctionPipe.prototype.transform = function (input) {
        return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["e" /* isFunction */])(input);
    };
    return IsFunctionPipe;
}());

IsFunctionPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'isFunction' },] },
];
/** @nocollapse */
IsFunctionPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=is-function.js.map

/***/ }),

/***/ 749:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IsGreaterEqualThanPipe; });

var IsGreaterEqualThanPipe = (function () {
    function IsGreaterEqualThanPipe() {
    }
    IsGreaterEqualThanPipe.prototype.transform = function (input, other) {
        return input >= other;
    };
    return IsGreaterEqualThanPipe;
}());

IsGreaterEqualThanPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'isGreaterEqualThan' },] },
];
/** @nocollapse */
IsGreaterEqualThanPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=is-greater-equal-than.js.map

/***/ }),

/***/ 750:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IsGreaterThanPipe; });

var IsGreaterThanPipe = (function () {
    function IsGreaterThanPipe() {
    }
    IsGreaterThanPipe.prototype.transform = function (input, other) {
        return input > other;
    };
    return IsGreaterThanPipe;
}());

IsGreaterThanPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'isGreaterThan' },] },
];
/** @nocollapse */
IsGreaterThanPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=is-greater-than.js.map

/***/ }),

/***/ 751:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IsIdenticalToPipe; });

var IsIdenticalToPipe = (function () {
    function IsIdenticalToPipe() {
    }
    IsIdenticalToPipe.prototype.transform = function (input, other) {
        return input === other;
    };
    return IsIdenticalToPipe;
}());

IsIdenticalToPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'isIdenticalTo' },] },
];
/** @nocollapse */
IsIdenticalToPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=is-identical-to.js.map

/***/ }),

/***/ 752:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IsLessEqualThanPipe; });

var IsLessEqualThanPipe = (function () {
    function IsLessEqualThanPipe() {
    }
    IsLessEqualThanPipe.prototype.transform = function (input, other) {
        return input <= other;
    };
    return IsLessEqualThanPipe;
}());

IsLessEqualThanPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'isLessEqualThan' },] },
];
/** @nocollapse */
IsLessEqualThanPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=is-less-equal-than.js.map

/***/ }),

/***/ 753:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IsLessThanPipe; });

var IsLessThanPipe = (function () {
    function IsLessThanPipe() {
    }
    IsLessThanPipe.prototype.transform = function (input, other) {
        return input < other;
    };
    return IsLessThanPipe;
}());

IsLessThanPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'isLessThan' },] },
];
/** @nocollapse */
IsLessThanPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=is-less-than.js.map

/***/ }),

/***/ 754:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IsNotEqualToPipe; });

var IsNotEqualToPipe = (function () {
    function IsNotEqualToPipe() {
    }
    IsNotEqualToPipe.prototype.transform = function (input, other) {
        return input != other;
    };
    return IsNotEqualToPipe;
}());

IsNotEqualToPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'isNotEqualTo' },] },
];
/** @nocollapse */
IsNotEqualToPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=is-not-equal-to.js.map

/***/ }),

/***/ 755:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IsNotIdenticalToPipe; });

var IsNotIdenticalToPipe = (function () {
    function IsNotIdenticalToPipe() {
    }
    IsNotIdenticalToPipe.prototype.transform = function (input, other) {
        return input !== other;
    };
    return IsNotIdenticalToPipe;
}());

IsNotIdenticalToPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'isNotIdenticalTo' },] },
];
/** @nocollapse */
IsNotIdenticalToPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=is-not-identical-to.js.map

/***/ }),

/***/ 756:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IsNullPipe; });

var IsNullPipe = (function () {
    function IsNullPipe() {
    }
    IsNullPipe.prototype.transform = function (input) {
        return input === null;
    };
    return IsNullPipe;
}());

IsNullPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'isNull' },] },
];
/** @nocollapse */
IsNullPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=is-null.js.map

/***/ }),

/***/ 757:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IsNumberPipe; });


var IsNumberPipe = (function () {
    function IsNumberPipe() {
    }
    IsNumberPipe.prototype.transform = function (input) {
        return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["d" /* isNumber */])(input);
    };
    return IsNumberPipe;
}());

IsNumberPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'isNumber' },] },
];
/** @nocollapse */
IsNumberPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=is-number.js.map

/***/ }),

/***/ 758:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IsObjectPipe; });


var IsObjectPipe = (function () {
    function IsObjectPipe() {
    }
    IsObjectPipe.prototype.transform = function (input) {
        return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["a" /* isObject */])(input);
    };
    return IsObjectPipe;
}());

IsObjectPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'isObject' },] },
];
/** @nocollapse */
IsObjectPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=is-object.js.map

/***/ }),

/***/ 759:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IsStringPipe; });


var IsStringPipe = (function () {
    function IsStringPipe() {
    }
    IsStringPipe.prototype.transform = function (input) {
        return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(input);
    };
    return IsStringPipe;
}());

IsStringPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'isString' },] },
];
/** @nocollapse */
IsStringPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=is-string.js.map

/***/ }),

/***/ 760:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IsUndefinedPipe; });


var IsUndefinedPipe = (function () {
    function IsUndefinedPipe() {
    }
    IsUndefinedPipe.prototype.transform = function (input) {
        return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["g" /* isUndefined */])(input);
    };
    return IsUndefinedPipe;
}());

IsUndefinedPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'isUndefined' },] },
];
/** @nocollapse */
IsUndefinedPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=is-undefined.js.map

/***/ }),

/***/ 761:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BytesPipe; });


var BytesPipe = (function () {
    function BytesPipe() {
        this.dictionary = [
            { max: 1e3, type: 'B' },
            { max: 1e6, type: 'KB' },
            { max: 1e9, type: 'MB' },
            { max: 1e12, type: 'GB' }
        ];
    }
    BytesPipe.prototype.transform = function (value, precision) {
        if (!__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["h" /* isNumberFinite */])(value)) {
            return NaN;
        }
        var format = this.dictionary.find(function (d) { return value < d.max; }) || this.dictionary[this.dictionary.length - 1];
        var calc = value / (format.max / 1e3);
        var num = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["g" /* isUndefined */])(precision) ? calc : __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["i" /* applyPrecision */])(calc, precision);
        return num + " " + format.type;
    };
    return BytesPipe;
}());

BytesPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'bytes' },] },
];
/** @nocollapse */
BytesPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=bytes.js.map

/***/ }),

/***/ 762:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CeilPipe; });

var CeilPipe = (function () {
    function CeilPipe() {
    }
    CeilPipe.prototype.transform = function (num, precision) {
        if (precision === void 0) { precision = 0; }
        if (precision <= 0) {
            return Math.ceil(num);
        }
        var tho = Math.pow(10, precision);
        return Math.ceil(num * tho) / tho;
    };
    return CeilPipe;
}());

CeilPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'ceil' },] },
];
/** @nocollapse */
CeilPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=ceil.js.map

/***/ }),

/***/ 763:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DegreesPipe; });


var DegreesPipe = (function () {
    function DegreesPipe() {
    }
    DegreesPipe.prototype.transform = function (radians) {
        if (!__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["h" /* isNumberFinite */])(radians)) {
            return NaN;
        }
        return radians * 180 / Math.PI;
    };
    return DegreesPipe;
}());

DegreesPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'degrees' },] },
];
/** @nocollapse */
DegreesPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=degrees.js.map

/***/ }),

/***/ 764:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FloorPipe; });

var FloorPipe = (function () {
    function FloorPipe() {
    }
    FloorPipe.prototype.transform = function (num, precision) {
        if (precision === void 0) { precision = 0; }
        if (precision <= 0) {
            return Math.floor(num);
        }
        var tho = Math.pow(10, precision);
        return Math.floor(num * tho) / tho;
    };
    return FloorPipe;
}());

FloorPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'floor' },] },
];
/** @nocollapse */
FloorPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=floor.js.map

/***/ }),

/***/ 765:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__max__ = __webpack_require__(766);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__min__ = __webpack_require__(767);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__percentage__ = __webpack_require__(768);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__sum__ = __webpack_require__(772);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__floor__ = __webpack_require__(764);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__round__ = __webpack_require__(770);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__sqrt__ = __webpack_require__(771);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pow__ = __webpack_require__(769);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ceil__ = __webpack_require__(762);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__degrees__ = __webpack_require__(763);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__bytes__ = __webpack_require__(761);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__radians__ = __webpack_require__(817);
/* unused harmony reexport MaxPipe */
/* unused harmony reexport MinPipe */
/* unused harmony reexport PercentagePipe */
/* unused harmony reexport SumPipe */
/* unused harmony reexport FloorPipe */
/* unused harmony reexport RoundPipe */
/* unused harmony reexport SqrtPipe */
/* unused harmony reexport PowerPipe */
/* unused harmony reexport CeilPipe */
/* unused harmony reexport DegreesPipe */
/* unused harmony reexport BytesPipe */
/* unused harmony export MATH_PIPES */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NgMathPipesModule; });













var MATH_PIPES = [
    __WEBPACK_IMPORTED_MODULE_1__max__["a" /* MaxPipe */], __WEBPACK_IMPORTED_MODULE_2__min__["a" /* MinPipe */], __WEBPACK_IMPORTED_MODULE_3__percentage__["a" /* PercentagePipe */], __WEBPACK_IMPORTED_MODULE_4__sum__["a" /* SumPipe */], __WEBPACK_IMPORTED_MODULE_5__floor__["a" /* FloorPipe */], __WEBPACK_IMPORTED_MODULE_6__round__["a" /* RoundPipe */], __WEBPACK_IMPORTED_MODULE_7__sqrt__["a" /* SqrtPipe */], __WEBPACK_IMPORTED_MODULE_8__pow__["a" /* PowerPipe */],
    __WEBPACK_IMPORTED_MODULE_9__ceil__["a" /* CeilPipe */], __WEBPACK_IMPORTED_MODULE_10__degrees__["a" /* DegreesPipe */], __WEBPACK_IMPORTED_MODULE_11__bytes__["a" /* BytesPipe */], __WEBPACK_IMPORTED_MODULE_12__radians__["a" /* RadiansPipe */]
];
var NgMathPipesModule = (function () {
    function NgMathPipesModule() {
    }
    return NgMathPipesModule;
}());

NgMathPipesModule.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"], args: [{
                declarations: MATH_PIPES,
                imports: [],
                exports: MATH_PIPES
            },] },
];
/** @nocollapse */
NgMathPipesModule.ctorParameters = function () { return []; };











//# sourceMappingURL=index.js.map

/***/ }),

/***/ 766:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MaxPipe; });

var MaxPipe = (function () {
    function MaxPipe() {
    }
    MaxPipe.prototype.transform = function (arr) {
        return Array.isArray(arr)
            ? Math.max.apply(Math, arr) : arr;
    };
    return MaxPipe;
}());

MaxPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'max' },] },
];
/** @nocollapse */
MaxPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=max.js.map

/***/ }),

/***/ 767:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MinPipe; });

var MinPipe = (function () {
    function MinPipe() {
    }
    MinPipe.prototype.transform = function (arr) {
        return Array.isArray(arr)
            ? Math.min.apply(Math, arr) : arr;
    };
    return MinPipe;
}());

MinPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'min' },] },
];
/** @nocollapse */
MinPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=min.js.map

/***/ }),

/***/ 768:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PercentagePipe; });

var PercentagePipe = (function () {
    function PercentagePipe() {
    }
    PercentagePipe.prototype.transform = function (num, total, floor) {
        if (total === void 0) { total = 100; }
        if (floor === void 0) { floor = false; }
        if (isNaN(num)) {
            return num;
        }
        var percent = num * 100 / total;
        return floor ? Math.floor(percent) : percent;
    };
    return PercentagePipe;
}());

PercentagePipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'percentage' },] },
];
/** @nocollapse */
PercentagePipe.ctorParameters = function () { return []; };
//# sourceMappingURL=percentage.js.map

/***/ }),

/***/ 769:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PowerPipe; });

var PowerPipe = (function () {
    function PowerPipe() {
    }
    PowerPipe.prototype.transform = function (num, power) {
        if (power === void 0) { power = 2; }
        return !isNaN(num)
            ? Math.pow(num, power)
            : num;
    };
    return PowerPipe;
}());

PowerPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'pow' },] },
];
/** @nocollapse */
PowerPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=pow.js.map

/***/ }),

/***/ 770:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RoundPipe; });


var RoundPipe = (function () {
    function RoundPipe() {
    }
    RoundPipe.prototype.transform = function (num, precision) {
        if (precision === void 0) { precision = 0; }
        return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["i" /* applyPrecision */])(num, precision);
    };
    return RoundPipe;
}());

RoundPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'round' },] },
];
/** @nocollapse */
RoundPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=round.js.map

/***/ }),

/***/ 771:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SqrtPipe; });

var SqrtPipe = (function () {
    function SqrtPipe() {
    }
    SqrtPipe.prototype.transform = function (num) {
        return !isNaN(num)
            ? Math.sqrt(num)
            : num;
    };
    return SqrtPipe;
}());

SqrtPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'sqrt' },] },
];
/** @nocollapse */
SqrtPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=sqrt.js.map

/***/ }),

/***/ 772:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SumPipe; });

var SumPipe = (function () {
    function SumPipe() {
    }
    SumPipe.prototype.transform = function (arr) {
        return Array.isArray(arr)
            ? arr.reduce(function (sum, curr) { return sum + curr; }, 0)
            : arr;
    };
    return SumPipe;
}());

SumPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'sum' },] },
];
/** @nocollapse */
SumPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=sum.js.map

/***/ }),

/***/ 773:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DiffObjPipe; });


var DiffObjPipe = (function () {
    function DiffObjPipe() {
    }
    DiffObjPipe.prototype.transform = function (obj, original) {
        if (original === void 0) { original = {}; }
        if (Array.isArray(obj) || Array.isArray(original) || !__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["a" /* isObject */])(obj) || !__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["a" /* isObject */])(original)) {
            return {};
        }
        return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["b" /* getKeysTwoObjects */])(obj, original).reduce(function (diff, key) {
            return (!__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["c" /* isDeepEqual */])(original[key], obj[key]) ? diff[key] = obj[key] : {}), diff;
        }, {});
    };
    return DiffObjPipe;
}());

DiffObjPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'diffObj' },] },
];
/** @nocollapse */
DiffObjPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=diff-obj.js.map

/***/ }),

/***/ 774:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__keys__ = __webpack_require__(777);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__values__ = __webpack_require__(781);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pairs__ = __webpack_require__(779);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pick__ = __webpack_require__(780);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__omit__ = __webpack_require__(778);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__invert__ = __webpack_require__(776);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__invert_by__ = __webpack_require__(775);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__diff_obj__ = __webpack_require__(773);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_core__ = __webpack_require__(0);
/* unused harmony reexport KeysPipe */
/* unused harmony reexport ValuesPipe */
/* unused harmony reexport PairsPipe */
/* unused harmony reexport PickPipe */
/* unused harmony reexport OmitPipe */
/* unused harmony reexport InvertPipe */
/* unused harmony reexport InvertByPipe */
/* unused harmony reexport DiffObjPipe */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NgObjectPipesModule; });









var OBJECT_PIPES = [
    __WEBPACK_IMPORTED_MODULE_0__keys__["a" /* KeysPipe */], __WEBPACK_IMPORTED_MODULE_1__values__["a" /* ValuesPipe */], __WEBPACK_IMPORTED_MODULE_2__pairs__["a" /* PairsPipe */], __WEBPACK_IMPORTED_MODULE_3__pick__["a" /* PickPipe */], __WEBPACK_IMPORTED_MODULE_5__invert__["a" /* InvertPipe */], __WEBPACK_IMPORTED_MODULE_6__invert_by__["a" /* InvertByPipe */],
    __WEBPACK_IMPORTED_MODULE_4__omit__["a" /* OmitPipe */], __WEBPACK_IMPORTED_MODULE_7__diff_obj__["a" /* DiffObjPipe */]
];
var NgObjectPipesModule = (function () {
    function NgObjectPipesModule() {
    }
    return NgObjectPipesModule;
}());

NgObjectPipesModule.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_8__angular_core__["NgModule"], args: [{
                declarations: OBJECT_PIPES,
                imports: [],
                exports: OBJECT_PIPES
            },] },
];
/** @nocollapse */
NgObjectPipesModule.ctorParameters = function () { return []; };








//# sourceMappingURL=index.js.map

/***/ }),

/***/ 775:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InvertByPipe; });


var InvertByPipe = (function () {
    function InvertByPipe() {
    }
    InvertByPipe.prototype.transform = function (obj, cb) {
        if (Array.isArray(obj) || !__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["a" /* isObject */])(obj)) {
            return obj;
        }
        return Object.keys(obj).reduce(function (o, k) {
            var key = cb ? cb(obj[k]) : obj[k];
            return Array.isArray(o[key])
                ? (o[key].push(k), o)
                : Object.assign(o, (_a = {}, _a[key] = [k], _a));
            var _a;
        }, {});
    };
    return InvertByPipe;
}());

InvertByPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'invertBy' },] },
];
/** @nocollapse */
InvertByPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=invert-by.js.map

/***/ }),

/***/ 776:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InvertPipe; });


var InvertPipe = (function () {
    function InvertPipe() {
    }
    InvertPipe.prototype.transform = function (obj) {
        if (Array.isArray(obj) || !__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["a" /* isObject */])(obj)) {
            return obj;
        }
        return Object.keys(obj)
            .reduce(function (o, k) {
            return Object.assign(o, (_a = {}, _a[obj[k]] = k, _a));
            var _a;
        }, {});
    };
    return InvertPipe;
}());

InvertPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'invert' },] },
];
/** @nocollapse */
InvertPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=invert.js.map

/***/ }),

/***/ 777:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KeysPipe; });


var KeysPipe = (function () {
    function KeysPipe() {
    }
    KeysPipe.prototype.transform = function (obj) {
        if (Array.isArray(obj) || !__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["a" /* isObject */])(obj)) {
            return obj;
        }
        return Object.keys(obj);
    };
    return KeysPipe;
}());

KeysPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'keys' },] },
];
/** @nocollapse */
KeysPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=keys.js.map

/***/ }),

/***/ 778:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OmitPipe; });


var OmitPipe = (function () {
    function OmitPipe() {
    }
    OmitPipe.prototype.transform = function (obj) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (Array.isArray(obj) || !__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["a" /* isObject */])(obj)) {
            return obj;
        }
        return Object.keys(obj)
            .filter(function (k) { return !~args.indexOf(k); })
            .reduce(function (o, k) {
            return Object.assign(o, (_a = {}, _a[k] = obj[k], _a));
            var _a;
        }, {});
    };
    return OmitPipe;
}());

OmitPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'omit' },] },
];
/** @nocollapse */
OmitPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=omit.js.map

/***/ }),

/***/ 779:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PairsPipe; });


var PairsPipe = (function () {
    function PairsPipe() {
    }
    PairsPipe.prototype.transform = function (obj) {
        if (Array.isArray(obj) || !__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["a" /* isObject */])(obj)) {
            return obj;
        }
        return Object.keys(obj).map(function (k) { return [k, obj[k]]; });
    };
    return PairsPipe;
}());

PairsPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'pairs' },] },
];
/** @nocollapse */
PairsPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=pairs.js.map

/***/ }),

/***/ 780:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PickPipe; });


var PickPipe = (function () {
    function PickPipe() {
    }
    PickPipe.prototype.transform = function (obj) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (Array.isArray(obj) || !__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["a" /* isObject */])(obj)) {
            return obj;
        }
        return args.reduce(function (o, k) {
            return Object.assign(o, (_a = {}, _a[k] = obj[k], _a));
            var _a;
        }, {});
    };
    return PickPipe;
}());

PickPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'pick' },] },
];
/** @nocollapse */
PickPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=pick.js.map

/***/ }),

/***/ 781:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ValuesPipe; });


var ValuesPipe = (function () {
    function ValuesPipe() {
    }
    ValuesPipe.prototype.transform = function (obj) {
        if (Array.isArray(obj) || !__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["a" /* isObject */])(obj)) {
            return obj;
        }
        return Object.keys(obj).map(function (k) { return obj[k]; });
    };
    return ValuesPipe;
}());

ValuesPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'values' },] },
];
/** @nocollapse */
ValuesPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=values.js.map

/***/ }),

/***/ 782:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CamelizePipe; });


var CamelizePipe = (function () {
    function CamelizePipe() {
    }
    CamelizePipe.prototype.transform = function (text, chars) {
        if (chars === void 0) { chars = '\\s'; }
        if (!__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(text)) {
            return text;
        }
        return text.toLowerCase()
            .split(/[-_\s]/g)
            .filter(function (v) { return !!v; }).map(function (word, key) {
            return !key ? word : (word.slice(0, 1).toUpperCase() + word.slice(1));
        }).join('');
    };
    return CamelizePipe;
}());

CamelizePipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'camelize' },] },
];
/** @nocollapse */
CamelizePipe.ctorParameters = function () { return []; };
//# sourceMappingURL=camelize.js.map

/***/ }),

/***/ 783:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ucwords__ = __webpack_require__(799);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ltrim__ = __webpack_require__(787);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__repeat__ = __webpack_require__(789);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__rtrim__ = __webpack_require__(791);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__scan__ = __webpack_require__(792);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shorten__ = __webpack_require__(793);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__strip_tags__ = __webpack_require__(795);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__trim__ = __webpack_require__(797);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ucfirst__ = __webpack_require__(798);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__slugify__ = __webpack_require__(794);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__camelize__ = __webpack_require__(782);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__latinise__ = __webpack_require__(784);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__lines__ = __webpack_require__(785);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__underscore__ = __webpack_require__(800);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__match__ = __webpack_require__(788);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__test__ = __webpack_require__(796);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__lpad__ = __webpack_require__(786);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__rpad__ = __webpack_require__(790);
/* unused harmony reexport UcWordsPipe */
/* unused harmony reexport LeftTrimPipe */
/* unused harmony reexport RepeatPipe */
/* unused harmony reexport RightTrimPipe */
/* unused harmony reexport ScanPipe */
/* unused harmony reexport ShortenPipe */
/* unused harmony reexport StripTagsPipe */
/* unused harmony reexport TrimPipe */
/* unused harmony reexport UcFirstPipe */
/* unused harmony reexport SlugifyPipe */
/* unused harmony reexport CamelizePipe */
/* unused harmony reexport LatinisePipe */
/* unused harmony reexport LinesPipe */
/* unused harmony reexport UnderscorePipe */
/* unused harmony reexport MatchPipe */
/* unused harmony reexport TestPipe */
/* unused harmony reexport LeftPadPipe */
/* unused harmony reexport RightPadPipe */
/* unused harmony export STRING_PIPES */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NgStringPipesModule; });



















var STRING_PIPES = [
    __WEBPACK_IMPORTED_MODULE_2__ltrim__["a" /* LeftTrimPipe */], __WEBPACK_IMPORTED_MODULE_3__repeat__["a" /* RepeatPipe */], __WEBPACK_IMPORTED_MODULE_4__rtrim__["a" /* RightTrimPipe */], __WEBPACK_IMPORTED_MODULE_5__scan__["a" /* ScanPipe */], __WEBPACK_IMPORTED_MODULE_6__shorten__["a" /* ShortenPipe */],
    __WEBPACK_IMPORTED_MODULE_7__strip_tags__["a" /* StripTagsPipe */], __WEBPACK_IMPORTED_MODULE_8__trim__["a" /* TrimPipe */], __WEBPACK_IMPORTED_MODULE_9__ucfirst__["a" /* UcFirstPipe */], __WEBPACK_IMPORTED_MODULE_1__ucwords__["a" /* UcWordsPipe */], __WEBPACK_IMPORTED_MODULE_10__slugify__["a" /* SlugifyPipe */],
    __WEBPACK_IMPORTED_MODULE_11__camelize__["a" /* CamelizePipe */], __WEBPACK_IMPORTED_MODULE_12__latinise__["a" /* LatinisePipe */], __WEBPACK_IMPORTED_MODULE_13__lines__["a" /* LinesPipe */], __WEBPACK_IMPORTED_MODULE_14__underscore__["a" /* UnderscorePipe */], __WEBPACK_IMPORTED_MODULE_15__match__["a" /* MatchPipe */],
    __WEBPACK_IMPORTED_MODULE_16__test__["a" /* TestPipe */], __WEBPACK_IMPORTED_MODULE_17__lpad__["a" /* LeftPadPipe */], __WEBPACK_IMPORTED_MODULE_18__rpad__["a" /* RightPadPipe */]
];
var NgStringPipesModule = (function () {
    function NgStringPipesModule() {
    }
    return NgStringPipesModule;
}());

NgStringPipesModule.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"], args: [{
                declarations: STRING_PIPES,
                imports: [],
                exports: STRING_PIPES
            },] },
];
/** @nocollapse */
NgStringPipesModule.ctorParameters = function () { return []; };


















//# sourceMappingURL=index.js.map

/***/ }),

/***/ 784:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LatinisePipe; });


var LatinisePipe = (function () {
    function LatinisePipe() {
        // Source: http://semplicewebsites.com/removing-accents-javascript
        this.latinMap = { "Á": "A", "Ă": "A", "Ắ": "A", "Ặ": "A", "Ằ": "A", "Ẳ": "A", "Ẵ": "A", "Ǎ": "A", "Â": "A", "Ấ": "A", "Ậ": "A", "Ầ": "A", "Ẩ": "A", "Ẫ": "A", "Ä": "A", "Ǟ": "A", "Ȧ": "A", "Ǡ": "A", "Ạ": "A", "Ȁ": "A", "À": "A", "Ả": "A", "Ȃ": "A", "Ā": "A", "Ą": "A", "Å": "A", "Ǻ": "A", "Ḁ": "A", "Ⱥ": "A", "Ã": "A", "Ꜳ": "AA", "Æ": "AE", "Ǽ": "AE", "Ǣ": "AE", "Ꜵ": "AO", "Ꜷ": "AU", "Ꜹ": "AV", "Ꜻ": "AV", "Ꜽ": "AY", "Ḃ": "B", "Ḅ": "B", "Ɓ": "B", "Ḇ": "B", "Ƀ": "B", "Ƃ": "B", "Ć": "C", "Č": "C", "Ç": "C", "Ḉ": "C", "Ĉ": "C", "Ċ": "C", "Ƈ": "C", "Ȼ": "C", "Ď": "D", "Ḑ": "D", "Ḓ": "D", "Ḋ": "D", "Ḍ": "D", "Ɗ": "D", "Ḏ": "D", "ǲ": "D", "ǅ": "D", "Đ": "D", "Ƌ": "D", "Ǳ": "DZ", "Ǆ": "DZ", "É": "E", "Ĕ": "E", "Ě": "E", "Ȩ": "E", "Ḝ": "E", "Ê": "E", "Ế": "E", "Ệ": "E", "Ề": "E", "Ể": "E", "Ễ": "E", "Ḙ": "E", "Ë": "E", "Ė": "E", "Ẹ": "E", "Ȅ": "E", "È": "E", "Ẻ": "E", "Ȇ": "E", "Ē": "E", "Ḗ": "E", "Ḕ": "E", "Ę": "E", "Ɇ": "E", "Ẽ": "E", "Ḛ": "E", "Ꝫ": "ET", "Ḟ": "F", "Ƒ": "F", "Ǵ": "G", "Ğ": "G", "Ǧ": "G", "Ģ": "G", "Ĝ": "G", "Ġ": "G", "Ɠ": "G", "Ḡ": "G", "Ǥ": "G", "Ḫ": "H", "Ȟ": "H", "Ḩ": "H", "Ĥ": "H", "Ⱨ": "H", "Ḧ": "H", "Ḣ": "H", "Ḥ": "H", "Ħ": "H", "Í": "I", "Ĭ": "I", "Ǐ": "I", "Î": "I", "Ï": "I", "Ḯ": "I", "İ": "I", "Ị": "I", "Ȉ": "I", "Ì": "I", "Ỉ": "I", "Ȋ": "I", "Ī": "I", "Į": "I", "Ɨ": "I", "Ĩ": "I", "Ḭ": "I", "Ꝺ": "D", "Ꝼ": "F", "Ᵹ": "G", "Ꞃ": "R", "Ꞅ": "S", "Ꞇ": "T", "Ꝭ": "IS", "Ĵ": "J", "Ɉ": "J", "Ḱ": "K", "Ǩ": "K", "Ķ": "K", "Ⱪ": "K", "Ꝃ": "K", "Ḳ": "K", "Ƙ": "K", "Ḵ": "K", "Ꝁ": "K", "Ꝅ": "K", "Ĺ": "L", "Ƚ": "L", "Ľ": "L", "Ļ": "L", "Ḽ": "L", "Ḷ": "L", "Ḹ": "L", "Ⱡ": "L", "Ꝉ": "L", "Ḻ": "L", "Ŀ": "L", "Ɫ": "L", "ǈ": "L", "Ł": "L", "Ǉ": "LJ", "Ḿ": "M", "Ṁ": "M", "Ṃ": "M", "Ɱ": "M", "Ń": "N", "Ň": "N", "Ņ": "N", "Ṋ": "N", "Ṅ": "N", "Ṇ": "N", "Ǹ": "N", "Ɲ": "N", "Ṉ": "N", "Ƞ": "N", "ǋ": "N", "Ñ": "N", "Ǌ": "NJ", "Ó": "O", "Ŏ": "O", "Ǒ": "O", "Ô": "O", "Ố": "O", "Ộ": "O", "Ồ": "O", "Ổ": "O", "Ỗ": "O", "Ö": "O", "Ȫ": "O", "Ȯ": "O", "Ȱ": "O", "Ọ": "O", "Ő": "O", "Ȍ": "O", "Ò": "O", "Ỏ": "O", "Ơ": "O", "Ớ": "O", "Ợ": "O", "Ờ": "O", "Ở": "O", "Ỡ": "O", "Ȏ": "O", "Ꝋ": "O", "Ꝍ": "O", "Ō": "O", "Ṓ": "O", "Ṑ": "O", "Ɵ": "O", "Ǫ": "O", "Ǭ": "O", "Ø": "O", "Ǿ": "O", "Õ": "O", "Ṍ": "O", "Ṏ": "O", "Ȭ": "O", "Ƣ": "OI", "Ꝏ": "OO", "Ɛ": "E", "Ɔ": "O", "Ȣ": "OU", "Ṕ": "P", "Ṗ": "P", "Ꝓ": "P", "Ƥ": "P", "Ꝕ": "P", "Ᵽ": "P", "Ꝑ": "P", "Ꝙ": "Q", "Ꝗ": "Q", "Ŕ": "R", "Ř": "R", "Ŗ": "R", "Ṙ": "R", "Ṛ": "R", "Ṝ": "R", "Ȑ": "R", "Ȓ": "R", "Ṟ": "R", "Ɍ": "R", "Ɽ": "R", "Ꜿ": "C", "Ǝ": "E", "Ś": "S", "Ṥ": "S", "Š": "S", "Ṧ": "S", "Ş": "S", "Ŝ": "S", "Ș": "S", "Ṡ": "S", "Ṣ": "S", "Ṩ": "S", "ẞ": "SS", "Ť": "T", "Ţ": "T", "Ṱ": "T", "Ț": "T", "Ⱦ": "T", "Ṫ": "T", "Ṭ": "T", "Ƭ": "T", "Ṯ": "T", "Ʈ": "T", "Ŧ": "T", "Ɐ": "A", "Ꞁ": "L", "Ɯ": "M", "Ʌ": "V", "Ꜩ": "TZ", "Ú": "U", "Ŭ": "U", "Ǔ": "U", "Û": "U", "Ṷ": "U", "Ü": "U", "Ǘ": "U", "Ǚ": "U", "Ǜ": "U", "Ǖ": "U", "Ṳ": "U", "Ụ": "U", "Ű": "U", "Ȕ": "U", "Ù": "U", "Ủ": "U", "Ư": "U", "Ứ": "U", "Ự": "U", "Ừ": "U", "Ử": "U", "Ữ": "U", "Ȗ": "U", "Ū": "U", "Ṻ": "U", "Ų": "U", "Ů": "U", "Ũ": "U", "Ṹ": "U", "Ṵ": "U", "Ꝟ": "V", "Ṿ": "V", "Ʋ": "V", "Ṽ": "V", "Ꝡ": "VY", "Ẃ": "W", "Ŵ": "W", "Ẅ": "W", "Ẇ": "W", "Ẉ": "W", "Ẁ": "W", "Ⱳ": "W", "Ẍ": "X", "Ẋ": "X", "Ý": "Y", "Ŷ": "Y", "Ÿ": "Y", "Ẏ": "Y", "Ỵ": "Y", "Ỳ": "Y", "Ƴ": "Y", "Ỷ": "Y", "Ỿ": "Y", "Ȳ": "Y", "Ɏ": "Y", "Ỹ": "Y", "Ź": "Z", "Ž": "Z", "Ẑ": "Z", "Ⱬ": "Z", "Ż": "Z", "Ẓ": "Z", "Ȥ": "Z", "Ẕ": "Z", "Ƶ": "Z", "Ĳ": "IJ", "Œ": "OE", "ᴀ": "A", "ᴁ": "AE", "ʙ": "B", "ᴃ": "B", "ᴄ": "C", "ᴅ": "D", "ᴇ": "E", "ꜰ": "F", "ɢ": "G", "ʛ": "G", "ʜ": "H", "ɪ": "I", "ʁ": "R", "ᴊ": "J", "ᴋ": "K", "ʟ": "L", "ᴌ": "L", "ᴍ": "M", "ɴ": "N", "ᴏ": "O", "ɶ": "OE", "ᴐ": "O", "ᴕ": "OU", "ᴘ": "P", "ʀ": "R", "ᴎ": "N", "ᴙ": "R", "ꜱ": "S", "ᴛ": "T", "ⱻ": "E", "ᴚ": "R", "ᴜ": "U", "ᴠ": "V", "ᴡ": "W", "ʏ": "Y", "ᴢ": "Z", "á": "a", "ă": "a", "ắ": "a", "ặ": "a", "ằ": "a", "ẳ": "a", "ẵ": "a", "ǎ": "a", "â": "a", "ấ": "a", "ậ": "a", "ầ": "a", "ẩ": "a", "ẫ": "a", "ä": "a", "ǟ": "a", "ȧ": "a", "ǡ": "a", "ạ": "a", "ȁ": "a", "à": "a", "ả": "a", "ȃ": "a", "ā": "a", "ą": "a", "ᶏ": "a", "ẚ": "a", "å": "a", "ǻ": "a", "ḁ": "a", "ⱥ": "a", "ã": "a", "ꜳ": "aa", "æ": "ae", "ǽ": "ae", "ǣ": "ae", "ꜵ": "ao", "ꜷ": "au", "ꜹ": "av", "ꜻ": "av", "ꜽ": "ay", "ḃ": "b", "ḅ": "b", "ɓ": "b", "ḇ": "b", "ᵬ": "b", "ᶀ": "b", "ƀ": "b", "ƃ": "b", "ɵ": "o", "ć": "c", "č": "c", "ç": "c", "ḉ": "c", "ĉ": "c", "ɕ": "c", "ċ": "c", "ƈ": "c", "ȼ": "c", "ď": "d", "ḑ": "d", "ḓ": "d", "ȡ": "d", "ḋ": "d", "ḍ": "d", "ɗ": "d", "ᶑ": "d", "ḏ": "d", "ᵭ": "d", "ᶁ": "d", "đ": "d", "ɖ": "d", "ƌ": "d", "ı": "i", "ȷ": "j", "ɟ": "j", "ʄ": "j", "ǳ": "dz", "ǆ": "dz", "é": "e", "ĕ": "e", "ě": "e", "ȩ": "e", "ḝ": "e", "ê": "e", "ế": "e", "ệ": "e", "ề": "e", "ể": "e", "ễ": "e", "ḙ": "e", "ë": "e", "ė": "e", "ẹ": "e", "ȅ": "e", "è": "e", "ẻ": "e", "ȇ": "e", "ē": "e", "ḗ": "e", "ḕ": "e", "ⱸ": "e", "ę": "e", "ᶒ": "e", "ɇ": "e", "ẽ": "e", "ḛ": "e", "ꝫ": "et", "ḟ": "f", "ƒ": "f", "ᵮ": "f", "ᶂ": "f", "ǵ": "g", "ğ": "g", "ǧ": "g", "ģ": "g", "ĝ": "g", "ġ": "g", "ɠ": "g", "ḡ": "g", "ᶃ": "g", "ǥ": "g", "ḫ": "h", "ȟ": "h", "ḩ": "h", "ĥ": "h", "ⱨ": "h", "ḧ": "h", "ḣ": "h", "ḥ": "h", "ɦ": "h", "ẖ": "h", "ħ": "h", "ƕ": "hv", "í": "i", "ĭ": "i", "ǐ": "i", "î": "i", "ï": "i", "ḯ": "i", "ị": "i", "ȉ": "i", "ì": "i", "ỉ": "i", "ȋ": "i", "ī": "i", "į": "i", "ᶖ": "i", "ɨ": "i", "ĩ": "i", "ḭ": "i", "ꝺ": "d", "ꝼ": "f", "ᵹ": "g", "ꞃ": "r", "ꞅ": "s", "ꞇ": "t", "ꝭ": "is", "ǰ": "j", "ĵ": "j", "ʝ": "j", "ɉ": "j", "ḱ": "k", "ǩ": "k", "ķ": "k", "ⱪ": "k", "ꝃ": "k", "ḳ": "k", "ƙ": "k", "ḵ": "k", "ᶄ": "k", "ꝁ": "k", "ꝅ": "k", "ĺ": "l", "ƚ": "l", "ɬ": "l", "ľ": "l", "ļ": "l", "ḽ": "l", "ȴ": "l", "ḷ": "l", "ḹ": "l", "ⱡ": "l", "ꝉ": "l", "ḻ": "l", "ŀ": "l", "ɫ": "l", "ᶅ": "l", "ɭ": "l", "ł": "l", "ǉ": "lj", "ſ": "s", "ẜ": "s", "ẛ": "s", "ẝ": "s", "ḿ": "m", "ṁ": "m", "ṃ": "m", "ɱ": "m", "ᵯ": "m", "ᶆ": "m", "ń": "n", "ň": "n", "ņ": "n", "ṋ": "n", "ȵ": "n", "ṅ": "n", "ṇ": "n", "ǹ": "n", "ɲ": "n", "ṉ": "n", "ƞ": "n", "ᵰ": "n", "ᶇ": "n", "ɳ": "n", "ñ": "n", "ǌ": "nj", "ó": "o", "ŏ": "o", "ǒ": "o", "ô": "o", "ố": "o", "ộ": "o", "ồ": "o", "ổ": "o", "ỗ": "o", "ö": "o", "ȫ": "o", "ȯ": "o", "ȱ": "o", "ọ": "o", "ő": "o", "ȍ": "o", "ò": "o", "ỏ": "o", "ơ": "o", "ớ": "o", "ợ": "o", "ờ": "o", "ở": "o", "ỡ": "o", "ȏ": "o", "ꝋ": "o", "ꝍ": "o", "ⱺ": "o", "ō": "o", "ṓ": "o", "ṑ": "o", "ǫ": "o", "ǭ": "o", "ø": "o", "ǿ": "o", "õ": "o", "ṍ": "o", "ṏ": "o", "ȭ": "o", "ƣ": "oi", "ꝏ": "oo", "ɛ": "e", "ᶓ": "e", "ɔ": "o", "ᶗ": "o", "ȣ": "ou", "ṕ": "p", "ṗ": "p", "ꝓ": "p", "ƥ": "p", "ᵱ": "p", "ᶈ": "p", "ꝕ": "p", "ᵽ": "p", "ꝑ": "p", "ꝙ": "q", "ʠ": "q", "ɋ": "q", "ꝗ": "q", "ŕ": "r", "ř": "r", "ŗ": "r", "ṙ": "r", "ṛ": "r", "ṝ": "r", "ȑ": "r", "ɾ": "r", "ᵳ": "r", "ȓ": "r", "ṟ": "r", "ɼ": "r", "ᵲ": "r", "ᶉ": "r", "ɍ": "r", "ɽ": "r", "ↄ": "c", "ꜿ": "c", "ɘ": "e", "ɿ": "r", "ś": "s", "ṥ": "s", "š": "s", "ṧ": "s", "ş": "s", "ŝ": "s", "ș": "s", "ṡ": "s", "ṣ": "s", "ṩ": "s", "ʂ": "s", "ᵴ": "s", "ᶊ": "s", "ȿ": "s", "ɡ": "g", "ß": "ss", "ᴑ": "o", "ᴓ": "o", "ᴝ": "u", "ť": "t", "ţ": "t", "ṱ": "t", "ț": "t", "ȶ": "t", "ẗ": "t", "ⱦ": "t", "ṫ": "t", "ṭ": "t", "ƭ": "t", "ṯ": "t", "ᵵ": "t", "ƫ": "t", "ʈ": "t", "ŧ": "t", "ᵺ": "th", "ɐ": "a", "ᴂ": "ae", "ǝ": "e", "ᵷ": "g", "ɥ": "h", "ʮ": "h", "ʯ": "h", "ᴉ": "i", "ʞ": "k", "ꞁ": "l", "ɯ": "m", "ɰ": "m", "ᴔ": "oe", "ɹ": "r", "ɻ": "r", "ɺ": "r", "ⱹ": "r", "ʇ": "t", "ʌ": "v", "ʍ": "w", "ʎ": "y", "ꜩ": "tz", "ú": "u", "ŭ": "u", "ǔ": "u", "û": "u", "ṷ": "u", "ü": "u", "ǘ": "u", "ǚ": "u", "ǜ": "u", "ǖ": "u", "ṳ": "u", "ụ": "u", "ű": "u", "ȕ": "u", "ù": "u", "ủ": "u", "ư": "u", "ứ": "u", "ự": "u", "ừ": "u", "ử": "u", "ữ": "u", "ȗ": "u", "ū": "u", "ṻ": "u", "ų": "u", "ᶙ": "u", "ů": "u", "ũ": "u", "ṹ": "u", "ṵ": "u", "ᵫ": "ue", "ꝸ": "um", "ⱴ": "v", "ꝟ": "v", "ṿ": "v", "ʋ": "v", "ᶌ": "v", "ⱱ": "v", "ṽ": "v", "ꝡ": "vy", "ẃ": "w", "ŵ": "w", "ẅ": "w", "ẇ": "w", "ẉ": "w", "ẁ": "w", "ⱳ": "w", "ẘ": "w", "ẍ": "x", "ẋ": "x", "ᶍ": "x", "ý": "y", "ŷ": "y", "ÿ": "y", "ẏ": "y", "ỵ": "y", "ỳ": "y", "ƴ": "y", "ỷ": "y", "ỿ": "y", "ȳ": "y", "ẙ": "y", "ɏ": "y", "ỹ": "y", "ź": "z", "ž": "z", "ẑ": "z", "ʑ": "z", "ⱬ": "z", "ż": "z", "ẓ": "z", "ȥ": "z", "ẕ": "z", "ᵶ": "z", "ᶎ": "z", "ʐ": "z", "ƶ": "z", "ɀ": "z", "ﬀ": "ff", "ﬃ": "ffi", "ﬄ": "ffl", "ﬁ": "fi", "ﬂ": "fl", "ĳ": "ij", "œ": "oe", "ﬆ": "st", "ₐ": "a", "ₑ": "e", "ᵢ": "i", "ⱼ": "j", "ₒ": "o", "ᵣ": "r", "ᵤ": "u", "ᵥ": "v", "ₓ": "x" };
    }
    LatinisePipe.prototype.transform = function (text, chars) {
        var _this = this;
        if (chars === void 0) { chars = '\\s'; }
        return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(text)
            ? text.replace(/[^A-Za-z0-9]/g, function (key) {
                return _this.latinMap[key] || key;
            }) : text;
    };
    return LatinisePipe;
}());

LatinisePipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'latinise' },] },
];
/** @nocollapse */
LatinisePipe.ctorParameters = function () { return []; };
//# sourceMappingURL=latinise.js.map

/***/ }),

/***/ 785:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LinesPipe; });


var LinesPipe = (function () {
    function LinesPipe() {
    }
    LinesPipe.prototype.transform = function (text, chars) {
        if (chars === void 0) { chars = '\\s'; }
        return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(text)
            ? text.replace(/\r\n/g, '\n').split('\n')
            : text;
    };
    return LinesPipe;
}());

LinesPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'lines' },] },
];
/** @nocollapse */
LinesPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=lines.js.map

/***/ }),

/***/ 786:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeftPadPipe; });


var LeftPadPipe = (function () {
    function LeftPadPipe() {
    }
    LeftPadPipe.prototype.transform = function (str, length, padCharacter) {
        if (padCharacter === void 0) { padCharacter = ' '; }
        if (!__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(str) || str.length >= length) {
            return str;
        }
        while (str.length < length) {
            str = padCharacter + str;
        }
        return str;
    };
    return LeftPadPipe;
}());

LeftPadPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'lpad' },] },
];
/** @nocollapse */
LeftPadPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=lpad.js.map

/***/ }),

/***/ 787:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeftTrimPipe; });


var LeftTrimPipe = (function () {
    function LeftTrimPipe() {
    }
    LeftTrimPipe.prototype.transform = function (text, chars) {
        if (chars === void 0) { chars = '\\s'; }
        return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(text)
            ? text.replace(new RegExp("^[" + chars + "]+"), '')
            : text;
    };
    return LeftTrimPipe;
}());

LeftTrimPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'ltrim' },] },
];
/** @nocollapse */
LeftTrimPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=ltrim.js.map

/***/ }),

/***/ 788:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MatchPipe; });


var MatchPipe = (function () {
    function MatchPipe() {
    }
    MatchPipe.prototype.transform = function (text, pattern, flags) {
        if (!__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(text)) {
            return text;
        }
        return text.match(new RegExp(pattern, flags));
    };
    return MatchPipe;
}());

MatchPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'match' },] },
];
/** @nocollapse */
MatchPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=match.js.map

/***/ }),

/***/ 789:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RepeatPipe; });


var RepeatPipe = (function () {
    function RepeatPipe() {
    }
    RepeatPipe.prototype.transform = function (str, n, separator) {
        if (n === void 0) { n = 1; }
        if (separator === void 0) { separator = ''; }
        if (n <= 0) {
            throw new RangeError();
        }
        return n == 1 ? str : this.repeat(str, n - 1, separator);
    };
    RepeatPipe.prototype.repeat = function (str, n, separator) {
        return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(str)
            ? (n == 0 ? str : (str + separator + this.repeat(str, n - 1, separator)))
            : str;
    };
    return RepeatPipe;
}());

RepeatPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'repeat' },] },
];
/** @nocollapse */
RepeatPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=repeat.js.map

/***/ }),

/***/ 790:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RightPadPipe; });


var RightPadPipe = (function () {
    function RightPadPipe() {
    }
    RightPadPipe.prototype.transform = function (str, length, padCharacter) {
        if (length === void 0) { length = 1; }
        if (padCharacter === void 0) { padCharacter = ' '; }
        if (!__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(str) || str.length >= length) {
            return str;
        }
        while (str.length < length) {
            str = str + padCharacter;
        }
        return str;
    };
    return RightPadPipe;
}());

RightPadPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'rpad' },] },
];
/** @nocollapse */
RightPadPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=rpad.js.map

/***/ }),

/***/ 791:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RightTrimPipe; });


var RightTrimPipe = (function () {
    function RightTrimPipe() {
    }
    RightTrimPipe.prototype.transform = function (text, chars) {
        if (chars === void 0) { chars = '\\s'; }
        return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(text)
            ? text.replace(new RegExp("[" + chars + "]+$"), '')
            : text;
    };
    return RightTrimPipe;
}());

RightTrimPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'rtrim' },] },
];
/** @nocollapse */
RightTrimPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=rtrim.js.map

/***/ }),

/***/ 792:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScanPipe; });


var ScanPipe = (function () {
    function ScanPipe() {
    }
    ScanPipe.prototype.transform = function (text, args) {
        if (args === void 0) { args = []; }
        return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(text)
            ? text.replace(/\{(\d+)}/g, function (match, index) { return !__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["g" /* isUndefined */])(args[index]) ? args[index] : match; })
            : text;
    };
    return ScanPipe;
}());

ScanPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'scan' },] },
];
/** @nocollapse */
ScanPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=scan.js.map

/***/ }),

/***/ 793:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShortenPipe; });


var ShortenPipe = (function () {
    function ShortenPipe() {
    }
    ShortenPipe.prototype.transform = function (text, length, suffix, wordBreak) {
        if (length === void 0) { length = 0; }
        if (suffix === void 0) { suffix = ''; }
        if (wordBreak === void 0) { wordBreak = true; }
        if (!__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(text)) {
            return text;
        }
        if (text.length > length) {
            if (wordBreak) {
                return text.slice(0, length) + suffix;
            }
            if (!!~text.indexOf(' ', length)) {
                return text.slice(0, text.indexOf(' ', length)) + suffix;
            }
        }
        return text;
    };
    return ShortenPipe;
}());

ShortenPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'shorten' },] },
];
/** @nocollapse */
ShortenPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=shorten.js.map

/***/ }),

/***/ 794:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SlugifyPipe; });


var SlugifyPipe = (function () {
    function SlugifyPipe() {
    }
    SlugifyPipe.prototype.transform = function (str) {
        return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(str)
            ? str.toLowerCase().trim()
                .replace(/[^\w\-]+/g, ' ')
                .replace(/\s+/g, '-')
            : str;
    };
    return SlugifyPipe;
}());

SlugifyPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'slugify' },] },
];
/** @nocollapse */
SlugifyPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=slugify.js.map

/***/ }),

/***/ 795:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StripTagsPipe; });

var StripTagsPipe = (function () {
    function StripTagsPipe() {
    }
    StripTagsPipe.prototype.transform = function (text) {
        var allowedTags = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            allowedTags[_i - 1] = arguments[_i];
        }
        return allowedTags.length > 0
            ? text.replace(new RegExp("<(?!/?(" + allowedTags.join('|') + ")s*/?)[^>]+>", 'g'), '')
            : text.replace(/<(?:.|\s)*?>/g, '');
    };
    return StripTagsPipe;
}());

StripTagsPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'stripTags' },] },
];
/** @nocollapse */
StripTagsPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=strip-tags.js.map

/***/ }),

/***/ 796:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TestPipe; });


var TestPipe = (function () {
    function TestPipe() {
    }
    TestPipe.prototype.transform = function (text, pattern, flags) {
        if (!__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(text)) {
            return text;
        }
        return (new RegExp(pattern, flags)).test(text);
    };
    return TestPipe;
}());

TestPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'test' },] },
];
/** @nocollapse */
TestPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=test.js.map

/***/ }),

/***/ 797:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrimPipe; });


var TrimPipe = (function () {
    function TrimPipe() {
    }
    TrimPipe.prototype.transform = function (text, chars) {
        if (chars === void 0) { chars = '\\s'; }
        return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(text) ? text.replace(new RegExp("^[" + chars + "]+|[" + chars + "]+$", 'g'), '') : text;
    };
    return TrimPipe;
}());

TrimPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'trim' },] },
];
/** @nocollapse */
TrimPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=trim.js.map

/***/ }),

/***/ 798:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UcFirstPipe; });


var UcFirstPipe = (function () {
    function UcFirstPipe() {
    }
    UcFirstPipe.prototype.transform = function (text) {
        return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(text)
            ? (text.slice(0, 1).toUpperCase() + text.slice(1))
            : text;
    };
    return UcFirstPipe;
}());

UcFirstPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'ucfirst' },] },
];
/** @nocollapse */
UcFirstPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=ucfirst.js.map

/***/ }),

/***/ 799:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UcWordsPipe; });


var UcWordsPipe = (function () {
    function UcWordsPipe() {
    }
    UcWordsPipe.prototype.transform = function (text) {
        if (__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(text)) {
            return text.split(' ')
                .map(function (sub) { return sub.slice(0, 1).toUpperCase() + sub.slice(1); })
                .join(' ');
        }
        return text;
    };
    return UcWordsPipe;
}());

UcWordsPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'ucwords' },] },
];
/** @nocollapse */
UcWordsPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=ucwords.js.map

/***/ }),

/***/ 800:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UnderscorePipe; });


var UnderscorePipe = (function () {
    function UnderscorePipe() {
    }
    UnderscorePipe.prototype.transform = function (text, chars) {
        if (chars === void 0) { chars = '\\s'; }
        return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["f" /* isString */])(text)
            ? text.trim()
                .replace(/\s+/g, '')
                .replace(/[A-Z]/g, function (c, k) {
                return k ? "_" + c.toLowerCase() : c.toLowerCase();
            })
            : text;
    };
    return UnderscorePipe;
}());

UnderscorePipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'underscore' },] },
];
/** @nocollapse */
UnderscorePipe.ctorParameters = function () { return []; };
//# sourceMappingURL=underscore.js.map

/***/ }),

/***/ 816:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pipes_array_index__ = __webpack_require__(730);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pipes_object_index__ = __webpack_require__(774);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_string_index__ = __webpack_require__(783);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pipes_math_index__ = __webpack_require__(765);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pipes_boolean_index__ = __webpack_require__(744);
/* unused harmony namespace reexport */
/* unused harmony namespace reexport */
/* unused harmony namespace reexport */
/* unused harmony namespace reexport */
/* unused harmony namespace reexport */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NgPipesModule; });






var NgPipesModule = (function () {
    function NgPipesModule() {
    }
    return NgPipesModule;
}());

NgPipesModule.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"], args: [{
                exports: [__WEBPACK_IMPORTED_MODULE_1__pipes_array_index__["a" /* NgArrayPipesModule */], __WEBPACK_IMPORTED_MODULE_3__pipes_string_index__["a" /* NgStringPipesModule */], __WEBPACK_IMPORTED_MODULE_4__pipes_math_index__["a" /* NgMathPipesModule */], __WEBPACK_IMPORTED_MODULE_5__pipes_boolean_index__["a" /* NgBooleanPipesModule */], __WEBPACK_IMPORTED_MODULE_2__pipes_object_index__["a" /* NgObjectPipesModule */]]
            },] },
];
/** @nocollapse */
NgPipesModule.ctorParameters = function () { return []; };





//# sourceMappingURL=index.js.map

/***/ }),

/***/ 817:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__helpers_helpers__ = __webpack_require__(713);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RadiansPipe; });


var RadiansPipe = (function () {
    function RadiansPipe() {
    }
    RadiansPipe.prototype.transform = function (degrees) {
        if (!__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__helpers_helpers__["h" /* isNumberFinite */])(degrees)) {
            return NaN;
        }
        return degrees * Math.PI / 180;
    };
    return RadiansPipe;
}());

RadiansPipe.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"], args: [{ name: 'radians' },] },
];
/** @nocollapse */
RadiansPipe.ctorParameters = function () { return []; };
//# sourceMappingURL=radians.js.map

/***/ }),

/***/ 827:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__state_booking_actions__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__add_item_modal_add_item_modal_component__ = __webpack_require__(828);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ng_bootstrap_ng_bootstrap__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__delete_item_modal_delete_item_modal_component__ = __webpack_require__(829);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__edit_item_modal_edit_item_modal_component__ = __webpack_require__(830);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_style_loader_all_bookings_scss__ = __webpack_require__(1015);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_style_loader_all_bookings_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_style_loader_all_bookings_scss__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AllBookings; });








var AllBookings = (function () {
    function AllBookings(store, modalService) {
        var _this = this;
        this.store = store;
        this.modalService = modalService;
        this.page = 1;
        this.limit = 4;
        this.pic = true;
        this.myImgUrl = '../../../../assets/img/default.png';
        this.c = true;
        this.activeCustomer = [];
        this.store
            .select('booking')
            .subscribe(function (res) {
            if (res.booking) {
                console.log(res);
                // alert("dhgdhg")
                _this.customerData = res.booking.data;
                //  this.activeCustomer=this.customerData;
                console.log("cdkjhbcjdksb", _this.customerData);
            }
        });
        //Dispatch action on load ..
        this.store.dispatch({ type: __WEBPACK_IMPORTED_MODULE_2__state_booking_actions__["a" /* ActionTypes */].APP_GETALL_BOOKING, payload: { currentPage: this.page } });
    }
    //  block(data1, isBlocked){
    // let obj = {
    //   'userId': data1._id,
    //   'isBlocked' : isBlocked
    // };
    // this.store.dispatch({
    //   type     : booking.ActionTypes.APP_BLOCK_USER_BY_ID,
    //   payload  : obj
    // });
    //    this.updateCustomerBlock(data1, isBlocked);
    //    }
    //     updateCustomerBlock(data1, isBlocked) {
    //   data1.isBlocked = isBlocked;
    // }
    //  getuserdetails(data)
    //   { console.log("bjhh")
    //   var obj={
    //     customer:data
    //   }
    // this.store.dispatch({ type: booking.ActionTypes.APP_GET_USER_BY_ID,payload:obj});
    //  this.lgModalShow();}
    AllBookings.prototype.lgModalShow = function () {
        var activeModal = this.modalService.open(__WEBPACK_IMPORTED_MODULE_3__add_item_modal_add_item_modal_component__["a" /* AddItemModal */], { size: 'lg' });
        activeModal.componentInstance.modalHeader = 'Large Modal';
    };
    //  staticModalShow(data) {
    //    var obj={
    //   customer:data
    // }
    //   // this.store.dispatch({ type: booking.ActionTypes.APP_GET_USER_BY_ID,payload:obj});
    //   const activeModal = this.modalService.open(CancelBookingModal, {size: 'lg',
    //     backdrop: 'static'});
    //   activeModal.componentInstance.modalHeader = 'Static modal';
    // }     
    AllBookings.prototype.deleteCustomerConfirm = function (data) {
        console.log("Working inside deleteCustomerConfirm");
        // let obj = {
        //   'categoryId':uData
        // };
        // this.store.dispatch({
        //   type: booking.ActionTypes.DELETE_ITEM_RECORD,
        //   payload:obj
        // });
        console.log("FFFFFf", data);
        this.store.dispatch({
            type: __WEBPACK_IMPORTED_MODULE_2__state_booking_actions__["a" /* ActionTypes */].ADD_ID_RECORD,
            payload: data
        });
        var customerActionModel = this.modalService.open(__WEBPACK_IMPORTED_MODULE_5__delete_item_modal_delete_item_modal_component__["a" /* DeleteCustomerModal */], { size: 'lg' });
        customerActionModel.componentInstance.modalHeader = 'Large Modal';
    };
    AllBookings.prototype.editCustomerConfirm = function (data) {
        console.log("FFFFFf", data);
        this.store.dispatch({
            type: __WEBPACK_IMPORTED_MODULE_2__state_booking_actions__["a" /* ActionTypes */].ADD_ID_RECORD,
            payload: data
        });
        var customerActionModel = this.modalService.open(__WEBPACK_IMPORTED_MODULE_6__edit_item_modal_edit_item_modal_component__["a" /* EditItemModal */], { size: 'lg' });
        customerActionModel.componentInstance.modalHeader = 'Large Modal';
    };
    return AllBookings;
}());
AllBookings = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'allbookings',
        template: __webpack_require__(999),
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["Store"], __WEBPACK_IMPORTED_MODULE_4__ng_bootstrap_ng_bootstrap__["d" /* NgbModal */]])
], AllBookings);



/***/ }),

/***/ 828:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__state_booking_actions__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(6);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddItemModal; });






var AddItemModal = (function () {
    function AddItemModal(modalService, activeModal, fb, store) {
        this.modalService = modalService;
        this.activeModal = activeModal;
        this.store = store;
        this.page = 1;
        this.submitted = false;
        this.src = '../../../../assets/img/default.png';
        this.form = fb.group({
            'name': ['', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]],
            'image': ['', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]]
        });
        this.store
            .select('booking')
            .subscribe(function (res) {
            // alert(res);
            // console.log(res.activeCustomer.customer);
            // this.activeCustomer = (res.activeCustomer) ? res.activeCustomer.customer.bankDetails : null;
        });
    }
    ;
    AddItemModal.prototype.selected = function (event) {
        var _this = this;
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.onload = function (event) {
                _this.src = event.target.result;
            };
            console.log(event.target.files[0]);
            this.document_image = event.target.files[0];
            reader.readAsDataURL(event.target.files[0]);
        }
    };
    AddItemModal.prototype.onSubmit = function (values) {
        // alert("hiiiii")
        event.preventDefault();
        this.submitted = true;
        console.log(values);
        {
            var data = {
                name: this.name,
                image: this.document_image
            };
            console.log(data);
            var fd = new FormData();
            // for(let prop in data){
            // fd.append(prop,data[prop]);
            fd.append('name', this.name);
            fd.append('image', this.document_image);
            console.log("hellooooooo", fd);
            //  }
            this.store.dispatch({
                type: __WEBPACK_IMPORTED_MODULE_3__state_booking_actions__["a" /* ActionTypes */].APP_ADD_ITEM,
                payload: fd
            });
        }
    };
    AddItemModal.prototype.closeModal = function () {
        this.activeModal.close();
    };
    return AddItemModal;
}());
AddItemModal = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'add-item-modal',
        template: __webpack_require__(1002)
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["d" /* NgbModal */],
        __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["c" /* NgbActiveModal */],
        __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormBuilder"],
        __WEBPACK_IMPORTED_MODULE_1__ngrx_store__["Store"]])
], AddItemModal);



/***/ }),

/***/ 829:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__state_booking_actions__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_style_loader_delete_item_modal_scss__ = __webpack_require__(1016);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_style_loader_delete_item_modal_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_style_loader_delete_item_modal_scss__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeleteCustomerModal; });




// import * as lang from '../../../../multilingual/state/lang.actions';
// import {Language} from '../../../../multilingual/model/lang.model';

var DeleteCustomerModal = (function () {
    // language = new Language();
    function DeleteCustomerModal(activeModal, store) {
        //  this.store
        //       .select('lang')
        //       .subscribe((res: any) => {
        //           //setting language
        //           if(res.resourceBundle != null)
        //           {
        //               for(var j=0;j<res.resourceBundle.length;j++)
        //               {
        //                   //console.log(res.resourceBundle)
        //                   this.language[res.resourceBundle[j].messageKey]=res.resourceBundle[j].customMessage
        var _this = this;
        this.activeModal = activeModal;
        this.store = store;
        this.activeCustomer = [];
        //               }
        //           }
        //       });
        this.store
            .select('booking')
            .subscribe(function (res) {
            //  this.activeCustomer=res.data;
            _this.activeCustomer = res.idrecord;
            //  foreach(let data of active )
            // this.activeCustomer = (res.data) ? res.data : null;
            console.log("dddddd", _this.activeCustomer);
            // console.log("heyyyy",this.activeCustomer._id);
        });
    }
    DeleteCustomerModal.prototype.ngOnInit = function () { };
    DeleteCustomerModal.prototype.closeModal = function () {
        this.activeModal.close();
    };
    DeleteCustomerModal.prototype.deleteCustomer = function () {
        var obj = {
            'categoryId': this.activeCustomer._id
            // 'categoryId':this.activeCustomer._id
        };
        this.store.dispatch({
            type: __WEBPACK_IMPORTED_MODULE_3__state_booking_actions__["a" /* ActionTypes */].DELETE_ITEM_RECORD,
            payload: obj
        });
        this.activeModal.close();
    };
    return DeleteCustomerModal;
}());
DeleteCustomerModal = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'delete-item-modal',
        styles: [(__webpack_require__(851))],
        template: "\n\n  <div class=\"modal-content\">\n  <div class=\"modal-header\">\n    \n    <h3>Do you want to delete this category?</h3>\n\n    \n    <button class=\"close\" aria-label=\"Close\" (click)=\"closeModal()\">\n      <span aria-hidden=\"true\" style=\"color:white\">&times;</span>\n    </button>\n  </div>\n    \n  <div class=\"modal-footer\">\n \n \n          <i class=\"fa fa-trash-o \" aria-hidden=\"true\" style=\"color:red ; cursor:pointer\" (click)=\"deleteCustomer(activeCustomer._id)\"></i>\n           <i class=\"fa fa-times\" aria-hidden=\"true\" style=\"color:red ; cursor:pointer\" (click)=\"closeModal()\"></i>\n       \n \n  </div>\n  \n\n</div>\n\n"
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["c" /* NgbActiveModal */], __WEBPACK_IMPORTED_MODULE_1__ngrx_store__["Store"]])
], DeleteCustomerModal);



/***/ }),

/***/ 830:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__state_booking_actions__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_style_loader_edit_item_modal_scss__ = __webpack_require__(1017);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_style_loader_edit_item_modal_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_style_loader_edit_item_modal_scss__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditItemModal; });





// import * as lang from '../../../../multilingual/state/lang.actions';
// import {Language} from '../../../../multilingual/model/lang.model';

var EditItemModal = (function () {
    function EditItemModal(activeModal, fb, store) {
        var _this = this;
        this.activeModal = activeModal;
        this.store = store;
        this.submitted = false;
        this.imageSelected = false;
        this.page = 1;
        this.limit = 100;
        this.myImgUrl = '../../../../assets/img/user.png';
        this.form = fb.group({
            'name': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].compose([
                    __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required,
                ])],
            'image': [''],
        });
        this.store
            .select('booking')
            .subscribe(function (res) {
            console.log(res);
            // this.activeCustomer = (res.activeCustomer)?res.activeCustomer.customer:null;
            _this.imageSelected = false;
            _this.activeCustomer = res.idrecord;
            _this.src = _this.activeCustomer.image.thumb;
            // console.log(this.activeCustomer);
        });
    }
    EditItemModal.prototype.selected = function (event) {
        var _this = this;
        this.imageSelected = true;
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.onload = function (event) {
                _this.src = event.target.result;
            };
            console.log(event.target.files[0]);
            this.document_image = event.target.files[0];
            console.log(this.document_image);
            reader.readAsDataURL(event.target.files[0]);
        }
    };
    EditItemModal.prototype.onSubmit = function (values, $event) {
        $event.preventDefault();
        this.submitted = true;
        console.log(values);
        // alert(this.activeCustomer.name);
        // alert(this.activeCustomer._id)
        console.log(values['name']);
        var obj = {
            name: values['name'],
            'categoryId': this.activeCustomer._id,
            image: this.document_image
            // 'categoryId':this.activeCustomer._id
        };
        //      var fd = new FormData();
        //       fd.append('name',values['name'] || this.activeCustomer.name);
        //       // // if(this.imageSelected)
        //       // // {
        //       // fd.append('image',this.document_image );
        //       // // }
        //      fd.append('categoryId', JSON.stringify(obj));
        console.log(obj);
        this.store.dispatch({
            type: __WEBPACK_IMPORTED_MODULE_4__state_booking_actions__["a" /* ActionTypes */].APP_EDIT_USER_BY_ID,
            payload: obj
        });
        this.activeModal.close();
    };
    EditItemModal.prototype.closeModal = function () {
        this.activeModal.close();
    };
    return EditItemModal;
}());
EditItemModal = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'edit-item-modal',
        template: __webpack_require__(1003)
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["c" /* NgbActiveModal */], __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"], __WEBPACK_IMPORTED_MODULE_1__ngrx_store__["Store"]])
], EditItemModal);



/***/ }),

/***/ 851:
/***/ (function(module, exports) {

module.exports = ".modal-buttons .btn {\n  margin: auto; }\n\n.modal-content {\n  color: #7d7d7d;\n  margin: auto; }\n\n.modal-content {\n  margin-top: 200px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  overflow: auto;\n  margin: auto;\n  /* @media(max-width: 768px) {\n    min-height: calc(100vh - 20px);\n  }*/ }\n"

/***/ }),

/***/ 852:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Bookings; });

var Bookings = (function () {
    function Bookings() {
    }
    Bookings.prototype.ngOnInit = function () {
    };
    return Bookings;
}());
Bookings = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'Bookings',
        template: "<router-outlet></router-outlet>"
    }),
    __metadata("design:paramtypes", [])
], Bookings);



/***/ }),

/***/ 853:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_AllBookings_all_bookings_component__ = __webpack_require__(827);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routing; });

//import { PastBookings } from './components/PastBookings/past-bookings.component';

//import { OnBookings} from './components/OnBookings/on-bookings.component';
//import { ByIdDetails } from './components/byIdDetails/byId-details.component';
//import { CancelBooking } from './components/CancelBooking/cancel-booking.component';
// noinspection TypeScriptValidateTypes
var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_1__components_AllBookings_all_bookings_component__["a" /* AllBookings */],
        children: [
            //{ path: 'pastbookings', component:PastBookings  },
            { path: 'Booking', component: __WEBPACK_IMPORTED_MODULE_1__components_AllBookings_all_bookings_component__["a" /* AllBookings */] },
        ]
    }
];
var routing = __WEBPACK_IMPORTED_MODULE_0__angular_router__["RouterModule"].forChild(routes);


/***/ }),

/***/ 854:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__(14);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BookingModal; });



//import { PipeTransform, Pipe } from '@angular/core';
var BookingModal = (function () {
    function BookingModal(activeModal, store) {
        this.activeModal = activeModal;
        this.store = store;
        this.store
            .select('booking')
            .subscribe(function (res) {
        });
    }
    BookingModal.prototype.ngOnInit = function () { };
    BookingModal.prototype.closeModal = function () {
        this.activeModal.close();
    };
    return BookingModal;
}());
BookingModal = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'add-service-modal',
        styles: [(__webpack_require__(986))],
        template: __webpack_require__(1000)
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["c" /* NgbActiveModal */], __WEBPACK_IMPORTED_MODULE_1__ngrx_store__["Store"]])
], BookingModal);



/***/ }),

/***/ 855:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(6);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CancelBookingModal; });



;
var types = ['success', 'error', 'info', 'warning'];

var CancelBookingModal = (function () {
    function CancelBookingModal(activeModal, fb, store) {
        var _this = this;
        this.activeModal = activeModal;
        this.store = store;
        this.submitted = false;
        this.page = 1;
        this.limit = 4;
        this.myImgUrl = '../../../../assets/img/default.png';
        this.form = fb.group({
            'name': [''],
            'bank': [''],
            'country': [''],
            'beneficiary': [''],
            'iban': ['']
        });
        this.store
            .select('booking')
            .subscribe(function (res) {
            _this.activeCustomer = (res.activeCustomer) ? res.activeCustomer.customer : null;
        });
    }
    CancelBookingModal.prototype.onSubmit = function (values) {
        event.preventDefault();
        this.submitted = true;
        console.log(values);
        var fd = new FormData();
        fd.append('userName', values['name'] || this.activeCustomer.userName);
        fd.append('bankName', values['bank'] || this.activeCustomer.bankDetails.bankName);
        fd.append('bankCountry', values['country'] || this.activeCustomer.bankDetails.bankCountry);
        fd.append('beneficiaryName', values['beneficiary'] || this.activeCustomer.bankDetails.beneficiaryName);
        fd.append('ibanNumber', values['iban'] || this.activeCustomer.bankDetails.beneficiaryName);
        fd.append('userId', this.activeCustomer._id);
        console.log(fd);
        //  this.store.dispatch({
        //               type:booking.ActionTypes. APP_EDIT_USER_BY_ID,
        //                payload: fd
        //           })
        // this.store.dispatch({ type: booking.ActionTypes.APP_GETALL_BOOKING, payload: {currentPage:this.page,limit:this.limit} })
        this.activeModal.close();
    };
    CancelBookingModal.prototype.closeModal = function () {
        this.activeModal.close();
    };
    return CancelBookingModal;
}());
CancelBookingModal = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'cancel-booking-modal',
        styles: [(__webpack_require__(987))],
        template: __webpack_require__(1001)
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["c" /* NgbActiveModal */], __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"], __WEBPACK_IMPORTED_MODULE_1__ngrx_store__["Store"]])
], CancelBookingModal);



/***/ }),

/***/ 985:
/***/ (function(module, exports) {

module.exports = ".all {\n  width: 50px;\n  height: 50px !important; }\n\n.edit {\n  color: black;\n  font-size: 30px;\n  margin-top: 30px;\n  margin-left: 20px;\n  cursor: pointer;\n  position: relative;\n  display: inline-block; }\n  .edit:hover .alt {\n    display: block; }\n\n.details {\n  color: black;\n  font-size: 20px;\n  margin-top: 30px;\n  margin-left: 20px;\n  background-color: transparent;\n  border: none; }\n\n.bank-details {\n  color: slategray;\n  font-size: 20px;\n  margin-top: 30px;\n  margin-left: 20px;\n  cursor: pointer;\n  background-color: black;\n  border: none; }\n\n.edit1 {\n  background-color: transparent;\n  border: none; }\n\n.table.user {\n  color: black; }\n\n.img {\n  height: 120px;\n  width: 130px; }\n\n/* Tooltip text */\n.edit1 .tooltiptext {\n  visibility: hidden;\n  width: 57px;\n  height: 30px;\n  background-color: black;\n  color: #fff;\n  text-align: center;\n  padding: 3px 0;\n  border-radius: 1px;\n  font-size: 15px;\n  cursor: pointer;\n  /* Position the tooltip text - see examples below! */\n  position: absolute;\n  z-index: 1; }\n\n.edit1:hover .tooltiptext {\n  visibility: visible; }\n\n.alt {\n  display: none; }\n"

/***/ }),

/***/ 986:
/***/ (function(module, exports) {

module.exports = ".modal-buttons .btn {\n  margin-right: 20px; }\n\n.modal-content {\n  color: #7d7d7d; }\n"

/***/ }),

/***/ 987:
/***/ (function(module, exports) {

module.exports = ".modal-buttons .btn {\n  margin: auto; }\n\n.modal-content {\n  color: #7d7d7d;\n  margin: auto; }\n\n.modal-content {\n  margin-top: 200px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  overflow: auto;\n  margin: auto;\n  /* @media(max-width: 768px) {\n    min-height: calc(100vh - 20px);\n  }*/ }\n"

/***/ }),

/***/ 988:
/***/ (function(module, exports) {

module.exports = ".modal-buttons .btn {\n  margin: auto; }\n\n.modal-content {\n  color: #7d7d7d;\n  margin: auto; }\n\n.modal-content {\n  margin-top: 200px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  overflow: auto;\n  margin: auto;\n  background-color: black;\n  /* @media(max-width: 768px) {\n    min-height: calc(100vh - 20px);\n  }*/ }\n"

/***/ }),

/***/ 999:
/***/ (function(module, exports) {

module.exports = "<div class=\"widgets\">\n  <button (click)=\"lgModalShow()\" class=\"btn btn-success\">Add Categories</button>\n\n  <table class=\"table  table-striped user table-responsive\">\n    <thead>\n      <tr>\n        <th><b>S.No</b></th>\n       \n        <th><b> Name</b></th>\n        <th><b></b></th>\n        <th><b></b></th>\n         <th><b>Image</b></th>\n          <!--<th><b></b></th>-->\n          \n         <th><b>Action</b></th>\n        <!--<th><b>Competition Name</b></th>\n        <th><b>Money Earned</b></th>\n        <th><b>Edit</b></th>\n        <th><b>Block/Unblock</b></th>\n        <th><b>Bank Details</b></th>-->\n\n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor=\"let data of customerData;let i=index \">\n        <td>{{i+1}}</td>\n       \n        \n        <td>{{ data.name }}</td>\n        <td></td>\n         <td></td>\n          <!--<td></td>\n           <td></td>-->\n        <!--<td>{{ data.email}} </td>-->\n        <!--<td><img class=\"img\" [src]=\"data.customer.profilePicture[0] != null ? data.customer.profilePicture : myImgUrl\" /></td>-->\n          <td><img class=\"img\" [src]=\"data.image.thumb\"/></td>\n          <!--<td><img class=\"img\" [src]=\"data.image.original\"/></td>-->\n            <td colspan=\"2\">  \n                <button class=\"btn btn-danger\"(click)=\"deleteCustomerConfirm(data)\">\n          <i class=\"fa fa-trash-o edit1\" aria-hidden=\"true\" style=\"color:red\"></i>\n          <span class=\"tooltiptext\" style=\"color:black\">delete</span>\n        </button>\n\n            <button class=\"btn btn-success\"(click)=\"editCustomerConfirm(data)\">\n          <i class=\"fa fa-pencil white_color\" aria-hidden=\"true\" style=\"color:green\"></i>\n          <span class=\"tooltiptext\" >edit</span>\n        </button>\n\n            </td>\n\n\n        <!--<td></td>\n        <td> <button class=\"details\">{{data.customer.totalEarnings}}</button></td>\n        <td><button class=\"edit1\"><i class=\"fa fa-edit edit\"  (click)=\"staticModalShow(data)\"><span class=\"tooltiptext\">edit</span></i></button></td>\n        <td> <button class=\"edit1\" *ngIf=\"data.isBlocked == false\">\n        <i class=\"fa fa-unlock edit\" (click)=\"block(data, true)\"></i>\n        <span class=\"tooltiptext\">block</span>\n      </button>\n          <button class=\"edit1\" *ngIf=\"data.isBlocked == true\">\n        <i class=\"fa fa-lock edit\" (click)=\"block(data, false)\"></i>\n        <span class=\"tooltiptext\">unblock</span>\n      </button>\n        </td>\n        <td> <button class=\"bank-details\" (click)=\"getuserdetails(data)\">View</button></td>-->\n\n\n      </tr>\n    </tbody>\n  </table>\n\n\n\n  <!--<pagination-controls (pageChange)=\"pageChanged($event)\" maxSize=\"4\" directionLinks=\"true\" autoHide=\"true\" previousLabel=\"Previous\"\n    nextLabel=\"Next\" screenReaderPaginationLabel=\"Pagination\" screenReaderPageLabel=\"page\" screenReaderCurrentLabel=\"You're on page\"></pagination-controls>-->\n\n</div>"

/***/ })

});
//# sourceMappingURL=2.chunk.js.map