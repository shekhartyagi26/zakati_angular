var ac_main =
webpackJsonpac__name_([4],[
/* 0 */,
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(0);

/***/ }),
/* 13 */,
/* 14 */,
/* 15 */,
/* 16 */,
/* 17 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__services__ = __webpack_require__(18);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__services__["b"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__theme_constants__ = __webpack_require__(58);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_1__theme_constants__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_1__theme_constants__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_1__theme_constants__["c"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_1__theme_constants__["d"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__theme_configProvider__ = __webpack_require__(57);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_2__theme_configProvider__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__theme_config__ = __webpack_require__(56);
/* unused harmony namespace reexport */






/***/ }),
/* 18 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baImageLoader__ = __webpack_require__(417);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_0__baImageLoader__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__baMenu__ = __webpack_require__(419);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_1__baMenu__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__baThemePreloader__ = __webpack_require__(421);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_2__baThemePreloader__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__baThemeSpinner__ = __webpack_require__(423);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_3__baThemeSpinner__["a"]; });




//export * from './notification';


/***/ }),
/* 19 */,
/* 20 */,
/* 21 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActionTypes; });
/* unused harmony export AuthLoginAction */
/* unused harmony export AuthForgotPassword */
/* unused harmony export AuthLoginErrorAction */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return AuthLoginSuccessAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return AuthLogoutAction; });
/* unused harmony export AuthLogoutErrorAction */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return AuthLogoutSuccessAction; });
/* unused harmony export AuthPassRequestAction */
/* unused harmony export AuthPassRequestErrorAction */
/* unused harmony export AuthPassRequestSuccessAction */
/* unused harmony export AuthPassVerifyAction */
/* unused harmony export AuthPassVerifyErrorAction */
/* unused harmony export AuthPassVerifySuccessAction */
/* unused harmony export AuthRegisterAction */
/* unused harmony export AuthRegisterErrorAction */
/* unused harmony export AuthRegisterSuccessAction */
/* unused harmony export AuthForgotAction */
/* unused harmony export AuthForgotPassErrorAction */
/* unused harmony export AuthForgotPassSuccessAction */
/* unused harmony export AuthResetpasswordAction */
/* unused harmony export AuthResetPasswordErrorAction */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return AuthResetPasswordSuccessAction; });
/* unused harmony export AuthForgotPassTokenVerifyAction */
/* unused harmony export AuthForgotPassTokenVerifyErrorAction */
/* unused harmony export AuthForgotPassTokenVerifySuccessAction */
var ActionTypes = {
    AUTH_LOGIN: 'AUTH_LOGIN',
    AUTH_LOGIN_ERROR: 'AUTH_LOGIN_ERROR',
    AUTH_LOGIN_SUCCESS: 'AUTH_LOGIN_SUCCESS',
    AUTH_LOGOUT: 'AUTH_LOGOUT',
    AUTH_LOGOUT_ERROR: 'AUTH_LOGOUT_ERROR',
    AUTH_LOGOUT_SUCCESS: 'AUTH_LOGOUT_SUCCESS',
    AUTH_PASS_REQUEST: 'AUTH_PASS_REQUEST',
    AUTH_PASS_REQUEST_ERROR: 'AUTH_PASS_REQUEST_ERROR',
    AUTH_PASS_REQUEST_SUCCESS: 'AUTH_PASS_REQUEST_SUCCESS',
    AUTH_PASS_VERIFY: 'AUTH_PASS_VERIFY',
    AUTH_PASS_VERIFY_ERROR: 'AUTH_PASS_VERIFY_ERROR',
    AUTH_PASS_VERIFY_SUCCESS: 'AUTH_PASS_VERIFY_SUCCESS',
    AUTH_REGISTER: 'AUTH_REGISTER',
    AUTH_REGISTER_ERROR: 'AUTH_REGISTER_ERROR',
    AUTH_REGISTER_SUCCESS: 'AUTH_REGISTER_SUCCESS',
    // AUTH_FORGOT_PASSWORD:      'AUTH_FORGOT_PASSWORD',
    AUTH_FORGOT_PASSWORD: 'AUTH_FORGOT_PASSWORD',
    AUTH_FORGOT_PASSWORD_SUCCESS: 'AUTH_FORGOT_PASSWORD_SUCCESS',
    AUTH_FORGOT_PASSWORD_ERROR: 'AUTH_FORGOT_PASSWORD_ERROR',
    AUTH_RESET_PASSWORD: 'AUTH_RESET_PASSWORD',
    AUTH_RESET_PASSWORD_SUCCESS: 'AUTH_RESET_PASSWORD_SUCCESS',
    AUTH_RESET_PASSWORD_ERROR: 'AUTH_RESET_PASSWORD_ERROR',
    AUTH_FORGOT_PASS_TOKEN: 'AUTH_FORGOT_PASS_TOKEN',
    AUTH_FORGOT_PASS_TOKEN_SUCCESS: 'AUTH_FORGOT_PASS_TOKEN_SUCCESS',
    AUTH_FORGOT_PASS_TOKEN_ERROR: 'AUTH_FORGOT_PASS_TOKEN_ERROR'
};
/** LOGIN **/
var AuthLoginAction = (function () {
    function AuthLoginAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.AUTH_LOGIN;
        console.log("action fired");
    }
    return AuthLoginAction;
}());

var AuthForgotPassword = (function () {
    function AuthForgotPassword(payload) {
        this.payload = payload;
        this.type = ActionTypes.AUTH_FORGOT_PASSWORD;
    }
    return AuthForgotPassword;
}());

var AuthLoginErrorAction = (function () {
    function AuthLoginErrorAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.AUTH_LOGIN_ERROR;
    }
    return AuthLoginErrorAction;
}());

var AuthLoginSuccessAction = (function () {
    function AuthLoginSuccessAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.AUTH_LOGIN_SUCCESS;
    }
    return AuthLoginSuccessAction;
}());

/** LOGOUT **/
var AuthLogoutAction = (function () {
    function AuthLogoutAction(payload) {
        if (payload === void 0) { payload = {}; }
        this.payload = payload;
        this.type = ActionTypes.AUTH_LOGOUT;
    }
    return AuthLogoutAction;
}());

var AuthLogoutErrorAction = (function () {
    function AuthLogoutErrorAction() {
        this.type = ActionTypes.AUTH_LOGOUT_ERROR;
    }
    return AuthLogoutErrorAction;
}());

var AuthLogoutSuccessAction = (function () {
    function AuthLogoutSuccessAction(payload) {
        if (payload === void 0) { payload = {}; }
        this.payload = payload;
        this.type = ActionTypes.AUTH_LOGOUT_SUCCESS;
    }
    return AuthLogoutSuccessAction;
}());

/** PASS_REQUEST **/
var AuthPassRequestAction = (function () {
    function AuthPassRequestAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.AUTH_PASS_REQUEST;
    }
    return AuthPassRequestAction;
}());

var AuthPassRequestErrorAction = (function () {
    function AuthPassRequestErrorAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.AUTH_PASS_REQUEST_ERROR;
    }
    return AuthPassRequestErrorAction;
}());

var AuthPassRequestSuccessAction = (function () {
    function AuthPassRequestSuccessAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.AUTH_PASS_REQUEST_SUCCESS;
    }
    return AuthPassRequestSuccessAction;
}());

/** PASS_VERIFY **/
var AuthPassVerifyAction = (function () {
    function AuthPassVerifyAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.AUTH_PASS_VERIFY;
    }
    return AuthPassVerifyAction;
}());

var AuthPassVerifyErrorAction = (function () {
    function AuthPassVerifyErrorAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.AUTH_PASS_VERIFY_ERROR;
    }
    return AuthPassVerifyErrorAction;
}());

var AuthPassVerifySuccessAction = (function () {
    function AuthPassVerifySuccessAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.AUTH_PASS_VERIFY_SUCCESS;
    }
    return AuthPassVerifySuccessAction;
}());

/** REGISTER **/
var AuthRegisterAction = (function () {
    function AuthRegisterAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.AUTH_REGISTER;
    }
    return AuthRegisterAction;
}());

var AuthRegisterErrorAction = (function () {
    function AuthRegisterErrorAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.AUTH_REGISTER_ERROR;
    }
    return AuthRegisterErrorAction;
}());

var AuthRegisterSuccessAction = (function () {
    function AuthRegisterSuccessAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.AUTH_REGISTER_SUCCESS;
    }
    return AuthRegisterSuccessAction;
}());

//*****************FORGOT PASSWORD*********************** */
var AuthForgotAction = (function () {
    function AuthForgotAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.AUTH_FORGOT_PASSWORD;
    }
    return AuthForgotAction;
}());

var AuthForgotPassErrorAction = (function () {
    function AuthForgotPassErrorAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.AUTH_FORGOT_PASSWORD_ERROR;
    }
    return AuthForgotPassErrorAction;
}());

var AuthForgotPassSuccessAction = (function () {
    function AuthForgotPassSuccessAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.AUTH_FORGOT_PASSWORD_SUCCESS;
    }
    return AuthForgotPassSuccessAction;
}());

//*****************FORGOT PASSWORD ENDS*********************** */
//**************************RESET PASSSWORD******************** */
var AuthResetpasswordAction = (function () {
    function AuthResetpasswordAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.AUTH_RESET_PASSWORD;
    }
    return AuthResetpasswordAction;
}());

var AuthResetPasswordErrorAction = (function () {
    function AuthResetPasswordErrorAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.AUTH_RESET_PASSWORD_ERROR;
    }
    return AuthResetPasswordErrorAction;
}());

var AuthResetPasswordSuccessAction = (function () {
    function AuthResetPasswordSuccessAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.AUTH_RESET_PASSWORD_SUCCESS;
    }
    return AuthResetPasswordSuccessAction;
}());

//**************************RESET PASSSWORD ENDS******************** */
//**************************VERIFY PASSSWORD ******************** */
var AuthForgotPassTokenVerifyAction = (function () {
    function AuthForgotPassTokenVerifyAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.AUTH_FORGOT_PASS_TOKEN;
    }
    return AuthForgotPassTokenVerifyAction;
}());

var AuthForgotPassTokenVerifyErrorAction = (function () {
    function AuthForgotPassTokenVerifyErrorAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.AUTH_FORGOT_PASS_TOKEN_ERROR;
    }
    return AuthForgotPassTokenVerifyErrorAction;
}());

var AuthForgotPassTokenVerifySuccessAction = (function () {
    function AuthForgotPassTokenVerifySuccessAction(payload) {
        this.payload = payload;
        this.type = ActionTypes.AUTH_FORGOT_PASS_TOKEN_SUCCESS;
    }
    return AuthForgotPassTokenVerifySuccessAction;
}());



/***/ }),
/* 22 */,
/* 23 */,
/* 24 */,
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(12);

/***/ }),
/* 26 */,
/* 27 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GlobalState; });


var GlobalState = (function () {
    function GlobalState() {
        var _this = this;
        this._data = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
        this._dataStream$ = this._data.asObservable();
        this._subscriptions = new Map();
        this._dataStream$.subscribe(function (data) { return _this._onEvent(data); });
    }
    GlobalState.prototype.notifyDataChanged = function (event, value) {
        var current = this._data[event];
        if (current !== value) {
            this._data[event] = value;
            this._data.next({
                event: event,
                data: this._data[event]
            });
        }
    };
    GlobalState.prototype.subscribe = function (event, callback) {
        var subscribers = this._subscriptions.get(event) || [];
        subscribers.push(callback);
        this._subscriptions.set(event, subscribers);
    };
    GlobalState.prototype._onEvent = function (data) {
        var subscribers = this._subscriptions.get(data['event']) || [];
        subscribers.forEach(function (callback) {
            callback.call(null, data['data']);
        });
    };
    return GlobalState;
}());
GlobalState = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], GlobalState);



/***/ }),
/* 28 */,
/* 29 */,
/* 30 */,
/* 31 */,
/* 32 */,
/* 33 */,
/* 34 */,
/* 35 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_uploader__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_translation_module__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__theme_config__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__theme_configProvider__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_baCard_baCardBlur_directive__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__directives__ = __webpack_require__(408);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pipes__ = __webpack_require__(415);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__validators__ = __webpack_require__(59);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NgaModule; });














var NGA_COMPONENTS = [
    __WEBPACK_IMPORTED_MODULE_8__components__["a" /* BaAmChart */],
    __WEBPACK_IMPORTED_MODULE_8__components__["b" /* BaBackTop */],
    __WEBPACK_IMPORTED_MODULE_8__components__["c" /* BaCard */],
    __WEBPACK_IMPORTED_MODULE_8__components__["d" /* BaChartistChart */],
    __WEBPACK_IMPORTED_MODULE_8__components__["e" /* BaCheckbox */],
    __WEBPACK_IMPORTED_MODULE_8__components__["f" /* BaContentTop */],
    __WEBPACK_IMPORTED_MODULE_8__components__["g" /* BaFullCalendar */],
    __WEBPACK_IMPORTED_MODULE_8__components__["h" /* BaMenuItem */],
    __WEBPACK_IMPORTED_MODULE_8__components__["i" /* BaMenu */],
    __WEBPACK_IMPORTED_MODULE_8__components__["j" /* BaMultiCheckbox */],
    __WEBPACK_IMPORTED_MODULE_8__components__["k" /* BaPageTop */],
    __WEBPACK_IMPORTED_MODULE_8__components__["l" /* BaPictureUploader */],
    __WEBPACK_IMPORTED_MODULE_8__components__["m" /* BaSidebar */],
    __WEBPACK_IMPORTED_MODULE_8__components__["n" /* BaFileUploader */],
];
var NGA_DIRECTIVES = [
    __WEBPACK_IMPORTED_MODULE_10__directives__["a" /* BaScrollPosition */],
    __WEBPACK_IMPORTED_MODULE_10__directives__["b" /* BaSlimScroll */],
    __WEBPACK_IMPORTED_MODULE_10__directives__["c" /* BaThemeRun */],
    __WEBPACK_IMPORTED_MODULE_9__components_baCard_baCardBlur_directive__["a" /* BaCardBlur */]
];
var NGA_PIPES = [
    __WEBPACK_IMPORTED_MODULE_11__pipes__["a" /* BaAppPicturePipe */],
    __WEBPACK_IMPORTED_MODULE_11__pipes__["b" /* BaKameleonPicturePipe */],
    __WEBPACK_IMPORTED_MODULE_11__pipes__["c" /* BaProfilePicturePipe */]
];
var NGA_SERVICES = [
    __WEBPACK_IMPORTED_MODULE_12__services__["c" /* BaImageLoaderService */],
    __WEBPACK_IMPORTED_MODULE_12__services__["d" /* BaThemePreloader */],
    __WEBPACK_IMPORTED_MODULE_12__services__["a" /* BaThemeSpinner */],
    __WEBPACK_IMPORTED_MODULE_12__services__["b" /* BaMenuService */]
];
var NGA_VALIDATORS = [
    __WEBPACK_IMPORTED_MODULE_13__validators__["b" /* EmailValidator */],
    __WEBPACK_IMPORTED_MODULE_13__validators__["a" /* EqualPasswordsValidator */]
];
var NgaModule = NgaModule_1 = (function () {
    function NgaModule() {
    }
    NgaModule.forRoot = function () {
        return {
            ngModule: NgaModule_1,
            providers: [
                __WEBPACK_IMPORTED_MODULE_7__theme_configProvider__["a" /* BaThemeConfigProvider */],
                __WEBPACK_IMPORTED_MODULE_6__theme_config__["a" /* BaThemeConfig */]
            ].concat(NGA_VALIDATORS, NGA_SERVICES),
        };
    };
    return NgaModule;
}());
NgaModule = NgaModule_1 = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: NGA_PIPES.concat(NGA_DIRECTIVES, NGA_COMPONENTS),
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["RouterModule"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormsModule"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["ReactiveFormsModule"],
            __WEBPACK_IMPORTED_MODULE_5__app_translation_module__["a" /* AppTranslationModule */],
            __WEBPACK_IMPORTED_MODULE_4_ngx_uploader__["a" /* NgUploaderModule */]
        ],
        exports: NGA_PIPES.concat(NGA_DIRECTIVES, NGA_COMPONENTS)
    })
], NgaModule);

var NgaModule_1;


/***/ }),
/* 36 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_http_loader__ = __webpack_require__(457);
/* unused harmony export createTranslateLoader */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppTranslationModule; });





function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_3__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/i18n/US/', '.json');
}
var translationOptions = {
    loader: {
        provide: __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["b" /* TranslateLoader */],
        useFactory: (createTranslateLoader),
        deps: [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]]
    }
};
var AppTranslationModule = (function () {
    function AppTranslationModule(translate) {
        this.translate = translate;
        translate.addLangs(["en", "fr"]);
        translate.setDefaultLang('en');
        //translate.use('fr');
    }
    return AppTranslationModule;
}());
AppTranslationModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [__WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["c" /* TranslateModule */].forRoot(translationOptions)],
        exports: [__WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["c" /* TranslateModule */]],
        providers: [__WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["a" /* TranslateService */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["a" /* TranslateService */]])
], AppTranslationModule);



/***/ }),
/* 37 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_jwt__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angular2_jwt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environment_environment__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ng_bootstrap_ng_bootstrap__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ngx_toastr__ = __webpack_require__(23);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return user_service; });








var types = ['success', 'error', 'info', 'warning'];
var user_service = (function () {
    function user_service(modalService, http, authHttp, jwtHelper, toastrService) {
        //api for login
        this.modalService = modalService;
        this.http = http;
        this.authHttp = authHttp;
        this.jwtHelper = jwtHelper;
        this.toastrService = toastrService;
        this.title = '';
        // type = types[0];
        this.message = '';
        //  version = VERSION;
        this.lastInserted = [];
        this.options = this.toastrService.toastrConfig;
    }
    user_service.prototype.login = function (data) {
        //  let token=localStorage.getItem('token')
        // //console.log(token)
        // var header_token = 'Bearer '+token
        var url = __WEBPACK_IMPORTED_MODULE_4__environment_environment__["a" /* environment */].APP.API_URL + __WEBPACK_IMPORTED_MODULE_4__environment_environment__["a" /* environment */].APP.LOGIN_API;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]({ 'Content-Type': 'application/json' });
        // headers.append('Authorization',header_token);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        //console.log(url)
        return this.http.post(url, data, options)
            .map(function (res) { return res.json(); })
            .catch(function (error) {
            console.log(error);
            try {
                return (__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__["Observable"].throw(error.json()));
            }
            catch (jsonError) {
                // If the error couldn't be parsed as JSON data
                // then it's possible the API is down or something
                // went wrong with the parsing of the successful
                // response. In any case, to keep things simple,
                // we'll just create a minimum representation of
                // a parsed error.
                var minimumViableError = {
                    success: false
                };
                return (__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__["Observable"].throw(minimumViableError));
            }
        });
    };
    //api for logout
    user_service.prototype.logoutUser = function () {
        //console.log("looged out service")
        var token = localStorage.getItem('token');
        // //console.log(token)
        var header_token = token;
        //console.log(header_token)
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]({ 'Content-Type': 'application/json' });
        headers.append('Authorization', header_token);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        var data = '';
        var url = __WEBPACK_IMPORTED_MODULE_4__environment_environment__["a" /* environment */].APP.API_URL + __WEBPACK_IMPORTED_MODULE_4__environment_environment__["a" /* environment */].APP.LOGOUT_API;
        return this.http.post(url, data, options)
            .map(function (res) { return res.json(); })
            .catch(function (error) {
            console.log(error);
            try {
                return (__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__["Observable"].throw(error.json()));
            }
            catch (jsonError) {
                // If the error couldn't be parsed as JSON data
                // then it's possible the API is down or something
                // went wrong with the parsing of the successful
                // response. In any case, to keep things simple,
                // we'll just create a minimum representation of
                // a parsed error.
                var minimumViableError = {
                    success: false
                };
                return (__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__["Observable"].throw(minimumViableError));
            }
        });
    };
    //  forgotpassword(payload){
    //   //  let token=localStorage.getItem('token')
    //   // //console.log(token)
    //   // var header_token = 'Bearer '+token
    //   let url = environment.APP.API_URL+environment.APP.FORGOT_PASSWORD+ "?email=" + payload.email;
    //   let headers = new Headers({ 'Content-Type': 'application/json' });
    //    // headers.append('Authorization',header_token);
    //   let options = new RequestOptions({ headers: headers }); 
    //   //console.log(url)
    //   return this.http.get(url,options)
    //     .map((res: Response) => res.json())
    //     .catch((error: any) => {
    //       console.log(error)
    //       try {
    //         return (Observable.throw(error.json()));
    //       } catch (jsonError) {
    //         // If the error couldn't be parsed as JSON data
    //         // then it's possible the API is down or something
    //         // went wrong with the parsing of the successful
    //         // response. In any case, to keep things simple,
    //         // we'll just create a minimum representation of
    //         // a parsed error.
    //         var minimumViableError = {
    //           success: false
    //         };
    //         return (Observable.throw(minimumViableError));
    //       }
    //     })
    // }
    // show()          {
    //   console.log("cghmvhjnb")
    //              const activeModal = this.modalService.open(LoginModal, {size: 'lg',
    //     backdrop: 'static'});
    //   activeModal.componentInstance.modalHeader = 'Static modal';
    //             }
    //**********************FORGOT **************************/
    user_service.prototype.forgotPass = function (data) {
        var token = localStorage.getItem('access_token');
        //  let token=localStorage.getItem('token')
        // //console.log(token)
        // var header_token = 'Bearer '+token
        var url = __WEBPACK_IMPORTED_MODULE_4__environment_environment__["a" /* environment */].APP.API_URL + __WEBPACK_IMPORTED_MODULE_4__environment_environment__["a" /* environment */].APP.FORGOT_PASSWORD;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]({ 'Content-Type': 'application/json' });
        // headers.append('Authorization',header_token);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        //console.log(url)
        return this.http.put(url, data, options)
            .map(function (res) { return res.json(); })
            .catch(function (error) {
            console.log(error);
            //  let m = 'Email does not match !';
            //       let t = 'Authentication';
            //       const opt = cloneDeep(this.options);
            //       const inserted = this.toastrService[types[1]](m, t, opt);
            //       if (inserted) {
            //         this.lastInserted.push(inserted.toastId);
            //       // }
            //     }
            try {
                return (__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__["Observable"].throw(error.json()));
            }
            catch (jsonError) {
                // If the error couldn't be parsed as JSON data
                // then it's possible the API is down or something
                // went wrong with the parsing of the successful
                // response. In any case, to keep things simple,
                // we'll just create a minimum representation of
                // a parsed error.
                var minimumViableError = {
                    success: false
                };
                return (__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__["Observable"].throw(minimumViableError));
            }
        });
    };
    // /*****************************FORGOT PASS ENDS************************ */
    // //******************************VERIFY ACCESS TOKEN******************* */
    user_service.prototype.authenticateResetPassToken = function (data) {
        var url = __WEBPACK_IMPORTED_MODULE_4__environment_environment__["a" /* environment */].APP.API_URL + __WEBPACK_IMPORTED_MODULE_4__environment_environment__["a" /* environment */].APP.VERIFY_TOKEN;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        return this.http.put(url, data, options)
            .map(function (res) { return res.json(); })
            .catch(function (error) {
            try {
                return (__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__["Observable"].throw(error.json()));
            }
            catch (jsonError) {
                var minimumViableError = {
                    success: false
                };
                return (__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__["Observable"].throw(minimumViableError));
            }
        });
    };
    //   //**************************TOKEN ENDS******************************** */
    // //**********************************RESET********************************* */
    user_service.prototype.resetPassword = function (data) {
        //  let token=localStorage.getItem('token')
        // //console.log(token)
        // var header_token = 'Bearer '+token
        var url = __WEBPACK_IMPORTED_MODULE_4__environment_environment__["a" /* environment */].APP.API_URL + __WEBPACK_IMPORTED_MODULE_4__environment_environment__["a" /* environment */].APP.RESET_API;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]({ 'Content-Type': 'application/json', });
        // headers.append('Authorization',header_token);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        //console.log(url)
        return this.http.put(url, data, options)
            .map(function (res) { return res.json(); })
            .catch(function (error) {
            console.log(error);
            try {
                return (__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__["Observable"].throw(error.json()));
            }
            catch (jsonError) {
                // If the error couldn't be parsed as JSON data
                // then it's possible the API is down or something
                // went wrong with the parsing of the successful
                // response. In any case, to keep things simple,
                // we'll just create a minimum representation of
                // a parsed error.
                var minimumViableError = {
                    success: false
                };
                return (__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__["Observable"].throw(minimumViableError));
            }
        });
    };
    return user_service;
}());
user_service = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__ng_bootstrap_ng_bootstrap__["d" /* NgbModal */], __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"], __WEBPACK_IMPORTED_MODULE_2_angular2_jwt__["AuthHttp"], __WEBPACK_IMPORTED_MODULE_2_angular2_jwt__["JwtHelper"], __WEBPACK_IMPORTED_MODULE_7_ngx_toastr__["b" /* ToastrService */]])
], user_service);



/***/ }),
/* 38 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
var environment = {
    production: false,
    APP: {
        API_URL: "http://34.208.129.161:3000",
        LOGIN_API: '/api/v1/admin/login',
        LOGOUT_API: '/api/v1/admin/logout',
        FORGOT_PASSWORD: '/api/v1/user/forgotPassword',
        GET_ALL_BOOKING: '/api/v1/category',
        GET_ALL_COMPETITIONS: '/api/v1/admin/getUsers',
        GET_USER_BY_ID: '/admin/getCustomerWithId',
        BLOCK_USER_BY_ID: '/admin/blockOption',
        DELETE_USER_BY_ID: '/admin/deleteUser',
        EDIT_USER_BY_ID: '/api/v1/category/',
        GET_DASHBOARD_COUNT: '/admin/getAllUsersRegisteredToday',
        GET_USER_REPORT_MONTHLY: '/admin/geUserReportMonthly',
        CREATE_COMPETITION: '/admin/createCompetition',
        VERIFY_TOKEN: '/api/v1/user/verifyResetToken',
        RESET_API: '/api/v1/user/resetPassword',
        ADD_ITEM: '/api/v1/category',
        DELETE_ITEM_BY_ID: '/api/v1/category/',
    }
};


/***/ }),
/* 39 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_observable_of__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_observable_of___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_observable_of__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_do__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_do___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_do__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_delay__ = __webpack_require__(301);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_delay___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_delay__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });




var AuthService = (function () {
    function AuthService() {
        this.isLoggedIn = false;
    }
    AuthService.prototype.login = function () {
        var token = localStorage.getItem('tokenSession');
        if (token) {
            //console.log("token present")
            this.isLoggedIn = true;
            //console.log(token)
            return this.isLoggedIn;
        }
        else {
            this.isLoggedIn = false;
            return this.isLoggedIn;
        }
    };
    AuthService.prototype.logout = function () {
        console.log("logout is performed");
        window.localStorage.removeItem('tokenSession');
    };
    return AuthService;
}());
AuthService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], AuthService);



/***/ }),
/* 40 */,
/* 41 */,
/* 42 */,
/* 43 */,
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(126);

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(209);

/***/ }),
/* 46 */,
/* 47 */,
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(423);

/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(462);

/***/ }),
/* 50 */,
/* 51 */,
/* 52 */,
/* 53 */,
/* 54 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });
var User = (function () {
    function User() {
    }
    return User;
}());



/***/ }),
/* 55 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_service_userService_user_service__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__theme_services__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngrx_store__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ng_bootstrap_ng_bootstrap__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__auth_state_auth_actions__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__auth_model_user_model__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_animations__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_style_loader_forgot_password_scss__ = __webpack_require__(538);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_style_loader_forgot_password_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_style_loader_forgot_password_scss__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ngx_toastr__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_lodash__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_lodash__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgotPassword; });











// TOAST


var types = ['success', 'error', 'info', 'warning'];
////////
var ForgotPassword = (function () {
    function ForgotPassword(fb, translate, user_service, baThemeSpinner, store, toastrService, modalService) {
        var _this = this;
        this.translate = translate;
        this.user_service = user_service;
        this.baThemeSpinner = baThemeSpinner;
        this.store = store;
        this.toastrService = toastrService;
        this.modalService = modalService;
        this.title = '';
        this.type = types[0];
        this.message = '';
        this.version = __WEBPACK_IMPORTED_MODULE_0__angular_core__["VERSION"];
        this.lastInserted = [];
        this.submitted = false;
        this.user = new __WEBPACK_IMPORTED_MODULE_8__auth_model_user_model__["a" /* User */]();
        // TOAST
        this.options = this.toastrService.toastrConfig;
        ////////
        this.translate.use('en');
        this.form = fb.group({
            email: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([
                    __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required,
                    this.isEmail
                ])],
        });
        this.email = this.form.controls['email'];
        this.store
            .select('app')
            .subscribe(function (res) {
            //console.log(res)
            //this.domains = res.domains
            _this.settings = res.settings;
            console.log(res);
            if (_this.settings.nodeEnv === 'development') {
                // this.credentials.email = 'admin@example.com'
                // this.credentials.password = 'password'
            }
        });
    }
    ForgotPassword.prototype.isEmail = function (control) {
        console.log(control.value);
        if (control.value) {
            if (!control.value.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
                return {
                    noEmail: true
                };
            }
        }
    };
    ForgotPassword.prototype.onSubmit = function (values) {
        event.preventDefault();
        this.submitted = true;
        if (this.form.valid) {
            var data = {
                emailOrPhone: this.user.email,
            };
            // var fd=new FormData();
            //     fd.append('emailorPhone',this.user.email);
            //this.baThemeSpinner.show();
            this.store.dispatch({
                type: __WEBPACK_IMPORTED_MODULE_7__auth_state_auth_actions__["a" /* ActionTypes */].AUTH_FORGOT_PASSWORD,
                payload: data
            });
        }
    };
    // TOAST
    ForgotPassword.prototype.openToast = function () {
        var m = 'amar';
        var t = 'amar';
        var opt = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_12_lodash__["cloneDeep"])(this.options);
        var inserted = this.toastrService[this.type](m, t, opt);
        if (inserted) {
            this.lastInserted.push(inserted.toastId);
        }
        return inserted;
    };
    //////////
    ForgotPassword.prototype.staticModalShow = function () {
        console.log();
    };
    return ForgotPassword;
}());
ForgotPassword = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'forgot-password',
        template: __webpack_require__(515),
        animations: [
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["trigger"])('flyInOut', [
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["state"])('in', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["style"])({
                    transform: 'translateX(0)'
                })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["transition"])('void => *', [
                    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["animate"])(700, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["keyframes"])([
                        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["style"])({
                            opacity: 0,
                            transform: 'translateX(-100%)',
                            offset: 0
                        }),
                        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["style"])({
                            opacity: 1,
                            transform: 'translateX(15px)',
                            offset: 0.3
                        }),
                        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["style"])({
                            opacity: 1,
                            transform: 'translateX(0)',
                            offset: 1.0
                        })
                    ]))
                ]),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["transition"])('* => void', [
                    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["animate"])(700, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["keyframes"])([
                        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["style"])({
                            opacity: 1,
                            transform: 'translateX(0)',
                            offset: 0
                        }),
                        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["style"])({
                            opacity: 1,
                            transform: 'translateX(-15px)',
                            offset: 0.7
                        }),
                        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["style"])({
                            opacity: 0,
                            transform: 'translateX(100%)',
                            offset: 1.0
                        })
                    ]))
                ])
            ]),
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["trigger"])('shrinkOut', [
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["state"])('in', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["style"])({
                    height: '*'
                })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["transition"])('* => void', [
                    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["style"])({
                        height: '*'
                    }),
                    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["animate"])(2500, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["style"])({
                        height: 0
                    }))
                ])
            ])
        ]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"],
        __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["a" /* TranslateService */],
        __WEBPACK_IMPORTED_MODULE_2__auth_service_userService_user_service__["a" /* user_service */],
        __WEBPACK_IMPORTED_MODULE_3__theme_services__["a" /* BaThemeSpinner */],
        __WEBPACK_IMPORTED_MODULE_5__ngrx_store__["Store"],
        __WEBPACK_IMPORTED_MODULE_11_ngx_toastr__["b" /* ToastrService */],
        __WEBPACK_IMPORTED_MODULE_6__ng_bootstrap_ng_bootstrap__["d" /* NgbModal */]])
], ForgotPassword);



/***/ }),
/* 56 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__theme_configProvider__ = __webpack_require__(57);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaThemeConfig; });


var BaThemeConfig = (function () {
    function BaThemeConfig(_baConfig) {
        this._baConfig = _baConfig;
    }
    BaThemeConfig.prototype.config = function () {
        // this._baConfig.changeTheme({ name: 'my-theme' });
        //
        // let colorScheme = {
        //   primary: '#209e91',
        //   info: '#2dacd1',
        //   success: '#90b900',
        //   warning: '#dfb81c',
        //   danger: '#e85656',
        // };
        //
        // this._baConfig.changeColors({
        //   default: '#4e4e55',
        //   defaultText: '#aaaaaa',
        //   border: '#dddddd',
        //   borderDark: '#aaaaaa',
        //
        //   primary: colorScheme.primary,
        //   info: colorScheme.info,
        //   success: colorScheme.success,
        //   warning: colorScheme.warning,
        //   danger: colorScheme.danger,
        //
        //   primaryLight: colorHelper.tint(colorScheme.primary, 30),
        //   infoLight: colorHelper.tint(colorScheme.info, 30),
        //   successLight: colorHelper.tint(colorScheme.success, 30),
        //   warningLight: colorHelper.tint(colorScheme.warning, 30),
        //   dangerLight: colorHelper.tint(colorScheme.danger, 30),
        //
        //   primaryDark: colorHelper.shade(colorScheme.primary, 15),
        //   infoDark: colorHelper.shade(colorScheme.info, 15),
        //   successDark: colorHelper.shade(colorScheme.success, 15),
        //   warningDark: colorHelper.shade(colorScheme.warning, 15),
        //   dangerDark: colorHelper.shade(colorScheme.danger, 15),
        //
        //   dashboard: {
        //     blueStone: '#005562',
        //     surfieGreen: '#0e8174',
        //     silverTree: '#6eba8c',
        //     gossip: '#b9f2a1',
        //     white: '#10c4b5',
        //   },
        //
        //   custom: {
        //     dashboardPieChart: colorHelper.hexToRgbA('#aaaaaa', 0.8),
        //     dashboardLineChart: '#6eba8c',
        //   },
        // });
    };
    return BaThemeConfig;
}());
BaThemeConfig = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__theme_configProvider__["a" /* BaThemeConfigProvider */]])
], BaThemeConfig);



/***/ }),
/* 57 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__theme_constants__ = __webpack_require__(58);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaThemeConfigProvider; });



var BaThemeConfigProvider = (function () {
    function BaThemeConfigProvider() {
        this.basic = {
            default: '#ffffff',
            defaultText: '#ffffff',
            border: '#dddddd',
            borderDark: '#aaaaaa',
        };
        // main functional color scheme
        this.colorScheme = {
            primary: '#00abff',
            info: '#40daf1',
            success: '#8bd22f',
            warning: '#e7ba08',
            danger: '#f95372',
        };
        // dashboard colors for charts
        this.dashboardColors = {
            blueStone: '#40daf1',
            surfieGreen: '#00abff',
            silverTree: '#1b70ef',
            gossip: '#3c4eb9',
            white: '#ffffff',
        };
        this.conf = {
            theme: {
                name: 'ng2',
            },
            colors: {
                default: this.basic.default,
                defaultText: this.basic.defaultText,
                border: this.basic.border,
                borderDark: this.basic.borderDark,
                primary: this.colorScheme.primary,
                info: this.colorScheme.info,
                success: this.colorScheme.success,
                warning: this.colorScheme.warning,
                danger: this.colorScheme.danger,
                primaryLight: __WEBPACK_IMPORTED_MODULE_2__theme_constants__["a" /* colorHelper */].tint(this.colorScheme.primary, 30),
                infoLight: __WEBPACK_IMPORTED_MODULE_2__theme_constants__["a" /* colorHelper */].tint(this.colorScheme.info, 30),
                successLight: __WEBPACK_IMPORTED_MODULE_2__theme_constants__["a" /* colorHelper */].tint(this.colorScheme.success, 30),
                warningLight: __WEBPACK_IMPORTED_MODULE_2__theme_constants__["a" /* colorHelper */].tint(this.colorScheme.warning, 30),
                dangerLight: __WEBPACK_IMPORTED_MODULE_2__theme_constants__["a" /* colorHelper */].tint(this.colorScheme.danger, 30),
                primaryDark: __WEBPACK_IMPORTED_MODULE_2__theme_constants__["a" /* colorHelper */].shade(this.colorScheme.primary, 15),
                infoDark: __WEBPACK_IMPORTED_MODULE_2__theme_constants__["a" /* colorHelper */].shade(this.colorScheme.info, 15),
                successDark: __WEBPACK_IMPORTED_MODULE_2__theme_constants__["a" /* colorHelper */].shade(this.colorScheme.success, 15),
                warningDark: __WEBPACK_IMPORTED_MODULE_2__theme_constants__["a" /* colorHelper */].shade(this.colorScheme.warning, 15),
                dangerDark: __WEBPACK_IMPORTED_MODULE_2__theme_constants__["a" /* colorHelper */].shade(this.colorScheme.danger, 15),
                dashboard: {
                    blueStone: this.dashboardColors.blueStone,
                    surfieGreen: this.dashboardColors.surfieGreen,
                    silverTree: this.dashboardColors.silverTree,
                    gossip: this.dashboardColors.gossip,
                    white: this.dashboardColors.white,
                },
                custom: {
                    dashboardPieChart: __WEBPACK_IMPORTED_MODULE_2__theme_constants__["a" /* colorHelper */].hexToRgbA(this.basic.defaultText, 0.8),
                    dashboardLineChart: this.basic.defaultText,
                }
            }
        };
    }
    BaThemeConfigProvider.prototype.get = function () {
        return this.conf;
    };
    BaThemeConfigProvider.prototype.changeTheme = function (theme) {
        __WEBPACK_IMPORTED_MODULE_1_lodash__["merge"](this.get().theme, theme);
    };
    BaThemeConfigProvider.prototype.changeColors = function (colors) {
        __WEBPACK_IMPORTED_MODULE_1_lodash__["merge"](this.get().colors, colors);
    };
    return BaThemeConfigProvider;
}());
BaThemeConfigProvider = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], BaThemeConfigProvider);



/***/ }),
/* 58 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export IMAGES_ROOT */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return layoutSizes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return layoutPaths; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return colorHelper; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return isMobile; });
var IMAGES_ROOT = 'assets/img/';
var layoutSizes = {
    resWidthCollapseSidebar: 1200,
    resWidthHideSidebar: 500
};
var layoutPaths = {
    images: {
        root: IMAGES_ROOT,
        profile: IMAGES_ROOT + 'app/profile/',
        amMap: 'assets/img/theme/vendor/ammap/',
        amChart: 'assets/img/theme/vendor/amcharts/dist/amcharts/images/'
    }
};
var colorHelper = (function () {
    function colorHelper() {
    }
    return colorHelper;
}());

colorHelper.shade = function (color, weight) {
    return colorHelper.mix('#000000', color, weight);
};
colorHelper.tint = function (color, weight) {
    return colorHelper.mix('#ffffff', color, weight);
};
colorHelper.hexToRgbA = function (hex, alpha) {
    var c;
    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
        c = hex.substring(1).split('');
        if (c.length == 3) {
            c = [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c = '0x' + c.join('');
        return 'rgba(' + [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',') + ',' + alpha + ')';
    }
    throw new Error('Bad Hex');
};
colorHelper.mix = function (color1, color2, weight) {
    var d2h = function (d) { return d.toString(16); };
    var h2d = function (h) { return parseInt(h, 16); };
    var result = "#";
    for (var i = 1; i < 7; i += 2) {
        var color1Part = h2d(color1.substr(i, 2));
        var color2Part = h2d(color2.substr(i, 2));
        var resultPart = d2h(Math.floor(color2Part + (color1Part - color2Part) * (weight / 100.0)));
        result += ('0' + resultPart).slice(-2);
    }
    return result;
};
var isMobile = function () { return (/android|webos|iphone|ipad|ipod|blackberry|windows phone/).test(navigator.userAgent.toLowerCase()); };


/***/ }),
/* 59 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__email_validator__ = __webpack_require__(424);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__email_validator__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__equalPasswords_validator__ = __webpack_require__(425);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_1__equalPasswords_validator__["a"]; });




/***/ }),
/* 60 */,
/* 61 */,
/* 62 */,
/* 63 */,
/* 64 */,
/* 65 */,
/* 66 */,
/* 67 */,
/* 68 */,
/* 69 */,
/* 70 */,
/* 71 */,
/* 72 */,
/* 73 */,
/* 74 */,
/* 75 */,
/* 76 */,
/* 77 */,
/* 78 */,
/* 79 */,
/* 80 */,
/* 81 */,
/* 82 */,
/* 83 */,
/* 84 */,
/* 85 */,
/* 86 */,
/* 87 */,
/* 88 */,
/* 89 */,
/* 90 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return decorateModuleRef; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ENV_PROVIDERS; });
// Angular 2
// rc2 workaround


// Environment Providers
var PROVIDERS = [];
// Angular debug tools in the dev console
// https://github.com/angular/angular/blob/86405345b781a9dc2438c0fbe3e9409245647019/TOOLS_JS.md
var _decorateModuleRef = function identity(value) { return value; };
if (false) {
    // Production
    // disableDebugTools();
    enableProdMode();
    PROVIDERS = PROVIDERS.slice();
}
else {
    _decorateModuleRef = function (modRef) {
        var appRef = modRef.injector.get(__WEBPACK_IMPORTED_MODULE_1__angular_core__["ApplicationRef"]);
        var cmpRef = appRef.components[0];
        var _ng = window.ng;
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["enableDebugTools"])(cmpRef);
        window.ng.probe = _ng.probe;
        window.ng.coreTokens = _ng.coreTokens;
        return modRef;
    };
    // Development
    PROVIDERS = PROVIDERS.slice();
}
var decorateModuleRef = _decorateModuleRef;
var ENV_PROVIDERS = PROVIDERS.slice();


/***/ }),
/* 91 */,
/* 92 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActionTypes; });
/* unused harmony export AppGetAllBookingDetail */
/* unused harmony export AppBookingDetail */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return AppBookingDetailSuccess; });
/* unused harmony export AppGetUserById */
/* unused harmony export AppGetUserByIdSuccess */
/* unused harmony export AppEditUserById */
/* unused harmony export AppEditUserByIdSuccess */
/* unused harmony export AppBlockUserById */
/* unused harmony export AppAddItemSuccess */
/* unused harmony export AppAddItem */
/* unused harmony export AppDeleteItemRecord */
/* unused harmony export AppDeleteItemConfirm */
/* unused harmony export AppIdRecord */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return AppIdRecordSuccess; });
var ActionTypes = {
    APP_GETALL_BOOKING: 'APP_GETALL_BOOKING',
    APP_BOOKING_DETAIL: 'APP_BOOKING_DETAIL',
    APP_BOOKING_DETAIL_SUCCESS: 'APP_BOOKING_DETAIL_SUCCESS',
    APP_BOOKING_DETAIL_AFTER_SUCCESS: 'APP_BOOKING_DETAIL_AFTER_SUCCESS',
    APP_GET_USER_BY_ID: 'APP_GET_USER_BY_ID',
    APP_GET_USER_BY_ID_SUCCESS: 'APP_USER_BY_ID_SUCCESS',
    APP_CANCEL_BOOKING_BY_ID: 'APP_CANCEL_BOOKING_BY_ID',
    APP_CANCEL_BOOKING_BY_ID_SUCCESS: 'APP_CANCEL_BOOKING_BY_ID_SUCCESS',
    APP_EDIT_USER_BY_ID: 'APP_EDIT_USER_BY_ID',
    APP_EDIT_USER_BY_ID_SUCCESS: 'APP_EDIT_USER_BY_ID_SUCCESS',
    APP_BLOCK_USER_BY_ID: 'APP_BLOCK_USER_BY_ID',
    APP_ADD_ITEM: 'APP_ADD_ITEM',
    APP_ADD_ITEM_SUCCESS: 'APP_ADD_ITEM_SUCCESS',
    DELETE_ITEM_RECORD: 'DELETE_ITEM_RECORD',
    DELETE_ITEM_RECORD_CONFIRM: 'DELETE_ITEM_RECORD_CONFIRM',
    ADD_ID_RECORD: 'ADD_ID_RECORD',
    ADD_ID_RECORD_SUCCESS: 'ADD_ID_RECORD_SUCCESS',
};
var AppGetAllBookingDetail = (function () {
    function AppGetAllBookingDetail(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_GETALL_BOOKING;
        console.log("kjhj");
    }
    return AppGetAllBookingDetail;
}());

var AppBookingDetail = (function () {
    function AppBookingDetail(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_BOOKING_DETAIL;
    }
    return AppBookingDetail;
}());

var AppBookingDetailSuccess = (function () {
    function AppBookingDetailSuccess(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_BOOKING_DETAIL_SUCCESS;
    }
    return AppBookingDetailSuccess;
}());

var AppGetUserById = (function () {
    function AppGetUserById(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_GET_USER_BY_ID;
    }
    return AppGetUserById;
}());

var AppGetUserByIdSuccess = (function () {
    function AppGetUserByIdSuccess(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_GET_USER_BY_ID_SUCCESS;
    }
    return AppGetUserByIdSuccess;
}());

var AppEditUserById = (function () {
    function AppEditUserById(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_EDIT_USER_BY_ID;
        console.log("ffrd");
    }
    return AppEditUserById;
}());

var AppEditUserByIdSuccess = (function () {
    function AppEditUserByIdSuccess(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_EDIT_USER_BY_ID_SUCCESS;
    }
    return AppEditUserByIdSuccess;
}());

var AppBlockUserById = (function () {
    function AppBlockUserById(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_BLOCK_USER_BY_ID;
    }
    return AppBlockUserById;
}());

var AppAddItemSuccess = (function () {
    function AppAddItemSuccess(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_ADD_ITEM_SUCCESS;
    }
    return AppAddItemSuccess;
}());

var AppAddItem = (function () {
    function AppAddItem(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_ADD_ITEM;
        console.log("bhjbh");
    }
    return AppAddItem;
}());

var AppDeleteItemRecord = (function () {
    function AppDeleteItemRecord() {
        this.type = ActionTypes.DELETE_ITEM_RECORD;
    }
    return AppDeleteItemRecord;
}());

var AppDeleteItemConfirm = (function () {
    function AppDeleteItemConfirm() {
        this.type = ActionTypes.DELETE_ITEM_RECORD_CONFIRM;
    }
    return AppDeleteItemConfirm;
}());

var AppIdRecord = (function () {
    function AppIdRecord() {
        this.type = ActionTypes.ADD_ID_RECORD;
    }
    return AppIdRecord;
}());

var AppIdRecordSuccess = (function () {
    function AppIdRecordSuccess() {
        this.type = ActionTypes.ADD_ID_RECORD_SUCCESS;
    }
    return AppIdRecordSuccess;
}());



/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(456);

/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(48);

/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(502);

/***/ }),
/* 96 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActionTypes; });
/* unused harmony export AppGetAllCompetitions */
/* unused harmony export AppCompetitionDetail */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return AppCompetitionDetailSuccess; });
/* unused harmony export AppCreateCompetitionSuccess */
/* unused harmony export AppCreateCompetition */
var ActionTypes = {
    APP_GETALL_COMPETITIONS: 'APP_GETALL_COMPETITIONS',
    APP_COMPETITION_DETAIL: 'APP_COMPETITION_DETAIL',
    APP_COMPETITION_DETAIL_SUCCESS: 'APP_COMPETITION_DETAIL_SUCCESS',
    APP_CREATE_COMPETITION: 'APP_CREATE_COMPETITION',
    APP_CREATE_COMPETITION_SUCCESS: 'APP_CREATE_COMPETITION_SUCCESS'
};
// type cre = {
//               competitionName: string;
//               competitionMessage: string,
//               prizeValue:string;
//               termsAndConditions:string;
//               startDateTime:Date;
//               endDateTime:Date;
//           }
var AppGetAllCompetitions = (function () {
    function AppGetAllCompetitions(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_GETALL_COMPETITIONS;
        console.log("kjhj");
    }
    return AppGetAllCompetitions;
}());

var AppCompetitionDetail = (function () {
    function AppCompetitionDetail(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_COMPETITION_DETAIL;
    }
    return AppCompetitionDetail;
}());

var AppCompetitionDetailSuccess = (function () {
    function AppCompetitionDetailSuccess(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_COMPETITION_DETAIL_SUCCESS;
    }
    return AppCompetitionDetailSuccess;
}());

var AppCreateCompetitionSuccess = (function () {
    function AppCreateCompetitionSuccess(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_CREATE_COMPETITION_SUCCESS;
    }
    return AppCreateCompetitionSuccess;
}());

var AppCreateCompetition = (function () {
    function AppCreateCompetition(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_CREATE_COMPETITION;
        console.log("bhjbh");
    }
    return AppCreateCompetition;
}());



/***/ }),
/* 97 */,
/* 98 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__authPublic_service__ = __webpack_require__(99);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });



var AuthGuard = (function () {
    function AuthGuard(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        var url = state.url;
        return this.checkLogin(url);
    };
    AuthGuard.prototype.checkLogin = function (url) {
        //console.log(this.authService.login())
        if (this.authService.login()) {
            return true;
        }
        // Store the attempted URL for redirecting
        //this.authService.redirectUrl = url;
        // Navigate to the login page with extras
        this.router.navigate(['/pages']);
        return false;
    };
    return AuthGuard;
}());
AuthGuard = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__authPublic_service__["a" /* AuthPublicPagesService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"]])
], AuthGuard);



/***/ }),
/* 99 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_observable_of__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_observable_of___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_observable_of__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_do__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_do___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_do__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_delay__ = __webpack_require__(301);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_delay___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_delay__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthPublicPagesService; });




var AuthPublicPagesService = (function () {
    function AuthPublicPagesService() {
        this.isLoggedIn = true;
    }
    AuthPublicPagesService.prototype.login = function () {
        var token = localStorage.getItem('tokenSession');
        if (token) {
            //console.log("token present")
            this.isLoggedIn = false;
            //console.log(token)
            return this.isLoggedIn;
        }
        else {
            this.isLoggedIn = true;
            return this.isLoggedIn;
        }
    };
    AuthPublicPagesService.prototype.logout = function () {
        console.log("logout is performed");
        window.localStorage.removeItem('tokenSession');
    };
    return AuthPublicPagesService;
}());
AuthPublicPagesService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], AuthPublicPagesService);



/***/ }),
/* 100 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActionTypes; });
/* unused harmony export AppGetDashBoardCount */
/* unused harmony export AppGetUserReportMonthly */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return AppGetUserReportMonthlySuccess; });
/* unused harmony export AppGetDashBoardCountSuccess */
var ActionTypes = {
    GET_DASHBOARD_COUNT: 'GET_DASHBOARD_COUNT',
    GET_DASHBOARD_COUNT_SUCCESS: 'GET_DASHBOARD_COUNT_SUCCESS',
    GET_USER_REPORT_MONTHLY: 'GET_USER_REPORT_MONTHLY',
    GET_USER_REPORT_MONTHLY_SUCCESS: 'GET_USER_REPORT_MONTHLY_SUCCESS'
};
// /** GET DASHBOARD COUNT **/
var AppGetDashBoardCount = (function () {
    function AppGetDashBoardCount() {
        this.type = ActionTypes.GET_DASHBOARD_COUNT;
    }
    return AppGetDashBoardCount;
}());

var AppGetUserReportMonthly = (function () {
    function AppGetUserReportMonthly(payload) {
        this.type = ActionTypes.GET_USER_REPORT_MONTHLY;
    }
    return AppGetUserReportMonthly;
}());

var AppGetUserReportMonthlySuccess = (function () {
    function AppGetUserReportMonthlySuccess(payload) {
        this.payload = payload;
        this.type = ActionTypes.GET_USER_REPORT_MONTHLY_SUCCESS;
    }
    return AppGetUserReportMonthlySuccess;
}());

var AppGetDashBoardCountSuccess = (function () {
    function AppGetDashBoardCountSuccess(payload) {
        this.payload = payload;
        this.type = ActionTypes.GET_DASHBOARD_COUNT_SUCCESS;
    }
    return AppGetDashBoardCountSuccess;
}());



/***/ }),
/* 101 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__theme__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_menu__ = __webpack_require__(357);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Pages; });



var Pages = (function () {
    function Pages(_menuService) {
        this._menuService = _menuService;
    }
    Pages.prototype.ngOnInit = function () {
        this._menuService.updateMenuByRoutes(__WEBPACK_IMPORTED_MODULE_2__pages_menu__["a" /* PAGES_MENU */]);
    };
    return Pages;
}());
Pages = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'pages',
        template: "\n    <ba-sidebar></ba-sidebar>\n    <ba-page-top></ba-page-top>\n    <div class=\"al-main\">\n      <div class=\"al-content\">\n        <ba-content-top></ba-content-top>\n        <router-outlet></router-outlet>\n      </div>\n    </div>\n  \n    "
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__theme__["a" /* BaMenuService */]])
], Pages);



/***/ }),
/* 102 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_service_userService_user_service__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__theme_services__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngrx_store__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ng_bootstrap_ng_bootstrap__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__auth_state_auth_actions__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__auth_model_user_model__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_animations__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_style_loader_login_scss__ = __webpack_require__(539);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_style_loader_login_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_style_loader_login_scss__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ngx_toastr__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_lodash__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_lodash__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Login; });











// TOAST


var types = ['success', 'error', 'info', 'warning'];
////////
var Login = (function () {
    function Login(fb, translate, user_service, baThemeSpinner, store, toastrService, // TOAST
        modalService) {
        var _this = this;
        this.translate = translate;
        this.user_service = user_service;
        this.baThemeSpinner = baThemeSpinner;
        this.store = store;
        this.toastrService = toastrService;
        this.modalService = modalService;
        this.title = '';
        this.type = types[0];
        this.message = '';
        this.version = __WEBPACK_IMPORTED_MODULE_0__angular_core__["VERSION"];
        this.lastInserted = [];
        this.submitted = false;
        this.remember = false;
        this.user = new __WEBPACK_IMPORTED_MODULE_8__auth_model_user_model__["a" /* User */]();
        // TOAST
        this.options = this.toastrService.toastrConfig;
        ////////
        this.translate.use('en');
        this.form = fb.group({
            email: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([
                    this.isEmail
                ])],
            password: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([
                    __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required,
                ])],
            remember: ['']
        });
        this.email = this.form.controls['email'];
        this.password = this.form.controls['password'];
        this.session = sessionStorage.getItem('email1');
        this.store
            .select('auth')
            .subscribe(function (res) {
            console.log(res);
            if (res.er) {
                console.log(res.er);
                _this.erm = res.er.message;
                console.log(_this.erm);
            }
            //console.log(res.message)
            //this.domains = res.domains
        });
    }
    Login.prototype.isEmail = function (control) {
        console.log(control.value);
        if (control.value) {
            if (!control.value.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
                return {
                    noEmail: true
                };
            }
        }
    };
    Login.prototype.onSubmit = function (values) {
        event.preventDefault();
        this.submitted = true;
        if (this.form.valid) {
            if (this.remember) {
                sessionStorage.setItem('email1', this.user.email);
            }
            else {
                sessionStorage.removeItem('email1');
            }
            var data = {
                emailOrPhone: this.user.email || this.session,
                password: this.user.password,
            };
            this.baThemeSpinner.show();
            this.store.dispatch({
                type: __WEBPACK_IMPORTED_MODULE_7__auth_state_auth_actions__["a" /* ActionTypes */].AUTH_LOGIN,
                payload: data
            });
        }
    };
    // TOAST
    Login.prototype.openToast = function () {
        var m = 'amar';
        var t = 'amar';
        var opt = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_12_lodash__["cloneDeep"])(this.options);
        var inserted = this.toastrService[this.type](m, t, opt);
        if (inserted) {
            this.lastInserted.push(inserted.toastId);
        }
        return inserted;
    };
    return Login;
}());
Login = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'login',
        template: __webpack_require__(516),
        animations: [
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["trigger"])('flyInOut', [
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["state"])('in', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["style"])({
                    transform: 'translateX(0)'
                })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["transition"])('void => *', [
                    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["animate"])(700, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["keyframes"])([
                        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["style"])({
                            opacity: 0,
                            transform: 'translateX(-100%)',
                            offset: 0
                        }),
                        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["style"])({
                            opacity: 1,
                            transform: 'translateX(15px)',
                            offset: 0.3
                        }),
                        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["style"])({
                            opacity: 1,
                            transform: 'translateX(0)',
                            offset: 1.0
                        })
                    ]))
                ]),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["transition"])('* => void', [
                    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["animate"])(700, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["keyframes"])([
                        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["style"])({
                            opacity: 1,
                            transform: 'translateX(0)',
                            offset: 0
                        }),
                        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["style"])({
                            opacity: 1,
                            transform: 'translateX(-15px)',
                            offset: 0.7
                        }),
                        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["style"])({
                            opacity: 0,
                            transform: 'translateX(100%)',
                            offset: 1.0
                        })
                    ]))
                ])
            ]),
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["trigger"])('shrinkOut', [
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["state"])('in', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["style"])({
                    height: '*'
                })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["transition"])('* => void', [
                    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["style"])({
                        height: '*'
                    }),
                    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["animate"])(2500, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__angular_animations__["style"])({
                        height: 0
                    }))
                ])
            ])
        ]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"],
        __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["a" /* TranslateService */],
        __WEBPACK_IMPORTED_MODULE_2__auth_service_userService_user_service__["a" /* user_service */],
        __WEBPACK_IMPORTED_MODULE_3__theme_services__["a" /* BaThemeSpinner */],
        __WEBPACK_IMPORTED_MODULE_5__ngrx_store__["Store"],
        __WEBPACK_IMPORTED_MODULE_11_ngx_toastr__["b" /* ToastrService */],
        __WEBPACK_IMPORTED_MODULE_6__ng_bootstrap_ng_bootstrap__["d" /* NgbModal */]])
], Login);



/***/ }),
/* 103 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__theme_validators__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_style_loader_register_scss__ = __webpack_require__(540);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_style_loader_register_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_style_loader_register_scss__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Register; });




var Register = (function () {
    function Register(fb) {
        this.submitted = false;
        this.form = fb.group({
            'name': ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].minLength(4)])],
            'email': ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_2__theme_validators__["b" /* EmailValidator */].email])],
            'passwords': fb.group({
                'password': ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].minLength(4)])],
                'repeatPassword': ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].minLength(4)])]
            }, { validator: __WEBPACK_IMPORTED_MODULE_2__theme_validators__["a" /* EqualPasswordsValidator */].validate('password', 'repeatPassword') })
        });
        this.name = this.form.controls['name'];
        this.email = this.form.controls['email'];
        this.passwords = this.form.controls['passwords'];
        this.password = this.passwords.controls['password'];
        this.repeatPassword = this.passwords.controls['repeatPassword'];
    }
    Register.prototype.onSubmit = function (values) {
        this.submitted = true;
        if (this.form.valid) {
            // your code goes here
            // console.log(values);
        }
    };
    return Register;
}());
Register = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'register',
        template: __webpack_require__(517),
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]])
], Register);



/***/ }),
/* 104 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_state_auth_actions__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__theme_validators__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__auth_model_user_model__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_style_loader_reset_scss__ = __webpack_require__(541);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_style_loader_reset_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_style_loader_reset_scss__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResetComponent; });




// import * as lang from '../../../multilingual/state/lang.actions';



// import { Language } from '../../../multilingual/model/lang.model.ts'

var ResetComponent = (function () {
    function ResetComponent(fb, route, store) {
        var _this = this;
        this.route = route;
        this.store = store;
        this.tokenVerified = false;
        this.errorFlag = false;
        this.successFlag = false;
        this.submitted = false;
        this.user = new __WEBPACK_IMPORTED_MODULE_6__auth_model_user_model__["a" /* User */]();
        this.form = fb.group({
            'passwords': fb.group({
                'password': ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].minLength(8)])],
                'confirmpassword': ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].minLength(8)])]
            }, { validator: __WEBPACK_IMPORTED_MODULE_5__theme_validators__["a" /* EqualPasswordsValidator */].validate('password', 'confirmpassword') })
        });
        this.sub = this.route.params.subscribe(function (params) {
            _this.id = params['id']; // (+) converts string 'id' to a number
            console.log("GOT ID", _this.id);
        });
        this.passwords = this.form.controls['passwords'];
        this.password = this.passwords.controls['password'];
        this.confirmpassword = this.passwords.controls['confirmpassword'];
        // START :: STORE 
        this.store
            .select('auth')
            .subscribe(function (res) {
            var data = res;
            console.log(res);
            if (data.hasOwnProperty('verify_token_success_payload')) {
                if (data.verify_token_success_payload.status == 200) {
                    _this.tokenVerified = true;
                }
                else {
                    _this.responseMessage = data.verify_token_success_payload.message;
                }
                delete data["verify_token_success_payload"];
            }
            else if (data.hasOwnProperty('reset_pass_success_payload')) {
                if (data.reset_pass_success_payload.status == 200) {
                    _this.errorFlag = false;
                    _this.successFlag = true;
                    _this.responseMessage = data.reset_pass_success_payload.message;
                }
                else {
                    _this.errorFlag = true;
                    setTimeout(function () {
                        _this.errorFlag = false;
                    }, 3000);
                    _this.successFlag = false;
                    _this.responseMessage = data.reset_pass_success_payload.message;
                }
                delete data["reset_pass_success_payload"];
            }
        });
        // END :: STORE
    }
    ResetComponent.prototype.ngOnInit = function () {
        // this.token = this.route.snapshot.queryParams["resetToken"];
        //   this.authenticateToken(this.token);
        //   sessionStorage.setItem("resetToken", this.token);
    };
    ResetComponent.prototype.authenticateToken = function (token) {
        var data = {
            'resetToken': token,
        };
        this.store.dispatch({
            type: __WEBPACK_IMPORTED_MODULE_3__auth_state_auth_actions__["a" /* ActionTypes */].AUTH_FORGOT_PASS_TOKEN,
            payload: data
        });
    };
    ResetComponent.prototype.onSubmit = function (values) {
        event.preventDefault();
        this.submitted = true;
        var token = sessionStorage.getItem('resetToken');
        if (this.form.valid) {
            var data = {
                "password": this.user.password,
                "resetToken": this.id,
            };
            // this.baThemeSpinner.show();
            this.store.dispatch({
                type: __WEBPACK_IMPORTED_MODULE_3__auth_state_auth_actions__["a" /* ActionTypes */].AUTH_RESET_PASSWORD,
                payload: data
            });
        }
    };
    return ResetComponent;
}());
ResetComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'reset-password',
        template: __webpack_require__(518)
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"], __WEBPACK_IMPORTED_MODULE_4__angular_router__["ActivatedRoute"], __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["Store"]])
], ResetComponent);



/***/ }),
/* 105 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environment_environment__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return booking_service; });





//import { bookingDetail } from "../../pages/Booking/bookingDetail";
//import { ListResult } from "../../pages/Booking/list-result.interface";
var booking_service = (function () {
    // console.log(header_token)
    function booking_service(http) {
        this.http = http;
        //getting token for headers
        this.token = localStorage.getItem('token');
        this.header_token = 'Bearer ' + this.token;
    }
    booking_service.prototype.additem = function (payload) {
        var token = localStorage.getItem('token');
        var header_token = token;
        var url = __WEBPACK_IMPORTED_MODULE_3__environment_environment__["a" /* environment */].APP.API_URL + __WEBPACK_IMPORTED_MODULE_3__environment_environment__["a" /* environment */].APP.ADD_ITEM;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Authorization', header_token);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        return this.http.post(url, payload, options)
            .map(function (res) { return res.json(); })
            .catch(function (error) {
            console.log(error);
            try {
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json()));
            }
            catch (jsonError) {
                // If the error couldn't be parsed as JSON data
                // then it's possible the API is down or something
                // went wrong with the parsing of the successful
                // response. In any case, to keep things simple,
                // we'll just create a minimum representation of
                // a parsed error.
                var minimumViableError = {
                    success: false
                };
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(minimumViableError));
            }
        });
    };
    booking_service.prototype.getAllBooking = function (payload) {
        var token = localStorage.getItem('token');
        var header_token = 'Bearer ' + token;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]({ 'Content-Type': 'application/json' });
        headers.append('Authorization', header_token);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        var skip = (payload.currentPage - 1) * payload.limit;
        var url = __WEBPACK_IMPORTED_MODULE_3__environment_environment__["a" /* environment */].APP.API_URL + __WEBPACK_IMPORTED_MODULE_3__environment_environment__["a" /* environment */].APP.GET_ALL_BOOKING;
        //console.log(url);
        return this.http.get(url, options)
            .map(function (res) {
            console.log(res.json());
            return res.json();
        })
            .catch(function (error) {
            console.log(error);
            try {
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json()));
            }
            catch (jsonError) {
                var minimumViableError = {
                    success: false
                };
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(minimumViableError));
            }
        });
    };
    booking_service.prototype.getAllBookingPagination = function (url) {
        var token = localStorage.getItem('token');
        var header_token = 'Bearer ' + token;
        console.log(header_token);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]({ 'Content-Type': 'application/json', 'content-language': 'en' });
        headers.append('Authorization', header_token);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        //let url=environment.APP.API_URL+environment.APP.GET_ALL_BOOKING+"?limit="+limit+"&skip="+skip;
        return this.http.get(url, options)
            .map(function (res) {
            //console.log(res.json());
            return res.json();
        })
            .catch(function (error) {
            console.log(error);
            try {
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json()));
            }
            catch (jsonError) {
                // If the error couldn't be parsed as JSON data
                // then it's possible the API is down or something
                // went wrong with the parsing of the successful
                // response. In any case, to keep things simple,
                // we'll just create a minimum representation of
                // a parsed error.
                var minimumViableError = {
                    success: false
                };
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(minimumViableError));
            }
        });
        // }
        // else{
        //  return this.token;
        // }
    };
    booking_service.prototype.getUserById = function (payload) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]({ 'Content-Type': 'application/json', });
        headers.append('Authorization', this.header_token);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        var url = __WEBPACK_IMPORTED_MODULE_3__environment_environment__["a" /* environment */].APP.API_URL + __WEBPACK_IMPORTED_MODULE_3__environment_environment__["a" /* environment */].APP.GET_USER_BY_ID + "?userId=" + payload.userId;
        return this.http.get(url, options)
            .map(function (res) { return res.json(); })
            .catch(function (error) {
            console.log(error);
            try {
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json()));
            }
            catch (jsonError) {
                // If the error couldn't be parsed as JSON data
                // then it's possible the API is down or something
                // went wrong with the parsing of the successful
                // response. In any case, to keep things simple,
                // we'll just create a minimum representation of
                // a parsed error.
                var minimumViableError = {
                    success: false
                };
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(minimumViableError));
            }
        });
    };
    booking_service.prototype.blockUserById = function (payload) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]({ 'Content-Type': 'application/json', });
        headers.append('Authorization', this.header_token);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        var url = __WEBPACK_IMPORTED_MODULE_3__environment_environment__["a" /* environment */].APP.API_URL + __WEBPACK_IMPORTED_MODULE_3__environment_environment__["a" /* environment */].APP.BLOCK_USER_BY_ID;
        return this.http.put(url, payload, options)
            .map(function (res) { return res.json(); })
            .catch(function (error) {
            console.log(error);
            try {
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json()));
            }
            catch (jsonError) {
                // If the error couldn't be parsed as JSON data
                // then it's possible the API is down or something
                // went wrong with the parsing of the successful
                // response. In any case, to keep things simple,
                // we'll just create a minimum representation of
                // a parsed error.
                var minimumViableError = {
                    success: false
                };
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(minimumViableError));
            }
        });
    };
    booking_service.prototype.deleteCustomerRecord = function (payload) {
        console.log(payload);
        var token = localStorage.getItem('token');
        var header_token = token;
        // PARAMS
        var params = new FormData();
        params.append('categoryId', payload.categoryId);
        // HEADERS
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]({});
        headers.append('Authorization', header_token);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({
            'headers': headers,
            'body': { 'categoryId': payload.categoryId }
        });
        console.log(options);
        // URL
        // let url = 'http://54.200.235.37:3001/admin/deleteUser';
        var url = __WEBPACK_IMPORTED_MODULE_3__environment_environment__["a" /* environment */].APP.API_URL + __WEBPACK_IMPORTED_MODULE_3__environment_environment__["a" /* environment */].APP.DELETE_ITEM_BY_ID + payload.categoryId;
        // HTTP REQUEST
        return this.http.delete(url, options)
            .map(function (res) { return res.json(); })
            .catch(function (error) {
            try {
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json()));
            }
            catch (jsonError) {
                var minimumViableError = {
                    success: false
                };
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(minimumViableError));
            }
        });
    };
    ;
    booking_service.prototype.editUser = function (payload) {
        // console.log("ssssss",payload);
        // // let token=localStorage.getItem('token');
        // //       let header_token = token;
        //     let headers = new Headers({  });
        //    headers.append('Authorization',this.header_token);
        //    let options = new RequestOptions({ headers: headers }); 
        //    let url=environment.APP.API_URL+environment.APP.EDIT_USER_BY_ID;
        //    return this.http.put(url,options)
        //     .map((res: Response) => res.json())
        //     .catch((error: any) => {
        //       console.log(error)
        //       try {
        //         return (Observable.throw(error.json()));
        //       } catch (jsonError) {
        //         // If the error couldn't be parsed as JSON data
        //         // then it's possible the API is down or something
        //         // went wrong with the parsing of the successful
        //         // response. In any case, to keep things simple,
        //         // we'll just create a minimum representation of
        //         // a parsed error.
        //         var minimumViableError = {
        //           success: false
        //         };
        //         return (Observable.throw(minimumViableError));
        //       }
        //     })
        console.log(payload);
        var token = localStorage.getItem('token');
        var header_token = token;
        // PARAMS
        var params = new FormData();
        params.append('categoryId', payload.categoryId);
        // HEADERS
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]({});
        // alert("aaaasssss"+payload.name);
        var fd = new FormData();
        fd.append('name', payload.name);
        // alert(fd);
        console.log("aaaaaa", fd);
        headers.append('Authorization', header_token);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({
            'headers': headers
        });
        console.log(options);
        // URL
        // let url = 'http://54.200.235.37:3001/admin/deleteUser';
        var url = __WEBPACK_IMPORTED_MODULE_3__environment_environment__["a" /* environment */].APP.API_URL + __WEBPACK_IMPORTED_MODULE_3__environment_environment__["a" /* environment */].APP.EDIT_USER_BY_ID + payload.categoryId;
        // HTTP REQUEST
        return this.http.put(url, fd, options)
            .map(function (res) { return res.json(); })
            .catch(function (error) {
            try {
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json()));
            }
            catch (jsonError) {
                var minimumViableError = {
                    success: false
                };
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(minimumViableError));
            }
        });
    };
    return booking_service;
}());
booking_service = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]])
], booking_service);



/***/ }),
/* 106 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environment_environment__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return competition_service; });





//import { bookingDetail } from "../../pages/Booking/bookingDetail";
//import { ListResult } from "../../pages/Booking/list-result.interface";
var competition_service = (function () {
    // console.log(header_token)
    function competition_service(http) {
        this.http = http;
        //getting token for headers
        this.token = localStorage.getItem('token');
        this.header_token = 'Bearer ' + this.token;
    }
    competition_service.prototype.getAllCompetitions = function (payload) {
        var token = localStorage.getItem('token');
        var header_token = token;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]({ 'Content-Type': 'application/json' });
        headers.append('Authorization', header_token);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        // let skip = (payload.currentPage - 1) * payload.limit;
        var url = __WEBPACK_IMPORTED_MODULE_3__environment_environment__["a" /* environment */].APP.API_URL + __WEBPACK_IMPORTED_MODULE_3__environment_environment__["a" /* environment */].APP.GET_ALL_COMPETITIONS;
        //console.log(url);
        return this.http.get(url, options)
            .map(function (res) {
            console.log(res.json());
            return res.json();
        })
            .catch(function (error) {
            console.log(error);
            try {
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json()));
            }
            catch (jsonError) {
                var minimumViableError = {
                    success: false
                };
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(minimumViableError));
            }
        });
    };
    competition_service.prototype.createcompetition = function (payload) {
        var token = localStorage.getItem('token');
        var header_token = 'Bearer ' + token;
        var url = __WEBPACK_IMPORTED_MODULE_3__environment_environment__["a" /* environment */].APP.API_URL + __WEBPACK_IMPORTED_MODULE_3__environment_environment__["a" /* environment */].APP.CREATE_COMPETITION;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Authorization', header_token);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        return this.http.post(url, payload, options)
            .map(function (res) { return res.json(); })
            .catch(function (error) {
            console.log(error);
            try {
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json()));
            }
            catch (jsonError) {
                // If the error couldn't be parsed as JSON data
                // then it's possible the API is down or something
                // went wrong with the parsing of the successful
                // response. In any case, to keep things simple,
                // we'll just create a minimum representation of
                // a parsed error.
                var minimumViableError = {
                    success: false
                };
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(minimumViableError));
            }
        });
    };
    competition_service.prototype.getAllCompetitionPagination = function (url) {
        var token = localStorage.getItem('token');
        var header_token = 'Bearer ' + token;
        console.log(header_token);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]({ 'Content-Type': 'application/json', 'content-language': 'en' });
        headers.append('Authorization', header_token);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        //let url=environment.APP.API_URL+environment.APP.GET_ALL_BOOKING+"?limit="+limit+"&skip="+skip;
        return this.http.get(url, options)
            .map(function (res) {
            //console.log(res.json());
            return res.json();
        })
            .catch(function (error) {
            console.log(error);
            try {
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json()));
            }
            catch (jsonError) {
                // If the error couldn't be parsed as JSON data
                // then it's possible the API is down or something
                // went wrong with the parsing of the successful
                // response. In any case, to keep things simple,
                // we'll just create a minimum representation of
                // a parsed error.
                var minimumViableError = {
                    success: false
                };
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(minimumViableError));
            }
        });
        // }
        // else{
        //  return this.token;
        // }
    };
    return competition_service;
}());
competition_service = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]])
], competition_service);



/***/ }),
/* 107 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environment_environment_prod__ = __webpack_require__(347);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return dashboard_service; });





var dashboard_service = (function () {
    //getting token for headers
    function dashboard_service(http) {
        this.http = http;
    }
    dashboard_service.prototype.getDashBoardData = function () {
        var token = localStorage.getItem('token');
        var header_token = 'Bearer ' + token;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]({ 'Content-Type': 'application/json', });
        headers.append('Authorization', header_token);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        var url = __WEBPACK_IMPORTED_MODULE_3__environment_environment_prod__["a" /* environment */].APP.API_URL + __WEBPACK_IMPORTED_MODULE_3__environment_environment_prod__["a" /* environment */].APP.GET_DASHBOARD_COUNT;
        return this.http.get(url, options)
            .map(function (res) {
            //console.log(res.json());
            return res.json();
        })
            .catch(function (error) {
            console.log(error);
            try {
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json()));
            }
            catch (jsonError) {
                var minimumViableError = {
                    success: false
                };
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(minimumViableError));
            }
        });
    };
    dashboard_service.prototype.getUserReportMonthly = function (payload) {
        var token = localStorage.getItem('token');
        var header_token = 'Bearer ' + token;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]({ 'Content-Type': 'application/json', });
        headers.append('Authorization', header_token);
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        var url = __WEBPACK_IMPORTED_MODULE_3__environment_environment_prod__["a" /* environment */].APP.API_URL + __WEBPACK_IMPORTED_MODULE_3__environment_environment_prod__["a" /* environment */].APP.GET_USER_REPORT_MONTHLY + "?month=" + payload.month + "&year=" + payload.year;
        return this.http.get(url, options)
            .map(function (res) {
            //console.log(res.json());
            return res.json();
        })
            .catch(function (error) {
            console.log(error);
            try {
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error.json()));
            }
            catch (jsonError) {
                var minimumViableError = {
                    success: false
                };
                return (__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(minimumViableError));
            }
        });
    };
    return dashboard_service;
}());
dashboard_service = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]])
], dashboard_service);



/***/ }),
/* 108 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActionTypes; });
/* unused harmony export AppAddSettingAction */
/* unused harmony export AppRedirectDashboardAction */
/* unused harmony export AppRedirectLoginAction */
/* unused harmony export AppRedirectRouterAction */
/* unused harmony export AppGetAllBookingDetail */
/* unused harmony export AppGetAllUsers */
/* unused harmony export AppRegisterService */
/* unused harmony export AppRegisterCustomer */
/* unused harmony export AppRegisterServiceSuccess */
/* unused harmony export AppBookingDetail */
/* unused harmony export AppBookingDetailSuccess */
/* unused harmony export AppBookingDetailById */
/* unused harmony export AppBookingDetailByIdSuccess */
/* unused harmony export AppGetAllUser */
/* unused harmony export AppGetAllUserSuccess */
/* unused harmony export AppGetAllUserAfterSuccess */
/* unused harmony export AppGetAllUserById */
/* unused harmony export AppBlockUserById */
/* unused harmony export AppEditUserById */
/* unused harmony export AppEditUserByIdSuccess */
/* unused harmony export AppEditServiceById */
/* unused harmony export AppEditServiceByIdSuccess */
/* unused harmony export AppDeleteUserById */
/* unused harmony export AppDeleteUserByIdSuccess */
/* unused harmony export AppGetAllUserByIdSuccess */
/* unused harmony export AppCancelBookingDetail */
/* unused harmony export AppCancelBookingDetailSuccess */
/* unused harmony export AppRegisterDriver */
/* unused harmony export AppRegisterDriverSuccess */
/* unused harmony export AppEditDriverById */
/* unused harmony export AppEditDriverByIdSuccess */
var ActionTypes = {
    APP_SETTING_ADD: 'APP_SETTING_ADD',
    APP_REDIRECT_DASHBOARD: 'APP_REDIRECT_DASHBOARD',
    APP_REDIRECT_LOGIN: 'APP_REDIRECT_LOGIN',
    APP_REDIRECT_ROUTER: 'APP_REDIRECT_ROUTER',
    APP_GETALL_BOOKING: 'APP_GETALL_BOOKING',
    APP_BOOKING_DETAIL: 'APP_BOOKING_DETAIL',
    APP_GETALL_USERS: 'APP_GETALL_USERS',
    APP_BOOKING_DETAIL_SUCCESS: 'APP_BOOKING_DETAIL_SUCCESS',
    APP_GETALL_BOOKING_BY_ID: 'APP_GETALL_BOOKING_BY_ID',
    APP_GETALL_BOOKING_BY_ID_SUCCESS: 'APP_GETALL_BOOKING_BY_ID_SUCCESS',
    APP_CANCEL_BOOKING_BY_ID: 'APP_CANCEL_BOOKING_BY_ID',
    APP_CANCEL_BOOKING_BY_ID_SUCCESS: 'APP_CANCEL_BOOKING_BY_ID_SUCCESS',
    APP_GETTALL_USER: 'APP_GETTALL_USER',
    APP_GETTALL_USER_SUCCESS: 'APP_GETTALL_USER_SUCCESS',
    APP_GETTALL_USER_AFTER_SUCCESS: 'APP_GETTALL_USER_AFTER_SUCCESS',
    APP_GETTALL_USER_BY_ID: 'APP_GETTALL_USER_BY_ID',
    APP_BLOCK_USER_BY_ID: 'APP_BLOCK_USER_BY_ID',
    APP_EDIT_USER_BY_ID: 'APP_EDIT_USER_BY_ID',
    APP_EDIT_USER_BY_ID_SUCCESS: 'APP_EDIT_USER_BY_ID_SUCCESS',
    APP_EDIT_SERVICE_BY_ID: 'APP_EDIT_SERVICE_BY_ID',
    APP_EDIT_SERVICE_BY_ID_SUCCESS: 'APP_EDIT_SERVICE_BY_ID_SUCCESS',
    APP_DELETE_USER_BY_ID: 'APP_DELETE_USER_BY_ID',
    APP_DELETE_USER_BY_ID_SUCCESS: 'APP_DELETE_USER_BY_ID_SUCCESS',
    APP_GETTALL_USER_BY_ID_SUCCESS: 'APP_GETTALL_USER_BY_ID_SUCCESS',
    APP_REGISTER_SERVICE: 'APP_REGISTER_SERVICE',
    APP_REGISTER_SERVICE_SUCCESS: 'APP_REGISTER_SERVICE_SUCCESS',
    APP_REGISTER_CUSTOMER: 'APP_REGISTER_CUSTOMER',
    APP_REGISTER_DRIVER: 'APP_REGISTER_DRIVER',
    APP_REGISTER_DRIVER_SUCCESS: 'APP_REGISTER_DRIVER_SUCCESS',
    APP_EDIT_DRIVER_BY_ID: 'APP_EDIT_DRIVER_BY_ID',
    APP_EDIT_DRIVER_BY_ID_SUCCESS: 'APP_EDIT_DRIVER_BY_ID_SUCCESS',
};
var AppAddSettingAction = (function () {
    function AppAddSettingAction() {
        this.type = ActionTypes.APP_SETTING_ADD;
    }
    return AppAddSettingAction;
}());

var AppRedirectDashboardAction = (function () {
    function AppRedirectDashboardAction() {
        this.type = ActionTypes.APP_REDIRECT_DASHBOARD;
    }
    return AppRedirectDashboardAction;
}());

var AppRedirectLoginAction = (function () {
    function AppRedirectLoginAction() {
        this.type = ActionTypes.APP_REDIRECT_LOGIN;
    }
    return AppRedirectLoginAction;
}());

var AppRedirectRouterAction = (function () {
    function AppRedirectRouterAction() {
        this.type = ActionTypes.APP_REDIRECT_ROUTER;
    }
    return AppRedirectRouterAction;
}());

var AppGetAllBookingDetail = (function () {
    function AppGetAllBookingDetail(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_GETALL_BOOKING;
    }
    return AppGetAllBookingDetail;
}());

var AppGetAllUsers = (function () {
    function AppGetAllUsers() {
        this.type = ActionTypes.APP_GETALL_USERS;
    }
    return AppGetAllUsers;
}());

var AppRegisterService = (function () {
    function AppRegisterService() {
        this.type = ActionTypes.APP_REGISTER_SERVICE;
    }
    return AppRegisterService;
}());

var AppRegisterCustomer = (function () {
    function AppRegisterCustomer() {
        this.type = ActionTypes.APP_REGISTER_CUSTOMER;
        console.log("asd");
    }
    return AppRegisterCustomer;
}());

var AppRegisterServiceSuccess = (function () {
    function AppRegisterServiceSuccess() {
        this.type = ActionTypes.APP_REGISTER_SERVICE_SUCCESS;
    }
    return AppRegisterServiceSuccess;
}());

var AppBookingDetail = (function () {
    function AppBookingDetail(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_BOOKING_DETAIL;
    }
    return AppBookingDetail;
}());

var AppBookingDetailSuccess = (function () {
    function AppBookingDetailSuccess(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_BOOKING_DETAIL_SUCCESS;
    }
    return AppBookingDetailSuccess;
}());

var AppBookingDetailById = (function () {
    function AppBookingDetailById(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_GETALL_BOOKING_BY_ID;
    }
    return AppBookingDetailById;
}());

var AppBookingDetailByIdSuccess = (function () {
    function AppBookingDetailByIdSuccess(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_GETALL_BOOKING_BY_ID_SUCCESS;
    }
    return AppBookingDetailByIdSuccess;
}());

var AppGetAllUser = (function () {
    function AppGetAllUser(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_GETTALL_USER;
    }
    return AppGetAllUser;
}());

var AppGetAllUserSuccess = (function () {
    function AppGetAllUserSuccess(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_GETTALL_USER_SUCCESS;
    }
    return AppGetAllUserSuccess;
}());

var AppGetAllUserAfterSuccess = (function () {
    function AppGetAllUserAfterSuccess(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_GETTALL_USER_AFTER_SUCCESS;
    }
    return AppGetAllUserAfterSuccess;
}());

var AppGetAllUserById = (function () {
    function AppGetAllUserById(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_GETTALL_USER_BY_ID;
    }
    return AppGetAllUserById;
}());

var AppBlockUserById = (function () {
    function AppBlockUserById(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_BLOCK_USER_BY_ID;
    }
    return AppBlockUserById;
}());

var AppEditUserById = (function () {
    function AppEditUserById(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_EDIT_USER_BY_ID;
        console.log("e");
    }
    return AppEditUserById;
}());

var AppEditUserByIdSuccess = (function () {
    function AppEditUserByIdSuccess() {
        this.type = ActionTypes.APP_EDIT_USER_BY_ID_SUCCESS;
    }
    return AppEditUserByIdSuccess;
}());

var AppEditServiceById = (function () {
    function AppEditServiceById(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_EDIT_SERVICE_BY_ID;
        console.log("e");
    }
    return AppEditServiceById;
}());

var AppEditServiceByIdSuccess = (function () {
    function AppEditServiceByIdSuccess() {
        this.type = ActionTypes.APP_EDIT_SERVICE_BY_ID_SUCCESS;
    }
    return AppEditServiceByIdSuccess;
}());

var AppDeleteUserById = (function () {
    function AppDeleteUserById(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_DELETE_USER_BY_ID;
    }
    return AppDeleteUserById;
}());

var AppDeleteUserByIdSuccess = (function () {
    function AppDeleteUserByIdSuccess(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_DELETE_USER_BY_ID_SUCCESS;
        console.log("g");
    }
    return AppDeleteUserByIdSuccess;
}());

var AppGetAllUserByIdSuccess = (function () {
    function AppGetAllUserByIdSuccess(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_GETTALL_USER_BY_ID_SUCCESS;
    }
    return AppGetAllUserByIdSuccess;
}());

var AppCancelBookingDetail = (function () {
    function AppCancelBookingDetail() {
        this.type = ActionTypes.APP_CANCEL_BOOKING_BY_ID;
    }
    return AppCancelBookingDetail;
}());

var AppCancelBookingDetailSuccess = (function () {
    function AppCancelBookingDetailSuccess(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_CANCEL_BOOKING_BY_ID_SUCCESS;
    }
    return AppCancelBookingDetailSuccess;
}());

var AppRegisterDriver = (function () {
    function AppRegisterDriver() {
        this.type = ActionTypes.APP_REGISTER_DRIVER;
    }
    return AppRegisterDriver;
}());

var AppRegisterDriverSuccess = (function () {
    function AppRegisterDriverSuccess() {
        this.type = ActionTypes.APP_REGISTER_DRIVER_SUCCESS;
    }
    return AppRegisterDriverSuccess;
}());

var AppEditDriverById = (function () {
    function AppEditDriverById(payload) {
        this.payload = payload;
        this.type = ActionTypes.APP_EDIT_DRIVER_BY_ID;
        console.log("e");
    }
    return AppEditDriverById;
}());

var AppEditDriverByIdSuccess = (function () {
    function AppEditDriverByIdSuccess() {
        this.type = ActionTypes.APP_EDIT_DRIVER_BY_ID_SUCCESS;
    }
    return AppEditDriverByIdSuccess;
}());



/***/ }),
/* 109 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_service__ = __webpack_require__(39);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });



var AuthGuard = (function () {
    function AuthGuard(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        var url = state.url;
        return this.checkLogin(url);
    };
    AuthGuard.prototype.checkLogin = function (url) {
        //console.log(this.authService.login())
        if (this.authService.login()) {
            return true;
        }
        // Store the attempted URL for redirecting
        //this.authService.redirectUrl = url;
        // Navigate to the login page with extras
        this.router.navigate(['/login']);
        return false;
    };
    return AuthGuard;
}());
AuthGuard = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"]])
], AuthGuard);



/***/ }),
/* 110 */,
/* 111 */,
/* 112 */,
/* 113 */,
/* 114 */,
/* 115 */,
/* 116 */,
/* 117 */,
/* 118 */,
/* 119 */,
/* 120 */,
/* 121 */,
/* 122 */,
/* 123 */,
/* 124 */,
/* 125 */,
/* 126 */,
/* 127 */,
/* 128 */,
/* 129 */,
/* 130 */,
/* 131 */,
/* 132 */,
/* 133 */,
/* 134 */,
/* 135 */,
/* 136 */,
/* 137 */,
/* 138 */,
/* 139 */,
/* 140 */,
/* 141 */,
/* 142 */,
/* 143 */,
/* 144 */,
/* 145 */,
/* 146 */,
/* 147 */,
/* 148 */,
/* 149 */,
/* 150 */,
/* 151 */,
/* 152 */,
/* 153 */,
/* 154 */,
/* 155 */,
/* 156 */,
/* 157 */,
/* 158 */,
/* 159 */,
/* 160 */,
/* 161 */,
/* 162 */,
/* 163 */,
/* 164 */,
/* 165 */,
/* 166 */,
/* 167 */,
/* 168 */,
/* 169 */,
/* 170 */,
/* 171 */,
/* 172 */,
/* 173 */,
/* 174 */,
/* 175 */,
/* 176 */,
/* 177 */,
/* 178 */,
/* 179 */,
/* 180 */,
/* 181 */,
/* 182 */,
/* 183 */,
/* 184 */,
/* 185 */,
/* 186 */,
/* 187 */,
/* 188 */,
/* 189 */,
/* 190 */,
/* 191 */,
/* 192 */,
/* 193 */,
/* 194 */,
/* 195 */,
/* 196 */,
/* 197 */,
/* 198 */,
/* 199 */,
/* 200 */,
/* 201 */,
/* 202 */,
/* 203 */,
/* 204 */,
/* 205 */,
/* 206 */,
/* 207 */,
/* 208 */,
/* 209 */,
/* 210 */,
/* 211 */,
/* 212 */,
/* 213 */,
/* 214 */,
/* 215 */,
/* 216 */,
/* 217 */,
/* 218 */,
/* 219 */,
/* 220 */,
/* 221 */,
/* 222 */,
/* 223 */,
/* 224 */,
/* 225 */,
/* 226 */,
/* 227 */,
/* 228 */,
/* 229 */,
/* 230 */,
/* 231 */,
/* 232 */,
/* 233 */,
/* 234 */,
/* 235 */,
/* 236 */,
/* 237 */,
/* 238 */,
/* 239 */,
/* 240 */,
/* 241 */,
/* 242 */,
/* 243 */,
/* 244 */,
/* 245 */,
/* 246 */,
/* 247 */,
/* 248 */,
/* 249 */,
/* 250 */,
/* 251 */,
/* 252 */,
/* 253 */,
/* 254 */,
/* 255 */,
/* 256 */,
/* 257 */,
/* 258 */,
/* 259 */,
/* 260 */,
/* 261 */,
/* 262 */,
/* 263 */,
/* 264 */,
/* 265 */,
/* 266 */,
/* 267 */,
/* 268 */,
/* 269 */,
/* 270 */,
/* 271 */,
/* 272 */,
/* 273 */,
/* 274 */,
/* 275 */,
/* 276 */,
/* 277 */,
/* 278 */,
/* 279 */,
/* 280 */,
/* 281 */,
/* 282 */,
/* 283 */,
/* 284 */,
/* 285 */,
/* 286 */,
/* 287 */,
/* 288 */,
/* 289 */,
/* 290 */,
/* 291 */,
/* 292 */,
/* 293 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(174);

/***/ }),
/* 294 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(193);

/***/ }),
/* 295 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(2);

/***/ }),
/* 296 */,
/* 297 */,
/* 298 */,
/* 299 */,
/* 300 */,
/* 301 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(450);

/***/ }),
/* 302 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(476);

/***/ }),
/* 303 */,
/* 304 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_module__ = __webpack_require__(339);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__app_module__["a"]; });



/***/ }),
/* 305 */,
/* 306 */,
/* 307 */,
/* 308 */,
/* 309 */,
/* 310 */,
/* 311 */,
/* 312 */,
/* 313 */,
/* 314 */,
/* 315 */,
/* 316 */,
/* 317 */,
/* 318 */,
/* 319 */,
/* 320 */,
/* 321 */,
/* 322 */,
/* 323 */,
/* 324 */,
/* 325 */,
/* 326 */,
/* 327 */,
/* 328 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(454);

/***/ }),
/* 329 */,
/* 330 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(13);

/***/ }),
/* 331 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(421);

/***/ }),
/* 332 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(514);

/***/ }),
/* 333 */,
/* 334 */,
/* 335 */,
/* 336 */,
/* 337 */,
/* 338 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__global_state__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__theme_services__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__theme_theme_config__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__theme_theme_constants__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_style_loader_app_scss__ = __webpack_require__(537);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_style_loader_app_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_style_loader_app_scss__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_style_loader_theme_initial_scss__ = __webpack_require__(536);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_style_loader_theme_initial_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_style_loader_theme_initial_scss__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return App; });







/*
 * App Component
 * Top Level Component
 */
var App = (function () {
    function App(_state, _imageLoader, _spinner, viewContainerRef, themeConfig) {
        var _this = this;
        this._state = _state;
        this._imageLoader = _imageLoader;
        this._spinner = _spinner;
        this.viewContainerRef = viewContainerRef;
        this.themeConfig = themeConfig;
        this.isMenuCollapsed = false;
        themeConfig.config();
        this._loadImages();
        this._state.subscribe('menu.isCollapsed', function (isCollapsed) {
            _this.isMenuCollapsed = isCollapsed;
        });
    }
    App.prototype.ngAfterViewInit = function () {
        var _this = this;
        // hide spinner once all loaders are completed
        __WEBPACK_IMPORTED_MODULE_2__theme_services__["d" /* BaThemePreloader */].load().then(function (values) {
            _this._spinner.hide();
        });
    };
    App.prototype._loadImages = function () {
        // register some loaders
        __WEBPACK_IMPORTED_MODULE_2__theme_services__["d" /* BaThemePreloader */].registerLoader(this._imageLoader.load(__WEBPACK_IMPORTED_MODULE_4__theme_theme_constants__["b" /* layoutPaths */].images.root + 'sky-bg.jpg'));
    };
    return App;
}());
App = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app',
        template: "\n  \n     \n     <main [ngClass]=\"{'menu-collapsed': isMenuCollapsed}\" baThemeRun>\n  \n      <router-outlet></router-outlet>\n   \n    </main>\n  \n  "
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__global_state__["a" /* GlobalState */],
        __WEBPACK_IMPORTED_MODULE_2__theme_services__["c" /* BaImageLoaderService */],
        __WEBPACK_IMPORTED_MODULE_2__theme_services__["a" /* BaThemeSpinner */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"],
        __WEBPACK_IMPORTED_MODULE_3__theme_theme_config__["a" /* BaThemeConfig */]])
], App);



/***/ }),
/* 339 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angularclass_hmr__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angularclass_hmr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__angularclass_hmr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ng_bootstrap_ng_bootstrap__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__auth_auth_module__ = __webpack_require__(343);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_angular2_jwt__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_angular2_jwt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__theme_services_authService_authGuard_service__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__theme_services_authService_auth_service__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_common_service__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services_competitionService_competition_service__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__services_dashboardService_dashboard_service__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_ngx_pagination__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__environment__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__app_routing__ = __webpack_require__(340);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__app_component__ = __webpack_require__(338);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__app_service__ = __webpack_require__(341);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__app_store__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__global_state__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__theme_nga_module__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_pages_module__ = __webpack_require__(358);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23_ngx_toastr__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__angular_common__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__services_bookingService_booking_service__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__angular_platform_browser_animations__ = __webpack_require__(334);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__publicPages_public_pages_module__ = __webpack_require__(364);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });







//import {PaginatePipe, PaginationService} from 'ng2-pagination';
//import {ToastModule} from 'ng2-toastr/ng2-toastr';







 //
//import { storeuserInfoService } from './theme/services/storeService/storeUserInfo.service';
/*
 * Platform and Environment providers/directives/pipes
 */


// App is our top level component



//redux import
// import {StoreModule} from '@ngrx/store';
// import {StoreDevtoolsModule} from '@ngrx/store-devtools';
// import {StoreLogMonitorModule, useLogMonitor} from '@ngrx/store-log-monitor';
// import {userInfo} from './theme/stores/userInfo.store';








// Application wide providers
var APP_PROVIDERS = [
    __WEBPACK_IMPORTED_MODULE_18__app_service__["a" /* AppState */],
    __WEBPACK_IMPORTED_MODULE_20__global_state__["a" /* GlobalState */]
];
/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
var AppModule = (function () {
    function AppModule(appRef, appState) {
        this.appRef = appRef;
        this.appState = appState;
    }
    AppModule.prototype.hmrOnInit = function (store) {
        if (!store || !store.state)
            return;
        console.log('HMR store', JSON.stringify(store, null, 2));
        // set state
        this.appState._state = store.state;
        // set input values
        if ('restoreInputValues' in store) {
            var restoreInputValues = store.restoreInputValues;
            setTimeout(restoreInputValues);
        }
        this.appRef.tick();
        delete store.state;
        delete store.restoreInputValues;
    };
    AppModule.prototype.hmrOnDestroy = function (store) {
        var cmpLocation = this.appRef.components.map(function (cmp) { return cmp.location.nativeElement; });
        // save state
        var state = this.appState._state;
        store.state = state;
        // recreate root elements
        store.disposeOldHosts = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_5__angularclass_hmr__["createNewHosts"])(cmpLocation);
        // save input values
        store.restoreInputValues = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_5__angularclass_hmr__["createInputTransfer"])();
        // remove styles
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_5__angularclass_hmr__["removeNgStyles"])();
    };
    AppModule.prototype.hmrAfterDestroy = function (store) {
        // display new elements
        store.disposeOldHosts();
        delete store.disposeOldHosts;
    };
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        bootstrap: [__WEBPACK_IMPORTED_MODULE_17__app_component__["a" /* App */]],
        declarations: [
            __WEBPACK_IMPORTED_MODULE_17__app_component__["a" /* App */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["BrowserModule"],
            //ToastModule.forRoot(),
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["HttpModule"],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["RouterModule"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormsModule"],
            __WEBPACK_IMPORTED_MODULE_7__auth_auth_module__["a" /* AdminAuthModule */],
            __WEBPACK_IMPORTED_MODULE_26__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
            __WEBPACK_IMPORTED_MODULE_24__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_23_ngx_toastr__["a" /* ToastrModule */].forRoot(),
            // StoreModule.provideStore({userInfo}),
            // StoreDevtoolsModule.instrumentStore({
            //   monitor: useLogMonitor({
            //     visible: false,
            //     position: 'right'
            //   })
            // }),
            // StoreLogMonitorModule,
            __WEBPACK_IMPORTED_MODULE_19__app_store__["a" /* AppStoreModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["ReactiveFormsModule"],
            __WEBPACK_IMPORTED_MODULE_21__theme_nga_module__["a" /* NgaModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_6__ng_bootstrap_ng_bootstrap__["a" /* NgbModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_22__pages_pages_module__["a" /* PagesModule */],
            __WEBPACK_IMPORTED_MODULE_27__publicPages_public_pages_module__["a" /* PublicPageModule */],
            __WEBPACK_IMPORTED_MODULE_14_ngx_pagination__["a" /* NgxPaginationModule */],
            __WEBPACK_IMPORTED_MODULE_16__app_routing__["a" /* routing */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_15__environment__["b" /* ENV_PROVIDERS */],
            APP_PROVIDERS,
            __WEBPACK_IMPORTED_MODULE_10__theme_services_authService_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_25__services_bookingService_booking_service__["a" /* booking_service */],
            __WEBPACK_IMPORTED_MODULE_12__services_competitionService_competition_service__["a" /* competition_service */],
            __WEBPACK_IMPORTED_MODULE_11__services_common_service__["a" /* common_service */],
            __WEBPACK_IMPORTED_MODULE_13__services_dashboardService_dashboard_service__["a" /* dashboard_service */],
            __WEBPACK_IMPORTED_MODULE_9__theme_services_authService_authGuard_service__["a" /* AuthGuard */],
            __WEBPACK_IMPORTED_MODULE_8_angular2_jwt__["JwtHelper"],
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_8_angular2_jwt__["provideAuth"])({
                headerName: 'Authorization',
                tokenName: 'token',
                tokenGetter: (function () { return sessionStorage.getItem('token'); }),
                globalHeaders: [{ 'Content-Type': 'application/json' }],
                noJwtError: true
            })
        ]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["ApplicationRef"],
        __WEBPACK_IMPORTED_MODULE_18__app_service__["a" /* AppState */]])
], AppModule);



/***/ }),
/* 340 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__(7);
/* unused harmony export routes */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routing; });

var routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: '**', redirectTo: 'login' }
];
var routing = __WEBPACK_IMPORTED_MODULE_0__angular_router__["RouterModule"].forRoot(routes, { useHash: true });


/***/ }),
/* 341 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppState; });

var AppState = (function () {
    function AppState() {
        this._state = {};
    }
    Object.defineProperty(AppState.prototype, "state", {
        // already return a clone of the current state
        get: function () {
            return this._state = this._clone(this._state);
        },
        // never allow mutation
        set: function (value) {
            throw new Error('do not mutate the `.state` directly');
        },
        enumerable: true,
        configurable: true
    });
    AppState.prototype.get = function (prop) {
        // use our state getter for the clone
        var state = this.state;
        return state.hasOwnProperty(prop) ? state[prop] : state;
    };
    AppState.prototype.set = function (prop, value) {
        // internally mutate our state
        return this._state[prop] = value;
    };
    AppState.prototype._clone = function (object) {
        // simple object clone
        return JSON.parse(JSON.stringify(object));
    };
    return AppState;
}());
AppState = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], AppState);



/***/ }),
/* 342 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store_devtools__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store_devtools___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__ngrx_store_devtools__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store_log_monitor__ = __webpack_require__(447);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store_log_monitor___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__ngrx_store_log_monitor__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngrx_effects__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__state__ = __webpack_require__(369);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__auth_state__ = __webpack_require__(346);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_Bookings_state__ = __webpack_require__(350);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_Competitions_state__ = __webpack_require__(353);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_dashboard_state__ = __webpack_require__(356);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppStoreModule; });










var AppStoreModule = (function () {
    function AppStoreModule() {
    }
    return AppStoreModule;
}());
AppStoreModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__ngrx_store__["StoreModule"].provideStore({
                app: __WEBPACK_IMPORTED_MODULE_5__state__["a" /* app */],
                auth: __WEBPACK_IMPORTED_MODULE_6__auth_state__["a" /* auth */],
                booking: __WEBPACK_IMPORTED_MODULE_7__pages_Bookings_state__["a" /* booking */],
                competition: __WEBPACK_IMPORTED_MODULE_8__pages_Competitions_state__["a" /* competition */],
                dashBoard: __WEBPACK_IMPORTED_MODULE_9__pages_dashboard_state__["a" /* dashBoard */]
            }),
            __WEBPACK_IMPORTED_MODULE_2__ngrx_store_devtools__["StoreDevtoolsModule"].instrumentStore({
                monitor: __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__ngrx_store_log_monitor__["useLogMonitor"])({
                    visible: false,
                    position: 'right'
                })
            }),
            __WEBPACK_IMPORTED_MODULE_4__ngrx_effects__["a" /* EffectsModule */].run(__WEBPACK_IMPORTED_MODULE_5__state__["b" /* AppEffects */]),
            __WEBPACK_IMPORTED_MODULE_4__ngrx_effects__["a" /* EffectsModule */].run(__WEBPACK_IMPORTED_MODULE_6__auth_state__["b" /* AuthEffects */]),
            __WEBPACK_IMPORTED_MODULE_4__ngrx_effects__["a" /* EffectsModule */].run(__WEBPACK_IMPORTED_MODULE_7__pages_Bookings_state__["b" /* BookingEffects */]),
            __WEBPACK_IMPORTED_MODULE_4__ngrx_effects__["a" /* EffectsModule */].run(__WEBPACK_IMPORTED_MODULE_8__pages_Competitions_state__["b" /* CompetitionEffects */]),
            __WEBPACK_IMPORTED_MODULE_4__ngrx_effects__["a" /* EffectsModule */].run(__WEBPACK_IMPORTED_MODULE_9__pages_dashboard_state__["b" /* DashboardEffects */])
        ],
    })
], AppStoreModule);



/***/ }),
/* 343 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__theme_nga_module__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_translation_module__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ng_bootstrap_ng_bootstrap__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__service_userService_user_service__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__service_authGuardPublicPages_authGuardPublic_service__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__service_authGuardPublicPages_authPublic_service__ = __webpack_require__(99);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminAuthModule; });




//import { routing }       from './auth.routes';


//import { ColmenaUiModule } from '@colmena/colmena-angular-ui'




// import { Login } from './components/login/login.component'
// import { LogoutComponent } from './components/logout/logout.component'
// import { RecoverComponent } from './components/recover/recover.component'
// import { Register } from './components/register/register.component'
// import { ResetComponent } from './components/reset/reset.component'
var AdminAuthModule = (function () {
    function AdminAuthModule() {
    }
    return AdminAuthModule;
}());
AdminAuthModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormsModule"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["ReactiveFormsModule"],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["RouterModule"],
            __WEBPACK_IMPORTED_MODULE_6__ng_bootstrap_ng_bootstrap__["b" /* NgbModalModule */],
            //routing,
            __WEBPACK_IMPORTED_MODULE_4__theme_nga_module__["a" /* NgaModule */],
            __WEBPACK_IMPORTED_MODULE_5__app_translation_module__["a" /* AppTranslationModule */]
            //ColmenaUiModule,
        ],
        declarations: [],
        providers: [
            __WEBPACK_IMPORTED_MODULE_7__service_userService_user_service__["a" /* user_service */],
            __WEBPACK_IMPORTED_MODULE_8__service_authGuardPublicPages_authGuardPublic_service__["a" /* AuthGuard */],
            __WEBPACK_IMPORTED_MODULE_9__service_authGuardPublicPages_authPublic_service__["a" /* AuthPublicPagesService */]
        ]
    })
], AdminAuthModule);



/***/ }),
/* 344 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_effects__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_userService_user_service__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__theme_services_authService_auth_service__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__theme_services__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_router__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ng_bootstrap_ng_bootstrap__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ngx_toastr__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_lodash__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__auth_actions__ = __webpack_require__(21);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthEffects; });






//import { toast_service  } from '../../services/common.service';





var types = ['success', 'error', 'info', 'warning'];
//import { UiService } from '@colmena/colmena-angular-ui'
///import { UserApi } from '@lb-sdk'

var AuthEffects = (function () {
    //********************RESET PASS ENDS*************************** */
    function AuthEffects(actions$, store, user_service, authService, baThemeSpinner, 
        //private toast_service:toast_service,
        router, modalService, toastrService
        // private userApi: UserApi,
        // private ui: UiService,
    ) {
        var _this = this;
        this.actions$ = actions$;
        this.store = store;
        this.user_service = user_service;
        this.authService = authService;
        this.baThemeSpinner = baThemeSpinner;
        this.router = router;
        this.modalService = modalService;
        this.toastrService = toastrService;
        this.title = '';
        // type = types[0];
        this.message = '';
        this.version = __WEBPACK_IMPORTED_MODULE_0__angular_core__["VERSION"];
        this.lastInserted = [];
        this.login = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_11__auth_actions__["a" /* ActionTypes */].AUTH_LOGIN)
            .do(function (action) {
            var number = 100;
            _this.user_service.login(action.payload).subscribe(function (result) {
                //console.log(result);
                if (result.message = "success") {
                    // console.log("logged in")
                    // console.log("fcfgcfgcg",result)
                    //hide loader
                    // this.baThemeSpinner.hide(number);
                    var m = 'Email or password  match !';
                    var t = 'Authentication';
                    var opt = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_10_lodash__["cloneDeep"])(_this.options);
                    var inserted = _this.toastrService[types[0]](m, t, opt);
                    if (inserted) {
                        _this.lastInserted.push(inserted.toastId);
                    }
                    _this.baThemeSpinner.hide(number);
                    //this.toast_service.showSuccess();
                    _this.store.dispatch({ type: 'AUTH_GET_USER_ROLES', payload: result });
                    _this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_11__auth_actions__["c" /* AuthLoginSuccessAction */](result));
                    //token store in localstorage
                    console.log("gcghcd", result);
                    localStorage.setItem('token', result.data.accessToken);
                    var tokenSession = localStorage.getItem('token');
                    localStorage.setItem('tokenSession', JSON.stringify(result.data.accessToken));
                    //console.log("api integrated succussfully")
                    var loggedIn = _this.authService.login();
                    if (loggedIn) {
                        // Get the redirect URL from our auth service
                        // If no redirect has been set, use the default
                        var redirect = _this.authService.redirectUrl ? _this.authService.redirectUrl : 'pages';
                        // Redirect the user
                        console.log(redirect);
                        _this.router.navigate([redirect]);
                    }
                }
                else {
                    console.log(result);
                }
            }, function (error) {
                _this.baThemeSpinner.hide();
                // this.store.dispatch(new auth.AuthLoginErrorAction(error))
                // console.log(error.message)
                var m = 'Email or password  donot match !';
                var t = 'Authentication';
                var opt = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_10_lodash__["cloneDeep"])(_this.options);
                var inserted = _this.toastrService[types[1]](m, t, opt);
                if (inserted) {
                    _this.lastInserted.push(inserted.toastId);
                }
            });
        });
        this.loginError = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_11__auth_actions__["a" /* ActionTypes */].AUTH_LOGIN_ERROR)
            .do(function (action) {
            //  let m = 'Email or password  donot match !';
            //     let t = 'Authentication';
            //     const opt = cloneDeep(this.options);
            //     const inserted = this.toastrService[types[1]](m, t, opt);
            //     if (inserted) {
            //       this.lastInserted.push(inserted.toastId);
            //     }
            console.log(action);
        });
        this.loginSuccess = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_11__auth_actions__["a" /* ActionTypes */].AUTH_LOGIN_SUCCESS)
            .do(function (action) {
            window.localStorage.setItem('token', JSON.stringify(action.payload));
            //this.ui.toastSuccess('Sign in successful', `You are logged in as ${get(action, 'payload.user.email')}`)
            //return this.store.dispatch({ type: 'APP_REDIRECT_ROUTER' })
        });
        this.register = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_11__auth_actions__["a" /* ActionTypes */].AUTH_REGISTER)
            .do(function (action) {
            console.log("registerede api call");
            // this.userApi.create(action.payload)
            //   .subscribe(
            //     (success: any) => this.store.dispatch(new auth.AuthRegisterSuccessAction({
            //       realm: action.payload.realm,
            //       email: action.payload.email,
            //       password: action.payload.password,
            //     })),
            //     (error) => this.store.dispatch(new auth.AuthRegisterErrorAction(error)),
            //   )
        });
        this.registerSuccess = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_11__auth_actions__["a" /* ActionTypes */].AUTH_REGISTER_SUCCESS)
            .do(function (action) {
            // this.ui.toastSuccess('Successfully registered', `${action.payload.email} has been created`)
            // return this.store.dispatch(new auth.AuthLoginAction(action.payload))
        });
        this.registerError = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_11__auth_actions__["a" /* ActionTypes */].AUTH_REGISTER_ERROR)
            .do(function (action) { });
        this.logout = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_11__auth_actions__["a" /* ActionTypes */].AUTH_LOGOUT)
            .do(function () {
            var number = 100;
            _this.baThemeSpinner.show();
            _this.user_service.logoutUser().subscribe(function (result) {
                console.log(result);
                if (result.message = 'success') {
                    var m = 'Logout Successfully!';
                    var t = 'Authentication';
                    var opt = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_10_lodash__["cloneDeep"])(_this.options);
                    var inserted = _this.toastrService[types[0]](m, t, opt);
                    if (inserted) {
                        _this.lastInserted.push(inserted.toastId);
                        _this.baThemeSpinner.hide(number);
                        //clear localstorage
                        _this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_11__auth_actions__["d" /* AuthLogoutSuccessAction */](result)),
                            //this.authService.logout();
                            window.localStorage.removeItem('token');
                        window.localStorage.removeItem('tokenSession');
                        window.localStorage.removeItem('language');
                        window.localStorage.clear();
                        _this.router.navigate(['login']);
                    }
                }
                (function (error) {
                    _this.baThemeSpinner.hide(number);
                    console.log(error);
                });
            });
        });
        this.logoutError = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_11__auth_actions__["a" /* ActionTypes */].AUTH_LOGOUT_ERROR)
            .do(function (action) {
            // window.localStorage.removeItem('token')
            // this.ui.toastError(get(action, 'payload.name'), get(action, 'payload.message'))
            // return this.store.dispatch({ type: 'APP_REDIRECT_ROUTER' })
        });
        this.logoutSuccess = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_11__auth_actions__["a" /* ActionTypes */].AUTH_LOGOUT_SUCCESS)
            .do(function () {
            // window.localStorage.removeItem('token')
            // this.ui.toastSuccess('Log out successful', 'You are logged out')
            // return this.store.dispatch({ type: 'APP_REDIRECT_ROUTER' })
        });
        this.getUserInfo$ = this.actions$
            .ofType('AUTH_GET_USER_ROLES')
            .do(function (action) {
            console.log('AUTH_GET_USER_ROLES', action.payload);
            // this.userApi.info(action.payload.userId)
            //   .subscribe(res => {
            //     console.log('set roles')
            //     window.localStorage.setItem('roles', JSON.stringify(res.roles))
            //     this.store.dispatch({ type: 'AUTH_SET_ROLES', payload: res.roles})
            //   })
        });
        //     @Effect({ dispatch: false })
        //   forgotpassword: Observable<Action> = this.actions$
        //     .ofType(auth.ActionTypes.AUTH_FORGOT_PASSWORD)
        //     .do((action) => {
        //        let number =100
        //        this.user_service.forgotpassword(action.payload).subscribe((result) =>{
        //          console.log("password reset link sent" )
        //             if(result.message='success'){
        //     const activeModal = this.modalService.open(ForgotModal, {size: 'sm',
        //       backdrop: 'static'});
        //     activeModal.componentInstance.modalHeader = 'Static modal';
        //   }  
        //    this.baThemeSpinner.hide(number);
        //     }
        //        )
        // })
        //***********************FORGOT PASS****************************** */
        this.forgot = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_11__auth_actions__["a" /* ActionTypes */].AUTH_FORGOT_PASSWORD)
            .do(function (action) {
            var number = 100;
            _this.user_service.forgotPass(action.payload).subscribe(function (result) {
                console.log(result);
                if (result.message) {
                    // console.log("logged in")
                    // console.log("fcfgcfgcg",result)
                    //this.toast_service.showSuccess();
                    //this.store.dispatch({ type: 'AUTH_GET_USER_ROLES', payload: result })
                    //  this.store.dispatch(new auth.AuthForgotPassSuccessAction(result))
                    // if(result.message){
                    var m = result.message;
                    var t = '';
                    var opt = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_10_lodash__["cloneDeep"])(_this.options);
                    var inserted = _this.toastrService[types[0]](m, t, opt);
                    if (inserted) {
                        _this.lastInserted.push(inserted.toastId);
                        // }
                    }
                    //token store in localstorage
                    // localStorage.setItem('token', result.data.access_token)
                    // var tokenSession = localStorage.getItem('token')
                    // localStorage.setItem('tokenSession', JSON.stringify(result.data.access_token))
                    //console.log("api integrated succussfully")
                    // var loggedIn = this.authService.forgot()
                    // if (loggedIn) {
                    // Get the redirect URL from our auth service
                    // If no redirect has been set, use the default
                    // let redirect = this.authService.redirectUrl ? this.authService.redirectUrl : 'pages';
                    // Redirect the user
                    // console.log(redirect)
                    // this.router.navigate([redirect]);
                    //}
                    //navigation to homepage
                    //this.router.navigate(['pages'])
                    //this.login();
                }
                else {
                    // this.baThemeSpinner.hide();
                    //console.log(result.message)
                    // if(result.message){
                    var m = 'Email does not match !';
                    var t = 'Authentication';
                    var opt = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_10_lodash__["cloneDeep"])(_this.options);
                    var inserted = _this.toastrService[types[1]](m, t, opt);
                    if (inserted) {
                        _this.lastInserted.push(inserted.toastId);
                        // }
                    }
                }
            });
        });
        this.forgotError = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_11__auth_actions__["a" /* ActionTypes */].AUTH_FORGOT_PASSWORD_ERROR)
            .do(function (action) {
            console.log(action);
            var m = 'Email does not match !';
            var t = 'Authentication';
            var opt = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_10_lodash__["cloneDeep"])(_this.options);
            var inserted = _this.toastrService[types[1]](m, t, opt);
            if (inserted) {
                _this.lastInserted.push(inserted.toastId);
            }
        });
        this.forgotSuccess = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_11__auth_actions__["a" /* ActionTypes */].AUTH_FORGOT_PASSWORD_SUCCESS)
            .do(function (action) {
            window.localStorage.setItem('token', JSON.stringify(action.payload));
            //this.ui.toastSuccess('Sign in successful', `You are logged in as ${get(action, 'payload.user.email')}`)
            //return this.store.dispatch({ type: 'APP_REDIRECT_ROUTER' })
        });
        //*********************FORGOT PASS ENDS************************** */    
        //********************VERIFY PASS*************************** */
        this.forgotPasswordTokenAuthenticate = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_11__auth_actions__["a" /* ActionTypes */].AUTH_FORGOT_PASS_TOKEN)
            .do(function (action) {
            _this.user_service.authenticateResetPassToken(action.payload).subscribe(function (result) {
                //console.log(result);
                if (result.status == 200) {
                    var m = "Token verified";
                    var t = 'Authentication';
                    var opt = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_10_lodash__["cloneDeep"])(_this.options);
                    var inserted = _this.toastrService[types[0]](m, t, opt);
                    if (inserted) {
                        _this.lastInserted.push(inserted.toastId);
                        _this.store.dispatch({
                            type: __WEBPACK_IMPORTED_MODULE_11__auth_actions__["a" /* ActionTypes */].AUTH_FORGOT_PASS_TOKEN_SUCCESS,
                            payload: result
                        });
                    }
                    else {
                        var m_1 = "token receved";
                        var t_1 = 'Authentication';
                        var opt_1 = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_10_lodash__["cloneDeep"])(_this.options);
                        var inserted_1 = _this.toastrService[types[1]](m_1, t_1, opt_1);
                        if (inserted_1) {
                            _this.lastInserted.push(inserted_1.toastId);
                        }
                    }
                }
            });
        });
        this.forgotPasswordTokenAuthenticateError = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_11__auth_actions__["a" /* ActionTypes */].AUTH_RESET_PASSWORD_ERROR)
            .do(function (action) {
            //Magic here
        });
        this.forgotPasswordTokenAuthenticateSuccess = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_11__auth_actions__["a" /* ActionTypes */].AUTH_RESET_PASSWORD_SUCCESS)
            .do(function (action) {
            //Magic here
        });
        //********************VERIFY PASS ENDS*************************** */
        //********************RESET PASS*************************** */
        this.reset = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_11__auth_actions__["a" /* ActionTypes */].AUTH_RESET_PASSWORD)
            .do(function (action) {
            var number = 100;
            _this.user_service.resetPassword(action.payload).subscribe(function (result) {
                if (result.status == 200) {
                    _this.baThemeSpinner.hide(number);
                    _this.store.dispatch({ type: 'AUTH_GET_USER_ROLES', payload: result });
                    _this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_11__auth_actions__["e" /* AuthResetPasswordSuccessAction */](result));
                    var m = result.message;
                    var t = 'Successful';
                    var opt = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_10_lodash__["cloneDeep"])(_this.options);
                    var inserted = _this.toastrService[types[2]](m, t, opt);
                    if (inserted) {
                        _this.lastInserted.push(inserted.toastId, setInterval(5000));
                        _this.router.navigate(['login']);
                    }
                }
                else if (result.status == 202) {
                    _this.baThemeSpinner.hide();
                    // console.log(error.message)
                    if (result.message) {
                        var m = 'Email Doesnot exist !';
                        var t = 'Authentication';
                        var opt = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_10_lodash__["cloneDeep"])(_this.options);
                        var inserted = _this.toastrService[types[1]](m, t, opt);
                        if (inserted) {
                            _this.lastInserted.push(inserted.toastId);
                        }
                    }
                }
            });
        });
        this.resetError = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_11__auth_actions__["a" /* ActionTypes */].AUTH_RESET_PASSWORD_ERROR)
            .do(function (action) {
            console.log(action);
        });
        this.resetSuccess = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_11__auth_actions__["a" /* ActionTypes */].AUTH_RESET_PASSWORD_SUCCESS)
            .do(function (action) {
            window.localStorage.setItem('token', JSON.stringify(action.payload));
        });
        this.options = this.toastrService.toastrConfig;
    }
    return AuthEffects;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], AuthEffects.prototype, "login", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], AuthEffects.prototype, "loginError", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], AuthEffects.prototype, "loginSuccess", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], AuthEffects.prototype, "register", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], AuthEffects.prototype, "registerSuccess", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], AuthEffects.prototype, "registerError", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], AuthEffects.prototype, "logout", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], AuthEffects.prototype, "logoutError", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], AuthEffects.prototype, "logoutSuccess", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], AuthEffects.prototype, "getUserInfo$", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], AuthEffects.prototype, "forgot", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], AuthEffects.prototype, "forgotError", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], AuthEffects.prototype, "forgotSuccess", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], AuthEffects.prototype, "forgotPasswordTokenAuthenticate", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], AuthEffects.prototype, "forgotPasswordTokenAuthenticateError", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], AuthEffects.prototype, "forgotPasswordTokenAuthenticateSuccess", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], AuthEffects.prototype, "reset", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], AuthEffects.prototype, "resetError", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], AuthEffects.prototype, "resetSuccess", void 0);
AuthEffects = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["c" /* Actions */],
        __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["Store"],
        __WEBPACK_IMPORTED_MODULE_4__service_userService_user_service__["a" /* user_service */],
        __WEBPACK_IMPORTED_MODULE_5__theme_services_authService_auth_service__["a" /* AuthService */],
        __WEBPACK_IMPORTED_MODULE_6__theme_services__["a" /* BaThemeSpinner */],
        __WEBPACK_IMPORTED_MODULE_7__angular_router__["Router"],
        __WEBPACK_IMPORTED_MODULE_8__ng_bootstrap_ng_bootstrap__["d" /* NgbModal */],
        __WEBPACK_IMPORTED_MODULE_9_ngx_toastr__["b" /* ToastrService */]
        // private userApi: UserApi,
        // private ui: UiService,
    ])
], AuthEffects);



/***/ }),
/* 345 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return auth; });
var initialState = {
    currentUser: null,
    loggedIn: false,
    realms: [],
    roles: {
        assigned: [],
        unassigned: [],
    },
};
var auth = function (state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case 'AUTH_LOGIN':
            //console.log("hello",state)
            return Object.assign({}, state);
        case 'AUTH_LOGIN_ERROR':
            //console.log("hello",state)
            return Object.assign({}, state, { er: action.payload });
        case 'AUTH_LOGOUT_SUCCESS':
            return Object.assign({}, state, { currentUser: null, loggedIn: false });
        case 'AUTH_SET_TOKEN':
        case 'AUTH_LOGIN_SUCCESS':
            console.log('action.payload', action.payload);
            return Object.assign({}, state, { currentUser: action.payload, loggedIn: true });
        case 'AUTH_REALMS_ADD':
            return Object.assign({}, state, { realms: state.realms.concat([action.payload]) });
        case 'AUTH_SET_ROLES':
            return Object.assign({}, state, {
                roles: {
                    assigned: state.roles.assigned.concat(action.payload.assigned),
                    unassigned: state.roles.unassigned.concat(action.payload.unassigned),
                },
            });
        // case 'AUTH_FORGOT_PASSWORD':
        //  {
        //   //console.log("hello",state)
        //   return Object.assign({}, state)
        //   }
        //****************FORGOT PASS************* */
        case 'AUTH_FORGOT_PASSWORD':
            return Object.assign({}, state);
        case 'AUTH_FORGOT_PASSWORD_SUCCESS':
            console.log('action.payload', action.payload);
            return Object.assign({}, state, { currentUser: action.payload, loggedIn: true });
        //********************FORGOT PASS ENDS***************************** */
        //********************VERYFY PASS TOKEN ***************************** */
        case 'AUTH_FORGOT_PASS_TOKEN_SUCCESS':
            return Object.assign({}, state, { verify_token_success_payload: action.payload });
        //********************VERIFY PASS ENDS***************************** */
        //*************************RESET PASS******************************* */
        case 'AUTH_RESET_PASSWORD':
            return Object.assign({}, state);
        case 'AUTH_RESET_PASSWORD_SUCCESS':
            console.log('action.payload', action.payload);
            return Object.assign({}, state, { currentUser: action.payload, loggedIn: true });
        //*************************RESET PASS END******************************* */
        default:
            return state;
    }
};


/***/ }),
/* 346 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__auth_actions__ = __webpack_require__(21);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__auth_effects__ = __webpack_require__(344);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_1__auth_effects__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_reducers__ = __webpack_require__(345);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_2__auth_reducers__["a"]; });





/***/ }),
/* 347 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
var environment = {
    production: true,
    APP: {
        // dev API_URL : 'http://34.208.129.161:3000',
        // test API_URL:'http://35.166.40.101:3001',
        API_URL: "http://34.208.129.161:3000",
        LOGIN_API: '/api/v1/admin/login',
        LOGOUT_API: '/api/v1/admin/logout',
        FORGOT_PASSWORD: '/api/v1/user/forgotPassword',
        GET_ALL_BOOKING: '/api/v1/category',
        GET_ALL_COMPETITIONS: '/api/v1/admin/getUsers',
        GET_USER_BY_ID: '/admin/getCustomerWithId',
        BLOCK_USER_BY_ID: '/admin/blockOption',
        DELETE_USER_BY_ID: '/admin/deleteUser',
        EDIT_USER_BY_ID: '/api/v1/category/',
        GET_DASHBOARD_COUNT: '/admin/getAllUsersRegisteredToday',
        GET_USER_REPORT_MONTHLY: '/admin/geUserReportMonthly',
        CREATE_COMPETITION: '/admin/createCompetition',
        VERIFY_TOKEN: '/api/v1/user/verifyResetToken',
        RESET_API: '/api/v1/user/resetPassword',
        ADD_ITEM: '/api/v1/category',
        DELETE_ITEM_BY_ID: '/api/v1/category/'
    }
};


/***/ }),
/* 348 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_effects__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_bookingService_booking_service__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_toastr__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ng_bootstrap_ng_bootstrap__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__booking_actions__ = __webpack_require__(92);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BookingEffects; });





//import { AuthService  } from '../../theme/services/authService/auth.service';


var types = ['success', 'error', 'info', 'warning'];


var BookingEffects = (function () {
    function BookingEffects(actions$, store, router, booking_service, toastrService, modalService) {
        var _this = this;
        this.actions$ = actions$;
        this.store = store;
        this.router = router;
        this.booking_service = booking_service;
        this.toastrService = toastrService;
        this.modalService = modalService;
        this.getAllBooking$ = this.actions$
            .ofType('APP_GETALL_BOOKING')
            .do(function (action) {
            _this.booking_service.getAllBooking(action.payload).subscribe(function (result) {
                if (result.message) {
                    // alert(result);
                    console.log("here");
                    console.log(result);
                    //creating state payload for next action
                    _this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_8__booking_actions__["b" /* AppBookingDetailSuccess */](result));
                }
            }, function (error) {
                console.log(error);
            });
        });
        this.getUserById = this.actions$
            .ofType('APP_GET_USER_BY_ID')
            .do(function (action) {
        });
        this.getUserByIdSuccess = this.actions$
            .ofType('APP_GET_USER_BY_ID_SUCCESS')
            .do(function (action) {
        });
        this.editUserById$ = this.actions$
            .ofType('APP_EDIT_USER_BY_ID')
            .do(function (action) {
            _this.booking_service.editUser(action.payload).subscribe(function (result) {
                if (result.message) {
                    console.log("here");
                    console.log(result);
                    // this.store.dispatch(new booking.AppEditUserByIdSuccess(result))  
                    _this.store.dispatch({ type: __WEBPACK_IMPORTED_MODULE_8__booking_actions__["a" /* ActionTypes */].APP_GETALL_BOOKING, payload: { currentPage: 1 } });
                }
            }, function (error) {
                console.log(error);
            });
        });
        this.editUserByIdSuccess = this.actions$
            .ofType('APP_EDIT_USER_BY_ID_SUCCESS')
            .do(function (action) {
        });
        this.blockUser$ = this.actions$
            .ofType('APP_BLOCK_USER_BY_ID')
            .do(function (action) {
            console.log("block user");
            _this.booking_service.blockUserById(action.payload)
                .subscribe(function (result) {
                console.log(result);
                if (result.message == 'Updated Successfully') {
                }
            }, function (error) {
                console.log(error.message);
            });
        });
        this.bookingDetailSuccess = this.actions$
            .ofType('APP_BOOKING_DETAIL_SUCCESS')
            .do(function (action) {
            //console.log("Running inside APP_BOOKING_DETAIL_SUCCESS ")
            //this.store.dispatch(new booking.AppBookingDetailAfterSuccess())
        });
        this.additem = this.actions$
            .ofType('APP_ADD_ITEM')
            .do(function (action) {
            _this.booking_service.additem(action.payload)
                .subscribe(function (result) {
                //console.log("is result...............",result)
                if (result.message) {
                    //    const activeModal = this.modalService.open(CustomerModal, {size: 'lg'});
                    //  activeModal.componentInstance.modalHeader = 'Large Modal';
                    // this.store.dispatch(new booking.AppAddItemSuccess(result))
                    //  this.store.dispatch({
                    //         type: booking.ActionTypes.APP_GETALL_BOOKING, 
                    //       });
                    _this.store.dispatch({ type: __WEBPACK_IMPORTED_MODULE_8__booking_actions__["a" /* ActionTypes */].APP_GETALL_BOOKING, payload: { currentPage: 1 } });
                }
            }, function (error) {
                console.log(error);
            });
        });
        this.additemsuccess = this.actions$
            .ofType('APP_ADD_ITEM_SUCCESS')
            .do(function (action) {
            console.log(action);
        });
        this.deleteItemRecordConfirm$ = this.actions$
            .ofType('DELETE_ITEM_RECORD_CONFIRM')
            .do(function (action) {
        });
        // SOFT DELETE CUSTOMER RECORD
        this.deleteItemRecord$ = this.actions$
            .ofType('DELETE_ITEM_RECORD')
            .do(function (action) {
            // let action = storeState[0];
            // let state = storeState[1].customer;
            _this.booking_service
                .deleteCustomerRecord(action.payload)
                .subscribe(function (result) {
                if (result.message) {
                    // console.log("ashi....",action.payload)
                    _this.store.dispatch({ type: __WEBPACK_IMPORTED_MODULE_8__booking_actions__["a" /* ActionTypes */].APP_GETALL_BOOKING, payload: { currentPage: 1 } });
                    // this.store.dispatch({
                    //   type: booking.ActionTypes.APP_GETALL_BOOKING, 
                    // });
                }
            }, function (error) {
                console.log(error);
            });
        });
        this.addid = this.actions$
            .ofType('ADD_ID_RECORD')
            .do(function (action) {
            console.log(action);
            _this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_8__booking_actions__["c" /* AppIdRecordSuccess */]());
        });
        this.addidsuccess = this.actions$
            .ofType('ADD_ID_RECORD_SUCCESS')
            .do(function (action) {
            console.log(action);
        });
    }
    return BookingEffects;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], BookingEffects.prototype, "getAllBooking$", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], BookingEffects.prototype, "getUserById", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], BookingEffects.prototype, "getUserByIdSuccess", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], BookingEffects.prototype, "editUserById$", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], BookingEffects.prototype, "editUserByIdSuccess", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], BookingEffects.prototype, "blockUser$", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], BookingEffects.prototype, "bookingDetailSuccess", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], BookingEffects.prototype, "additem", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], BookingEffects.prototype, "additemsuccess", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], BookingEffects.prototype, "deleteItemRecordConfirm$", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], BookingEffects.prototype, "deleteItemRecord$", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], BookingEffects.prototype, "addid", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], BookingEffects.prototype, "addidsuccess", void 0);
BookingEffects = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["c" /* Actions */],
        __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["Store"],
        __WEBPACK_IMPORTED_MODULE_5__angular_router__["Router"],
        __WEBPACK_IMPORTED_MODULE_4__services_bookingService_booking_service__["a" /* booking_service */],
        __WEBPACK_IMPORTED_MODULE_6_ngx_toastr__["b" /* ToastrService */],
        __WEBPACK_IMPORTED_MODULE_7__ng_bootstrap_ng_bootstrap__["d" /* NgbModal */]])
], BookingEffects);



/***/ }),
/* 349 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return booking; });
var initialState = {
    bookings: null,
    count: 0,
    currentPage: 0,
    limit: 0,
    active: null,
    activeCustomer: null,
};
var booking = function (state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case 'APP_GETALL_BOOKING':
            return Object.assign({}, state, action.payload);
        case 'APP_BOOKING_DETAIL':
            return Object.assign({}, state, action.payload);
        case 'APP_BOOKING_DETAIL_SUCCESS':
            return Object.assign({}, state, { booking: action.payload });
        case 'APP_BOOKING_DETAIL_AFTER_SUCCESS':
            return Object.assign({}, state, { bookingDevelopment: action.payload });
        case 'APP_GET_USER_BY_ID':
            return Object.assign({}, state, { activeCustomer: action.payload });
        case 'APP_GET_USER_BY_ID_SUCCESS':
            return Object.assign({}, state, { activeCustomer: action.payload });
        case 'APP_EDIT_USER_BY_ID':
            return Object.assign({}, state, action.payload);
        case 'APP_EDIT_USER_BY_ID_SUCCESS':
            return Object.assign({}, state, action.payload);
        case 'APP_BLOCK_USER_BY_ID':
            return Object.assign({}, state, { active: action.payload });
        case 'APP_ADD_ITEM':
            return Object.assign({}, state);
        case 'APP_ADD_ITEM_SUCCESS':
            return Object.assign({}, action.payload);
        case 'DELETE_ITEM_RECORD_CONFIRM':
            return Object.assign({}, state, action.payload);
        case 'DELETE_ITEM_RECORD':
            return Object.assign({}, state, action.payload);
        case 'ADD_ID_RECORD':
            return Object.assign({}, state, { idrecord: action.payload });
        case 'ADD_ID_RECORD_SUCCESS':
            return Object.assign({}, state, action.payload);
        default:
            return state;
    }
};


/***/ }),
/* 350 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__booking_actions__ = __webpack_require__(92);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__booking_effects__ = __webpack_require__(348);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_1__booking_effects__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__booking_reducers__ = __webpack_require__(349);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_2__booking_reducers__["a"]; });





/***/ }),
/* 351 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_effects__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_competitionService_competition_service__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_toastr__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__competition_actions__ = __webpack_require__(96);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CompetitionEffects; });







var types = ['success', 'error', 'info', 'warning'];

var CompetitionEffects = (function () {
    function CompetitionEffects(actions$, store, router, competition_service, toastrService) {
        var _this = this;
        this.actions$ = actions$;
        this.store = store;
        this.router = router;
        this.competition_service = competition_service;
        this.toastrService = toastrService;
        this.getAllCompetitions$ = this.actions$
            .ofType('APP_GETALL_COMPETITIONS')
            .do(function (action) {
            _this.competition_service.getAllCompetitions(action.payload).subscribe(function (result) {
                if (result.message) {
                    console.log("here");
                    console.log(result);
                    //         const activeModal = this.modalService.open(CustomerModal, {size: 'lg'});
                    // activeModal.componentInstance.modalHeader = 'Large Modal';
                    _this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_7__competition_actions__["b" /* AppCompetitionDetailSuccess */](result));
                }
            }, function (error) {
                console.log(error);
            });
        });
        this.createcompetition = this.actions$
            .ofType('APP_CREATE_COMPETITION')
            .do(function (action) {
            _this.competition_service.createcompetition(action.payload)
                .subscribe(function (result) {
                //console.log("is result...............",result)
                if (result.message === 'Success') {
                    //    const activeModal = this.modalService.open(CustomerModal, {size: 'lg'});
                    //  activeModal.componentInstance.modalHeader = 'Large Modal';
                    // this.store.dispatch(new competition.AppCreateCompetitionSuccess(result))
                }
            }, function (error) {
                console.log(error);
            });
        });
        this.competition = this.actions$
            .ofType('APP_CREATE_COMPETITION_SUCCESS')
            .do(function (action) {
            console.log(action);
        });
        this.competitionDetailSuccess = this.actions$
            .ofType('APP_COMPETITION_DETAIL_SUCCESS')
            .do(function (action) {
            console.log("vhgv");
        });
    }
    return CompetitionEffects;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], CompetitionEffects.prototype, "getAllCompetitions$", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], CompetitionEffects.prototype, "createcompetition", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], CompetitionEffects.prototype, "competition", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], CompetitionEffects.prototype, "competitionDetailSuccess", void 0);
CompetitionEffects = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["c" /* Actions */],
        __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["Store"],
        __WEBPACK_IMPORTED_MODULE_5__angular_router__["Router"],
        __WEBPACK_IMPORTED_MODULE_4__services_competitionService_competition_service__["a" /* competition_service */],
        __WEBPACK_IMPORTED_MODULE_6_ngx_toastr__["b" /* ToastrService */]])
], CompetitionEffects);



/***/ }),
/* 352 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return competition; });
var initialState = {
    bookings: null,
    count: 0,
    currentPage: 0,
    limit: 0,
    active: null
};
var competition = function (state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case 'APP_GETALL_COMPETITIONS':
            return Object.assign({}, state, action.payload);
        case 'APP_COMPETITION_DETAIL':
            return Object.assign({}, state);
        case 'APP_CREATE_COMPETITION':
            return Object.assign({}, state);
        case 'APP_COMPETITION_DETAIL_SUCCESS':
            return Object.assign({}, state, action.payload);
        case 'APP_BOOKING_DETAIL_AFTER_SUCCESS':
            return Object.assign({}, state, { bookingDevelopment: action.payload });
        case 'APP_GETALL_BOOKING_BY_ID':
            return Object.assign({}, state, { activeBooking: action.payload });
        case 'APP_GETALL_BOOKING_BY_ID_SUCCESS':
            return Object.assign({}, state, { bookingId: action.payload });
        case 'APP_CANCEL_BOOKING_BY_ID':
            return Object.assign({}, state, { activeBooking: action.payload });
        case 'APP_CANCEL_BOOKING_BY_ID_SUCCESS':
            return Object.assign({}, state, { activeBooking: action.payload });
        case 'APP_EDIT_USER_BY_ID':
            return Object.assign({}, state);
        case 'APP_EDIT_USER_BY_ID_SUCCESS':
            return Object.assign({}, state);
        case 'APP_BLOCK_USER_BY_ID':
            return Object.assign({}, state, { active: action.payload });
        default:
            return state;
    }
};


/***/ }),
/* 353 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__competition_actions__ = __webpack_require__(96);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__competition_effects__ = __webpack_require__(351);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_1__competition_effects__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__competition_reducers__ = __webpack_require__(352);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_2__competition_reducers__["a"]; });





/***/ }),
/* 354 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_effects__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_dashboardService_dashboard_service__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__dashboard_actions__ = __webpack_require__(100);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardEffects; });







var DashboardEffects = (function () {
    function DashboardEffects(actions$, store, router, dashboard_service) {
        var _this = this;
        this.actions$ = actions$;
        this.store = store;
        this.router = router;
        this.dashboard_service = dashboard_service;
        this.dashboardCount = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_6__dashboard_actions__["a" /* ActionTypes */].GET_DASHBOARD_COUNT)
            .do(function (action) {
            _this.dashboard_service.getDashBoardData()
                .subscribe(function (result) {
                if (result.message == 'Success') {
                    //this.store.dispatch(new dashBoard.AppGetDashBoardCountSuccess(result))
                }
            }, function (error) {
                console.log(error.message);
            });
        });
        this.dashboardCountSuccess = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_6__dashboard_actions__["a" /* ActionTypes */].GET_DASHBOARD_COUNT_SUCCESS)
            .do(function (action) {
        });
        this.userreportmonthly = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_6__dashboard_actions__["a" /* ActionTypes */].GET_USER_REPORT_MONTHLY)
            .do(function (action) {
            _this.dashboard_service.getUserReportMonthly(action.payload)
                .subscribe(function (result) {
                console.log(result);
                if (result.message == 'Success') {
                    _this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_6__dashboard_actions__["b" /* AppGetUserReportMonthlySuccess */](result));
                }
            }, function (error) {
                console.log(error.message);
            });
        });
        this.userreportmonthlySuccess = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_6__dashboard_actions__["a" /* ActionTypes */].GET_USER_REPORT_MONTHLY_SUCCESS)
            .do(function (action) {
        });
    }
    return DashboardEffects;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], DashboardEffects.prototype, "dashboardCount", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], DashboardEffects.prototype, "dashboardCountSuccess", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], DashboardEffects.prototype, "userreportmonthly", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"])
], DashboardEffects.prototype, "userreportmonthlySuccess", void 0);
DashboardEffects = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["c" /* Actions */],
        __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["Store"],
        __WEBPACK_IMPORTED_MODULE_4__angular_router__["Router"],
        __WEBPACK_IMPORTED_MODULE_5__services_dashboardService_dashboard_service__["a" /* dashboard_service */]])
], DashboardEffects);



/***/ }),
/* 355 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return dashBoard; });
var initialState = {
    dashBoardCount: null,
    userreportmonthly: null
};
var dashBoard = function (state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case 'GET_DASHBOARD_COUNT':
            return Object.assign({}, state);
        case 'GET_DASHBOARD_COUNT_SUCCESS':
            return Object.assign({}, state, { dashBoardCount: action.payload });
        case 'GET_USER_REPORT_MONTHLY':
            return Object.assign({}, state);
        case 'GET_USER_REPORT_MONTHLY_SUCCESS':
            return Object.assign({}, state, { userreportmonthly: action.payload });
        default:
            return state;
    }
};


/***/ }),
/* 356 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__dashboard_actions__ = __webpack_require__(100);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__dashboard_effects__ = __webpack_require__(354);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_1__dashboard_effects__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dashboard_reducers__ = __webpack_require__(355);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_2__dashboard_reducers__["a"]; });





/***/ }),
/* 357 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PAGES_MENU; });
var PAGES_MENU = [
    {
        path: 'pages',
        children: [
            {
                path: 'dashboard',
                data: {
                    menu: {
                        title: 'general.menu.dashboard',
                        icon: 'ion-android-home',
                        selected: false,
                        expanded: false,
                        order: 0
                    }
                }
            },
            {
                path: 'Booking',
                data: {
                    menu: {
                        title: 'general.menu.Bookings',
                        icon: 'ion-android-person',
                        selected: false,
                        expanded: false,
                        order: 0
                    }
                },
            },
            {
                path: 'Competition',
                data: {
                    menu: {
                        title: 'general.menu.Competition',
                        icon: 'ion-android-person',
                        selected: false,
                        expanded: false,
                        order: 100,
                    }
                },
            },
        ]
    }
];


/***/ }),
/* 358 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_routing__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__theme_nga_module__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_translation_module__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ng_bootstrap_ng_bootstrap__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_component__ = __webpack_require__(101);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PagesModule; });







var PagesModule = (function () {
    function PagesModule() {
    }
    return PagesModule;
}());
PagesModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [__WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"], __WEBPACK_IMPORTED_MODULE_3__theme_nga_module__["a" /* NgaModule */], __WEBPACK_IMPORTED_MODULE_4__app_translation_module__["a" /* AppTranslationModule */], __WEBPACK_IMPORTED_MODULE_2__pages_routing__["a" /* routing */], __WEBPACK_IMPORTED_MODULE_5__ng_bootstrap_ng_bootstrap__["a" /* NgbModule */]],
        declarations: [__WEBPACK_IMPORTED_MODULE_6__pages_component__["a" /* Pages */]]
    })
], PagesModule);



/***/ }),
/* 359 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_component__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__theme_services_authService_authGuard_service__ = __webpack_require__(109);
/* unused harmony export routes */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routing; });



// noinspection TypeScriptValidateTypes
// export function loadChildren(path) { return System.import(path); };
var routes = [
    // {
    //   path: 'login',
    //   loadChildren: 'app/pages/login/login.module#LoginModule'
    // },
    // {
    //   path: 'register',
    //   loadChildren: 'app/pages/register/register.module#RegisterModule'
    // },
    {
        path: 'pages',
        component: __WEBPACK_IMPORTED_MODULE_1__pages_component__["a" /* Pages */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_2__theme_services_authService_authGuard_service__["a" /* AuthGuard */]],
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            { path: 'dashboard', loadChildren: function() { return __webpack_require__.e/* import() */(1).then(__webpack_require__.bind(null, 712))  .then( function(module) { return module['DashboardModule']; } ); } },
            { path: 'Booking', loadChildren: function() { return __webpack_require__.e/* import() */(2).then(__webpack_require__.bind(null, 710))  .then( function(module) { return module['BookingsModule']; } ); } },
            { path: 'Competition', loadChildren: function() { return __webpack_require__.e/* import() */(0).then(__webpack_require__.bind(null, 711))  .then( function(module) { return module['CompetitionsModule']; } ); } },
        ]
    }
];
var routing = __WEBPACK_IMPORTED_MODULE_0__angular_router__["RouterModule"].forChild(routes);


/***/ }),
/* 360 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_state_auth_actions__ = __webpack_require__(21);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LogoutComponent; });



var LogoutComponent = (function () {
    function LogoutComponent(store) {
        this.store = store;
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__auth_state_auth_actions__["b" /* AuthLogoutAction */]());
    }
    return LogoutComponent;
}());
LogoutComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: "",
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["Store"]])
], LogoutComponent);



/***/ }),
/* 361 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(8);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RecoverComponent; });


//import { AuthService } from '../../auth.service'
var RecoverComponent = (function () {
    function RecoverComponent(store) {
        var _this = this;
        this.store = store;
        this.user = {
            realm: 'system',
            email: '',
        };
        this.realms = [];
        this.store
            .select('app')
            .subscribe(function (res) { return _this.realms = res.domains; });
    }
    RecoverComponent.prototype.recover = function () {
        // this.authService.recover(this.user)
    };
    return RecoverComponent;
}());
RecoverComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-password-recover',
        template: "\n    \n  "
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["Store"]])
], RecoverComponent);



/***/ }),
/* 362 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__forgotpassword_forgot_password_component__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngrx_store__ = __webpack_require__(8);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgotModal; });





var ForgotModal = (function () {
    // modalHeader: string;
    // modalContent: string = `Lorem ipsum dolor sit amet,
    //  consectetuer adipiscing elit, sed diam nonummy
    //  nibh euismod tincidunt ut laoreet dolore magna aliquam
    //  erat volutpat. Ut wisi enim ad minim veniam, quis
    //  nostrud exerci tation ullamcorper suscipit lobortis
    //  nisl ut aliquip ex ea commodo consequat.`;
    function ForgotModal(fb, activeModal, forgot, store) {
        this.activeModal = activeModal;
        this.forgot = forgot;
        this.store = store;
        this.uploadFlag = false;
        this.customerData = [];
        this.page = 1;
        this.limit = 4;
    }
    //          console.log(res)
    //           if(res.message=="Success")
    //           {
    //   
    //   //  fd.append('name','pratosh');
    //   //  fd.append('password','pratosh');
    //   //  fd.append('email','pratosh');
    //   //  fd.append('primaryMobile','pratosh');
    //    for(let prop in data){
    //     // console.log('prop-->',prop,'val--->',data[prop])
    //     fd.append(prop,data[prop]);
    //    }
    //  this.baThemeSpinner.show();
    //  //console.log(auth.ActionTypes.AUTH_LOGIN)
    ForgotModal.prototype.ngOnInit = function () {
    };
    ForgotModal.prototype.closeModal = function () {
        this.activeModal.close();
    };
    return ForgotModal;
}());
ForgotModal = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'add-service-modal',
        styles: [(__webpack_require__(501))],
        template: __webpack_require__(519),
        providers: [__WEBPACK_IMPORTED_MODULE_2__forgotpassword_forgot_password_component__["a" /* ForgotPassword */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"], __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["c" /* NgbActiveModal */], __WEBPACK_IMPORTED_MODULE_2__forgotpassword_forgot_password_component__["a" /* ForgotPassword */], __WEBPACK_IMPORTED_MODULE_4__ngrx_store__["Store"]])
], ForgotModal);



/***/ }),
/* 363 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PublicPages; });

var PublicPages = (function () {
    function PublicPages() {
    }
    return PublicPages;
}());
PublicPages = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'publicpages',
        template: "\n   \n    \n        <router-outlet></router-outlet>\n     \n    "
    }),
    __metadata("design:paramtypes", [])
], PublicPages);



/***/ }),
/* 364 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__public_pages_routes__ = __webpack_require__(365);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__public_pages_component__ = __webpack_require__(363);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_translation_module__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_router__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_forgotpassword_forgot_password_component__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_login_login_component__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_logout_logout_component__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_recover_recover_component__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_register_register_component__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_reset_reset_component__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_ui_components_modals_forgot_modal_forgot_modal_component__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ng_bootstrap_ng_bootstrap__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__theme_nga_module__ = __webpack_require__(35);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PublicPageModule; });







//import { ColmenaUiModule } from '@colmena/colmena-angular-ui'









var PublicPageModule = (function () {
    function PublicPageModule() {
    }
    return PublicPageModule;
}());
PublicPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormsModule"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["ReactiveFormsModule"],
            __WEBPACK_IMPORTED_MODULE_6__angular_router__["RouterModule"],
            __WEBPACK_IMPORTED_MODULE_3__public_pages_routes__["a" /* routing */],
            __WEBPACK_IMPORTED_MODULE_5__app_translation_module__["a" /* AppTranslationModule */],
            __WEBPACK_IMPORTED_MODULE_15__theme_nga_module__["a" /* NgaModule */],
            __WEBPACK_IMPORTED_MODULE_3__public_pages_routes__["a" /* routing */],
            __WEBPACK_IMPORTED_MODULE_14__ng_bootstrap_ng_bootstrap__["b" /* NgbModalModule */],
        ],
        declarations: [
            __WEBPACK_IMPORTED_MODULE_4__public_pages_component__["a" /* PublicPages */],
            __WEBPACK_IMPORTED_MODULE_8__components_login_login_component__["a" /* Login */],
            __WEBPACK_IMPORTED_MODULE_9__components_logout_logout_component__["a" /* LogoutComponent */],
            __WEBPACK_IMPORTED_MODULE_10__components_recover_recover_component__["a" /* RecoverComponent */],
            __WEBPACK_IMPORTED_MODULE_11__components_register_register_component__["a" /* Register */],
            //NgbActiveModal,
            __WEBPACK_IMPORTED_MODULE_12__components_reset_reset_component__["a" /* ResetComponent */],
            __WEBPACK_IMPORTED_MODULE_7__components_forgotpassword_forgot_password_component__["a" /* ForgotPassword */],
            __WEBPACK_IMPORTED_MODULE_13__components_ui_components_modals_forgot_modal_forgot_modal_component__["a" /* ForgotModal */]
        ],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_13__components_ui_components_modals_forgot_modal_forgot_modal_component__["a" /* ForgotModal */]
        ],
    })
], PublicPageModule);



/***/ }),
/* 365 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_login_login_component__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_register_register_component__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_forgotpassword_forgot_password_component__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_reset_reset_component__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__auth_service_authGuardPublicPages_authGuardPublic_service__ = __webpack_require__(98);
/* unused harmony export routes */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routing; });






var routes = [
    {
        path: 'login',
        component: __WEBPACK_IMPORTED_MODULE_1__components_login_login_component__["a" /* Login */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_5__auth_service_authGuardPublicPages_authGuardPublic_service__["a" /* AuthGuard */]]
    },
    {
        path: 'register',
        component: __WEBPACK_IMPORTED_MODULE_2__components_register_register_component__["a" /* Register */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_5__auth_service_authGuardPublicPages_authGuardPublic_service__["a" /* AuthGuard */]]
    },
    {
        path: 'forgot-password',
        component: __WEBPACK_IMPORTED_MODULE_3__components_forgotpassword_forgot_password_component__["a" /* ForgotPassword */],
    },
    {
        path: 'resetPassword/:id',
        component: __WEBPACK_IMPORTED_MODULE_4__components_reset_reset_component__["a" /* ResetComponent */],
    }
    //   children: [
    //   	{ path: '', redirectTo: 'login', pathMatch: 'full' },
    //     { path: 'login', loadChildren: 'app/auth/components/login/login.module#LoginModule' },
    //     { path: 'logout', component: LogoutComponent },
    //     { path: 'password-recover', component: RecoverComponent },
    //     { path: 'password-reset', component: ResetComponent },
    //     { path: 'register', component: Register },
    //   ],}
];
var routing = __WEBPACK_IMPORTED_MODULE_0__angular_router__["RouterModule"].forChild(routes);


/***/ }),
/* 366 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__environment_environment__ = __webpack_require__(38);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return common_service; });


var common_service = (function () {
    function common_service() {
        this.url = __WEBPACK_IMPORTED_MODULE_1__environment_environment__["a" /* environment */].APP.API_URL;
        var token = localStorage.getItem('token');
        var header_token = 'Bearer ' + token;
    }
    return common_service;
}());
common_service = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], common_service);



/***/ }),
/* 367 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_effects__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_actions__ = __webpack_require__(108);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppEffects; });





// import { booking_service } from '../services/bookingService/booking.service';
// import { customer_service } from '../services/customerService/customer.service';
// import { serviceprovider_service } from '../services/serviceproviderService/serviceprovider.service';
// import { driver_service } from '../services/driverService/driver.service';

var AppEffects = (function () {
    function AppEffects(actions$, router, store) {
        var _this = this;
        this.actions$ = actions$;
        this.router = router;
        this.store = store;
        this.redirectDashboard = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_5__app_actions__["a" /* ActionTypes */].APP_REDIRECT_DASHBOARD)
            .do(function () { return _this.router.navigate(['/', 'dashboard']); });
        this.redirectLogin = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_5__app_actions__["a" /* ActionTypes */].APP_REDIRECT_LOGIN)
            .do(function () { return _this.router.navigate(['/', 'login']); });
        this.redirectRouter = this.actions$
            .ofType(__WEBPACK_IMPORTED_MODULE_5__app_actions__["a" /* ActionTypes */].APP_REDIRECT_ROUTER)
            .do(function () { return _this.router.navigate(['/', 'router']); });
        this.appRefresh$ = this.actions$
            .ofType('APP_RELOAD')
            .do(function () { return window.location.reload(); });
        this.getAllUsers$ = this.actions$
            .ofType('APP_GETALL_USERS')
            .do(function () { console.log("hey users"); });
        this.blockUser$ = this.actions$
            .ofType('APP_BLOCK_USER_BY_ID')
            .do(function (action) {
            console.log("block user");
            //   this.customer_service.blockUserById(action.payload)
            // .subscribe((result) => {
            //              console.log(result)
            //               if(result.message == 'Success')
            //               {
            //                  alert("blocked")
            //               }
            //               }
            //             , (error) => {
            //               console.log(error.message)
            //             });
        });
        this.deleteUser$ = this.actions$
            .ofType('APP_DELETE_USER_BY_ID')
            .do(function (action) {
            //   this.customer_service.deleteUserById(action.payload)
            // .subscribe((result) => {
            //              console.log(result)
            //               if(result.message == 'Success')
            //               {
            //                //this.store.dispatch(new app.AppDeleteUserByIdSuccess(result))
            //                 console.log("deleted")
            //               }
            // }
            //             , (error) => {
            //               console.log(error.message)
            //             });
        });
        this.registerservice = this.actions$
            .ofType('APP_REGISTER_SERVICE')
            .do(function (action) {
            // this.serviceprovider_service.getServiceRegister(action.payload)
            // .subscribe((result) => {
            //           //console.log("id result...............",result)
            //           if(result.message == 'Success')
            //           {
            //             console.log("created a new service");
            //           }
            //           }
            //         , (error) => {
            //           console.log(error.message)
            //         }
            //       );
        });
        this.registercustomer = this.actions$
            .ofType('APP_REGISTER_CUSTOMER')
            .do(function (action) {
            // this.customer_service.getCustomerRegister(action.payload)
            // .subscribe((result) => {
            //           //console.log("id result...............",result)
            //           if(result.message == 'Success')
            //           {
            //             console.log("created a new customer");
            //           }
            //           }
            //         , (error) => {
            //           console.log(error.message)
            //         }
            //       );
        });
        this.bookingDetailByIdSuccess = this.actions$
            .ofType('APP_GETALL_BOOKING_BY_ID_SUCCESS')
            .do(function () { });
        this.getAllUser = this.actions$
            .ofType('APP_GETTALL_USER')
            .do(function () { });
        this.editUserByIdSuccess = this.actions$
            .ofType('APP_EDIT_USER_BY_ID_SUCCESS')
            .do(function (action) {
            // this.customer_service.editcustomer(action.payload)
            //   .subscribe((result) => {
            //             //console.log("id result...............",result)
            //             if(result.message == 'Success')
            //             {
            //               console.log("changed user")
            //             }
            //             }
            //           , (error) => {
            //             console.log(error.message)
            //           }
            //         );
        });
        this.editServiceByIdSuccess = this.actions$
            .ofType('APP_EDIT_SERVICE_BY_ID_SUCCESS')
            .do(function (action) {
            // this.serviceprovider_service.editservice(action.payload)
            //   .subscribe((result) => {
            //             // console.log("id result...............",result)
            //             if(result.message == 'Success')
            //             {
            //               console.log("changed service")
            //             }
            //             }
            //           , (error) => {
            //             console.log(error.message)
            //           }
            //         );
        });
        this.getAllUserSuccess = this.actions$
            .ofType('APP_GETTALL_USER_SUCCESS')
            .do(function (action) {
            //  console.log("payload",action.payload)
            // this.customer_service.getAllUser(action.payload)
            // .subscribe((result) => {
            //   //console.log("is result...............",result)
            //     if(result.message === 'Success'){
            //       // window.localStorage.setItem('token', JSON.stringify(action.payload))
            //               // localStorage.setItem('token',token);
            //       //header_token = 'Bearer'+ " " +this.token
            //               this.store.dispatch(new app.AppGetAllUserAfterSuccess(result))
            //     }
            //         }
            //       , (error) => {
            //         console.log(error)
            //       }
            //     );
        });
        this.editUserById = this.actions$
            .ofType('APP_EDIT_USER_BY_ID')
            .do(function (action) {
            // this.customer_service.getAllUserById(action.payload.id)
            //     .subscribe((result) => {
            //       //console.log("is result...............",result)
            //          if(result.message === 'Success'){
            // console.log("edit")
            // this.store.dispatch(new app.AppGetAllUserByIdSuccess(result))
            // this.router.navigate(['pages/Customers/editdetails']);
            //          }
            //     }
            //           , (error) => {
            //             console.log(error)
            //           }
            //         );
        });
        this.editServiceById = this.actions$
            .ofType('APP_EDIT_SERVICE_BY_ID')
            .do(function (action) {
            // this.customer_service.getAllUserById(action.payload.id)
            //     .subscribe((result) => {
            //       //console.log("is result...............",result)
            //          if(result.message === 'Success'){
            // console.log("edit")
            // this.store.dispatch(new app.AppGetAllUserByIdSuccess(result))
            // this.router.navigate(['pages/serviceProvider/editservice']);
            //          }
            //     }
            //           , (error) => {
            //             console.log(error)
            //           }
            //         );
        });
        this.getAllUserAfterSuccess = this.actions$
            .ofType('APP_GETTALL_USER_AFTER_SUCCESS')
            .do(function () { });
        this.getAllUserById = this.actions$
            .ofType('APP_GETTALL_USER_BY_ID')
            .do(function (action) {
            // this.customer_service.getAllUserById(action.payload.id)
            //  .subscribe((result) => {
            //    //console.log("is result...............",result)
            //       if(result.message === 'Success'){
            //                  //console.log(result.data.role[0])
            //                 this.router.navigate(['pages/'+action.payload.userType+'/byIdDetails']);
            //                 this.store.dispatch(new app.AppGetAllUserByIdSuccess(result))
            //       }
            //          }
            //        , (error) => {
            //          console.log(error)
            //        }
            //      );
        });
        this.getAllUserAfterSuccessById = this.actions$
            .ofType('APP_GETTALL_USER_BY_ID_SUCCESS')
            .do(function () { });
        this.deleteUserByIdSuccess = this.actions$
            .ofType('APP_DELETE_USER_BY_ID_SUCCESS')
            .do(function () { console.log("d"); });
    }
    return AppEffects;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"])
], AppEffects.prototype, "redirectDashboard", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"])
], AppEffects.prototype, "redirectLogin", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"])
], AppEffects.prototype, "redirectRouter", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], AppEffects.prototype, "appRefresh$", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], AppEffects.prototype, "getAllUsers$", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"])
], AppEffects.prototype, "blockUser$", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"])
], AppEffects.prototype, "deleteUser$", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"])
], AppEffects.prototype, "registerservice", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"])
], AppEffects.prototype, "registercustomer", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], AppEffects.prototype, "bookingDetailByIdSuccess", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], AppEffects.prototype, "getAllUser", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], AppEffects.prototype, "editUserByIdSuccess", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], AppEffects.prototype, "editServiceByIdSuccess", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], AppEffects.prototype, "getAllUserSuccess", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], AppEffects.prototype, "editUserById", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], AppEffects.prototype, "editServiceById", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], AppEffects.prototype, "getAllUserAfterSuccess", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], AppEffects.prototype, "getAllUserById", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], AppEffects.prototype, "getAllUserAfterSuccessById", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["b" /* Effect */])({ dispatch: false }),
    __metadata("design:type", Object)
], AppEffects.prototype, "deleteUserByIdSuccess", void 0);
AppEffects = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngrx_effects__["c" /* Actions */],
        __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"],
        __WEBPACK_IMPORTED_MODULE_3__ngrx_store__["Store"]])
], AppEffects);



/***/ }),
/* 368 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_lodash__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return app; });

var initialState = {
    activeDomain: null,
    domains: [],
    settings: {},
    contentDashboard: [],
    systemDashboard: [],
    blocked: false
};
var app = function (state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case 'APP_DEVELOPMENT_ENABLE':
            return Object.assign({}, state, { development: true });
        case 'APP_SETTING_ADD':
            var settings = state.settings;
            settings[action.payload.key] = action.payload.value;
            return Object.assign({}, state, { settings: settings });
        case 'APP_CONTENT_DASHBOARD':
            return Object.assign({}, state, {
                contentDashboard: __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0_lodash__["sortBy"])(state.contentDashboard.concat([action.payload]), ['weight'])
            });
        case 'APP_SYSTEM_DASHBOARD':
            return Object.assign({}, state, {
                systemDashboard: __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0_lodash__["sortBy"])(state.systemDashboard.concat([action.payload]), ['weight'])
            });
        case 'APP_GETALL_BOOKING':
            return Object.assign({}, state, { bookingType: action.payload, userType: null });
        case 'APP_GETALL_USERS':
            return Object.assign({}, state);
        case 'APP_BOOKING_DETAIL':
            return Object.assign({}, state);
        case 'APP_BOOKING_DETAIL_SUCCESS':
            return Object.assign({}, state, { bookingDetails: action.payload, development: true });
        case 'APP_GETALL_BOOKING_BY_ID':
            return Object.assign({}, state);
        case 'APP_GETALL_BOOKING_BY_ID_SUCCESS':
            return Object.assign({}, state, { bookingId: action.payload });
        case 'APP_GETTALL_USER':
            return Object.assign({}, state, { userType: action.payload, bookingType: null });
        case 'APP_GETTALL_USER_SUCCESS':
            return Object.assign({}, state, { development: false });
        case 'APP_GETTALL_USER_AFTER_SUCCESS':
            return Object.assign({}, state, { userDetail: action.payload });
        case 'APP_GETTALL_USER_BY_ID':
            return Object.assign({}, state);
        case 'APP_BLOCK_USER_BY_ID':
            return Object.assign({}, state, { blocked: true });
        case 'APP_EDIT_USER_BY_ID':
            return Object.assign({}, state);
        case 'APP_EDIT_USER_BY_ID_SUCCESS':
            return Object.assign({}, state);
        case 'APP_EDIT_SERVICE_BY_ID':
            return Object.assign({}, state);
        case 'APP_EDIT_SERVICE_BY_ID_SUCCESS':
            return Object.assign({}, state);
        case 'APP_DELETE_USER_BY_ID':
            return Object.assign({}, state);
        case 'APP_DELETE_USER_BY_ID_SUCCESS':
            return Object.assign({});
        case 'APP_GETTALL_USER_BY_ID_SUCCESS':
            return Object.assign({}, state, { userById: action.payload });
        case 'APP_CANCEL_BOOKING_BY_ID':
            return Object.assign({}, state, { userById: action.payload });
        case 'APP_CANCEL_BOOKING_BY_ID_SUCCESS':
            return Object.assign({}, state, { userById: action.payload });
        case 'APP_REGISTER_SERVICE':
            return Object.assign({}, state);
        case 'APP_REGISTER_CUSTOMER':
            return Object.assign({}, state);
        case 'APP_REGISTER_SERVICE_SUCCESS':
            return Object.assign({}, state);
        case 'APP_CANCEL_BOOKING_BY_ID':
            return Object.assign({}, state);
        case 'APP_CANCEL_BOOKING_BY_ID_SUCCESS':
            return Object.assign({}, state);
        case 'APP_REGISTER_DRIVER':
            return Object.assign({}, state);
        case 'APP_REGISTER_DRIVER_SUCCESS':
            return Object.assign({}, state);
        case 'APP_EDIT_DRIVER_BY_ID':
            return Object.assign({}, state);
        case 'APP_EDIT_DRIVER_BY_ID_SUCCESS':
            return Object.assign({}, state);
        default:
            return state;
    }
};


/***/ }),
/* 369 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_actions__ = __webpack_require__(108);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_effects__ = __webpack_require__(367);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_1__app_effects__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_reducers__ = __webpack_require__(368);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_2__app_reducers__["a"]; });





/***/ }),
/* 370 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__theme_services__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_amcharts3__ = __webpack_require__(459);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_amcharts3___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_amcharts3__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_amcharts3_amcharts_plugins_responsive_responsive_js__ = __webpack_require__(460);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_amcharts3_amcharts_plugins_responsive_responsive_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_amcharts3_amcharts_plugins_responsive_responsive_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_amcharts3_amcharts_serial_js__ = __webpack_require__(461);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_amcharts3_amcharts_serial_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_amcharts3_amcharts_serial_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ammap3__ = __webpack_require__(462);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ammap3___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ammap3__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ammap3_ammap_maps_js_worldLow__ = __webpack_require__(463);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ammap3_ammap_maps_js_worldLow___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_ammap3_ammap_maps_js_worldLow__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__baAmChartTheme_service__ = __webpack_require__(371);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_style_loader_baAmChart_scss__ = __webpack_require__(542);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_style_loader_baAmChart_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_style_loader_baAmChart_scss__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaAmChart; });









var BaAmChart = (function () {
    function BaAmChart(_baAmChartThemeService) {
        this._baAmChartThemeService = _baAmChartThemeService;
        this.onChartReady = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this._loadChartsLib();
    }
    BaAmChart.prototype.ngOnInit = function () {
        AmCharts.themes.blur = this._baAmChartThemeService.getTheme();
    };
    BaAmChart.prototype.ngAfterViewInit = function () {
        var chart = AmCharts.makeChart(this._selector.nativeElement, this.baAmChartConfiguration);
        this.onChartReady.emit(chart);
    };
    BaAmChart.prototype._loadChartsLib = function () {
        __WEBPACK_IMPORTED_MODULE_1__theme_services__["d" /* BaThemePreloader */].registerLoader(new Promise(function (resolve, reject) {
            var amChartsReadyMsg = 'AmCharts ready';
            if (AmCharts.isReady) {
                resolve(amChartsReadyMsg);
            }
            else {
                AmCharts.ready(function () {
                    resolve(amChartsReadyMsg);
                });
            }
        }));
    };
    return BaAmChart;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], BaAmChart.prototype, "baAmChartConfiguration", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], BaAmChart.prototype, "baAmChartClass", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], BaAmChart.prototype, "onChartReady", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('baAmChart'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
], BaAmChart.prototype, "_selector", void 0);
BaAmChart = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'ba-am-chart',
        template: __webpack_require__(520),
        providers: [__WEBPACK_IMPORTED_MODULE_7__baAmChartTheme_service__["a" /* BaAmChartThemeService */]],
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__baAmChartTheme_service__["a" /* BaAmChartThemeService */]])
], BaAmChart);



/***/ }),
/* 371 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__theme__ = __webpack_require__(17);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaAmChartThemeService; });


var BaAmChartThemeService = (function () {
    function BaAmChartThemeService(_baConfig) {
        this._baConfig = _baConfig;
    }
    BaAmChartThemeService.prototype.getTheme = function () {
        var layoutColors = this._baConfig.get().colors;
        return {
            themeName: "blur",
            AmChart: {
                color: layoutColors.defaultText,
                backgroundColor: "#FFFFFF"
            },
            AmCoordinateChart: {
                colors: [layoutColors.primary, layoutColors.danger, layoutColors.warning, layoutColors.success, layoutColors.info, layoutColors.primaryDark, layoutColors.warningLight, layoutColors.successDark, layoutColors.successLight, layoutColors.primaryLight, layoutColors.warningDark]
            },
            AmStockChart: {
                colors: [layoutColors.primary, layoutColors.danger, layoutColors.warning, layoutColors.success, layoutColors.info, layoutColors.primaryDark, layoutColors.warningLight, layoutColors.successDark, layoutColors.successLight, layoutColors.primaryLight, layoutColors.warningDark]
            },
            AmSlicedChart: {
                colors: [layoutColors.primary, layoutColors.danger, layoutColors.warning, layoutColors.success, layoutColors.info, layoutColors.primaryDark, layoutColors.warningLight, layoutColors.successDark, layoutColors.successLight, layoutColors.primaryLight, layoutColors.warningDark],
                labelTickColor: "#FFFFFF",
                labelTickAlpha: 0.3
            },
            AmRectangularChart: {
                zoomOutButtonColor: '#FFFFFF',
                zoomOutButtonRollOverAlpha: 0.15,
                zoomOutButtonImage: "lens.png"
            },
            AxisBase: {
                axisColor: "#FFFFFF",
                axisAlpha: 0.3,
                gridAlpha: 0.1,
                gridColor: "#FFFFFF"
            },
            ChartScrollbar: {
                backgroundColor: "#FFFFFF",
                backgroundAlpha: 0.12,
                graphFillAlpha: 0.5,
                graphLineAlpha: 0,
                selectedBackgroundColor: "#FFFFFF",
                selectedBackgroundAlpha: 0.4,
                gridAlpha: 0.15
            },
            ChartCursor: {
                cursorColor: layoutColors.primary,
                color: "#FFFFFF",
                cursorAlpha: 0.5
            },
            AmLegend: {
                color: "#FFFFFF"
            },
            AmGraph: {
                lineAlpha: 0.9
            },
            GaugeArrow: {
                color: "#FFFFFF",
                alpha: 0.8,
                nailAlpha: 0,
                innerRadius: "40%",
                nailRadius: 15,
                startWidth: 15,
                borderAlpha: 0.8,
                nailBorderAlpha: 0
            },
            GaugeAxis: {
                tickColor: "#FFFFFF",
                tickAlpha: 1,
                tickLength: 15,
                minorTickLength: 8,
                axisThickness: 3,
                axisColor: '#FFFFFF',
                axisAlpha: 1,
                bandAlpha: 0.8
            },
            TrendLine: {
                lineColor: layoutColors.danger,
                lineAlpha: 0.8
            },
            // ammap
            AreasSettings: {
                alpha: 0.8,
                color: layoutColors.info,
                colorSolid: layoutColors.primaryDark,
                unlistedAreasAlpha: 0.4,
                unlistedAreasColor: "#FFFFFF",
                outlineColor: "#FFFFFF",
                outlineAlpha: 0.5,
                outlineThickness: 0.5,
                rollOverColor: layoutColors.primary,
                rollOverOutlineColor: "#FFFFFF",
                selectedOutlineColor: "#FFFFFF",
                selectedColor: "#f15135",
                unlistedAreasOutlineColor: "#FFFFFF",
                unlistedAreasOutlineAlpha: 0.5
            },
            LinesSettings: {
                color: "#FFFFFF",
                alpha: 0.8
            },
            ImagesSettings: {
                alpha: 0.8,
                labelColor: "#FFFFFF",
                color: "#FFFFFF",
                labelRollOverColor: layoutColors.primaryDark
            },
            ZoomControl: {
                buttonFillAlpha: 0.8,
                buttonIconColor: layoutColors.default,
                buttonRollOverColor: layoutColors.danger,
                buttonFillColor: layoutColors.primaryDark,
                buttonBorderColor: layoutColors.primaryDark,
                buttonBorderAlpha: 0,
                buttonCornerRadius: 0,
                gridColor: "#FFFFFF",
                gridBackgroundColor: "#FFFFFF",
                buttonIconAlpha: 0.6,
                gridAlpha: 0.6,
                buttonSize: 20
            },
            SmallMap: {
                mapColor: "#000000",
                rectangleColor: layoutColors.danger,
                backgroundColor: "#FFFFFF",
                backgroundAlpha: 0.7,
                borderThickness: 1,
                borderAlpha: 0.8
            },
            // the defaults below are set using CSS syntax, you can use any existing css property
            // if you don't use Stock chart, you can delete lines below
            PeriodSelector: {
                color: "#FFFFFF"
            },
            PeriodButton: {
                color: "#FFFFFF",
                background: "transparent",
                opacity: 0.7,
                border: "1px solid rgba(0, 0, 0, .3)",
                MozBorderRadius: "5px",
                borderRadius: "5px",
                margin: "1px",
                outline: "none",
                boxSizing: "border-box"
            },
            PeriodButtonSelected: {
                color: "#FFFFFF",
                backgroundColor: "#b9cdf5",
                border: "1px solid rgba(0, 0, 0, .3)",
                MozBorderRadius: "5px",
                borderRadius: "5px",
                margin: "1px",
                outline: "none",
                opacity: 1,
                boxSizing: "border-box"
            },
            PeriodInputField: {
                color: "#FFFFFF",
                background: "transparent",
                border: "1px solid rgba(0, 0, 0, .3)",
                outline: "none"
            },
            DataSetSelector: {
                color: "#FFFFFF",
                selectedBackgroundColor: "#b9cdf5",
                rollOverBackgroundColor: "#a8b0e4"
            },
            DataSetCompareList: {
                color: "#FFFFFF",
                lineHeight: "100%",
                boxSizing: "initial",
                webkitBoxSizing: "initial",
                border: "1px solid rgba(0, 0, 0, .3)"
            },
            DataSetSelect: {
                border: "1px solid rgba(0, 0, 0, .3)",
                outline: "none"
            }
        };
    };
    return BaAmChartThemeService;
}());
BaAmChartThemeService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__theme__["c" /* BaThemeConfigProvider */]])
], BaAmChartThemeService);



/***/ }),
/* 372 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baAmChart_component__ = __webpack_require__(370);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baAmChart_component__["a"]; });



/***/ }),
/* 373 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(jQuery) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaBackTop; });

var BaBackTop = (function () {
    function BaBackTop() {
        this.position = 400;
        this.showSpeed = 500;
        this.moveSpeed = 1000;
    }
    BaBackTop.prototype.ngAfterViewInit = function () {
        this._onWindowScroll();
    };
    BaBackTop.prototype._onClick = function () {
        jQuery('html, body').animate({ scrollTop: 0 }, { duration: this.moveSpeed });
        return false;
    };
    BaBackTop.prototype._onWindowScroll = function () {
        var el = this._selector.nativeElement;
        window.scrollY > this.position ? jQuery(el).fadeIn(this.showSpeed) : jQuery(el).fadeOut(this.showSpeed);
    };
    return BaBackTop;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Number)
], BaBackTop.prototype, "position", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Number)
], BaBackTop.prototype, "showSpeed", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Number)
], BaBackTop.prototype, "moveSpeed", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('baBackTop'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
], BaBackTop.prototype, "_selector", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('click'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Boolean)
], BaBackTop.prototype, "_onClick", null);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('window:scroll'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], BaBackTop.prototype, "_onWindowScroll", null);
BaBackTop = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'ba-back-top',
        styles: [__webpack_require__(503)],
        template: "\n    <i #baBackTop class=\"fa fa-angle-up back-top ba-back-top\" title=\"Back to Top\"></i>\n  "
    })
], BaBackTop);


/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(5)))

/***/ }),
/* 374 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baBackTop_component__ = __webpack_require__(373);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baBackTop_component__["a"]; });



/***/ }),
/* 375 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaCard; });

var BaCard = (function () {
    function BaCard() {
    }
    return BaCard;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], BaCard.prototype, "title", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], BaCard.prototype, "baCardClass", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], BaCard.prototype, "cardType", void 0);
BaCard = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'ba-card',
        template: __webpack_require__(521),
    })
], BaCard);



/***/ }),
/* 376 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__theme__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__baCardBlurHelper_service__ = __webpack_require__(377);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaCardBlur; });



var BaCardBlur = (function () {
    function BaCardBlur(_baConfig, _baCardBlurHelper, _el) {
        this._baConfig = _baConfig;
        this._baCardBlurHelper = _baCardBlurHelper;
        this._el = _el;
        this.isEnabled = false;
        if (this._isEnabled()) {
            this._baCardBlurHelper.init();
            this._getBodyImageSizesOnBgLoad();
            this._recalculateCardStylesOnBgLoad();
            this.isEnabled = true;
        }
    }
    BaCardBlur.prototype._onWindowResize = function () {
        if (this._isEnabled()) {
            this._bodyBgSize = this._baCardBlurHelper.getBodyBgImageSizes();
            this._recalculateCardStyle();
        }
    };
    BaCardBlur.prototype._getBodyImageSizesOnBgLoad = function () {
        var _this = this;
        this._baCardBlurHelper.bodyBgLoad().subscribe(function () {
            _this._bodyBgSize = _this._baCardBlurHelper.getBodyBgImageSizes();
        });
    };
    BaCardBlur.prototype._recalculateCardStylesOnBgLoad = function () {
        var _this = this;
        this._baCardBlurHelper.bodyBgLoad().subscribe(function (event) {
            setTimeout(_this._recalculateCardStyle.bind(_this));
        });
    };
    BaCardBlur.prototype._recalculateCardStyle = function () {
        if (!this._bodyBgSize) {
            return;
        }
        this._el.nativeElement.style.backgroundSize = Math.round(this._bodyBgSize.width) + 'px ' + Math.round(this._bodyBgSize.height) + 'px';
        this._el.nativeElement.style.backgroundPosition = Math.floor(this._bodyBgSize.positionX) + 'px ' + Math.floor(this._bodyBgSize.positionY) + 'px';
    };
    BaCardBlur.prototype._isEnabled = function () {
        return this._baConfig.get().theme.name == 'blur';
    };
    return BaCardBlur;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostBinding"])('class.card-blur'),
    __metadata("design:type", Boolean)
], BaCardBlur.prototype, "isEnabled", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('window:resize'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], BaCardBlur.prototype, "_onWindowResize", null);
BaCardBlur = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
        selector: '[baCardBlur]',
        providers: [__WEBPACK_IMPORTED_MODULE_2__baCardBlurHelper_service__["a" /* BaCardBlurHelper */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__theme__["c" /* BaThemeConfigProvider */], __WEBPACK_IMPORTED_MODULE_2__baCardBlurHelper_service__["a" /* BaCardBlurHelper */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]])
], BaCardBlur);



/***/ }),
/* 377 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaCardBlurHelper; });


var BaCardBlurHelper = (function () {
    function BaCardBlurHelper() {
    }
    BaCardBlurHelper.prototype.init = function () {
        this._genBgImage();
        this._genImageLoadSubject();
    };
    BaCardBlurHelper.prototype.bodyBgLoad = function () {
        return this.imageLoadSubject;
    };
    BaCardBlurHelper.prototype.getBodyBgImageSizes = function () {
        var elemW = document.documentElement.clientWidth;
        var elemH = document.documentElement.clientHeight;
        if (elemW <= 640)
            return;
        var imgRatio = (this.image.height / this.image.width); // original img ratio
        var containerRatio = (elemH / elemW); // container ratio
        var finalHeight, finalWidth;
        if (containerRatio > imgRatio) {
            finalHeight = elemH;
            finalWidth = (elemH / imgRatio);
        }
        else {
            finalWidth = elemW;
            finalHeight = (elemW * imgRatio);
        }
        return { width: finalWidth, height: finalHeight, positionX: (elemW - finalWidth) / 2, positionY: (elemH - finalHeight) / 2 };
    };
    BaCardBlurHelper.prototype._genBgImage = function () {
        this.image = new Image();
        var computedStyle = getComputedStyle(document.body.querySelector('main'), ':before');
        this.image.src = computedStyle.backgroundImage.replace(/url\((['"])?(.*?)\1\)/gi, '$2');
    };
    BaCardBlurHelper.prototype._genImageLoadSubject = function () {
        var _this = this;
        this.imageLoadSubject = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
        this.image.onerror = function (err) {
            _this.imageLoadSubject.complete();
        };
        this.image.onload = function () {
            _this.imageLoadSubject.next(null);
            _this.imageLoadSubject.complete();
        };
    };
    return BaCardBlurHelper;
}());
BaCardBlurHelper = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], BaCardBlurHelper);



/***/ }),
/* 378 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baCard_component__ = __webpack_require__(375);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baCard_component__["a"]; });



/***/ }),
/* 379 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_chartist__ = __webpack_require__(479);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_chartist___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_chartist__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_style_loader_baChartistChart_scss__ = __webpack_require__(543);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_style_loader_baChartistChart_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_style_loader_baChartistChart_scss__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaChartistChart; });



var BaChartistChart = (function () {
    function BaChartistChart() {
        this.onChartReady = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    BaChartistChart.prototype.ngAfterViewInit = function () {
        this.chart = new __WEBPACK_IMPORTED_MODULE_1_chartist__[this.baChartistChartType](this._selector.nativeElement, this.baChartistChartData, this.baChartistChartOptions, this.baChartistChartResponsive);
        this.onChartReady.emit(this.chart);
    };
    BaChartistChart.prototype.ngOnChanges = function (changes) {
        if (this.chart) {
            this.chart.update(this.baChartistChartData, this.baChartistChartOptions);
        }
    };
    BaChartistChart.prototype.ngOnDestroy = function () {
        if (this.chart) {
            this.chart.detach();
        }
    };
    return BaChartistChart;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], BaChartistChart.prototype, "baChartistChartType", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], BaChartistChart.prototype, "baChartistChartData", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], BaChartistChart.prototype, "baChartistChartOptions", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], BaChartistChart.prototype, "baChartistChartResponsive", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], BaChartistChart.prototype, "baChartistChartClass", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], BaChartistChart.prototype, "onChartReady", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('baChartistChart'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
], BaChartistChart.prototype, "_selector", void 0);
BaChartistChart = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'ba-chartist-chart',
        template: __webpack_require__(522),
        providers: [],
    })
], BaChartistChart);



/***/ }),
/* 380 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baChartistChart_component__ = __webpack_require__(379);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baChartistChart_component__["a"]; });



/***/ }),
/* 381 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(6);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaCheckbox; });


var BaCheckbox = (function () {
    function BaCheckbox(state) {
        this.model = state;
        state.valueAccessor = this;
    }
    BaCheckbox.prototype.onChange = function (value) {
    };
    BaCheckbox.prototype.onTouch = function (value) {
    };
    BaCheckbox.prototype.writeValue = function (state) {
        this.state = state;
    };
    BaCheckbox.prototype.registerOnChange = function (fn) {
        this.onChange = function (state) {
            this.writeValue(state);
            this.model.viewToModelUpdate(state);
        };
    };
    BaCheckbox.prototype.registerOnTouched = function (fn) {
        this.onTouch = fn;
    };
    return BaCheckbox;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], BaCheckbox.prototype, "disabled", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], BaCheckbox.prototype, "label", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], BaCheckbox.prototype, "value", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], BaCheckbox.prototype, "baCheckboxClass", void 0);
BaCheckbox = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'ba-checkbox[ngModel]',
        styles: [__webpack_require__(505)],
        template: __webpack_require__(523),
        providers: [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["NgModel"]]
    }),
    __param(0, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Self"])()),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["NgModel"]])
], BaCheckbox);



/***/ }),
/* 382 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baCheckbox_component__ = __webpack_require__(381);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baCheckbox_component__["a"]; });



/***/ }),
/* 383 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__global_state__ = __webpack_require__(27);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaContentTop; });


var BaContentTop = (function () {
    function BaContentTop(_state) {
        var _this = this;
        this._state = _state;
        this.activePageTitle = '';
        this._state.subscribe('menu.activeLink', function (activeLink) {
            if (activeLink) {
                _this.activePageTitle = activeLink.title;
            }
        });
    }
    return BaContentTop;
}());
BaContentTop = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'ba-content-top',
        styles: [__webpack_require__(506)],
        template: __webpack_require__(524),
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__global_state__["a" /* GlobalState */]])
], BaContentTop);



/***/ }),
/* 384 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baContentTop_component__ = __webpack_require__(383);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baContentTop_component__["a"]; });



/***/ }),
/* 385 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_uploader__ = __webpack_require__(88);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaFileUploader; });


var BaFileUploader = (function () {
    function BaFileUploader(renderer) {
        this.renderer = renderer;
        this.fileUploaderOptions = { url: '' };
        this.onFileUpload = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.onFileUploadCompleted = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.defaultValue = '';
    }
    BaFileUploader.prototype.bringFileSelector = function () {
        this.renderer.invokeElementMethod(this._fileUpload.nativeElement, 'click');
        return false;
    };
    BaFileUploader.prototype.beforeFileUpload = function (uploadingFile) {
        var files = this._fileUpload.nativeElement.files;
        if (files.length) {
            var file = files[0];
            this._onChangeFileSelect(files[0]);
            if (!this._canFleUploadOnServer()) {
                uploadingFile.setAbort();
            }
            else {
                this.uploadFileInProgress = true;
            }
        }
    };
    BaFileUploader.prototype._onChangeFileSelect = function (file) {
        this._inputText.nativeElement.value = file.name;
    };
    BaFileUploader.prototype._onFileUpload = function (data) {
        if (data['done'] || data['abort'] || data['error']) {
            this._onFileUploadCompleted(data);
        }
        else {
            this.onFileUpload.emit(data);
        }
    };
    BaFileUploader.prototype._onFileUploadCompleted = function (data) {
        this.uploadFileInProgress = false;
        this.onFileUploadCompleted.emit(data);
    };
    BaFileUploader.prototype._canFleUploadOnServer = function () {
        return !!this.fileUploaderOptions['url'];
    };
    return BaFileUploader;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ngx_uploader__["b" /* NgUploaderOptions */])
], BaFileUploader.prototype, "fileUploaderOptions", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], BaFileUploader.prototype, "onFileUpload", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], BaFileUploader.prototype, "onFileUploadCompleted", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], BaFileUploader.prototype, "defaultValue", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('fileUpload'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
], BaFileUploader.prototype, "_fileUpload", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('inputText'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
], BaFileUploader.prototype, "_inputText", void 0);
BaFileUploader = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'ba-file-uploader',
        styles: [__webpack_require__(507)],
        template: __webpack_require__(525),
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"]])
], BaFileUploader);



/***/ }),
/* 386 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baFileUploader_component__ = __webpack_require__(385);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baFileUploader_component__["a"]; });



/***/ }),
/* 387 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(jQuery) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_fullcalendar_dist_fullcalendar_js__ = __webpack_require__(487);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_fullcalendar_dist_fullcalendar_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_fullcalendar_dist_fullcalendar_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_style_loader_baFullCalendar_scss__ = __webpack_require__(544);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_style_loader_baFullCalendar_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_style_loader_baFullCalendar_scss__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaFullCalendar; });



var BaFullCalendar = (function () {
    function BaFullCalendar() {
        this.onCalendarReady = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    BaFullCalendar.prototype.ngAfterViewInit = function () {
        var calendar = jQuery(this._selector.nativeElement).fullCalendar(this.baFullCalendarConfiguration);
        this.onCalendarReady.emit(calendar);
    };
    return BaFullCalendar;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], BaFullCalendar.prototype, "baFullCalendarConfiguration", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], BaFullCalendar.prototype, "baFullCalendarClass", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], BaFullCalendar.prototype, "onCalendarReady", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('baFullCalendar'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
], BaFullCalendar.prototype, "_selector", void 0);
BaFullCalendar = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'ba-full-calendar',
        template: __webpack_require__(526),
    })
], BaFullCalendar);


/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(5)))

/***/ }),
/* 388 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baFullCalendar_component__ = __webpack_require__(387);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baFullCalendar_component__["a"]; });



/***/ }),
/* 389 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(jQuery) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__global_state__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_style_loader_baMenu_scss__ = __webpack_require__(545);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_style_loader_baMenu_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_style_loader_baMenu_scss__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaMenu; });





var BaMenu = (function () {
    function BaMenu(_router, _service, _state) {
        this._router = _router;
        this._service = _service;
        this._state = _state;
        this.sidebarCollapsed = false;
        this.expandMenu = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.outOfArea = -200;
    }
    BaMenu.prototype.updateMenu = function (newMenuItems) {
        this.menuItems = newMenuItems;
        this.selectMenuAndNotify();
    };
    BaMenu.prototype.selectMenuAndNotify = function () {
        if (this.menuItems) {
            this.menuItems = this._service.selectMenuItem(this.menuItems);
            //console.log(this.menuItems)
            this._state.notifyDataChanged('menu.activeLink', this._service.getCurrentItem());
        }
    };
    BaMenu.prototype.ngOnInit = function () {
        var _this = this;
        this._onRouteChange = this._router.events.subscribe(function (event) {
            if (event instanceof __WEBPACK_IMPORTED_MODULE_1__angular_router__["NavigationEnd"]) {
                if (_this.menuItems) {
                    _this.selectMenuAndNotify();
                }
                else {
                    // on page load we have to wait as event is fired before menu elements are prepared
                    setTimeout(function () { return _this.selectMenuAndNotify(); });
                }
            }
        });
        this._menuItemsSub = this._service.menuItems.subscribe(this.updateMenu.bind(this));
    };
    BaMenu.prototype.ngOnDestroy = function () {
        this._onRouteChange.unsubscribe();
        this._menuItemsSub.unsubscribe();
    };
    BaMenu.prototype.hoverItem = function ($event) {
        this.showHoverElem = true;
        this.hoverElemHeight = $event.currentTarget.clientHeight;
        // TODO: get rid of magic 66 constant
        this.hoverElemTop = $event.currentTarget.getBoundingClientRect().top - 66;
    };
    BaMenu.prototype.toggleSubMenu = function ($event) {
        var submenu = jQuery($event.currentTarget).next();
        if (this.sidebarCollapsed) {
            this.expandMenu.emit(null);
            if (!$event.item.expanded) {
                $event.item.expanded = true;
            }
        }
        else {
            $event.item.expanded = !$event.item.expanded;
            submenu.slideToggle();
        }
        return false;
    };
    return BaMenu;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], BaMenu.prototype, "sidebarCollapsed", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Number)
], BaMenu.prototype, "menuHeight", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], BaMenu.prototype, "expandMenu", void 0);
BaMenu = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'ba-menu',
        template: __webpack_require__(527)
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"], __WEBPACK_IMPORTED_MODULE_2__services__["b" /* BaMenuService */], __WEBPACK_IMPORTED_MODULE_3__global_state__["a" /* GlobalState */]])
], BaMenu);


/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(5)))

/***/ }),
/* 390 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_style_loader_baMenuItem_scss__ = __webpack_require__(546);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_style_loader_baMenuItem_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_style_loader_baMenuItem_scss__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaMenuItem; });


var BaMenuItem = (function () {
    function BaMenuItem() {
        this.child = false;
        this.itemHover = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.toggleSubMenu = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    BaMenuItem.prototype.onHoverItem = function ($event) {
        this.itemHover.emit($event);
    };
    BaMenuItem.prototype.onToggleSubMenu = function ($event, item) {
        $event.item = item;
        this.toggleSubMenu.emit($event);
        return false;
    };
    return BaMenuItem;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], BaMenuItem.prototype, "menuItem", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], BaMenuItem.prototype, "child", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], BaMenuItem.prototype, "itemHover", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], BaMenuItem.prototype, "toggleSubMenu", void 0);
BaMenuItem = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'ba-menu-item',
        template: __webpack_require__(528)
    })
], BaMenuItem);



/***/ }),
/* 391 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baMenuItem_component__ = __webpack_require__(390);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baMenuItem_component__["a"]; });



/***/ }),
/* 392 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baMenu_component__ = __webpack_require__(389);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baMenu_component__["a"]; });



/***/ }),
/* 393 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(6);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaMultiCheckbox; });


var BaMultiCheckbox = (function () {
    function BaMultiCheckbox(state) {
        this.model = state;
        state.valueAccessor = this;
    }
    BaMultiCheckbox.prototype.getProp = function (item, propName) {
        var prop = this.propertiesMapping[propName];
        if (!prop) {
            return item[propName];
        }
        else if (typeof prop === 'function') {
            return prop(item);
        }
        return item[prop];
    };
    BaMultiCheckbox.prototype.onChange = function (value) { };
    BaMultiCheckbox.prototype.onTouch = function (value) { };
    BaMultiCheckbox.prototype.writeValue = function (state) {
        this.state = state;
    };
    BaMultiCheckbox.prototype.registerOnChange = function (fn) {
        this.onChange = function (state) {
            this.writeValue(state);
            this.model.viewToModelUpdate(state);
        };
    };
    BaMultiCheckbox.prototype.registerOnTouched = function (fn) { this.onTouch = fn; };
    return BaMultiCheckbox;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], BaMultiCheckbox.prototype, "baMultiCheckboxClass", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], BaMultiCheckbox.prototype, "propertiesMapping", void 0);
BaMultiCheckbox = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'ba-multi-checkbox[ngModel]',
        template: __webpack_require__(529),
        styles: [__webpack_require__(511)],
        providers: [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["NgModel"]]
    }),
    __param(0, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Self"])()),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["NgModel"]])
], BaMultiCheckbox);



/***/ }),
/* 394 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baMultiCheckbox_component__ = __webpack_require__(393);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baMultiCheckbox_component__["a"]; });



/***/ }),
/* 395 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__global_state__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_style_loader_baPageTop_scss__ = __webpack_require__(547);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_style_loader_baPageTop_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_style_loader_baPageTop_scss__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_authService_auth_service__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngrx_store__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__auth_state_auth_actions__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_router__ = __webpack_require__(7);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaPageTop; });
// import {Component} from '@angular/core';
// import {GlobalState} from '../../../global.state';
// import 'style-loader!./baPageTop.scss';
// import { AuthService  } from '../../services/authService/auth.service';
// import { Store } from '@ngrx/store'
// import * as auth from '../../../auth/state/auth.actions'
// import { Router } from '@angular/router';
// @Component({
//   selector: 'ba-page-top',
//   template: require('./baPageTop.html'),
// })
// export class BaPageTop {
//   public isScrolled:boolean = false;
//   public isMenuCollapsed:boolean = false;
//   constructor(private _state:GlobalState,
//               private store: Store<any>,
//               private router:Router,
//               private authService:AuthService) {
//     this._state.subscribe('menu.isCollapsed', (isCollapsed) => {
//       this.isMenuCollapsed = isCollapsed;
//     });
//   }
//   public toggleMenu() {
//  alert("gdgdgdg")   ;
//  this.isMenuCollapsed = !this.isMenuCollapsed;
//     this._state.notifyDataChanged('menu.isCollapsed', this.isMenuCollapsed);
//     return true;
//   }
//   public scrolledChanged(isScrolled) {
//     this.isScrolled = isScrolled;
//   }
//   logout(){
//     this.store.dispatch(new auth.AuthLogoutAction())
//     // console.log("looged out")
//     // this.user_service.logoutUser().subscribe((result) => {
//     //     console.log(result);
//     //     if(result.message='success'){
//     //       //clear localstorage
//     //       this.authService.logout();
//     //       this.router.navigate(['login']);
//     //     }
//     //   }
//     //   , (error) => {
//     //     console.log(error)
//     //   }
//     // );
//   }
// }






// import * as lang from '../../../multilingual/state/lang.actions';
// import {Language} from '../../../multilingual/model/lang.model'

var BaPageTop = (function () {
    // language = new Language();
    function BaPageTop(_state, store, router, authService) {
        var _this = this;
        this._state = _state;
        this.store = store;
        this.router = router;
        this.authService = authService;
        this.isScrolled = false;
        this.isMenuCollapsed = false;
        this._state.subscribe('menu.isCollapsed', function (isCollapsed) {
            _this.isMenuCollapsed = isCollapsed;
        });
        // this.store
        //     .select('lang')
        //     .subscribe((res: any) => {
        //       this.resourceBundle = res.resourceBundle;
        //         //console.log("this is in menu services",res)
        //         //setting language
        //         if(res.resourceBundle != null) 
        //         {
        //             for(var j=0;j<res.resourceBundle.length;j++)
        //             {
        //                 //console.log("helloooooo..............")
        //                 this.language[res.resourceBundle[j].messageKey]=res.resourceBundle[j].customMessage
        //             }
        //         }
        //     });
    }
    BaPageTop.prototype.toggleMenu = function () {
        this.isMenuCollapsed = !this.isMenuCollapsed;
        // alert(this.isMenuCollapsed)
        this._state.notifyDataChanged('menu.isCollapsed', this.isMenuCollapsed);
        return false;
    };
    BaPageTop.prototype.scrolledChanged = function (isScrolled) {
        this.isScrolled = isScrolled;
    };
    BaPageTop.prototype.logout = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_5__auth_state_auth_actions__["b" /* AuthLogoutAction */]());
        // console.log("looged out")
        // this.user_service.logoutUser().subscribe((result) => {
        //     console.log(result);
        //     if(result.message='success'){
        //       //clear localstorage
        //       this.authService.logout();
        //       this.router.navigate(['login']);
        //     }
        //   }
        //   , (error) => {
        //     console.log(error)
        //   }
        // );
    };
    return BaPageTop;
}());
BaPageTop = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'ba-page-top',
        template: __webpack_require__(530),
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__global_state__["a" /* GlobalState */],
        __WEBPACK_IMPORTED_MODULE_4__ngrx_store__["Store"],
        __WEBPACK_IMPORTED_MODULE_6__angular_router__["Router"],
        __WEBPACK_IMPORTED_MODULE_3__services_authService_auth_service__["a" /* AuthService */]])
], BaPageTop);



/***/ }),
/* 396 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baPageTop_component__ = __webpack_require__(395);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baPageTop_component__["a"]; });



/***/ }),
/* 397 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_uploader__ = __webpack_require__(88);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaPictureUploader; });


var BaPictureUploader = (function () {
    function BaPictureUploader(renderer) {
        this.renderer = renderer;
        this.defaultPicture = '';
        this.picture = '';
        this.uploaderOptions = { url: '' };
        this.canDelete = true;
        this.onUpload = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.onUploadCompleted = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    BaPictureUploader.prototype.beforeUpload = function (uploadingFile) {
        var files = this._fileUpload.nativeElement.files;
        if (files.length) {
            var file = files[0];
            this._changePicture(file);
            if (!this._canUploadOnServer()) {
                uploadingFile.setAbort();
            }
            else {
                this.uploadInProgress = true;
            }
        }
    };
    BaPictureUploader.prototype.bringFileSelector = function () {
        this.renderer.invokeElementMethod(this._fileUpload.nativeElement, 'click');
        return false;
    };
    BaPictureUploader.prototype.removePicture = function () {
        this.picture = '';
        return false;
    };
    BaPictureUploader.prototype._changePicture = function (file) {
        var _this = this;
        var reader = new FileReader();
        reader.addEventListener('load', function (event) {
            _this.picture = event.target.result;
        }, false);
        reader.readAsDataURL(file);
    };
    BaPictureUploader.prototype._onUpload = function (data) {
        if (data['done'] || data['abort'] || data['error']) {
            this._onUploadCompleted(data);
        }
        else {
            this.onUpload.emit(data);
        }
    };
    BaPictureUploader.prototype._onUploadCompleted = function (data) {
        this.uploadInProgress = false;
        this.onUploadCompleted.emit(data);
    };
    BaPictureUploader.prototype._canUploadOnServer = function () {
        return !!this.uploaderOptions['url'];
    };
    return BaPictureUploader;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], BaPictureUploader.prototype, "defaultPicture", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], BaPictureUploader.prototype, "picture", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ngx_uploader__["b" /* NgUploaderOptions */])
], BaPictureUploader.prototype, "uploaderOptions", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], BaPictureUploader.prototype, "canDelete", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], BaPictureUploader.prototype, "onUpload", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], BaPictureUploader.prototype, "onUploadCompleted", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('fileUpload'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
], BaPictureUploader.prototype, "_fileUpload", void 0);
BaPictureUploader = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'ba-picture-uploader',
        styles: [__webpack_require__(513)],
        template: __webpack_require__(531),
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"]])
], BaPictureUploader);



/***/ }),
/* 398 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baPictureUploader_component__ = __webpack_require__(397);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baPictureUploader_component__["a"]; });



/***/ }),
/* 399 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__global_state__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__theme__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_style_loader_baSidebar_scss__ = __webpack_require__(548);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_style_loader_baSidebar_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_style_loader_baSidebar_scss__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaSidebar; });




var BaSidebar = (function () {
    function BaSidebar(_elementRef, _state) {
        var _this = this;
        this._elementRef = _elementRef;
        this._state = _state;
        this.isMenuCollapsed = false;
        this.isMenuShouldCollapsed = false;
        this._state.subscribe('menu.isCollapsed', function (isCollapsed) {
            _this.isMenuCollapsed = isCollapsed;
        });
    }
    BaSidebar.prototype.ngOnInit = function () {
        if (this._shouldMenuCollapse()) {
            this.menuCollapse();
        }
    };
    BaSidebar.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () { return _this.updateSidebarHeight(); });
    };
    BaSidebar.prototype.onWindowResize = function () {
        var isMenuShouldCollapsed = this._shouldMenuCollapse();
        if (this.isMenuShouldCollapsed !== isMenuShouldCollapsed) {
            this.menuCollapseStateChange(isMenuShouldCollapsed);
        }
        this.isMenuShouldCollapsed = isMenuShouldCollapsed;
        this.updateSidebarHeight();
    };
    BaSidebar.prototype.menuExpand = function () {
        this.menuCollapseStateChange(false);
    };
    BaSidebar.prototype.menuCollapse = function () {
        this.menuCollapseStateChange(true);
    };
    BaSidebar.prototype.menuCollapseStateChange = function (isCollapsed) {
        this.isMenuCollapsed = isCollapsed;
        this._state.notifyDataChanged('menu.isCollapsed', this.isMenuCollapsed);
    };
    BaSidebar.prototype.updateSidebarHeight = function () {
        // TODO: get rid of magic 84 constant
        this.menuHeight = this._elementRef.nativeElement.childNodes[0].clientHeight - 84;
    };
    BaSidebar.prototype._shouldMenuCollapse = function () {
        return window.innerWidth <= __WEBPACK_IMPORTED_MODULE_2__theme__["f" /* layoutSizes */].resWidthCollapseSidebar;
    };
    return BaSidebar;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('window:resize'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], BaSidebar.prototype, "onWindowResize", null);
BaSidebar = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'ba-sidebar',
        template: __webpack_require__(532)
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_1__global_state__["a" /* GlobalState */]])
], BaSidebar);



/***/ }),
/* 400 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baSidebar_component__ = __webpack_require__(399);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baSidebar_component__["a"]; });



/***/ }),
/* 401 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baPageTop__ = __webpack_require__(396);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "k", function() { return __WEBPACK_IMPORTED_MODULE_0__baPageTop__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__baSidebar__ = __webpack_require__(400);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "m", function() { return __WEBPACK_IMPORTED_MODULE_1__baSidebar__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__baMenu_components_baMenuItem__ = __webpack_require__(391);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "h", function() { return __WEBPACK_IMPORTED_MODULE_2__baMenu_components_baMenuItem__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__baMenu__ = __webpack_require__(392);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "i", function() { return __WEBPACK_IMPORTED_MODULE_3__baMenu__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__baContentTop__ = __webpack_require__(384);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_4__baContentTop__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__baCard__ = __webpack_require__(378);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_5__baCard__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__baAmChart__ = __webpack_require__(372);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_6__baAmChart__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__baChartistChart__ = __webpack_require__(380);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_7__baChartistChart__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__baBackTop__ = __webpack_require__(374);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_8__baBackTop__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__baFullCalendar__ = __webpack_require__(388);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_9__baFullCalendar__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__baPictureUploader__ = __webpack_require__(398);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "l", function() { return __WEBPACK_IMPORTED_MODULE_10__baPictureUploader__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__baCheckbox__ = __webpack_require__(382);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_11__baCheckbox__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__baMultiCheckbox__ = __webpack_require__(394);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "j", function() { return __WEBPACK_IMPORTED_MODULE_12__baMultiCheckbox__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__baFileUploader__ = __webpack_require__(386);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "n", function() { return __WEBPACK_IMPORTED_MODULE_13__baFileUploader__["a"]; });

//export * from './baMsgCenter';













//export * from './pagination';


/***/ }),
/* 402 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaScrollPosition; });

var BaScrollPosition = (function () {
    function BaScrollPosition() {
        this.scrollChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    BaScrollPosition.prototype.ngOnInit = function () {
        this.onWindowScroll();
    };
    BaScrollPosition.prototype.onWindowScroll = function () {
        var isScrolled = window.scrollY > this.maxHeight;
        if (isScrolled !== this._isScrolled) {
            this._isScrolled = isScrolled;
            this.scrollChange.emit(isScrolled);
        }
    };
    return BaScrollPosition;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Number)
], BaScrollPosition.prototype, "maxHeight", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
], BaScrollPosition.prototype, "scrollChange", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('window:scroll'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], BaScrollPosition.prototype, "onWindowScroll", null);
BaScrollPosition = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
        selector: '[baScrollPosition]'
    })
], BaScrollPosition);



/***/ }),
/* 403 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baScrollPosition_directive__ = __webpack_require__(402);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baScrollPosition_directive__["a"]; });



/***/ }),
/* 404 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(jQuery) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_jquery_slimscroll__ = __webpack_require__(490);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_jquery_slimscroll___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_jquery_slimscroll__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaSlimScroll; });


var BaSlimScroll = (function () {
    function BaSlimScroll(_elementRef) {
        this._elementRef = _elementRef;
    }
    BaSlimScroll.prototype.ngOnChanges = function (changes) {
        this._scroll();
    };
    BaSlimScroll.prototype._scroll = function () {
        this._destroy();
        this._init();
    };
    BaSlimScroll.prototype._init = function () {
        jQuery(this._elementRef.nativeElement).slimScroll(this.baSlimScrollOptions);
    };
    BaSlimScroll.prototype._destroy = function () {
        jQuery(this._elementRef.nativeElement).slimScroll({ destroy: true });
    };
    return BaSlimScroll;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], BaSlimScroll.prototype, "baSlimScrollOptions", void 0);
BaSlimScroll = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
        selector: '[baSlimScroll]'
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]])
], BaSlimScroll);


/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(5)))

/***/ }),
/* 405 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baSlimScroll_directive__ = __webpack_require__(404);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baSlimScroll_directive__["a"]; });



/***/ }),
/* 406 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__theme__ = __webpack_require__(17);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaThemeRun; });


var BaThemeRun = (function () {
    function BaThemeRun(_baConfig) {
        this._baConfig = _baConfig;
        this._classes = [];
    }
    BaThemeRun.prototype.ngOnInit = function () {
        this._assignTheme();
        this._assignMobile();
    };
    BaThemeRun.prototype._assignTheme = function () {
        this._addClass(this._baConfig.get().theme.name);
    };
    BaThemeRun.prototype._assignMobile = function () {
        if (__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__theme__["e" /* isMobile */])()) {
            this._addClass('mobile');
        }
    };
    BaThemeRun.prototype._addClass = function (cls) {
        this._classes.push(cls);
        this.classesString = this._classes.join(' ');
    };
    return BaThemeRun;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostBinding"])('class'),
    __metadata("design:type", String)
], BaThemeRun.prototype, "classesString", void 0);
BaThemeRun = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
        selector: '[baThemeRun]'
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__theme__["c" /* BaThemeConfigProvider */]])
], BaThemeRun);



/***/ }),
/* 407 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baThemeRun_directive__ = __webpack_require__(406);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baThemeRun_directive__["a"]; });



/***/ }),
/* 408 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baScrollPosition__ = __webpack_require__(403);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baScrollPosition__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__baThemeRun__ = __webpack_require__(407);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_1__baThemeRun__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__baSlimScroll__ = __webpack_require__(405);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_2__baSlimScroll__["a"]; });





/***/ }),
/* 409 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__theme__ = __webpack_require__(17);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaAppPicturePipe; });


var BaAppPicturePipe = (function () {
    function BaAppPicturePipe() {
    }
    BaAppPicturePipe.prototype.transform = function (input) {
        return __WEBPACK_IMPORTED_MODULE_1__theme__["b" /* layoutPaths */].images.root + input;
    };
    return BaAppPicturePipe;
}());
BaAppPicturePipe = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({ name: 'baAppPicture' })
], BaAppPicturePipe);



/***/ }),
/* 410 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baAppPicture_pipe__ = __webpack_require__(409);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baAppPicture_pipe__["a"]; });



/***/ }),
/* 411 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__theme__ = __webpack_require__(17);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaKameleonPicturePipe; });


var BaKameleonPicturePipe = (function () {
    function BaKameleonPicturePipe() {
    }
    BaKameleonPicturePipe.prototype.transform = function (input) {
        return __WEBPACK_IMPORTED_MODULE_1__theme__["b" /* layoutPaths */].images.root + 'theme/icon/kameleon/' + input + '.svg';
    };
    return BaKameleonPicturePipe;
}());
BaKameleonPicturePipe = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({ name: 'baKameleonPicture' })
], BaKameleonPicturePipe);



/***/ }),
/* 412 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baKameleonPicture_pipe__ = __webpack_require__(411);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baKameleonPicture_pipe__["a"]; });



/***/ }),
/* 413 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__theme__ = __webpack_require__(17);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaProfilePicturePipe; });


var BaProfilePicturePipe = (function () {
    function BaProfilePicturePipe() {
    }
    BaProfilePicturePipe.prototype.transform = function (input, ext) {
        if (ext === void 0) { ext = 'png'; }
        return __WEBPACK_IMPORTED_MODULE_1__theme__["b" /* layoutPaths */].images.profile + input + '.' + ext;
    };
    return BaProfilePicturePipe;
}());
BaProfilePicturePipe = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({ name: 'baProfilePicture' })
], BaProfilePicturePipe);



/***/ }),
/* 414 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baProfilePicture_pipe__ = __webpack_require__(413);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baProfilePicture_pipe__["a"]; });



/***/ }),
/* 415 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baProfilePicture__ = __webpack_require__(414);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_0__baProfilePicture__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__baAppPicture__ = __webpack_require__(410);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_1__baAppPicture__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__baKameleonPicture__ = __webpack_require__(412);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_2__baKameleonPicture__["a"]; });





/***/ }),
/* 416 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaImageLoaderService; });

var BaImageLoaderService = (function () {
    function BaImageLoaderService() {
    }
    BaImageLoaderService.prototype.load = function (src) {
        return new Promise(function (resolve, reject) {
            var img = new Image();
            img.src = src;
            img.onload = function () {
                resolve('Image with src ' + src + ' loaded successfully.');
            };
        });
    };
    return BaImageLoaderService;
}());
BaImageLoaderService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], BaImageLoaderService);



/***/ }),
/* 417 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baImageLoader_service__ = __webpack_require__(416);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baImageLoader_service__["a"]; });



/***/ }),
/* 418 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaMenuService; });




var BaMenuService = (function () {
    function BaMenuService(_router) {
        this._router = _router;
        this.menuItems = new __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__["BehaviorSubject"]([]);
        this._currentMenuItem = {};
    }
    /**
     * Updates the routes in the menu
     *
     * @param {Routes} routes Type compatible with app.menu.ts
     */
    BaMenuService.prototype.updateMenuByRoutes = function (routes) {
        var convertedRoutes = this.convertRoutesToMenus(__WEBPACK_IMPORTED_MODULE_2_lodash__["cloneDeep"](routes));
        this.menuItems.next(convertedRoutes);
    };
    BaMenuService.prototype.convertRoutesToMenus = function (routes) {
        var items = this._convertArrayToItems(routes);
        return this._skipEmpty(items);
    };
    BaMenuService.prototype.getCurrentItem = function () {
        return this._currentMenuItem;
    };
    BaMenuService.prototype.selectMenuItem = function (menuItems) {
        var _this = this;
        var items = [];
        menuItems.forEach(function (item) {
            _this._selectItem(item);
            if (item.selected) {
                _this._currentMenuItem = item;
            }
            if (item.children && item.children.length > 0) {
                item.children = _this.selectMenuItem(item.children);
            }
            items.push(item);
        });
        return items;
    };
    BaMenuService.prototype._skipEmpty = function (items) {
        var menu = [];
        items.forEach(function (item) {
            var menuItem;
            if (item.skip) {
                if (item.children && item.children.length > 0) {
                    menuItem = item.children;
                }
            }
            else {
                menuItem = item;
            }
            if (menuItem) {
                menu.push(menuItem);
            }
        });
        return [].concat.apply([], menu);
    };
    BaMenuService.prototype._convertArrayToItems = function (routes, parent) {
        var _this = this;
        var items = [];
        routes.forEach(function (route) {
            items.push(_this._convertObjectToItem(route, parent));
        });
        return items;
    };
    BaMenuService.prototype._convertObjectToItem = function (object, parent) {
        var item = {};
        if (object.data && object.data.menu) {
            // this is a menu object
            item = object.data.menu;
            item.route = object;
            delete item.route.data.menu;
        }
        else {
            item.route = object;
            item.skip = true;
        }
        // we have to collect all paths to correctly build the url then
        if (Array.isArray(item.route.path)) {
            item.route.paths = item.route.path;
        }
        else {
            item.route.paths = parent && parent.route && parent.route.paths ? parent.route.paths.slice(0) : ['/'];
            if (!!item.route.path)
                item.route.paths.push(item.route.path);
        }
        if (object.children && object.children.length > 0) {
            item.children = this._convertArrayToItems(object.children, item);
        }
        var prepared = this._prepareItem(item);
        // if current item is selected or expanded - then parent is expanded too
        if ((prepared.selected || prepared.expanded) && parent) {
            parent.expanded = true;
        }
        return prepared;
    };
    BaMenuService.prototype._prepareItem = function (object) {
        if (!object.skip) {
            object.target = object.target || '';
            object.pathMatch = object.pathMatch || 'full';
            return this._selectItem(object);
        }
        return object;
    };
    BaMenuService.prototype._selectItem = function (object) {
        object.selected = this._router.isActive(this._router.createUrlTree(object.route.paths), object.pathMatch === 'full');
        return object;
    };
    return BaMenuService;
}());
BaMenuService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"]])
], BaMenuService);



/***/ }),
/* 419 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baMenu_service__ = __webpack_require__(418);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baMenu_service__["a"]; });



/***/ }),
/* 420 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaThemePreloader; });

var BaThemePreloader = BaThemePreloader_1 = (function () {
    function BaThemePreloader() {
    }
    BaThemePreloader.registerLoader = function (method) {
        BaThemePreloader_1._loaders.push(method);
    };
    BaThemePreloader.clear = function () {
        BaThemePreloader_1._loaders = [];
    };
    BaThemePreloader.load = function () {
        return new Promise(function (resolve, reject) {
            BaThemePreloader_1._executeAll(resolve);
        });
    };
    BaThemePreloader._executeAll = function (done) {
        setTimeout(function () {
            Promise.all(BaThemePreloader_1._loaders).then(function (values) {
                done.call(null, values);
            }).catch(function (error) {
                console.error(error);
            });
        });
    };
    return BaThemePreloader;
}());
BaThemePreloader._loaders = [];
BaThemePreloader = BaThemePreloader_1 = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], BaThemePreloader);

var BaThemePreloader_1;


/***/ }),
/* 421 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baThemePreloader_service__ = __webpack_require__(420);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baThemePreloader_service__["a"]; });



/***/ }),
/* 422 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaThemeSpinner; });

var BaThemeSpinner = (function () {
    function BaThemeSpinner() {
        this._selector = 'preloader';
        this._element = document.getElementById(this._selector);
    }
    BaThemeSpinner.prototype.show = function () {
        this._element.style['display'] = 'block';
    };
    BaThemeSpinner.prototype.hide = function (delay) {
        var _this = this;
        if (delay === void 0) { delay = 0; }
        console.log("hide loader");
        setTimeout(function () {
            _this._element.style['display'] = 'none';
        }, delay);
    };
    return BaThemeSpinner;
}());
BaThemeSpinner = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], BaThemeSpinner);



/***/ }),
/* 423 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__baThemeSpinner_service__ = __webpack_require__(422);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__baThemeSpinner_service__["a"]; });



/***/ }),
/* 424 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmailValidator; });
var EmailValidator = (function () {
    function EmailValidator() {
    }
    EmailValidator.email = function (control) {
        var EMAIL_REGEXP = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (control.value && control.value !== '' && (control.value.length <= 5 || !EMAIL_REGEXP.test(control.value))) {
            return { invalid: true };
        }
    };
    return EmailValidator;
}());



/***/ }),
/* 425 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EqualPasswordsValidator; });
var EqualPasswordsValidator = (function () {
    function EqualPasswordsValidator() {
    }
    EqualPasswordsValidator.validate = function (firstField, secondField) {
        return function (c) {
            return (c.controls && c.controls[firstField].value == c.controls[secondField].value) ? null : {
                passwordsEqual: {
                    valid: false
                }
            };
        };
    };
    return EqualPasswordsValidator;
}());



/***/ }),
/* 426 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_environment__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angularclass_hmr__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angularclass_hmr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__angularclass_hmr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app__ = __webpack_require__(304);
/* harmony export (immutable) */ __webpack_exports__["main"] = main;
/*
 * Angular bootstraping
 */



/*
 * App Module
 * our top level module that holds all of our components
 */

/*
 * Bootstrap our Angular app with a top level NgModule
 */
function main() {
    return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["platformBrowserDynamic"])()
        .bootstrapModule(__WEBPACK_IMPORTED_MODULE_3__app__["a" /* AppModule */]).then(function(MODULE_REF) {
  if (false) {
    module["hot"]["accept"]();
    
    if (MODULE_REF.instance["hmrOnInit"]) {
      module["hot"]["data"] && MODULE_REF.instance["hmrOnInit"](module["hot"]["data"]);
    }
    if (MODULE_REF.instance["hmrOnStatus"]) {
      module["hot"]["apply"](function(status) {
        MODULE_REF.instance["hmrOnStatus"](status);
      });
    }
    if (MODULE_REF.instance["hmrOnCheck"]) {
      module["hot"]["check"](function(err, outdatedModules) {
        MODULE_REF.instance["hmrOnCheck"](err, outdatedModules);
      });
    }
    if (MODULE_REF.instance["hmrOnDecline"]) {
      module["hot"]["decline"](function(dependencies) {
        MODULE_REF.instance["hmrOnDecline"](dependencies);
      });
    }
    module["hot"]["dispose"](function(store) {
      MODULE_REF.instance["hmrOnDestroy"] && MODULE_REF.instance["hmrOnDestroy"](store);
      MODULE_REF.destroy();
      MODULE_REF.instance["hmrAfterDestroy"] && MODULE_REF.instance["hmrAfterDestroy"](store);
    });
  }
  return MODULE_REF;
})
        .then(__WEBPACK_IMPORTED_MODULE_1__app_environment__["a" /* decorateModuleRef */])
        .catch(function (err) { return console.error(err); });
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angularclass_hmr__["bootloader"])(main);


/***/ }),
/* 427 */,
/* 428 */,
/* 429 */,
/* 430 */,
/* 431 */,
/* 432 */,
/* 433 */,
/* 434 */,
/* 435 */,
/* 436 */,
/* 437 */,
/* 438 */,
/* 439 */,
/* 440 */,
/* 441 */,
/* 442 */,
/* 443 */,
/* 444 */,
/* 445 */,
/* 446 */,
/* 447 */,
/* 448 */,
/* 449 */,
/* 450 */,
/* 451 */,
/* 452 */,
/* 453 */,
/* 454 */,
/* 455 */,
/* 456 */,
/* 457 */,
/* 458 */,
/* 459 */,
/* 460 */,
/* 461 */,
/* 462 */,
/* 463 */,
/* 464 */,
/* 465 */,
/* 466 */,
/* 467 */,
/* 468 */,
/* 469 */,
/* 470 */,
/* 471 */,
/* 472 */,
/* 473 */,
/* 474 */,
/* 475 */,
/* 476 */,
/* 477 */,
/* 478 */,
/* 479 */,
/* 480 */,
/* 481 */,
/* 482 */,
/* 483 */,
/* 484 */,
/* 485 */,
/* 486 */,
/* 487 */,
/* 488 */,
/* 489 */,
/* 490 */,
/* 491 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 174,
	"./af.js": 174,
	"./ar": 181,
	"./ar-dz": 175,
	"./ar-dz.js": 175,
	"./ar-kw": 176,
	"./ar-kw.js": 176,
	"./ar-ly": 177,
	"./ar-ly.js": 177,
	"./ar-ma": 178,
	"./ar-ma.js": 178,
	"./ar-sa": 179,
	"./ar-sa.js": 179,
	"./ar-tn": 180,
	"./ar-tn.js": 180,
	"./ar.js": 181,
	"./az": 182,
	"./az.js": 182,
	"./be": 183,
	"./be.js": 183,
	"./bg": 184,
	"./bg.js": 184,
	"./bn": 185,
	"./bn.js": 185,
	"./bo": 186,
	"./bo.js": 186,
	"./br": 187,
	"./br.js": 187,
	"./bs": 188,
	"./bs.js": 188,
	"./ca": 189,
	"./ca.js": 189,
	"./cs": 190,
	"./cs.js": 190,
	"./cv": 191,
	"./cv.js": 191,
	"./cy": 192,
	"./cy.js": 192,
	"./da": 193,
	"./da.js": 193,
	"./de": 196,
	"./de-at": 194,
	"./de-at.js": 194,
	"./de-ch": 195,
	"./de-ch.js": 195,
	"./de.js": 196,
	"./dv": 197,
	"./dv.js": 197,
	"./el": 198,
	"./el.js": 198,
	"./en-au": 199,
	"./en-au.js": 199,
	"./en-ca": 200,
	"./en-ca.js": 200,
	"./en-gb": 201,
	"./en-gb.js": 201,
	"./en-ie": 202,
	"./en-ie.js": 202,
	"./en-nz": 203,
	"./en-nz.js": 203,
	"./eo": 204,
	"./eo.js": 204,
	"./es": 206,
	"./es-do": 205,
	"./es-do.js": 205,
	"./es.js": 206,
	"./et": 207,
	"./et.js": 207,
	"./eu": 208,
	"./eu.js": 208,
	"./fa": 209,
	"./fa.js": 209,
	"./fi": 210,
	"./fi.js": 210,
	"./fo": 211,
	"./fo.js": 211,
	"./fr": 214,
	"./fr-ca": 212,
	"./fr-ca.js": 212,
	"./fr-ch": 213,
	"./fr-ch.js": 213,
	"./fr.js": 214,
	"./fy": 215,
	"./fy.js": 215,
	"./gd": 216,
	"./gd.js": 216,
	"./gl": 217,
	"./gl.js": 217,
	"./gom-latn": 218,
	"./gom-latn.js": 218,
	"./he": 219,
	"./he.js": 219,
	"./hi": 220,
	"./hi.js": 220,
	"./hr": 221,
	"./hr.js": 221,
	"./hu": 222,
	"./hu.js": 222,
	"./hy-am": 223,
	"./hy-am.js": 223,
	"./id": 224,
	"./id.js": 224,
	"./is": 225,
	"./is.js": 225,
	"./it": 226,
	"./it.js": 226,
	"./ja": 227,
	"./ja.js": 227,
	"./jv": 228,
	"./jv.js": 228,
	"./ka": 229,
	"./ka.js": 229,
	"./kk": 230,
	"./kk.js": 230,
	"./km": 231,
	"./km.js": 231,
	"./kn": 232,
	"./kn.js": 232,
	"./ko": 233,
	"./ko.js": 233,
	"./ky": 234,
	"./ky.js": 234,
	"./lb": 235,
	"./lb.js": 235,
	"./lo": 236,
	"./lo.js": 236,
	"./lt": 237,
	"./lt.js": 237,
	"./lv": 238,
	"./lv.js": 238,
	"./me": 239,
	"./me.js": 239,
	"./mi": 240,
	"./mi.js": 240,
	"./mk": 241,
	"./mk.js": 241,
	"./ml": 242,
	"./ml.js": 242,
	"./mr": 243,
	"./mr.js": 243,
	"./ms": 245,
	"./ms-my": 244,
	"./ms-my.js": 244,
	"./ms.js": 245,
	"./my": 246,
	"./my.js": 246,
	"./nb": 247,
	"./nb.js": 247,
	"./ne": 248,
	"./ne.js": 248,
	"./nl": 250,
	"./nl-be": 249,
	"./nl-be.js": 249,
	"./nl.js": 250,
	"./nn": 251,
	"./nn.js": 251,
	"./pa-in": 252,
	"./pa-in.js": 252,
	"./pl": 253,
	"./pl.js": 253,
	"./pt": 255,
	"./pt-br": 254,
	"./pt-br.js": 254,
	"./pt.js": 255,
	"./ro": 256,
	"./ro.js": 256,
	"./ru": 257,
	"./ru.js": 257,
	"./sd": 258,
	"./sd.js": 258,
	"./se": 259,
	"./se.js": 259,
	"./si": 260,
	"./si.js": 260,
	"./sk": 261,
	"./sk.js": 261,
	"./sl": 262,
	"./sl.js": 262,
	"./sq": 263,
	"./sq.js": 263,
	"./sr": 265,
	"./sr-cyrl": 264,
	"./sr-cyrl.js": 264,
	"./sr.js": 265,
	"./ss": 266,
	"./ss.js": 266,
	"./sv": 267,
	"./sv.js": 267,
	"./sw": 268,
	"./sw.js": 268,
	"./ta": 269,
	"./ta.js": 269,
	"./te": 270,
	"./te.js": 270,
	"./tet": 271,
	"./tet.js": 271,
	"./th": 272,
	"./th.js": 272,
	"./tl-ph": 273,
	"./tl-ph.js": 273,
	"./tlh": 274,
	"./tlh.js": 274,
	"./tr": 275,
	"./tr.js": 275,
	"./tzl": 276,
	"./tzl.js": 276,
	"./tzm": 278,
	"./tzm-latn": 277,
	"./tzm-latn.js": 277,
	"./tzm.js": 278,
	"./uk": 279,
	"./uk.js": 279,
	"./ur": 280,
	"./ur.js": 280,
	"./uz": 282,
	"./uz-latn": 281,
	"./uz-latn.js": 281,
	"./uz.js": 282,
	"./vi": 283,
	"./vi.js": 283,
	"./x-pseudo": 284,
	"./x-pseudo.js": 284,
	"./yo": 285,
	"./yo.js": 285,
	"./zh-cn": 286,
	"./zh-cn.js": 286,
	"./zh-hk": 287,
	"./zh-hk.js": 287,
	"./zh-tw": 288,
	"./zh-tw.js": 288
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 491;

/***/ }),
/* 492 */,
/* 493 */,
/* 494 */,
/* 495 */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),
/* 496 */
/***/ (function(module, exports) {

module.exports = "@charset \"UTF-8\";\n/*! normalize.css v6.0.0 | MIT License | github.com/necolas/normalize.css */\n/* Document\n   ========================================================================== */\n/**\n * 1. Correct the line height in all browsers.\n * 2. Prevent adjustments of font size after orientation changes in\n *    IE on Windows Phone and in iOS.\n */\nhtml {\n  line-height: 1.15;\n  /* 1 */\n  -ms-text-size-adjust: 100%;\n  /* 2 */\n  -webkit-text-size-adjust: 100%;\n  /* 2 */ }\n\n/* Sections\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\narticle,\naside,\nfooter,\nheader,\nnav,\nsection {\n  display: block; }\n\n/**\n * Correct the font size and margin on `h1` elements within `section` and\n * `article` contexts in Chrome, Firefox, and Safari.\n */\nh1 {\n  font-size: 2em;\n  margin: 0.67em 0; }\n\n/* Grouping content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n * 1. Add the correct display in IE.\n */\nfigcaption,\nfigure,\nmain {\n  /* 1 */\n  display: block; }\n\n/**\n * Add the correct margin in IE 8.\n */\nfigure {\n  margin: 1em 40px; }\n\n/**\n * 1. Add the correct box sizing in Firefox.\n * 2. Show the overflow in Edge and IE.\n */\nhr {\n  box-sizing: content-box;\n  /* 1 */\n  height: 0;\n  /* 1 */\n  overflow: visible;\n  /* 2 */ }\n\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\npre {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */ }\n\n/* Text-level semantics\n   ========================================================================== */\n/**\n * 1. Remove the gray background on active links in IE 10.\n * 2. Remove gaps in links underline in iOS 8+ and Safari 8+.\n */\na {\n  background-color: transparent;\n  /* 1 */\n  -webkit-text-decoration-skip: objects;\n  /* 2 */ }\n\n/**\n * 1. Remove the bottom border in Chrome 57- and Firefox 39-.\n * 2. Add the correct text decoration in Chrome, Edge, IE, Opera, and Safari.\n */\nabbr[title] {\n  border-bottom: none;\n  /* 1 */\n  text-decoration: underline;\n  /* 2 */\n  text-decoration: underline dotted;\n  /* 2 */ }\n\n/**\n * Prevent the duplicate application of `bolder` by the next rule in Safari 6.\n */\nb,\nstrong {\n  font-weight: inherit; }\n\n/**\n * Add the correct font weight in Chrome, Edge, and Safari.\n */\nb,\nstrong {\n  font-weight: bolder; }\n\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\ncode,\nkbd,\nsamp {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */ }\n\n/**\n * Add the correct font style in Android 4.3-.\n */\ndfn {\n  font-style: italic; }\n\n/**\n * Add the correct background and color in IE 9-.\n */\nmark {\n  background-color: #ff0;\n  color: #000; }\n\n/**\n * Add the correct font size in all browsers.\n */\nsmall {\n  font-size: 80%; }\n\n/**\n * Prevent `sub` and `sup` elements from affecting the line height in\n * all browsers.\n */\nsub,\nsup {\n  font-size: 75%;\n  line-height: 0;\n  position: relative;\n  vertical-align: baseline; }\n\nsub {\n  bottom: -0.25em; }\n\nsup {\n  top: -0.5em; }\n\n/* Embedded content\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\naudio,\nvideo {\n  display: inline-block; }\n\n/**\n * Add the correct display in iOS 4-7.\n */\naudio:not([controls]) {\n  display: none;\n  height: 0; }\n\n/**\n * Remove the border on images inside links in IE 10-.\n */\nimg {\n  border-style: none; }\n\n/**\n * Hide the overflow in IE.\n */\nsvg:not(:root) {\n  overflow: hidden; }\n\n/* Forms\n   ========================================================================== */\n/**\n * Remove the margin in Firefox and Safari.\n */\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  margin: 0; }\n\n/**\n * Show the overflow in IE.\n * 1. Show the overflow in Edge.\n */\nbutton,\ninput {\n  /* 1 */\n  overflow: visible; }\n\n/**\n * Remove the inheritance of text transform in Edge, Firefox, and IE.\n * 1. Remove the inheritance of text transform in Firefox.\n */\nbutton,\nselect {\n  /* 1 */\n  text-transform: none; }\n\n/**\n * 1. Prevent a WebKit bug where (2) destroys native `audio` and `video`\n *    controls in Android 4.\n * 2. Correct the inability to style clickable types in iOS and Safari.\n */\nbutton,\nhtml [type=\"button\"],\n[type=\"reset\"],\n[type=\"submit\"] {\n  -webkit-appearance: button;\n  /* 2 */ }\n\n/**\n * Remove the inner border and padding in Firefox.\n */\nbutton::-moz-focus-inner,\n[type=\"button\"]::-moz-focus-inner,\n[type=\"reset\"]::-moz-focus-inner,\n[type=\"submit\"]::-moz-focus-inner {\n  border-style: none;\n  padding: 0; }\n\n/**\n * Restore the focus styles unset by the previous rule.\n */\nbutton:-moz-focusring,\n[type=\"button\"]:-moz-focusring,\n[type=\"reset\"]:-moz-focusring,\n[type=\"submit\"]:-moz-focusring {\n  outline: 1px dotted ButtonText; }\n\n/**\n * 1. Correct the text wrapping in Edge and IE.\n * 2. Correct the color inheritance from `fieldset` elements in IE.\n * 3. Remove the padding so developers are not caught out when they zero out\n *    `fieldset` elements in all browsers.\n */\nlegend {\n  box-sizing: border-box;\n  /* 1 */\n  color: inherit;\n  /* 2 */\n  display: table;\n  /* 1 */\n  max-width: 100%;\n  /* 1 */\n  padding: 0;\n  /* 3 */\n  white-space: normal;\n  /* 1 */ }\n\n/**\n * 1. Add the correct display in IE 9-.\n * 2. Add the correct vertical alignment in Chrome, Firefox, and Opera.\n */\nprogress {\n  display: inline-block;\n  /* 1 */\n  vertical-align: baseline;\n  /* 2 */ }\n\n/**\n * Remove the default vertical scrollbar in IE.\n */\ntextarea {\n  overflow: auto; }\n\n/**\n * 1. Add the correct box sizing in IE 10-.\n * 2. Remove the padding in IE 10-.\n */\n[type=\"checkbox\"],\n[type=\"radio\"] {\n  box-sizing: border-box;\n  /* 1 */\n  padding: 0;\n  /* 2 */ }\n\n/**\n * Correct the cursor style of increment and decrement buttons in Chrome.\n */\n[type=\"number\"]::-webkit-inner-spin-button,\n[type=\"number\"]::-webkit-outer-spin-button {\n  height: auto; }\n\n/**\n * 1. Correct the odd appearance in Chrome and Safari.\n * 2. Correct the outline style in Safari.\n */\n[type=\"search\"] {\n  -webkit-appearance: textfield;\n  /* 1 */\n  outline-offset: -2px;\n  /* 2 */ }\n\n/**\n * Remove the inner padding and cancel buttons in Chrome and Safari on macOS.\n */\n[type=\"search\"]::-webkit-search-cancel-button,\n[type=\"search\"]::-webkit-search-decoration {\n  -webkit-appearance: none; }\n\n/**\n * 1. Correct the inability to style clickable types in iOS and Safari.\n * 2. Change font properties to `inherit` in Safari.\n */\n::-webkit-file-upload-button {\n  -webkit-appearance: button;\n  /* 1 */\n  font: inherit;\n  /* 2 */ }\n\n/* Interactive\n   ========================================================================== */\n/*\n * Add the correct display in IE 9-.\n * 1. Add the correct display in Edge, IE, and Firefox.\n */\ndetails,\nmenu {\n  display: block; }\n\n/*\n * Add the correct display in all browsers.\n */\nsummary {\n  display: list-item; }\n\n/* Scripting\n   ========================================================================== */\n/**\n * Add the correct display in IE 9-.\n */\ncanvas {\n  display: inline-block; }\n\n/**\n * Add the correct display in IE.\n */\ntemplate {\n  display: none; }\n\n/* Hidden\n   ========================================================================== */\n/**\n * Add the correct display in IE 10-.\n */\n[hidden] {\n  display: none; }\n\n.card.card-blur {\n  background: url(\"assets/img/blur-bg-blurred.jpg\");\n  transition: none;\n  background-attachment: fixed; }\n  .card.card-blur .card-header, .card.card-blur .card-footer {\n    background: transparent; }\n\n.card {\n  color: #ffffff;\n  background-color: rgba(255, 255, 255, 0.1);\n  border: 0;\n  border-radius: 7px;\n  position: relative;\n  margin-bottom: 24px;\n  box-shadow: 1px 1px 4px rgba(0, 0, 0, 0.15); }\n  .card ::-webkit-scrollbar {\n    width: 0.4em;\n    height: 0.4em; }\n  .card ::-webkit-scrollbar-thumb {\n    background: rgba(0, 0, 0, 0.6);\n    cursor: pointer; }\n  .card ::-webkit-scrollbar-track {\n    background: rgba(255, 255, 255, 0.7); }\n  .card body {\n    scrollbar-face-color: rgba(0, 0, 0, 0.6);\n    scrollbar-track-color: rgba(255, 255, 255, 0.7); }\n  .card.animated {\n    animation-duration: 0.5s; }\n  .card.small-card {\n    height: 114px; }\n  .card.xsmall-card {\n    height: 187px; }\n  .card.medium-card {\n    height: 400px; }\n  .card.xmedium-card {\n    height: 550px; }\n  .card.large-card {\n    height: 974px; }\n  .card.viewport100 {\n    height: calc(100vh - 218px); }\n  .card.with-scroll .card-body {\n    height: calc(100% - 44px);\n    overflow-y: auto;\n    flex: 1; }\n\n.card > .card-body {\n  padding: 15px 22px;\n  height: 100%; }\n\n.card > .card-header {\n  color: #ffffff;\n  border-bottom-left-radius: 0;\n  border-bottom-right-radius: 0; }\n\n.card > .card-footer {\n  color: #ffffff; }\n\n.card-header, .card-footer {\n  color: #ffffff;\n  border-bottom: 1px solid rgba(255, 255, 255, 0.3);\n  height: 44px;\n  font-size: 16px;\n  padding: 14px 22px;\n  background-color: transparent; }\n\n.card-title {\n  font-weight: 400;\n  font-size: 16px;\n  text-transform: uppercase;\n  opacity: 0.9;\n  color: #ffffff; }\n\n.card-primary > .card-header {\n  background-color: #33bcff;\n  border-color: #33bcff; }\n\n.card-success > .card-header {\n  background-color: #a2db59;\n  border-color: #a2db59; }\n\n.card-info > .card-header {\n  background-color: #66e1f4;\n  border-color: #66e1f4; }\n\n.card-warning > .card-header {\n  background-color: #ecc839;\n  border-color: #ecc839; }\n\n.card-danger > .card-header {\n  background-color: #fa758e;\n  border-color: #fa758e; }\n\n.accordion-card.card.card-primary .card-header p, .accordion-card.card.card-primary .card-header div, .accordion-card.card.card-primary .card-header span, .accordion-card.card.card-success .card-header p, .accordion-card.card.card-success .card-header div, .accordion-card.card.card-success .card-header span, .accordion-card.card.card-info .card-header p, .accordion-card.card.card-info .card-header div, .accordion-card.card.card-info .card-header span, .accordion-card.card.card-warning .card-header p, .accordion-card.card.card-warning .card-header div, .accordion-card.card.card-warning .card-header span, .accordion-card.card.card-danger .card-header p, .accordion-card.card.card-danger .card-header div, .accordion-card.card.card-danger .card-header span {\n  color: rgba(255, 255, 255, 0.8); }\n\n.card-group .card.accordion-card .card-header {\n  border-bottom: 0; }\n\n.card-group .card .card-header {\n  border-bottom: 1px solid #ddd; }\n\n.p-with-code {\n  line-height: 1.5em; }\n\n.contextual-example-card {\n  height: 120px; }\n\n.footer-card {\n  height: 142px; }\n\n.light-text {\n  font-weight: 300; }\n\n.dropdown-item {\n  line-height: 1; }\n\n.dropdown-menu {\n  font-size: inherit; }\n\n/** Different tabs positions, which were removed from bootstrap */\n.tabs-below .nav-tabs, .tabs-right .nav-tabs, .tabs-left .nav-tabs {\n  border-bottom: 0; }\n\n.tabs-right .nav-tabs, .tabs-left .nav-tabs {\n  min-width: 100px; }\n\n.tabs-right .tab-content, .tabs-left .tab-content {\n  width: calc(100% - 100px);\n  overflow-y: auto; }\n\n.tabs-right .tab-content {\n  margin-right: 100px; }\n\n.tabs-left .tab-content {\n  margin-left: 100px; }\n\n.tab-content > .tab-pane,\n.pill-content > .pill-pane {\n  display: none; }\n\n.tab-content > .active,\n.pill-content > .active {\n  display: block; }\n\n.tabs-below > .nav-tabs > li {\n  margin-top: -1px;\n  margin-bottom: 0; }\n\n.tabs-left, .tabs-right {\n  height: 100%; }\n  .tabs-left > .nav-tabs > li, .tabs-right > .nav-tabs > li {\n    float: none;\n    margin-bottom: 0; }\n    .tabs-left > .nav-tabs > li > a, .tabs-right > .nav-tabs > li > a {\n      min-width: 74px;\n      margin-right: 0;\n      margin-bottom: 3px; }\n\n.tabs-left > .nav-tabs {\n  float: left;\n  border-bottom-left-radius: 5px; }\n  .tabs-left > .nav-tabs > li > a {\n    margin-right: -1px; }\n\n.tabs-right > .nav.nav-tabs {\n  float: right;\n  border-top-left-radius: 0;\n  border-bottom-right-radius: 5px; }\n  .tabs-right > .nav.nav-tabs > li:first-of-type a {\n    border-top-left-radius: 0; }\n\n/** /Different tabs positions, which were removed from bootstrap */\n.nav-tabs > li.with-dropdown > a {\n  padding: 0; }\n\n.nav-tabs > li.with-dropdown .dropdown-toggle {\n  padding: 10px 15px;\n  display: inline-block;\n  cursor: pointer; }\n\n.tab-content {\n  padding: 15px 15px 5px 15px;\n  background: transparent;\n  color: #ffffff; }\n  .tab-content .tab-pane p {\n    color: #ffffff; }\n\n.nav.nav-tabs {\n  border-top-left-radius: 5px;\n  border-top-right-radius: 5px;\n  border-bottom: 1px solid transparent;\n  background-color: #4dc4ff; }\n  .nav.nav-tabs a {\n    color: #ffffff; }\n    .nav.nav-tabs a:hover {\n      color: #ffffff; }\n  .nav.nav-tabs > li > a {\n    margin-right: 0;\n    margin-bottom: 0;\n    border-radius: 0;\n    border: none; }\n    .nav.nav-tabs > li > a:hover {\n      border: none;\n      background-color: #4dc4ff; }\n  .nav.nav-tabs > li.active > a {\n    color: #ffffff;\n    background-color: #00abff; }\n  .nav.nav-tabs > li:first-of-type a {\n    border-top-left-radius: 5px; }\n  .nav.nav-tabs .dropdown-menu > li > a {\n    color: #7d7d7d; }\n    .nav.nav-tabs .dropdown-menu > li > a:hover {\n      color: #7d7d7d; }\n\n.blur .nav.nav-tabs {\n  background-color: rgba(0, 0, 0, 0.2); }\n  .blur .nav.nav-tabs a {\n    color: #ffffff; }\n    .blur .nav.nav-tabs a:hover {\n      color: #ffffff; }\n  .blur .nav.nav-tabs > li > a:hover {\n    background-color: rgba(0, 0, 0, 0.2); }\n  .blur .nav.nav-tabs > li.active > a {\n    color: #ffffff;\n    background-color: rgba(0, 0, 0, 0.25); }\n\n.nav .open > a, .nav .open > a:hover, .nav .open > a:focus {\n  background-color: transparent; }\n\n.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {\n  border: none; }\n\n.accordion-panel .panel-heading {\n  border-radius: 3px; }\n\n.accordion-panel.panel-open .panel-heading {\n  border-bottom-left-radius: 0;\n  border-bottom-right-radius: 0; }\n\n.accordion-panel:not(.panel-open) .panel-heading {\n  transition-delay: .3s; }\n\n.accordion-panel > .panel-heading + .panel-collapse > .panel-body {\n  border-top: none; }\n\n.accordion-panel .panel-heading {\n  padding: 0; }\n  .accordion-panel .panel-heading .accordion-toggle {\n    display: inline-block;\n    width: 100%;\n    padding: 14px 22px; }\n\n/*!\n * animate.css -http://daneden.me/animate\n * Version - 3.5.1\n * Licensed under the MIT license - http://opensource.org/licenses/MIT\n *\n * Copyright (c) 2016 Daniel Eden\n */\n.animated {\n  -webkit-animation-duration: 1s;\n  animation-duration: 1s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both; }\n\n.animated.infinite {\n  -webkit-animation-iteration-count: infinite;\n  animation-iteration-count: infinite; }\n\n.animated.hinge {\n  -webkit-animation-duration: 2s;\n  animation-duration: 2s; }\n\n.animated.flipOutX,\n.animated.flipOutY,\n.animated.bounceIn,\n.animated.bounceOut {\n  -webkit-animation-duration: .75s;\n  animation-duration: .75s; }\n\n@-webkit-keyframes bounce {\n  from, 20%, 53%, 80%, to {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0); }\n  40%, 43% {\n    -webkit-animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n    animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n    -webkit-transform: translate3d(0, -30px, 0);\n    transform: translate3d(0, -30px, 0); }\n  70% {\n    -webkit-animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n    animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n    -webkit-transform: translate3d(0, -15px, 0);\n    transform: translate3d(0, -15px, 0); }\n  90% {\n    -webkit-transform: translate3d(0, -4px, 0);\n    transform: translate3d(0, -4px, 0); } }\n\n@keyframes bounce {\n  from, 20%, 53%, 80%, to {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0); }\n  40%, 43% {\n    -webkit-animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n    animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n    -webkit-transform: translate3d(0, -30px, 0);\n    transform: translate3d(0, -30px, 0); }\n  70% {\n    -webkit-animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n    animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n    -webkit-transform: translate3d(0, -15px, 0);\n    transform: translate3d(0, -15px, 0); }\n  90% {\n    -webkit-transform: translate3d(0, -4px, 0);\n    transform: translate3d(0, -4px, 0); } }\n\n.bounce {\n  -webkit-animation-name: bounce;\n  animation-name: bounce;\n  -webkit-transform-origin: center bottom;\n  transform-origin: center bottom; }\n\n@-webkit-keyframes flash {\n  from, 50%, to {\n    opacity: 1; }\n  25%, 75% {\n    opacity: 0; } }\n\n@keyframes flash {\n  from, 50%, to {\n    opacity: 1; }\n  25%, 75% {\n    opacity: 0; } }\n\n.flash {\n  -webkit-animation-name: flash;\n  animation-name: flash; }\n\n/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */\n@-webkit-keyframes pulse {\n  from {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1); }\n  50% {\n    -webkit-transform: scale3d(1.05, 1.05, 1.05);\n    transform: scale3d(1.05, 1.05, 1.05); }\n  to {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1); } }\n\n@keyframes pulse {\n  from {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1); }\n  50% {\n    -webkit-transform: scale3d(1.05, 1.05, 1.05);\n    transform: scale3d(1.05, 1.05, 1.05); }\n  to {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1); } }\n\n.pulse {\n  -webkit-animation-name: pulse;\n  animation-name: pulse; }\n\n@-webkit-keyframes rubberBand {\n  from {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1); }\n  30% {\n    -webkit-transform: scale3d(1.25, 0.75, 1);\n    transform: scale3d(1.25, 0.75, 1); }\n  40% {\n    -webkit-transform: scale3d(0.75, 1.25, 1);\n    transform: scale3d(0.75, 1.25, 1); }\n  50% {\n    -webkit-transform: scale3d(1.15, 0.85, 1);\n    transform: scale3d(1.15, 0.85, 1); }\n  65% {\n    -webkit-transform: scale3d(0.95, 1.05, 1);\n    transform: scale3d(0.95, 1.05, 1); }\n  75% {\n    -webkit-transform: scale3d(1.05, 0.95, 1);\n    transform: scale3d(1.05, 0.95, 1); }\n  to {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1); } }\n\n@keyframes rubberBand {\n  from {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1); }\n  30% {\n    -webkit-transform: scale3d(1.25, 0.75, 1);\n    transform: scale3d(1.25, 0.75, 1); }\n  40% {\n    -webkit-transform: scale3d(0.75, 1.25, 1);\n    transform: scale3d(0.75, 1.25, 1); }\n  50% {\n    -webkit-transform: scale3d(1.15, 0.85, 1);\n    transform: scale3d(1.15, 0.85, 1); }\n  65% {\n    -webkit-transform: scale3d(0.95, 1.05, 1);\n    transform: scale3d(0.95, 1.05, 1); }\n  75% {\n    -webkit-transform: scale3d(1.05, 0.95, 1);\n    transform: scale3d(1.05, 0.95, 1); }\n  to {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1); } }\n\n.rubberBand {\n  -webkit-animation-name: rubberBand;\n  animation-name: rubberBand; }\n\n@-webkit-keyframes shake {\n  from, to {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0); }\n  10%, 30%, 50%, 70%, 90% {\n    -webkit-transform: translate3d(-10px, 0, 0);\n    transform: translate3d(-10px, 0, 0); }\n  20%, 40%, 60%, 80% {\n    -webkit-transform: translate3d(10px, 0, 0);\n    transform: translate3d(10px, 0, 0); } }\n\n@keyframes shake {\n  from, to {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0); }\n  10%, 30%, 50%, 70%, 90% {\n    -webkit-transform: translate3d(-10px, 0, 0);\n    transform: translate3d(-10px, 0, 0); }\n  20%, 40%, 60%, 80% {\n    -webkit-transform: translate3d(10px, 0, 0);\n    transform: translate3d(10px, 0, 0); } }\n\n.shake {\n  -webkit-animation-name: shake;\n  animation-name: shake; }\n\n@-webkit-keyframes headShake {\n  0% {\n    -webkit-transform: translateX(0);\n    transform: translateX(0); }\n  6.5% {\n    -webkit-transform: translateX(-6px) rotateY(-9deg);\n    transform: translateX(-6px) rotateY(-9deg); }\n  18.5% {\n    -webkit-transform: translateX(5px) rotateY(7deg);\n    transform: translateX(5px) rotateY(7deg); }\n  31.5% {\n    -webkit-transform: translateX(-3px) rotateY(-5deg);\n    transform: translateX(-3px) rotateY(-5deg); }\n  43.5% {\n    -webkit-transform: translateX(2px) rotateY(3deg);\n    transform: translateX(2px) rotateY(3deg); }\n  50% {\n    -webkit-transform: translateX(0);\n    transform: translateX(0); } }\n\n@keyframes headShake {\n  0% {\n    -webkit-transform: translateX(0);\n    transform: translateX(0); }\n  6.5% {\n    -webkit-transform: translateX(-6px) rotateY(-9deg);\n    transform: translateX(-6px) rotateY(-9deg); }\n  18.5% {\n    -webkit-transform: translateX(5px) rotateY(7deg);\n    transform: translateX(5px) rotateY(7deg); }\n  31.5% {\n    -webkit-transform: translateX(-3px) rotateY(-5deg);\n    transform: translateX(-3px) rotateY(-5deg); }\n  43.5% {\n    -webkit-transform: translateX(2px) rotateY(3deg);\n    transform: translateX(2px) rotateY(3deg); }\n  50% {\n    -webkit-transform: translateX(0);\n    transform: translateX(0); } }\n\n.headShake {\n  -webkit-animation-timing-function: ease-in-out;\n  animation-timing-function: ease-in-out;\n  -webkit-animation-name: headShake;\n  animation-name: headShake; }\n\n@-webkit-keyframes swing {\n  20% {\n    -webkit-transform: rotate3d(0, 0, 1, 15deg);\n    transform: rotate3d(0, 0, 1, 15deg); }\n  40% {\n    -webkit-transform: rotate3d(0, 0, 1, -10deg);\n    transform: rotate3d(0, 0, 1, -10deg); }\n  60% {\n    -webkit-transform: rotate3d(0, 0, 1, 5deg);\n    transform: rotate3d(0, 0, 1, 5deg); }\n  80% {\n    -webkit-transform: rotate3d(0, 0, 1, -5deg);\n    transform: rotate3d(0, 0, 1, -5deg); }\n  to {\n    -webkit-transform: rotate3d(0, 0, 1, 0deg);\n    transform: rotate3d(0, 0, 1, 0deg); } }\n\n@keyframes swing {\n  20% {\n    -webkit-transform: rotate3d(0, 0, 1, 15deg);\n    transform: rotate3d(0, 0, 1, 15deg); }\n  40% {\n    -webkit-transform: rotate3d(0, 0, 1, -10deg);\n    transform: rotate3d(0, 0, 1, -10deg); }\n  60% {\n    -webkit-transform: rotate3d(0, 0, 1, 5deg);\n    transform: rotate3d(0, 0, 1, 5deg); }\n  80% {\n    -webkit-transform: rotate3d(0, 0, 1, -5deg);\n    transform: rotate3d(0, 0, 1, -5deg); }\n  to {\n    -webkit-transform: rotate3d(0, 0, 1, 0deg);\n    transform: rotate3d(0, 0, 1, 0deg); } }\n\n.swing {\n  -webkit-transform-origin: top center;\n  transform-origin: top center;\n  -webkit-animation-name: swing;\n  animation-name: swing; }\n\n@-webkit-keyframes tada {\n  from {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1); }\n  10%, 20% {\n    -webkit-transform: scale3d(0.9, 0.9, 0.9) rotate3d(0, 0, 1, -3deg);\n    transform: scale3d(0.9, 0.9, 0.9) rotate3d(0, 0, 1, -3deg); }\n  30%, 50%, 70%, 90% {\n    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);\n    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg); }\n  40%, 60%, 80% {\n    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);\n    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg); }\n  to {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1); } }\n\n@keyframes tada {\n  from {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1); }\n  10%, 20% {\n    -webkit-transform: scale3d(0.9, 0.9, 0.9) rotate3d(0, 0, 1, -3deg);\n    transform: scale3d(0.9, 0.9, 0.9) rotate3d(0, 0, 1, -3deg); }\n  30%, 50%, 70%, 90% {\n    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);\n    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg); }\n  40%, 60%, 80% {\n    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);\n    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg); }\n  to {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1); } }\n\n.tada {\n  -webkit-animation-name: tada;\n  animation-name: tada; }\n\n/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */\n@-webkit-keyframes wobble {\n  from {\n    -webkit-transform: none;\n    transform: none; }\n  15% {\n    -webkit-transform: translate3d(-25%, 0, 0) rotate3d(0, 0, 1, -5deg);\n    transform: translate3d(-25%, 0, 0) rotate3d(0, 0, 1, -5deg); }\n  30% {\n    -webkit-transform: translate3d(20%, 0, 0) rotate3d(0, 0, 1, 3deg);\n    transform: translate3d(20%, 0, 0) rotate3d(0, 0, 1, 3deg); }\n  45% {\n    -webkit-transform: translate3d(-15%, 0, 0) rotate3d(0, 0, 1, -3deg);\n    transform: translate3d(-15%, 0, 0) rotate3d(0, 0, 1, -3deg); }\n  60% {\n    -webkit-transform: translate3d(10%, 0, 0) rotate3d(0, 0, 1, 2deg);\n    transform: translate3d(10%, 0, 0) rotate3d(0, 0, 1, 2deg); }\n  75% {\n    -webkit-transform: translate3d(-5%, 0, 0) rotate3d(0, 0, 1, -1deg);\n    transform: translate3d(-5%, 0, 0) rotate3d(0, 0, 1, -1deg); }\n  to {\n    -webkit-transform: none;\n    transform: none; } }\n\n@keyframes wobble {\n  from {\n    -webkit-transform: none;\n    transform: none; }\n  15% {\n    -webkit-transform: translate3d(-25%, 0, 0) rotate3d(0, 0, 1, -5deg);\n    transform: translate3d(-25%, 0, 0) rotate3d(0, 0, 1, -5deg); }\n  30% {\n    -webkit-transform: translate3d(20%, 0, 0) rotate3d(0, 0, 1, 3deg);\n    transform: translate3d(20%, 0, 0) rotate3d(0, 0, 1, 3deg); }\n  45% {\n    -webkit-transform: translate3d(-15%, 0, 0) rotate3d(0, 0, 1, -3deg);\n    transform: translate3d(-15%, 0, 0) rotate3d(0, 0, 1, -3deg); }\n  60% {\n    -webkit-transform: translate3d(10%, 0, 0) rotate3d(0, 0, 1, 2deg);\n    transform: translate3d(10%, 0, 0) rotate3d(0, 0, 1, 2deg); }\n  75% {\n    -webkit-transform: translate3d(-5%, 0, 0) rotate3d(0, 0, 1, -1deg);\n    transform: translate3d(-5%, 0, 0) rotate3d(0, 0, 1, -1deg); }\n  to {\n    -webkit-transform: none;\n    transform: none; } }\n\n.wobble {\n  -webkit-animation-name: wobble;\n  animation-name: wobble; }\n\n@-webkit-keyframes jello {\n  from, 11.1%, to {\n    -webkit-transform: none;\n    transform: none; }\n  22.2% {\n    -webkit-transform: skewX(-12.5deg) skewY(-12.5deg);\n    transform: skewX(-12.5deg) skewY(-12.5deg); }\n  33.3% {\n    -webkit-transform: skewX(6.25deg) skewY(6.25deg);\n    transform: skewX(6.25deg) skewY(6.25deg); }\n  44.4% {\n    -webkit-transform: skewX(-3.125deg) skewY(-3.125deg);\n    transform: skewX(-3.125deg) skewY(-3.125deg); }\n  55.5% {\n    -webkit-transform: skewX(1.5625deg) skewY(1.5625deg);\n    transform: skewX(1.5625deg) skewY(1.5625deg); }\n  66.6% {\n    -webkit-transform: skewX(-0.78125deg) skewY(-0.78125deg);\n    transform: skewX(-0.78125deg) skewY(-0.78125deg); }\n  77.7% {\n    -webkit-transform: skewX(0.39062deg) skewY(0.39062deg);\n    transform: skewX(0.39062deg) skewY(0.39062deg); }\n  88.8% {\n    -webkit-transform: skewX(-0.19531deg) skewY(-0.19531deg);\n    transform: skewX(-0.19531deg) skewY(-0.19531deg); } }\n\n@keyframes jello {\n  from, 11.1%, to {\n    -webkit-transform: none;\n    transform: none; }\n  22.2% {\n    -webkit-transform: skewX(-12.5deg) skewY(-12.5deg);\n    transform: skewX(-12.5deg) skewY(-12.5deg); }\n  33.3% {\n    -webkit-transform: skewX(6.25deg) skewY(6.25deg);\n    transform: skewX(6.25deg) skewY(6.25deg); }\n  44.4% {\n    -webkit-transform: skewX(-3.125deg) skewY(-3.125deg);\n    transform: skewX(-3.125deg) skewY(-3.125deg); }\n  55.5% {\n    -webkit-transform: skewX(1.5625deg) skewY(1.5625deg);\n    transform: skewX(1.5625deg) skewY(1.5625deg); }\n  66.6% {\n    -webkit-transform: skewX(-0.78125deg) skewY(-0.78125deg);\n    transform: skewX(-0.78125deg) skewY(-0.78125deg); }\n  77.7% {\n    -webkit-transform: skewX(0.39062deg) skewY(0.39062deg);\n    transform: skewX(0.39062deg) skewY(0.39062deg); }\n  88.8% {\n    -webkit-transform: skewX(-0.19531deg) skewY(-0.19531deg);\n    transform: skewX(-0.19531deg) skewY(-0.19531deg); } }\n\n.jello {\n  -webkit-animation-name: jello;\n  animation-name: jello;\n  -webkit-transform-origin: center;\n  transform-origin: center; }\n\n@-webkit-keyframes bounceIn {\n  from, 20%, 40%, 60%, 80%, to {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1); }\n  0% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3); }\n  20% {\n    -webkit-transform: scale3d(1.1, 1.1, 1.1);\n    transform: scale3d(1.1, 1.1, 1.1); }\n  40% {\n    -webkit-transform: scale3d(0.9, 0.9, 0.9);\n    transform: scale3d(0.9, 0.9, 0.9); }\n  60% {\n    opacity: 1;\n    -webkit-transform: scale3d(1.03, 1.03, 1.03);\n    transform: scale3d(1.03, 1.03, 1.03); }\n  80% {\n    -webkit-transform: scale3d(0.97, 0.97, 0.97);\n    transform: scale3d(0.97, 0.97, 0.97); }\n  to {\n    opacity: 1;\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1); } }\n\n@keyframes bounceIn {\n  from, 20%, 40%, 60%, 80%, to {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1); }\n  0% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3); }\n  20% {\n    -webkit-transform: scale3d(1.1, 1.1, 1.1);\n    transform: scale3d(1.1, 1.1, 1.1); }\n  40% {\n    -webkit-transform: scale3d(0.9, 0.9, 0.9);\n    transform: scale3d(0.9, 0.9, 0.9); }\n  60% {\n    opacity: 1;\n    -webkit-transform: scale3d(1.03, 1.03, 1.03);\n    transform: scale3d(1.03, 1.03, 1.03); }\n  80% {\n    -webkit-transform: scale3d(0.97, 0.97, 0.97);\n    transform: scale3d(0.97, 0.97, 0.97); }\n  to {\n    opacity: 1;\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1); } }\n\n.bounceIn {\n  -webkit-animation-name: bounceIn;\n  animation-name: bounceIn; }\n\n@-webkit-keyframes bounceInDown {\n  from, 60%, 75%, 90%, to {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1); }\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -3000px, 0);\n    transform: translate3d(0, -3000px, 0); }\n  60% {\n    opacity: 1;\n    -webkit-transform: translate3d(0, 25px, 0);\n    transform: translate3d(0, 25px, 0); }\n  75% {\n    -webkit-transform: translate3d(0, -10px, 0);\n    transform: translate3d(0, -10px, 0); }\n  90% {\n    -webkit-transform: translate3d(0, 5px, 0);\n    transform: translate3d(0, 5px, 0); }\n  to {\n    -webkit-transform: none;\n    transform: none; } }\n\n@keyframes bounceInDown {\n  from, 60%, 75%, 90%, to {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1); }\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -3000px, 0);\n    transform: translate3d(0, -3000px, 0); }\n  60% {\n    opacity: 1;\n    -webkit-transform: translate3d(0, 25px, 0);\n    transform: translate3d(0, 25px, 0); }\n  75% {\n    -webkit-transform: translate3d(0, -10px, 0);\n    transform: translate3d(0, -10px, 0); }\n  90% {\n    -webkit-transform: translate3d(0, 5px, 0);\n    transform: translate3d(0, 5px, 0); }\n  to {\n    -webkit-transform: none;\n    transform: none; } }\n\n.bounceInDown {\n  -webkit-animation-name: bounceInDown;\n  animation-name: bounceInDown; }\n\n@-webkit-keyframes bounceInLeft {\n  from, 60%, 75%, 90%, to {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1); }\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(-3000px, 0, 0);\n    transform: translate3d(-3000px, 0, 0); }\n  60% {\n    opacity: 1;\n    -webkit-transform: translate3d(25px, 0, 0);\n    transform: translate3d(25px, 0, 0); }\n  75% {\n    -webkit-transform: translate3d(-10px, 0, 0);\n    transform: translate3d(-10px, 0, 0); }\n  90% {\n    -webkit-transform: translate3d(5px, 0, 0);\n    transform: translate3d(5px, 0, 0); }\n  to {\n    -webkit-transform: none;\n    transform: none; } }\n\n@keyframes bounceInLeft {\n  from, 60%, 75%, 90%, to {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1); }\n  0% {\n    opacity: 0;\n    -webkit-transform: translate3d(-3000px, 0, 0);\n    transform: translate3d(-3000px, 0, 0); }\n  60% {\n    opacity: 1;\n    -webkit-transform: translate3d(25px, 0, 0);\n    transform: translate3d(25px, 0, 0); }\n  75% {\n    -webkit-transform: translate3d(-10px, 0, 0);\n    transform: translate3d(-10px, 0, 0); }\n  90% {\n    -webkit-transform: translate3d(5px, 0, 0);\n    transform: translate3d(5px, 0, 0); }\n  to {\n    -webkit-transform: none;\n    transform: none; } }\n\n.bounceInLeft {\n  -webkit-animation-name: bounceInLeft;\n  animation-name: bounceInLeft; }\n\n@-webkit-keyframes bounceInRight {\n  from, 60%, 75%, 90%, to {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1); }\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(3000px, 0, 0);\n    transform: translate3d(3000px, 0, 0); }\n  60% {\n    opacity: 1;\n    -webkit-transform: translate3d(-25px, 0, 0);\n    transform: translate3d(-25px, 0, 0); }\n  75% {\n    -webkit-transform: translate3d(10px, 0, 0);\n    transform: translate3d(10px, 0, 0); }\n  90% {\n    -webkit-transform: translate3d(-5px, 0, 0);\n    transform: translate3d(-5px, 0, 0); }\n  to {\n    -webkit-transform: none;\n    transform: none; } }\n\n@keyframes bounceInRight {\n  from, 60%, 75%, 90%, to {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1); }\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(3000px, 0, 0);\n    transform: translate3d(3000px, 0, 0); }\n  60% {\n    opacity: 1;\n    -webkit-transform: translate3d(-25px, 0, 0);\n    transform: translate3d(-25px, 0, 0); }\n  75% {\n    -webkit-transform: translate3d(10px, 0, 0);\n    transform: translate3d(10px, 0, 0); }\n  90% {\n    -webkit-transform: translate3d(-5px, 0, 0);\n    transform: translate3d(-5px, 0, 0); }\n  to {\n    -webkit-transform: none;\n    transform: none; } }\n\n.bounceInRight {\n  -webkit-animation-name: bounceInRight;\n  animation-name: bounceInRight; }\n\n@-webkit-keyframes bounceInUp {\n  from, 60%, 75%, 90%, to {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1); }\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 3000px, 0);\n    transform: translate3d(0, 3000px, 0); }\n  60% {\n    opacity: 1;\n    -webkit-transform: translate3d(0, -20px, 0);\n    transform: translate3d(0, -20px, 0); }\n  75% {\n    -webkit-transform: translate3d(0, 10px, 0);\n    transform: translate3d(0, 10px, 0); }\n  90% {\n    -webkit-transform: translate3d(0, -5px, 0);\n    transform: translate3d(0, -5px, 0); }\n  to {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0); } }\n\n@keyframes bounceInUp {\n  from, 60%, 75%, 90%, to {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1); }\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 3000px, 0);\n    transform: translate3d(0, 3000px, 0); }\n  60% {\n    opacity: 1;\n    -webkit-transform: translate3d(0, -20px, 0);\n    transform: translate3d(0, -20px, 0); }\n  75% {\n    -webkit-transform: translate3d(0, 10px, 0);\n    transform: translate3d(0, 10px, 0); }\n  90% {\n    -webkit-transform: translate3d(0, -5px, 0);\n    transform: translate3d(0, -5px, 0); }\n  to {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0); } }\n\n.bounceInUp {\n  -webkit-animation-name: bounceInUp;\n  animation-name: bounceInUp; }\n\n@-webkit-keyframes bounceOut {\n  20% {\n    -webkit-transform: scale3d(0.9, 0.9, 0.9);\n    transform: scale3d(0.9, 0.9, 0.9); }\n  50%, 55% {\n    opacity: 1;\n    -webkit-transform: scale3d(1.1, 1.1, 1.1);\n    transform: scale3d(1.1, 1.1, 1.1); }\n  to {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3); } }\n\n@keyframes bounceOut {\n  20% {\n    -webkit-transform: scale3d(0.9, 0.9, 0.9);\n    transform: scale3d(0.9, 0.9, 0.9); }\n  50%, 55% {\n    opacity: 1;\n    -webkit-transform: scale3d(1.1, 1.1, 1.1);\n    transform: scale3d(1.1, 1.1, 1.1); }\n  to {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3); } }\n\n.bounceOut {\n  -webkit-animation-name: bounceOut;\n  animation-name: bounceOut; }\n\n@-webkit-keyframes bounceOutDown {\n  20% {\n    -webkit-transform: translate3d(0, 10px, 0);\n    transform: translate3d(0, 10px, 0); }\n  40%, 45% {\n    opacity: 1;\n    -webkit-transform: translate3d(0, -20px, 0);\n    transform: translate3d(0, -20px, 0); }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 2000px, 0);\n    transform: translate3d(0, 2000px, 0); } }\n\n@keyframes bounceOutDown {\n  20% {\n    -webkit-transform: translate3d(0, 10px, 0);\n    transform: translate3d(0, 10px, 0); }\n  40%, 45% {\n    opacity: 1;\n    -webkit-transform: translate3d(0, -20px, 0);\n    transform: translate3d(0, -20px, 0); }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 2000px, 0);\n    transform: translate3d(0, 2000px, 0); } }\n\n.bounceOutDown {\n  -webkit-animation-name: bounceOutDown;\n  animation-name: bounceOutDown; }\n\n@-webkit-keyframes bounceOutLeft {\n  20% {\n    opacity: 1;\n    -webkit-transform: translate3d(20px, 0, 0);\n    transform: translate3d(20px, 0, 0); }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(-2000px, 0, 0);\n    transform: translate3d(-2000px, 0, 0); } }\n\n@keyframes bounceOutLeft {\n  20% {\n    opacity: 1;\n    -webkit-transform: translate3d(20px, 0, 0);\n    transform: translate3d(20px, 0, 0); }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(-2000px, 0, 0);\n    transform: translate3d(-2000px, 0, 0); } }\n\n.bounceOutLeft {\n  -webkit-animation-name: bounceOutLeft;\n  animation-name: bounceOutLeft; }\n\n@-webkit-keyframes bounceOutRight {\n  20% {\n    opacity: 1;\n    -webkit-transform: translate3d(-20px, 0, 0);\n    transform: translate3d(-20px, 0, 0); }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(2000px, 0, 0);\n    transform: translate3d(2000px, 0, 0); } }\n\n@keyframes bounceOutRight {\n  20% {\n    opacity: 1;\n    -webkit-transform: translate3d(-20px, 0, 0);\n    transform: translate3d(-20px, 0, 0); }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(2000px, 0, 0);\n    transform: translate3d(2000px, 0, 0); } }\n\n.bounceOutRight {\n  -webkit-animation-name: bounceOutRight;\n  animation-name: bounceOutRight; }\n\n@-webkit-keyframes bounceOutUp {\n  20% {\n    -webkit-transform: translate3d(0, -10px, 0);\n    transform: translate3d(0, -10px, 0); }\n  40%, 45% {\n    opacity: 1;\n    -webkit-transform: translate3d(0, 20px, 0);\n    transform: translate3d(0, 20px, 0); }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -2000px, 0);\n    transform: translate3d(0, -2000px, 0); } }\n\n@keyframes bounceOutUp {\n  20% {\n    -webkit-transform: translate3d(0, -10px, 0);\n    transform: translate3d(0, -10px, 0); }\n  40%, 45% {\n    opacity: 1;\n    -webkit-transform: translate3d(0, 20px, 0);\n    transform: translate3d(0, 20px, 0); }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -2000px, 0);\n    transform: translate3d(0, -2000px, 0); } }\n\n.bounceOutUp {\n  -webkit-animation-name: bounceOutUp;\n  animation-name: bounceOutUp; }\n\n@-webkit-keyframes fadeIn {\n  from {\n    opacity: 0; }\n  to {\n    opacity: 1; } }\n\n@keyframes fadeIn {\n  from {\n    opacity: 0; }\n  to {\n    opacity: 1; } }\n\n.fadeIn {\n  -webkit-animation-name: fadeIn;\n  animation-name: fadeIn; }\n\n@-webkit-keyframes fadeInDown {\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -100%, 0);\n    transform: translate3d(0, -100%, 0); }\n  to {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none; } }\n\n@keyframes fadeInDown {\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -100%, 0);\n    transform: translate3d(0, -100%, 0); }\n  to {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none; } }\n\n.fadeInDown {\n  -webkit-animation-name: fadeInDown;\n  animation-name: fadeInDown; }\n\n@-webkit-keyframes fadeInDownBig {\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -2000px, 0);\n    transform: translate3d(0, -2000px, 0); }\n  to {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none; } }\n\n@keyframes fadeInDownBig {\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -2000px, 0);\n    transform: translate3d(0, -2000px, 0); }\n  to {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none; } }\n\n.fadeInDownBig {\n  -webkit-animation-name: fadeInDownBig;\n  animation-name: fadeInDownBig; }\n\n@-webkit-keyframes fadeInLeft {\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(-100%, 0, 0);\n    transform: translate3d(-100%, 0, 0); }\n  to {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none; } }\n\n@keyframes fadeInLeft {\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(-100%, 0, 0);\n    transform: translate3d(-100%, 0, 0); }\n  to {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none; } }\n\n.fadeInLeft {\n  -webkit-animation-name: fadeInLeft;\n  animation-name: fadeInLeft; }\n\n@-webkit-keyframes fadeInLeftBig {\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(-2000px, 0, 0);\n    transform: translate3d(-2000px, 0, 0); }\n  to {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none; } }\n\n@keyframes fadeInLeftBig {\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(-2000px, 0, 0);\n    transform: translate3d(-2000px, 0, 0); }\n  to {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none; } }\n\n.fadeInLeftBig {\n  -webkit-animation-name: fadeInLeftBig;\n  animation-name: fadeInLeftBig; }\n\n@-webkit-keyframes fadeInRight {\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(100%, 0, 0);\n    transform: translate3d(100%, 0, 0); }\n  to {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none; } }\n\n@keyframes fadeInRight {\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(100%, 0, 0);\n    transform: translate3d(100%, 0, 0); }\n  to {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none; } }\n\n.fadeInRight {\n  -webkit-animation-name: fadeInRight;\n  animation-name: fadeInRight; }\n\n@-webkit-keyframes fadeInRightBig {\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(2000px, 0, 0);\n    transform: translate3d(2000px, 0, 0); }\n  to {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none; } }\n\n@keyframes fadeInRightBig {\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(2000px, 0, 0);\n    transform: translate3d(2000px, 0, 0); }\n  to {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none; } }\n\n.fadeInRightBig {\n  -webkit-animation-name: fadeInRightBig;\n  animation-name: fadeInRightBig; }\n\n@-webkit-keyframes fadeInUp {\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 100%, 0);\n    transform: translate3d(0, 100%, 0); }\n  to {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none; } }\n\n@keyframes fadeInUp {\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 100%, 0);\n    transform: translate3d(0, 100%, 0); }\n  to {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none; } }\n\n.fadeInUp {\n  -webkit-animation-name: fadeInUp;\n  animation-name: fadeInUp; }\n\n@-webkit-keyframes fadeInUpBig {\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 2000px, 0);\n    transform: translate3d(0, 2000px, 0); }\n  to {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none; } }\n\n@keyframes fadeInUpBig {\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 2000px, 0);\n    transform: translate3d(0, 2000px, 0); }\n  to {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none; } }\n\n.fadeInUpBig {\n  -webkit-animation-name: fadeInUpBig;\n  animation-name: fadeInUpBig; }\n\n@-webkit-keyframes fadeOut {\n  from {\n    opacity: 1; }\n  to {\n    opacity: 0; } }\n\n@keyframes fadeOut {\n  from {\n    opacity: 1; }\n  to {\n    opacity: 0; } }\n\n.fadeOut {\n  -webkit-animation-name: fadeOut;\n  animation-name: fadeOut; }\n\n@-webkit-keyframes fadeOutDown {\n  from {\n    opacity: 1; }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 100%, 0);\n    transform: translate3d(0, 100%, 0); } }\n\n@keyframes fadeOutDown {\n  from {\n    opacity: 1; }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 100%, 0);\n    transform: translate3d(0, 100%, 0); } }\n\n.fadeOutDown {\n  -webkit-animation-name: fadeOutDown;\n  animation-name: fadeOutDown; }\n\n@-webkit-keyframes fadeOutDownBig {\n  from {\n    opacity: 1; }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 2000px, 0);\n    transform: translate3d(0, 2000px, 0); } }\n\n@keyframes fadeOutDownBig {\n  from {\n    opacity: 1; }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(0, 2000px, 0);\n    transform: translate3d(0, 2000px, 0); } }\n\n.fadeOutDownBig {\n  -webkit-animation-name: fadeOutDownBig;\n  animation-name: fadeOutDownBig; }\n\n@-webkit-keyframes fadeOutLeft {\n  from {\n    opacity: 1; }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(-100%, 0, 0);\n    transform: translate3d(-100%, 0, 0); } }\n\n@keyframes fadeOutLeft {\n  from {\n    opacity: 1; }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(-100%, 0, 0);\n    transform: translate3d(-100%, 0, 0); } }\n\n.fadeOutLeft {\n  -webkit-animation-name: fadeOutLeft;\n  animation-name: fadeOutLeft; }\n\n@-webkit-keyframes fadeOutLeftBig {\n  from {\n    opacity: 1; }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(-2000px, 0, 0);\n    transform: translate3d(-2000px, 0, 0); } }\n\n@keyframes fadeOutLeftBig {\n  from {\n    opacity: 1; }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(-2000px, 0, 0);\n    transform: translate3d(-2000px, 0, 0); } }\n\n.fadeOutLeftBig {\n  -webkit-animation-name: fadeOutLeftBig;\n  animation-name: fadeOutLeftBig; }\n\n@-webkit-keyframes fadeOutRight {\n  from {\n    opacity: 1; }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(100%, 0, 0);\n    transform: translate3d(100%, 0, 0); } }\n\n@keyframes fadeOutRight {\n  from {\n    opacity: 1; }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(100%, 0, 0);\n    transform: translate3d(100%, 0, 0); } }\n\n.fadeOutRight {\n  -webkit-animation-name: fadeOutRight;\n  animation-name: fadeOutRight; }\n\n@-webkit-keyframes fadeOutRightBig {\n  from {\n    opacity: 1; }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(2000px, 0, 0);\n    transform: translate3d(2000px, 0, 0); } }\n\n@keyframes fadeOutRightBig {\n  from {\n    opacity: 1; }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(2000px, 0, 0);\n    transform: translate3d(2000px, 0, 0); } }\n\n.fadeOutRightBig {\n  -webkit-animation-name: fadeOutRightBig;\n  animation-name: fadeOutRightBig; }\n\n@-webkit-keyframes fadeOutUp {\n  from {\n    opacity: 1; }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -100%, 0);\n    transform: translate3d(0, -100%, 0); } }\n\n@keyframes fadeOutUp {\n  from {\n    opacity: 1; }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -100%, 0);\n    transform: translate3d(0, -100%, 0); } }\n\n.fadeOutUp {\n  -webkit-animation-name: fadeOutUp;\n  animation-name: fadeOutUp; }\n\n@-webkit-keyframes fadeOutUpBig {\n  from {\n    opacity: 1; }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -2000px, 0);\n    transform: translate3d(0, -2000px, 0); } }\n\n@keyframes fadeOutUpBig {\n  from {\n    opacity: 1; }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(0, -2000px, 0);\n    transform: translate3d(0, -2000px, 0); } }\n\n.fadeOutUpBig {\n  -webkit-animation-name: fadeOutUpBig;\n  animation-name: fadeOutUpBig; }\n\n@-webkit-keyframes flip {\n  from {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -360deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, -360deg);\n    -webkit-animation-timing-function: ease-out;\n    animation-timing-function: ease-out; }\n  40% {\n    -webkit-transform: perspective(400px) translate3d(0, 0, 150px) rotate3d(0, 1, 0, -190deg);\n    transform: perspective(400px) translate3d(0, 0, 150px) rotate3d(0, 1, 0, -190deg);\n    -webkit-animation-timing-function: ease-out;\n    animation-timing-function: ease-out; }\n  50% {\n    -webkit-transform: perspective(400px) translate3d(0, 0, 150px) rotate3d(0, 1, 0, -170deg);\n    transform: perspective(400px) translate3d(0, 0, 150px) rotate3d(0, 1, 0, -170deg);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in; }\n  80% {\n    -webkit-transform: perspective(400px) scale3d(0.95, 0.95, 0.95);\n    transform: perspective(400px) scale3d(0.95, 0.95, 0.95);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in; }\n  to {\n    -webkit-transform: perspective(400px);\n    transform: perspective(400px);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in; } }\n\n@keyframes flip {\n  from {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -360deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, -360deg);\n    -webkit-animation-timing-function: ease-out;\n    animation-timing-function: ease-out; }\n  40% {\n    -webkit-transform: perspective(400px) translate3d(0, 0, 150px) rotate3d(0, 1, 0, -190deg);\n    transform: perspective(400px) translate3d(0, 0, 150px) rotate3d(0, 1, 0, -190deg);\n    -webkit-animation-timing-function: ease-out;\n    animation-timing-function: ease-out; }\n  50% {\n    -webkit-transform: perspective(400px) translate3d(0, 0, 150px) rotate3d(0, 1, 0, -170deg);\n    transform: perspective(400px) translate3d(0, 0, 150px) rotate3d(0, 1, 0, -170deg);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in; }\n  80% {\n    -webkit-transform: perspective(400px) scale3d(0.95, 0.95, 0.95);\n    transform: perspective(400px) scale3d(0.95, 0.95, 0.95);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in; }\n  to {\n    -webkit-transform: perspective(400px);\n    transform: perspective(400px);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in; } }\n\n.animated.flip {\n  -webkit-backface-visibility: visible;\n  backface-visibility: visible;\n  -webkit-animation-name: flip;\n  animation-name: flip; }\n\n@-webkit-keyframes flipInX {\n  from {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in;\n    opacity: 0; }\n  40% {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in; }\n  60% {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 10deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, 10deg);\n    opacity: 1; }\n  80% {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -5deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, -5deg); }\n  to {\n    -webkit-transform: perspective(400px);\n    transform: perspective(400px); } }\n\n@keyframes flipInX {\n  from {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in;\n    opacity: 0; }\n  40% {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in; }\n  60% {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 10deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, 10deg);\n    opacity: 1; }\n  80% {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -5deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, -5deg); }\n  to {\n    -webkit-transform: perspective(400px);\n    transform: perspective(400px); } }\n\n.flipInX {\n  -webkit-backface-visibility: visible !important;\n  backface-visibility: visible !important;\n  -webkit-animation-name: flipInX;\n  animation-name: flipInX; }\n\n@-webkit-keyframes flipInY {\n  from {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in;\n    opacity: 0; }\n  40% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -20deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, -20deg);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in; }\n  60% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 10deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, 10deg);\n    opacity: 1; }\n  80% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -5deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, -5deg); }\n  to {\n    -webkit-transform: perspective(400px);\n    transform: perspective(400px); } }\n\n@keyframes flipInY {\n  from {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in;\n    opacity: 0; }\n  40% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -20deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, -20deg);\n    -webkit-animation-timing-function: ease-in;\n    animation-timing-function: ease-in; }\n  60% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 10deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, 10deg);\n    opacity: 1; }\n  80% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -5deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, -5deg); }\n  to {\n    -webkit-transform: perspective(400px);\n    transform: perspective(400px); } }\n\n.flipInY {\n  -webkit-backface-visibility: visible !important;\n  backface-visibility: visible !important;\n  -webkit-animation-name: flipInY;\n  animation-name: flipInY; }\n\n@-webkit-keyframes flipOutX {\n  from {\n    -webkit-transform: perspective(400px);\n    transform: perspective(400px); }\n  30% {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\n    opacity: 1; }\n  to {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\n    opacity: 0; } }\n\n@keyframes flipOutX {\n  from {\n    -webkit-transform: perspective(400px);\n    transform: perspective(400px); }\n  30% {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\n    opacity: 1; }\n  to {\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\n    transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\n    opacity: 0; } }\n\n.flipOutX {\n  -webkit-animation-name: flipOutX;\n  animation-name: flipOutX;\n  -webkit-backface-visibility: visible !important;\n  backface-visibility: visible !important; }\n\n@-webkit-keyframes flipOutY {\n  from {\n    -webkit-transform: perspective(400px);\n    transform: perspective(400px); }\n  30% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -15deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, -15deg);\n    opacity: 1; }\n  to {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\n    opacity: 0; } }\n\n@keyframes flipOutY {\n  from {\n    -webkit-transform: perspective(400px);\n    transform: perspective(400px); }\n  30% {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -15deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, -15deg);\n    opacity: 1; }\n  to {\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\n    transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\n    opacity: 0; } }\n\n.flipOutY {\n  -webkit-backface-visibility: visible !important;\n  backface-visibility: visible !important;\n  -webkit-animation-name: flipOutY;\n  animation-name: flipOutY; }\n\n@-webkit-keyframes lightSpeedIn {\n  from {\n    -webkit-transform: translate3d(100%, 0, 0) skewX(-30deg);\n    transform: translate3d(100%, 0, 0) skewX(-30deg);\n    opacity: 0; }\n  60% {\n    -webkit-transform: skewX(20deg);\n    transform: skewX(20deg);\n    opacity: 1; }\n  80% {\n    -webkit-transform: skewX(-5deg);\n    transform: skewX(-5deg);\n    opacity: 1; }\n  to {\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1; } }\n\n@keyframes lightSpeedIn {\n  from {\n    -webkit-transform: translate3d(100%, 0, 0) skewX(-30deg);\n    transform: translate3d(100%, 0, 0) skewX(-30deg);\n    opacity: 0; }\n  60% {\n    -webkit-transform: skewX(20deg);\n    transform: skewX(20deg);\n    opacity: 1; }\n  80% {\n    -webkit-transform: skewX(-5deg);\n    transform: skewX(-5deg);\n    opacity: 1; }\n  to {\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1; } }\n\n.lightSpeedIn {\n  -webkit-animation-name: lightSpeedIn;\n  animation-name: lightSpeedIn;\n  -webkit-animation-timing-function: ease-out;\n  animation-timing-function: ease-out; }\n\n@-webkit-keyframes lightSpeedOut {\n  from {\n    opacity: 1; }\n  to {\n    -webkit-transform: translate3d(100%, 0, 0) skewX(30deg);\n    transform: translate3d(100%, 0, 0) skewX(30deg);\n    opacity: 0; } }\n\n@keyframes lightSpeedOut {\n  from {\n    opacity: 1; }\n  to {\n    -webkit-transform: translate3d(100%, 0, 0) skewX(30deg);\n    transform: translate3d(100%, 0, 0) skewX(30deg);\n    opacity: 0; } }\n\n.lightSpeedOut {\n  -webkit-animation-name: lightSpeedOut;\n  animation-name: lightSpeedOut;\n  -webkit-animation-timing-function: ease-in;\n  animation-timing-function: ease-in; }\n\n@-webkit-keyframes rotateIn {\n  from {\n    -webkit-transform-origin: center;\n    transform-origin: center;\n    -webkit-transform: rotate3d(0, 0, 1, -200deg);\n    transform: rotate3d(0, 0, 1, -200deg);\n    opacity: 0; }\n  to {\n    -webkit-transform-origin: center;\n    transform-origin: center;\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1; } }\n\n@keyframes rotateIn {\n  from {\n    -webkit-transform-origin: center;\n    transform-origin: center;\n    -webkit-transform: rotate3d(0, 0, 1, -200deg);\n    transform: rotate3d(0, 0, 1, -200deg);\n    opacity: 0; }\n  to {\n    -webkit-transform-origin: center;\n    transform-origin: center;\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1; } }\n\n.rotateIn {\n  -webkit-animation-name: rotateIn;\n  animation-name: rotateIn; }\n\n@-webkit-keyframes rotateInDownLeft {\n  from {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: rotate3d(0, 0, 1, -45deg);\n    transform: rotate3d(0, 0, 1, -45deg);\n    opacity: 0; }\n  to {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1; } }\n\n@keyframes rotateInDownLeft {\n  from {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: rotate3d(0, 0, 1, -45deg);\n    transform: rotate3d(0, 0, 1, -45deg);\n    opacity: 0; }\n  to {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1; } }\n\n.rotateInDownLeft {\n  -webkit-animation-name: rotateInDownLeft;\n  animation-name: rotateInDownLeft; }\n\n@-webkit-keyframes rotateInDownRight {\n  from {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: rotate3d(0, 0, 1, 45deg);\n    transform: rotate3d(0, 0, 1, 45deg);\n    opacity: 0; }\n  to {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1; } }\n\n@keyframes rotateInDownRight {\n  from {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: rotate3d(0, 0, 1, 45deg);\n    transform: rotate3d(0, 0, 1, 45deg);\n    opacity: 0; }\n  to {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1; } }\n\n.rotateInDownRight {\n  -webkit-animation-name: rotateInDownRight;\n  animation-name: rotateInDownRight; }\n\n@-webkit-keyframes rotateInUpLeft {\n  from {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: rotate3d(0, 0, 1, 45deg);\n    transform: rotate3d(0, 0, 1, 45deg);\n    opacity: 0; }\n  to {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1; } }\n\n@keyframes rotateInUpLeft {\n  from {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: rotate3d(0, 0, 1, 45deg);\n    transform: rotate3d(0, 0, 1, 45deg);\n    opacity: 0; }\n  to {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1; } }\n\n.rotateInUpLeft {\n  -webkit-animation-name: rotateInUpLeft;\n  animation-name: rotateInUpLeft; }\n\n@-webkit-keyframes rotateInUpRight {\n  from {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: rotate3d(0, 0, 1, -90deg);\n    transform: rotate3d(0, 0, 1, -90deg);\n    opacity: 0; }\n  to {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1; } }\n\n@keyframes rotateInUpRight {\n  from {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: rotate3d(0, 0, 1, -90deg);\n    transform: rotate3d(0, 0, 1, -90deg);\n    opacity: 0; }\n  to {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: none;\n    transform: none;\n    opacity: 1; } }\n\n.rotateInUpRight {\n  -webkit-animation-name: rotateInUpRight;\n  animation-name: rotateInUpRight; }\n\n@-webkit-keyframes rotateOut {\n  from {\n    -webkit-transform-origin: center;\n    transform-origin: center;\n    opacity: 1; }\n  to {\n    -webkit-transform-origin: center;\n    transform-origin: center;\n    -webkit-transform: rotate3d(0, 0, 1, 200deg);\n    transform: rotate3d(0, 0, 1, 200deg);\n    opacity: 0; } }\n\n@keyframes rotateOut {\n  from {\n    -webkit-transform-origin: center;\n    transform-origin: center;\n    opacity: 1; }\n  to {\n    -webkit-transform-origin: center;\n    transform-origin: center;\n    -webkit-transform: rotate3d(0, 0, 1, 200deg);\n    transform: rotate3d(0, 0, 1, 200deg);\n    opacity: 0; } }\n\n.rotateOut {\n  -webkit-animation-name: rotateOut;\n  animation-name: rotateOut; }\n\n@-webkit-keyframes rotateOutDownLeft {\n  from {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    opacity: 1; }\n  to {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: rotate3d(0, 0, 1, 45deg);\n    transform: rotate3d(0, 0, 1, 45deg);\n    opacity: 0; } }\n\n@keyframes rotateOutDownLeft {\n  from {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    opacity: 1; }\n  to {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: rotate3d(0, 0, 1, 45deg);\n    transform: rotate3d(0, 0, 1, 45deg);\n    opacity: 0; } }\n\n.rotateOutDownLeft {\n  -webkit-animation-name: rotateOutDownLeft;\n  animation-name: rotateOutDownLeft; }\n\n@-webkit-keyframes rotateOutDownRight {\n  from {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    opacity: 1; }\n  to {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: rotate3d(0, 0, 1, -45deg);\n    transform: rotate3d(0, 0, 1, -45deg);\n    opacity: 0; } }\n\n@keyframes rotateOutDownRight {\n  from {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    opacity: 1; }\n  to {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: rotate3d(0, 0, 1, -45deg);\n    transform: rotate3d(0, 0, 1, -45deg);\n    opacity: 0; } }\n\n.rotateOutDownRight {\n  -webkit-animation-name: rotateOutDownRight;\n  animation-name: rotateOutDownRight; }\n\n@-webkit-keyframes rotateOutUpLeft {\n  from {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    opacity: 1; }\n  to {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: rotate3d(0, 0, 1, -45deg);\n    transform: rotate3d(0, 0, 1, -45deg);\n    opacity: 0; } }\n\n@keyframes rotateOutUpLeft {\n  from {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    opacity: 1; }\n  to {\n    -webkit-transform-origin: left bottom;\n    transform-origin: left bottom;\n    -webkit-transform: rotate3d(0, 0, 1, -45deg);\n    transform: rotate3d(0, 0, 1, -45deg);\n    opacity: 0; } }\n\n.rotateOutUpLeft {\n  -webkit-animation-name: rotateOutUpLeft;\n  animation-name: rotateOutUpLeft; }\n\n@-webkit-keyframes rotateOutUpRight {\n  from {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    opacity: 1; }\n  to {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: rotate3d(0, 0, 1, 90deg);\n    transform: rotate3d(0, 0, 1, 90deg);\n    opacity: 0; } }\n\n@keyframes rotateOutUpRight {\n  from {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    opacity: 1; }\n  to {\n    -webkit-transform-origin: right bottom;\n    transform-origin: right bottom;\n    -webkit-transform: rotate3d(0, 0, 1, 90deg);\n    transform: rotate3d(0, 0, 1, 90deg);\n    opacity: 0; } }\n\n.rotateOutUpRight {\n  -webkit-animation-name: rotateOutUpRight;\n  animation-name: rotateOutUpRight; }\n\n@-webkit-keyframes hinge {\n  0% {\n    -webkit-transform-origin: top left;\n    transform-origin: top left;\n    -webkit-animation-timing-function: ease-in-out;\n    animation-timing-function: ease-in-out; }\n  20%, 60% {\n    -webkit-transform: rotate3d(0, 0, 1, 80deg);\n    transform: rotate3d(0, 0, 1, 80deg);\n    -webkit-transform-origin: top left;\n    transform-origin: top left;\n    -webkit-animation-timing-function: ease-in-out;\n    animation-timing-function: ease-in-out; }\n  40%, 80% {\n    -webkit-transform: rotate3d(0, 0, 1, 60deg);\n    transform: rotate3d(0, 0, 1, 60deg);\n    -webkit-transform-origin: top left;\n    transform-origin: top left;\n    -webkit-animation-timing-function: ease-in-out;\n    animation-timing-function: ease-in-out;\n    opacity: 1; }\n  to {\n    -webkit-transform: translate3d(0, 700px, 0);\n    transform: translate3d(0, 700px, 0);\n    opacity: 0; } }\n\n@keyframes hinge {\n  0% {\n    -webkit-transform-origin: top left;\n    transform-origin: top left;\n    -webkit-animation-timing-function: ease-in-out;\n    animation-timing-function: ease-in-out; }\n  20%, 60% {\n    -webkit-transform: rotate3d(0, 0, 1, 80deg);\n    transform: rotate3d(0, 0, 1, 80deg);\n    -webkit-transform-origin: top left;\n    transform-origin: top left;\n    -webkit-animation-timing-function: ease-in-out;\n    animation-timing-function: ease-in-out; }\n  40%, 80% {\n    -webkit-transform: rotate3d(0, 0, 1, 60deg);\n    transform: rotate3d(0, 0, 1, 60deg);\n    -webkit-transform-origin: top left;\n    transform-origin: top left;\n    -webkit-animation-timing-function: ease-in-out;\n    animation-timing-function: ease-in-out;\n    opacity: 1; }\n  to {\n    -webkit-transform: translate3d(0, 700px, 0);\n    transform: translate3d(0, 700px, 0);\n    opacity: 0; } }\n\n.hinge {\n  -webkit-animation-name: hinge;\n  animation-name: hinge; }\n\n/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */\n@-webkit-keyframes rollIn {\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(-100%, 0, 0) rotate3d(0, 0, 1, -120deg);\n    transform: translate3d(-100%, 0, 0) rotate3d(0, 0, 1, -120deg); }\n  to {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none; } }\n\n@keyframes rollIn {\n  from {\n    opacity: 0;\n    -webkit-transform: translate3d(-100%, 0, 0) rotate3d(0, 0, 1, -120deg);\n    transform: translate3d(-100%, 0, 0) rotate3d(0, 0, 1, -120deg); }\n  to {\n    opacity: 1;\n    -webkit-transform: none;\n    transform: none; } }\n\n.rollIn {\n  -webkit-animation-name: rollIn;\n  animation-name: rollIn; }\n\n/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */\n@-webkit-keyframes rollOut {\n  from {\n    opacity: 1; }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(100%, 0, 0) rotate3d(0, 0, 1, 120deg);\n    transform: translate3d(100%, 0, 0) rotate3d(0, 0, 1, 120deg); } }\n\n@keyframes rollOut {\n  from {\n    opacity: 1; }\n  to {\n    opacity: 0;\n    -webkit-transform: translate3d(100%, 0, 0) rotate3d(0, 0, 1, 120deg);\n    transform: translate3d(100%, 0, 0) rotate3d(0, 0, 1, 120deg); } }\n\n.rollOut {\n  -webkit-animation-name: rollOut;\n  animation-name: rollOut; }\n\n@-webkit-keyframes zoomIn {\n  from {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3); }\n  50% {\n    opacity: 1; } }\n\n@keyframes zoomIn {\n  from {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3); }\n  50% {\n    opacity: 1; } }\n\n.zoomIn {\n  -webkit-animation-name: zoomIn;\n  animation-name: zoomIn; }\n\n@-webkit-keyframes zoomInDown {\n  from {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -1000px, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -1000px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n  60% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1); } }\n\n@keyframes zoomInDown {\n  from {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -1000px, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -1000px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n  60% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1); } }\n\n.zoomInDown {\n  -webkit-animation-name: zoomInDown;\n  animation-name: zoomInDown; }\n\n@-webkit-keyframes zoomInLeft {\n  from {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(-1000px, 0, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(-1000px, 0, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n  60% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(10px, 0, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(10px, 0, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1); } }\n\n@keyframes zoomInLeft {\n  from {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(-1000px, 0, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(-1000px, 0, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n  60% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(10px, 0, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(10px, 0, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1); } }\n\n.zoomInLeft {\n  -webkit-animation-name: zoomInLeft;\n  animation-name: zoomInLeft; }\n\n@-webkit-keyframes zoomInRight {\n  from {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(1000px, 0, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(1000px, 0, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n  60% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(-10px, 0, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(-10px, 0, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1); } }\n\n@keyframes zoomInRight {\n  from {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(1000px, 0, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(1000px, 0, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n  60% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(-10px, 0, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(-10px, 0, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1); } }\n\n.zoomInRight {\n  -webkit-animation-name: zoomInRight;\n  animation-name: zoomInRight; }\n\n@-webkit-keyframes zoomInUp {\n  from {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 1000px, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 1000px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n  60% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1); } }\n\n@keyframes zoomInUp {\n  from {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 1000px, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 1000px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n  60% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1); } }\n\n.zoomInUp {\n  -webkit-animation-name: zoomInUp;\n  animation-name: zoomInUp; }\n\n@-webkit-keyframes zoomOut {\n  from {\n    opacity: 1; }\n  50% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3); }\n  to {\n    opacity: 0; } }\n\n@keyframes zoomOut {\n  from {\n    opacity: 1; }\n  50% {\n    opacity: 0;\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3); }\n  to {\n    opacity: 0; } }\n\n.zoomOut {\n  -webkit-animation-name: zoomOut;\n  animation-name: zoomOut; }\n\n@-webkit-keyframes zoomOutDown {\n  40% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n  to {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 2000px, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 2000px, 0);\n    -webkit-transform-origin: center bottom;\n    transform-origin: center bottom;\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1); } }\n\n@keyframes zoomOutDown {\n  40% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n  to {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 2000px, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 2000px, 0);\n    -webkit-transform-origin: center bottom;\n    transform-origin: center bottom;\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1); } }\n\n.zoomOutDown {\n  -webkit-animation-name: zoomOutDown;\n  animation-name: zoomOutDown; }\n\n@-webkit-keyframes zoomOutLeft {\n  40% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(42px, 0, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(42px, 0, 0); }\n  to {\n    opacity: 0;\n    -webkit-transform: scale(0.1) translate3d(-2000px, 0, 0);\n    transform: scale(0.1) translate3d(-2000px, 0, 0);\n    -webkit-transform-origin: left center;\n    transform-origin: left center; } }\n\n@keyframes zoomOutLeft {\n  40% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(42px, 0, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(42px, 0, 0); }\n  to {\n    opacity: 0;\n    -webkit-transform: scale(0.1) translate3d(-2000px, 0, 0);\n    transform: scale(0.1) translate3d(-2000px, 0, 0);\n    -webkit-transform-origin: left center;\n    transform-origin: left center; } }\n\n.zoomOutLeft {\n  -webkit-animation-name: zoomOutLeft;\n  animation-name: zoomOutLeft; }\n\n@-webkit-keyframes zoomOutRight {\n  40% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(-42px, 0, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(-42px, 0, 0); }\n  to {\n    opacity: 0;\n    -webkit-transform: scale(0.1) translate3d(2000px, 0, 0);\n    transform: scale(0.1) translate3d(2000px, 0, 0);\n    -webkit-transform-origin: right center;\n    transform-origin: right center; } }\n\n@keyframes zoomOutRight {\n  40% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(-42px, 0, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(-42px, 0, 0); }\n  to {\n    opacity: 0;\n    -webkit-transform: scale(0.1) translate3d(2000px, 0, 0);\n    transform: scale(0.1) translate3d(2000px, 0, 0);\n    -webkit-transform-origin: right center;\n    transform-origin: right center; } }\n\n.zoomOutRight {\n  -webkit-animation-name: zoomOutRight;\n  animation-name: zoomOutRight; }\n\n@-webkit-keyframes zoomOutUp {\n  40% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n  to {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -2000px, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -2000px, 0);\n    -webkit-transform-origin: center bottom;\n    transform-origin: center bottom;\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1); } }\n\n@keyframes zoomOutUp {\n  40% {\n    opacity: 1;\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19); }\n  to {\n    opacity: 0;\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -2000px, 0);\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -2000px, 0);\n    -webkit-transform-origin: center bottom;\n    transform-origin: center bottom;\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1); } }\n\n.zoomOutUp {\n  -webkit-animation-name: zoomOutUp;\n  animation-name: zoomOutUp; }\n\n@-webkit-keyframes slideInDown {\n  from {\n    -webkit-transform: translate3d(0, -100%, 0);\n    transform: translate3d(0, -100%, 0);\n    visibility: visible; }\n  to {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0); } }\n\n@keyframes slideInDown {\n  from {\n    -webkit-transform: translate3d(0, -100%, 0);\n    transform: translate3d(0, -100%, 0);\n    visibility: visible; }\n  to {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0); } }\n\n.slideInDown {\n  -webkit-animation-name: slideInDown;\n  animation-name: slideInDown; }\n\n@-webkit-keyframes slideInLeft {\n  from {\n    -webkit-transform: translate3d(-100%, 0, 0);\n    transform: translate3d(-100%, 0, 0);\n    visibility: visible; }\n  to {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0); } }\n\n@keyframes slideInLeft {\n  from {\n    -webkit-transform: translate3d(-100%, 0, 0);\n    transform: translate3d(-100%, 0, 0);\n    visibility: visible; }\n  to {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0); } }\n\n.slideInLeft {\n  -webkit-animation-name: slideInLeft;\n  animation-name: slideInLeft; }\n\n@-webkit-keyframes slideInRight {\n  from {\n    -webkit-transform: translate3d(100%, 0, 0);\n    transform: translate3d(100%, 0, 0);\n    visibility: visible; }\n  to {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0); } }\n\n@keyframes slideInRight {\n  from {\n    -webkit-transform: translate3d(100%, 0, 0);\n    transform: translate3d(100%, 0, 0);\n    visibility: visible; }\n  to {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0); } }\n\n.slideInRight {\n  -webkit-animation-name: slideInRight;\n  animation-name: slideInRight; }\n\n@-webkit-keyframes slideInUp {\n  from {\n    -webkit-transform: translate3d(0, 100%, 0);\n    transform: translate3d(0, 100%, 0);\n    visibility: visible; }\n  to {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0); } }\n\n@keyframes slideInUp {\n  from {\n    -webkit-transform: translate3d(0, 100%, 0);\n    transform: translate3d(0, 100%, 0);\n    visibility: visible; }\n  to {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0); } }\n\n.slideInUp {\n  -webkit-animation-name: slideInUp;\n  animation-name: slideInUp; }\n\n@-webkit-keyframes slideOutDown {\n  from {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0); }\n  to {\n    visibility: hidden;\n    -webkit-transform: translate3d(0, 100%, 0);\n    transform: translate3d(0, 100%, 0); } }\n\n@keyframes slideOutDown {\n  from {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0); }\n  to {\n    visibility: hidden;\n    -webkit-transform: translate3d(0, 100%, 0);\n    transform: translate3d(0, 100%, 0); } }\n\n.slideOutDown {\n  -webkit-animation-name: slideOutDown;\n  animation-name: slideOutDown; }\n\n@-webkit-keyframes slideOutLeft {\n  from {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0); }\n  to {\n    visibility: hidden;\n    -webkit-transform: translate3d(-100%, 0, 0);\n    transform: translate3d(-100%, 0, 0); } }\n\n@keyframes slideOutLeft {\n  from {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0); }\n  to {\n    visibility: hidden;\n    -webkit-transform: translate3d(-100%, 0, 0);\n    transform: translate3d(-100%, 0, 0); } }\n\n.slideOutLeft {\n  -webkit-animation-name: slideOutLeft;\n  animation-name: slideOutLeft; }\n\n@-webkit-keyframes slideOutRight {\n  from {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0); }\n  to {\n    visibility: hidden;\n    -webkit-transform: translate3d(100%, 0, 0);\n    transform: translate3d(100%, 0, 0); } }\n\n@keyframes slideOutRight {\n  from {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0); }\n  to {\n    visibility: hidden;\n    -webkit-transform: translate3d(100%, 0, 0);\n    transform: translate3d(100%, 0, 0); } }\n\n.slideOutRight {\n  -webkit-animation-name: slideOutRight;\n  animation-name: slideOutRight; }\n\n@-webkit-keyframes slideOutUp {\n  from {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0); }\n  to {\n    visibility: hidden;\n    -webkit-transform: translate3d(0, -100%, 0);\n    transform: translate3d(0, -100%, 0); } }\n\n@keyframes slideOutUp {\n  from {\n    -webkit-transform: translate3d(0, 0, 0);\n    transform: translate3d(0, 0, 0); }\n  to {\n    visibility: hidden;\n    -webkit-transform: translate3d(0, -100%, 0);\n    transform: translate3d(0, -100%, 0); } }\n\n.slideOutUp {\n  -webkit-animation-name: slideOutUp;\n  animation-name: slideOutUp; }\n\nh1, h2, h3, h4, h5, h6 {\n  font-family: \"Roboto\", sans-serif;\n  width: 100%;\n  margin-top: 0; }\n\nh1.color, h2.color, h3.color, h4.color, h5.color, h6.color {\n  color: #f95372; }\n\nbody a {\n  color: #285eb8;\n  text-decoration: none !important;\n  transition: color 0.2s ease; }\n  body a:hover {\n    color: #163364; }\n\nh1 {\n  font-size: 32px;\n  margin-bottom: 0.5rem; }\n\nh2 {\n  font-size: 24px; }\n\nh3 {\n  font-size: 20px; }\n\nh4 {\n  font-size: 18px; }\n\nh5 {\n  font-size: 15px; }\n\n.typography-document-samples p {\n  margin: 0; }\n\n.typography-document-samples .typography-widget {\n  height: 100%; }\n  .typography-document-samples .typography-widget .card {\n    height: 620px; }\n  .typography-document-samples .typography-widget .card-title {\n    text-align: center;\n    width: 100%; }\n  .typography-document-samples .typography-widget .card.with-scroll .card-body {\n    height: calc(100% - 45px); }\n  .typography-document-samples .typography-widget .card-content {\n    padding: 15px 22px 5px 22px; }\n\n.heading-widget h1, .heading-widget h2, .heading-widget h3, .heading-widget h4, .heading-widget h5, .heading-widget h6 {\n  width: 100%;\n  font-weight: 300;\n  text-align: center; }\n\n.heading-widget p {\n  line-height: 16px;\n  font-weight: 400;\n  text-align: center; }\n\n.more-text-widget {\n  text-align: center;\n  font-size: 14px; }\n  .more-text-widget p {\n    line-height: 17px; }\n  .more-text-widget .gray {\n    color: #767676; }\n  .more-text-widget .black {\n    color: #585858; }\n  .more-text-widget .light-text {\n    font-weight: 300; }\n  .more-text-widget .regular-text {\n    font-weight: 400; }\n  .more-text-widget .upper-text {\n    text-transform: uppercase; }\n  .more-text-widget .bold-text {\n    font-weight: 700; }\n  .more-text-widget .small-text {\n    padding: 5px 0 0 0; }\n    .more-text-widget .small-text p {\n      font-size: 9px;\n      font-weight: 300;\n      line-height: 10px; }\n\n.color-widget {\n  text-align: center;\n  font-size: 14px;\n  font-weight: 400; }\n  .color-widget p {\n    line-height: 17px; }\n  .color-widget .section-block {\n    margin: 14px 0; }\n  .color-widget .yellow-text p {\n    color: #e7ba08; }\n  .color-widget .red-text p {\n    color: #f95372; }\n  .color-widget .links h3 {\n    margin-bottom: 10px; }\n  .color-widget .links p {\n    margin-bottom: 0; }\n    .color-widget .links p.hovered a {\n      color: #163364; }\n\n.lists-widget {\n  font-weight: 400; }\n  .lists-widget .list-header {\n    width: 100%;\n    text-align: center; }\n  .lists-widget .accent {\n    margin-top: 30px;\n    color: #ecc839;\n    line-height: 14px;\n    font-size: 14px;\n    padding-left: 11px;\n    border-left: 4px solid #ecc839;\n    margin-left: 13px; }\n  .lists-widget ul.blur, .lists-widget ol.blur {\n    padding-left: 13px;\n    margin-bottom: 19px;\n    list-style: none;\n    padding-top: 1px; }\n    .lists-widget ul.blur li, .lists-widget ol.blur li {\n      margin-top: 5px;\n      font-size: 14px; }\n      .lists-widget ul.blur li ul, .lists-widget ul.blur li ol, .lists-widget ol.blur li ul, .lists-widget ol.blur li ol {\n        padding-left: 20px;\n        margin-bottom: 0;\n        list-style: none; }\n  .lists-widget ul.blur li:before {\n    content: \"• \";\n    color: #ecc839;\n    width: 10px;\n    display: inline-block; }\n  .lists-widget ol.blur {\n    counter-reset: section; }\n    .lists-widget ol.blur li {\n      color: #ecc839;\n      padding-left: 0;\n      line-height: 14px;\n      position: relative; }\n      .lists-widget ol.blur li span {\n        color: #ffffff;\n        display: block; }\n      .lists-widget ol.blur li ol {\n        padding-left: 0;\n        margin-left: 12px; }\n      .lists-widget ol.blur li:before {\n        content: counters(section, \".\") \".\";\n        counter-increment: section;\n        width: 19px;\n        position: absolute;\n        left: 0;\n        top: 0;\n        white-space: nowrap;\n        overflow: hidden;\n        text-overflow: ellipsis; }\n    .lists-widget ol.blur > li span {\n      padding-left: 14px; }\n    .lists-widget ol.blur ol {\n      counter-reset: section; }\n      .lists-widget ol.blur ol > li:before {\n        width: 30px; }\n      .lists-widget ol.blur ol > li span {\n        padding-left: 27px; }\n      .lists-widget ol.blur ol ol > li:before {\n        width: 40px; }\n      .lists-widget ol.blur ol ol > li span {\n        padding-left: 40px; }\n\n.columns-section {\n  background-color: #ffffff; }\n\np {\n  margin-bottom: 12px;\n  font-family: \"Roboto\", sans-serif;\n  font-size: 14px; }\n\np.small-text {\n  color: #ffffff;\n  font-size: 12px;\n  line-height: 16px;\n  margin-bottom: 8px; }\n\n.cols-two {\n  margin-bottom: 50px; }\n  .cols-two > div {\n    float: left;\n    width: 350px;\n    margin-left: 40px; }\n    .cols-two > div:first-child {\n      margin-left: 0; }\n\n.cols-three {\n  margin-bottom: 50px; }\n  .cols-three > div {\n    float: left;\n    width: 222px;\n    margin-left: 40px; }\n    .cols-three > div:first-child {\n      margin-left: 0; }\n\na.learn-more {\n  font-size: 14px;\n  font-weight: 700;\n  text-decoration: none;\n  line-height: 24px; }\n\n.img-wrapper {\n  margin-bottom: 19px;\n  margin-top: 5px;\n  overflow: hidden;\n  height: 180px; }\n  .img-wrapper img {\n    width: 100%; }\n\n.cols-three p {\n  margin-bottom: 10px; }\n\n.banner {\n  position: relative;\n  margin-bottom: 20px; }\n\n.large-banner-wrapper {\n  overflow: hidden;\n  height: 400px; }\n  .large-banner-wrapper img {\n    height: 100%;\n    width: 100%;\n    display: block; }\n\n.banner-text-wrapper {\n  margin-top: -400px;\n  height: 400px;\n  text-align: center; }\n\n.banner-text {\n  padding: 85px 90px 60px;\n  display: inline-block;\n  margin: 67px auto;\n  background: #ffffff;\n  min-width: 432px;\n  overflow: hidden;\n  background: rgba(0, 0, 0, 0.75); }\n  .banner-text h1 {\n    font-weight: 700;\n    width: 100%;\n    color: #ffffff;\n    margin-bottom: 10px; }\n  .banner-text p {\n    font-size: 24px;\n    line-height: 30px;\n    font-weight: 300;\n    color: #00abff;\n    margin-bottom: 0px; }\n\n@media (max-width: 600px) {\n  .banner-text {\n    padding: 55px 60px 30px;\n    min-width: 0; }\n    .banner-text h1 {\n      font-size: 24px; }\n    .banner-text p {\n      font-size: 16px; } }\n\n@media (max-width: 400px) {\n  .banner-text {\n    min-width: 0;\n    width: 100%;\n    height: 100%;\n    margin: 0; } }\n\n.photo-desc {\n  margin-top: 12px;\n  text-align: center; }\n\n.text-info {\n  width: 90%; }\n  .text-info p {\n    margin-bottom: 10px; }\n\n.section-block {\n  padding-bottom: 12px; }\n\n.separator {\n  height: 1px;\n  background: rgba(255, 255, 255, 0.3);\n  width: 100%;\n  margin-bottom: 19px;\n  margin-top: 16px; }\n\n.section {\n  padding: 0 20px 50px 20px; }\n\n.card.banner-column-panel {\n  padding: 0;\n  margin-bottom: 90px; }\n  .card.banner-column-panel .card-body {\n    padding: 0; }\n\n@media screen and (min-width: 1620px) {\n  .col-xlg-1 {\n    max-width: 8.33333333%;\n    flex: 0 0 8.33333333%; }\n  .col-xlg-2 {\n    max-width: 16.66666667%;\n    flex: 0 0 16.66666667%; }\n  .col-xlg-3 {\n    max-width: 25%;\n    flex: 0 0 25%; }\n  .col-xlg-4 {\n    max-width: 33.33333333%;\n    flex: 0 0 33.33333333%; }\n  .col-xlg-5 {\n    max-width: 41.66666667%;\n    flex: 0 0 41.66666667%; }\n  .col-xlg-6 {\n    max-width: 50%;\n    flex: 0 0 50%; }\n  .col-xlg-7 {\n    max-width: 58.33333333%;\n    flex: 0 0 58.33333333%; }\n  .col-xlg-8 {\n    max-width: 66.66666667%;\n    flex: 0 0 66.66666667%; }\n  .col-xlg-9 {\n    max-width: 75%;\n    flex: 0 0 75%; }\n  .col-xlg-10 {\n    max-width: 83.33333333%;\n    flex: 0 0 83.33333333%; }\n  .col-xlg-11 {\n    max-width: 91.66666667%;\n    flex: 0 0 91.66666667%; }\n  .col-xlg-12 {\n    max-width: 100%;\n    flex: 0 0 100%; } }\n\n.btn:focus, .btn:active:focus, .btn.active:focus,\n.btn.focus, .btn:active.focus, .btn.active.focus {\n  outline: none; }\n\n.btn {\n  cursor: pointer;\n  border-radius: 5px;\n  transition: all 0.1s ease;\n  padding: 0.344rem 1rem;\n  font-size: 0.9rem; }\n\n.btn:hover {\n  transform: scale(1.2); }\n\n.open > .btn.dropdown-toggle.btn.btn-primary {\n  background: #00abff;\n  border-color: #0093e7;\n  background-color: #0091d9;\n  border-color: #0091d9; }\n\n.open > .btn.dropdown-toggle.btn-success {\n  background: #8bd22f;\n  border-color: #73ba17;\n  background-color: #76b328;\n  border-color: #76b328; }\n\n.open > .btn.dropdown-toggle.btn-info {\n  background: #40daf1;\n  border-color: #28c2d9;\n  background-color: #36b9cd;\n  border-color: #36b9cd; }\n\n.open > .btn.dropdown-toggle.btn-warning {\n  background: #e7ba08;\n  border-color: #cfa200;\n  background-color: #c49e07;\n  border-color: #c49e07; }\n\n.open > .btn.dropdown-toggle.btn-danger {\n  background: #f95372;\n  border-color: #e13b5a;\n  background-color: #d44761;\n  border-color: #d44761; }\n\nbutton.btn.btn-primary {\n  background: #00abff;\n  border-color: #00abff; }\n  button.btn.btn-primary.disabled, button.btn.btn-primary[disabled], fieldset[disabled] button.btn.btn-primary, button.btn.btn-primary.disabled:hover, button.btn.btn-primary[disabled]:hover,\n  fieldset[disabled] button.btn.btn-primary:hover, button.btn.btn-primary.disabled:focus, button.btn.btn-primary[disabled]:focus, fieldset[disabled] button.btn.btn-primary:focus, button.btn.btn-primary.disabled.focus, button.btn.btn-primary[disabled].focus, fieldset[disabled] button.btn.btn-primary.focus, button.btn.btn-primary.disabled:active, button.btn.btn-primary[disabled]:active, fieldset[disabled] button.btn.btn-primary:active, button.btn.btn-primary.disabled.active, button.btn.btn-primary[disabled].active,\n  fieldset[disabled] button.btn.btn-primary.active {\n    background: #00abff;\n    border-color: #0cb7ff; }\n    button.btn.btn-primary.disabled:hover, button.btn.btn-primary[disabled]:hover, fieldset[disabled] button.btn.btn-primary:hover, button.btn.btn-primary.disabled:hover:hover, button.btn.btn-primary[disabled]:hover:hover,\n    fieldset[disabled] button.btn.btn-primary:hover:hover, button.btn.btn-primary.disabled:focus:hover, button.btn.btn-primary[disabled]:focus:hover, fieldset[disabled] button.btn.btn-primary:focus:hover, button.btn.btn-primary.disabled.focus:hover, button.btn.btn-primary[disabled].focus:hover, fieldset[disabled] button.btn.btn-primary.focus:hover, button.btn.btn-primary.disabled:active:hover, button.btn.btn-primary[disabled]:active:hover, fieldset[disabled] button.btn.btn-primary:active:hover, button.btn.btn-primary.disabled.active:hover, button.btn.btn-primary[disabled].active:hover,\n    fieldset[disabled] button.btn.btn-primary.active:hover {\n      transform: none; }\n  button.btn.btn-primary:hover, button.btn.btn-primary:focus, button.btn.btn-primary.focus, button.btn.btn-primary:active, button.btn.btn-primary.active {\n    background: #00abff;\n    border-color: #0093e7; }\n  button.btn.btn-primary:active, button.btn.btn-primary:target {\n    background-color: #0091d9; }\n\nbutton.btn.btn-default {\n  border-width: 1px;\n  color: #ffffff;\n  background: transparent;\n  border-color: rgba(255, 255, 255, 0.5); }\n  button.btn.btn-default.disabled, button.btn.btn-default[disabled], fieldset[disabled] button.btn.btn-default, button.btn.btn-default.disabled:hover, button.btn.btn-default[disabled]:hover,\n  fieldset[disabled] button.btn.btn-default:hover, button.btn.btn-default.disabled:focus, button.btn.btn-default[disabled]:focus, fieldset[disabled] button.btn.btn-default:focus, button.btn.btn-default.disabled.focus, button.btn.btn-default[disabled].focus, fieldset[disabled] button.btn.btn-default.focus, button.btn.btn-default.disabled:active, button.btn.btn-default[disabled]:active, fieldset[disabled] button.btn.btn-default:active, button.btn.btn-default.disabled.active, button.btn.btn-default[disabled].active,\n  fieldset[disabled] button.btn.btn-default.active {\n    background: transparent;\n    border-color: rgba(255, 255, 255, 0.5); }\n    button.btn.btn-default.disabled:hover, button.btn.btn-default[disabled]:hover, fieldset[disabled] button.btn.btn-default:hover, button.btn.btn-default.disabled:hover:hover, button.btn.btn-default[disabled]:hover:hover,\n    fieldset[disabled] button.btn.btn-default:hover:hover, button.btn.btn-default.disabled:focus:hover, button.btn.btn-default[disabled]:focus:hover, fieldset[disabled] button.btn.btn-default:focus:hover, button.btn.btn-default.disabled.focus:hover, button.btn.btn-default[disabled].focus:hover, fieldset[disabled] button.btn.btn-default.focus:hover, button.btn.btn-default.disabled:active:hover, button.btn.btn-default[disabled]:active:hover, fieldset[disabled] button.btn.btn-default:active:hover, button.btn.btn-default.disabled.active:hover, button.btn.btn-default[disabled].active:hover,\n    fieldset[disabled] button.btn.btn-default.active:hover {\n      transform: none; }\n  button.btn.btn-default:hover, button.btn.btn-default:focus, button.btn.btn-default.focus, button.btn.btn-default:active, button.btn.btn-default.active {\n    background: transparent;\n    border-color: rgba(231, 231, 231, 0.5); }\n  button.btn.btn-default:active, button.btn.btn-default:target {\n    background-color: rgba(0, 0, 0, 0.2);\n    color: #ffffff; }\n\nbutton.btn.btn-success {\n  background: #8bd22f;\n  border-color: #8bd22f; }\n  button.btn.btn-success.disabled, button.btn.btn-success[disabled], fieldset[disabled] button.btn.btn-success, button.btn.btn-success.disabled:hover, button.btn.btn-success[disabled]:hover,\n  fieldset[disabled] button.btn.btn-success:hover, button.btn.btn-success.disabled:focus, button.btn.btn-success[disabled]:focus, fieldset[disabled] button.btn.btn-success:focus, button.btn.btn-success.disabled.focus, button.btn.btn-success[disabled].focus, fieldset[disabled] button.btn.btn-success.focus, button.btn.btn-success.disabled:active, button.btn.btn-success[disabled]:active, fieldset[disabled] button.btn.btn-success:active, button.btn.btn-success.disabled.active, button.btn.btn-success[disabled].active,\n  fieldset[disabled] button.btn.btn-success.active {\n    background: #8bd22f;\n    border-color: #97de3b; }\n    button.btn.btn-success.disabled:hover, button.btn.btn-success[disabled]:hover, fieldset[disabled] button.btn.btn-success:hover, button.btn.btn-success.disabled:hover:hover, button.btn.btn-success[disabled]:hover:hover,\n    fieldset[disabled] button.btn.btn-success:hover:hover, button.btn.btn-success.disabled:focus:hover, button.btn.btn-success[disabled]:focus:hover, fieldset[disabled] button.btn.btn-success:focus:hover, button.btn.btn-success.disabled.focus:hover, button.btn.btn-success[disabled].focus:hover, fieldset[disabled] button.btn.btn-success.focus:hover, button.btn.btn-success.disabled:active:hover, button.btn.btn-success[disabled]:active:hover, fieldset[disabled] button.btn.btn-success:active:hover, button.btn.btn-success.disabled.active:hover, button.btn.btn-success[disabled].active:hover,\n    fieldset[disabled] button.btn.btn-success.active:hover {\n      transform: none; }\n  button.btn.btn-success:hover, button.btn.btn-success:focus, button.btn.btn-success.focus, button.btn.btn-success:active, button.btn.btn-success.active {\n    background: #8bd22f;\n    border-color: #73ba17; }\n  button.btn.btn-success:active, button.btn.btn-success:target {\n    background-color: #76b328; }\n\nbutton.btn.btn-info {\n  background: #40daf1;\n  border-color: #40daf1; }\n  button.btn.btn-info.disabled, button.btn.btn-info[disabled], fieldset[disabled] button.btn.btn-info, button.btn.btn-info.disabled:hover, button.btn.btn-info[disabled]:hover,\n  fieldset[disabled] button.btn.btn-info:hover, button.btn.btn-info.disabled:focus, button.btn.btn-info[disabled]:focus, fieldset[disabled] button.btn.btn-info:focus, button.btn.btn-info.disabled.focus, button.btn.btn-info[disabled].focus, fieldset[disabled] button.btn.btn-info.focus, button.btn.btn-info.disabled:active, button.btn.btn-info[disabled]:active, fieldset[disabled] button.btn.btn-info:active, button.btn.btn-info.disabled.active, button.btn.btn-info[disabled].active,\n  fieldset[disabled] button.btn.btn-info.active {\n    background: #40daf1;\n    border-color: #4ce6fd; }\n    button.btn.btn-info.disabled:hover, button.btn.btn-info[disabled]:hover, fieldset[disabled] button.btn.btn-info:hover, button.btn.btn-info.disabled:hover:hover, button.btn.btn-info[disabled]:hover:hover,\n    fieldset[disabled] button.btn.btn-info:hover:hover, button.btn.btn-info.disabled:focus:hover, button.btn.btn-info[disabled]:focus:hover, fieldset[disabled] button.btn.btn-info:focus:hover, button.btn.btn-info.disabled.focus:hover, button.btn.btn-info[disabled].focus:hover, fieldset[disabled] button.btn.btn-info.focus:hover, button.btn.btn-info.disabled:active:hover, button.btn.btn-info[disabled]:active:hover, fieldset[disabled] button.btn.btn-info:active:hover, button.btn.btn-info.disabled.active:hover, button.btn.btn-info[disabled].active:hover,\n    fieldset[disabled] button.btn.btn-info.active:hover {\n      transform: none; }\n  button.btn.btn-info:hover, button.btn.btn-info:focus, button.btn.btn-info.focus, button.btn.btn-info:active, button.btn.btn-info.active {\n    background: #40daf1;\n    border-color: #28c2d9; }\n  button.btn.btn-info:active, button.btn.btn-info:target {\n    background-color: #36b9cd; }\n\nbutton.btn.btn-warning {\n  background: #e7ba08;\n  border-color: #e7ba08; }\n  button.btn.btn-warning.disabled, button.btn.btn-warning[disabled], fieldset[disabled] button.btn.btn-warning, button.btn.btn-warning.disabled:hover, button.btn.btn-warning[disabled]:hover,\n  fieldset[disabled] button.btn.btn-warning:hover, button.btn.btn-warning.disabled:focus, button.btn.btn-warning[disabled]:focus, fieldset[disabled] button.btn.btn-warning:focus, button.btn.btn-warning.disabled.focus, button.btn.btn-warning[disabled].focus, fieldset[disabled] button.btn.btn-warning.focus, button.btn.btn-warning.disabled:active, button.btn.btn-warning[disabled]:active, fieldset[disabled] button.btn.btn-warning:active, button.btn.btn-warning.disabled.active, button.btn.btn-warning[disabled].active,\n  fieldset[disabled] button.btn.btn-warning.active {\n    background: #e7ba08;\n    border-color: #f3c614; }\n    button.btn.btn-warning.disabled:hover, button.btn.btn-warning[disabled]:hover, fieldset[disabled] button.btn.btn-warning:hover, button.btn.btn-warning.disabled:hover:hover, button.btn.btn-warning[disabled]:hover:hover,\n    fieldset[disabled] button.btn.btn-warning:hover:hover, button.btn.btn-warning.disabled:focus:hover, button.btn.btn-warning[disabled]:focus:hover, fieldset[disabled] button.btn.btn-warning:focus:hover, button.btn.btn-warning.disabled.focus:hover, button.btn.btn-warning[disabled].focus:hover, fieldset[disabled] button.btn.btn-warning.focus:hover, button.btn.btn-warning.disabled:active:hover, button.btn.btn-warning[disabled]:active:hover, fieldset[disabled] button.btn.btn-warning:active:hover, button.btn.btn-warning.disabled.active:hover, button.btn.btn-warning[disabled].active:hover,\n    fieldset[disabled] button.btn.btn-warning.active:hover {\n      transform: none; }\n  button.btn.btn-warning:hover, button.btn.btn-warning:focus, button.btn.btn-warning.focus, button.btn.btn-warning:active, button.btn.btn-warning.active {\n    background: #e7ba08;\n    border-color: #cfa200; }\n  button.btn.btn-warning:active, button.btn.btn-warning:target {\n    background-color: #c49e07; }\n\nbutton.btn.btn-danger {\n  background: #f95372;\n  border-color: #f95372; }\n  button.btn.btn-danger.disabled, button.btn.btn-danger[disabled], fieldset[disabled] button.btn.btn-danger, button.btn.btn-danger.disabled:hover, button.btn.btn-danger[disabled]:hover,\n  fieldset[disabled] button.btn.btn-danger:hover, button.btn.btn-danger.disabled:focus, button.btn.btn-danger[disabled]:focus, fieldset[disabled] button.btn.btn-danger:focus, button.btn.btn-danger.disabled.focus, button.btn.btn-danger[disabled].focus, fieldset[disabled] button.btn.btn-danger.focus, button.btn.btn-danger.disabled:active, button.btn.btn-danger[disabled]:active, fieldset[disabled] button.btn.btn-danger:active, button.btn.btn-danger.disabled.active, button.btn.btn-danger[disabled].active,\n  fieldset[disabled] button.btn.btn-danger.active {\n    background: #f95372;\n    border-color: #ff5f7e; }\n    button.btn.btn-danger.disabled:hover, button.btn.btn-danger[disabled]:hover, fieldset[disabled] button.btn.btn-danger:hover, button.btn.btn-danger.disabled:hover:hover, button.btn.btn-danger[disabled]:hover:hover,\n    fieldset[disabled] button.btn.btn-danger:hover:hover, button.btn.btn-danger.disabled:focus:hover, button.btn.btn-danger[disabled]:focus:hover, fieldset[disabled] button.btn.btn-danger:focus:hover, button.btn.btn-danger.disabled.focus:hover, button.btn.btn-danger[disabled].focus:hover, fieldset[disabled] button.btn.btn-danger.focus:hover, button.btn.btn-danger.disabled:active:hover, button.btn.btn-danger[disabled]:active:hover, fieldset[disabled] button.btn.btn-danger:active:hover, button.btn.btn-danger.disabled.active:hover, button.btn.btn-danger[disabled].active:hover,\n    fieldset[disabled] button.btn.btn-danger.active:hover {\n      transform: none; }\n  button.btn.btn-danger:hover, button.btn.btn-danger:focus, button.btn.btn-danger.focus, button.btn.btn-danger:active, button.btn.btn-danger.active {\n    background: #f95372;\n    border-color: #e13b5a; }\n  button.btn.btn-danger:active, button.btn.btn-danger:target {\n    background-color: #d44761; }\n\nbutton.btn.btn-inverse {\n  background: #ffffff;\n  border-color: #ffffff;\n  color: #ffffff; }\n  button.btn.btn-inverse.disabled, button.btn.btn-inverse[disabled], fieldset[disabled] button.btn.btn-inverse, button.btn.btn-inverse.disabled:hover, button.btn.btn-inverse[disabled]:hover,\n  fieldset[disabled] button.btn.btn-inverse:hover, button.btn.btn-inverse.disabled:focus, button.btn.btn-inverse[disabled]:focus, fieldset[disabled] button.btn.btn-inverse:focus, button.btn.btn-inverse.disabled.focus, button.btn.btn-inverse[disabled].focus, fieldset[disabled] button.btn.btn-inverse.focus, button.btn.btn-inverse.disabled:active, button.btn.btn-inverse[disabled]:active, fieldset[disabled] button.btn.btn-inverse:active, button.btn.btn-inverse.disabled.active, button.btn.btn-inverse[disabled].active,\n  fieldset[disabled] button.btn.btn-inverse.active {\n    background: #ffffff;\n    border-color: white; }\n    button.btn.btn-inverse.disabled:hover, button.btn.btn-inverse[disabled]:hover, fieldset[disabled] button.btn.btn-inverse:hover, button.btn.btn-inverse.disabled:hover:hover, button.btn.btn-inverse[disabled]:hover:hover,\n    fieldset[disabled] button.btn.btn-inverse:hover:hover, button.btn.btn-inverse.disabled:focus:hover, button.btn.btn-inverse[disabled]:focus:hover, fieldset[disabled] button.btn.btn-inverse:focus:hover, button.btn.btn-inverse.disabled.focus:hover, button.btn.btn-inverse[disabled].focus:hover, fieldset[disabled] button.btn.btn-inverse.focus:hover, button.btn.btn-inverse.disabled:active:hover, button.btn.btn-inverse[disabled]:active:hover, fieldset[disabled] button.btn.btn-inverse:active:hover, button.btn.btn-inverse.disabled.active:hover, button.btn.btn-inverse[disabled].active:hover,\n    fieldset[disabled] button.btn.btn-inverse.active:hover {\n      transform: none; }\n  button.btn.btn-inverse:hover, button.btn.btn-inverse:focus, button.btn.btn-inverse.focus, button.btn.btn-inverse:active, button.btn.btn-inverse.active {\n    background: #ffffff;\n    border-color: #e7e7e7; }\n  button.btn.btn-inverse:active, button.btn.btn-inverse:target, button.btn.btn-inverse:hover {\n    background-color: #ffffff;\n    color: #ffffff; }\n\n.btn-with-icon i {\n  margin-right: 10px; }\n\n.btn-group :hover, .btn-toolbar :hover {\n  transform: none; }\n\n.btn-group button.btn.btn-primary {\n  border-color: #009ff3; }\n  .btn-group button.btn.btn-primary:hover {\n    border-color: #0093e7; }\n\n.btn-group button.btn.btn-danger {\n  border-color: #ed4766; }\n  .btn-group button.btn.btn-danger:hover {\n    border-color: #e13b5a; }\n\n.btn-group button.btn.btn-info {\n  border-color: #34cee5; }\n  .btn-group button.btn.btn-info:hover {\n    border-color: #28c2d9; }\n\n.btn-group button.btn.btn-success {\n  border-color: #7fc623; }\n  .btn-group button.btn.btn-success:hover {\n    border-color: #73ba17; }\n\n.btn-group button.btn.btn-warning {\n  border-color: #dbae00; }\n  .btn-group button.btn.btn-warning:hover {\n    border-color: #cfa200; }\n\n.btn-group .dropdown-menu {\n  margin-top: 0px; }\n\n.btn-toolbar {\n  display: inline-block; }\n\n.btn .caret {\n  margin-left: 2px; }\n\nbutton.progress-button .progress {\n  margin-bottom: 0;\n  border-radius: 0; }\n\nbutton.progress-button:hover {\n  transform: none; }\n\nbutton.progress-button.progress-button-style-shrink.btn.disabled.progress-button-dir-horizontal:hover {\n  transform: scaleY(0.3); }\n\nbutton.progress-button.progress-button-style-shrink.btn.disabled.progress-button-dir-vertical:hover {\n  transform: scaleX(0.1); }\n\nbutton.progress-button.btn.btn-primary {\n  border-radius: 0; }\n  button.progress-button.btn.btn-primary .content:after, button.progress-button.btn.btn-primary .content:before {\n    color: #002233; }\n  button.progress-button.btn.btn-primary.progress-button-style-move-up .content, button.progress-button.btn.btn-primary.progress-button-style-slide-down .content {\n    background-color: #0089cc; }\n  button.progress-button.btn.btn-primary.progress-button-style-lateral-lines .progress-inner {\n    border-color: #0089cc;\n    background: 0 0; }\n  button.progress-button.btn.btn-primary .progress {\n    background-color: #0089cc;\n    box-shadow: 0 1px 0 #0089cc; }\n  button.progress-button.btn.btn-primary .progress-inner {\n    background-color: #006799; }\n  button.progress-button.btn.btn-primary.progress-button-perspective {\n    background: none; }\n    button.progress-button.btn.btn-primary.progress-button-perspective .content {\n      background-color: #00abff; }\n\nbutton.progress-button.btn.btn-default {\n  border-radius: 0; }\n  button.progress-button.btn.btn-default .content:after, button.progress-button.btn.btn-default .content:before {\n    color: #999999; }\n  button.progress-button.btn.btn-default.progress-button-style-move-up .content, button.progress-button.btn.btn-default.progress-button-style-slide-down .content {\n    background-color: #e6e6e6; }\n  button.progress-button.btn.btn-default.progress-button-style-lateral-lines .progress-inner {\n    border-color: #e6e6e6;\n    background: 0 0; }\n  button.progress-button.btn.btn-default .progress {\n    background-color: #e6e6e6;\n    box-shadow: 0 1px 0 #e6e6e6; }\n  button.progress-button.btn.btn-default .progress-inner {\n    background-color: #cccccc; }\n  button.progress-button.btn.btn-default.progress-button-perspective {\n    background: none; }\n    button.progress-button.btn.btn-default.progress-button-perspective .content {\n      background-color: #ffffff; }\n\nbutton.progress-button.btn.btn-success {\n  border-radius: 0; }\n  button.progress-button.btn.btn-success .content:after, button.progress-button.btn.btn-success .content:before {\n    color: #1d2c09; }\n  button.progress-button.btn.btn-success.progress-button-style-move-up .content, button.progress-button.btn.btn-success.progress-button-style-slide-down .content {\n    background-color: #70a925; }\n  button.progress-button.btn.btn-success.progress-button-style-lateral-lines .progress-inner {\n    border-color: #70a925;\n    background: 0 0; }\n  button.progress-button.btn.btn-success .progress {\n    background-color: #70a925;\n    box-shadow: 0 1px 0 #70a925; }\n  button.progress-button.btn.btn-success .progress-inner {\n    background-color: #547f1c; }\n  button.progress-button.btn.btn-success.progress-button-perspective {\n    background: none; }\n    button.progress-button.btn.btn-success.progress-button-perspective .content {\n      background-color: #8bd22f; }\n\nbutton.progress-button.btn.btn-info {\n  border-radius: 0; }\n  button.progress-button.btn.btn-info .content:after, button.progress-button.btn.btn-info .content:before {\n    color: #07535e; }\n  button.progress-button.btn.btn-info.progress-button-style-move-up .content, button.progress-button.btn.btn-info.progress-button-style-slide-down .content {\n    background-color: #11d0ed; }\n  button.progress-button.btn.btn-info.progress-button-style-lateral-lines .progress-inner {\n    border-color: #11d0ed;\n    background: 0 0; }\n  button.progress-button.btn.btn-info .progress {\n    background-color: #11d0ed;\n    box-shadow: 0 1px 0 #11d0ed; }\n  button.progress-button.btn.btn-info .progress-inner {\n    background-color: #0ea6bd; }\n  button.progress-button.btn.btn-info.progress-button-perspective {\n    background: none; }\n    button.progress-button.btn.btn-info.progress-button-perspective .content {\n      background-color: #40daf1; }\n\nbutton.progress-button.btn.btn-warning {\n  border-radius: 0; }\n  button.progress-button.btn.btn-warning .content:after, button.progress-button.btn.btn-warning .content:before {\n    color: #221b01; }\n  button.progress-button.btn.btn-warning.progress-button-style-move-up .content, button.progress-button.btn.btn-warning.progress-button-style-slide-down .content {\n    background-color: #b69206; }\n  button.progress-button.btn.btn-warning.progress-button-style-lateral-lines .progress-inner {\n    border-color: #b69206;\n    background: 0 0; }\n  button.progress-button.btn.btn-warning .progress {\n    background-color: #b69206;\n    box-shadow: 0 1px 0 #b69206; }\n  button.progress-button.btn.btn-warning .progress-inner {\n    background-color: #846b05; }\n  button.progress-button.btn.btn-warning.progress-button-perspective {\n    background: none; }\n    button.progress-button.btn.btn-warning.progress-button-perspective .content {\n      background-color: #e7ba08; }\n\nbutton.progress-button.btn.btn-danger {\n  border-radius: 0; }\n  button.progress-button.btn.btn-danger .content:after, button.progress-button.btn.btn-danger .content:before {\n    color: #7c041b; }\n  button.progress-button.btn.btn-danger.progress-button-style-move-up .content, button.progress-button.btn.btn-danger.progress-button-style-slide-down .content {\n    background-color: #f7224a; }\n  button.progress-button.btn.btn-danger.progress-button-style-lateral-lines .progress-inner {\n    border-color: #f7224a;\n    background: 0 0; }\n  button.progress-button.btn.btn-danger .progress {\n    background-color: #f7224a;\n    box-shadow: 0 1px 0 #f7224a; }\n  button.progress-button.btn.btn-danger .progress-inner {\n    background-color: #de0830; }\n  button.progress-button.btn.btn-danger.progress-button-perspective {\n    background: none; }\n    button.progress-button.btn.btn-danger.progress-button-perspective .content {\n      background-color: #f95372; }\n\n.btn-raised {\n  box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.35); }\n\n.btn-mm {\n  padding: 5px 11px;\n  font-size: 13px; }\n\n.btn-xm {\n  padding: 8px 14px;\n  font-size: 16px; }\n\n.btn-group-xs > .btn, .btn-xs {\n  padding: 1px 5px;\n  font-size: 12px;\n  line-height: 1.5; }\n\n.btn-group-sm > .btn, .btn-sm {\n  padding: 5px 10px;\n  font-size: 12px;\n  line-height: 1.5;\n  border-radius: 3px; }\n\n.btn-group-lg > .btn, .btn-lg {\n  padding: 10px 16px;\n  font-size: 18px;\n  line-height: 1.3333333;\n  border-radius: 6px; }\n\n.dropdown button.btn.btn-default.dropdown-toggle {\n  color: #ffffff;\n  border: 1px solid rgba(255, 255, 255, 0.5);\n  background-color: transparent; }\n  .dropdown button.btn.btn-default.dropdown-toggle:focus, .dropdown button.btn.btn-default.dropdown-toggle:active {\n    background-color: #ffffff; }\n\n.ng2 .dropdown button.btn.btn-default.dropdown-toggle:focus, .ng2 .dropdown button.btn.btn-default.dropdown-toggle:active, .blur .dropdown button.btn.btn-default.dropdown-toggle:focus, .blur .dropdown button.btn.btn-default.dropdown-toggle:active {\n  background-color: transparent; }\n\n.bootstrap-select .dropdown-toggle:focus {\n  outline: none !important; }\n\n.bootstrap-select button.btn-default:focus {\n  color: #ffffff; }\n\n.bootstrap-select .btn {\n  transition: none; }\n\n.btn.create-btn {\n  color: white;\n  background-color: black;\n  margin-right: 360px; }\n\n.btn.cancel-btn {\n  color: white;\n  background-color: black;\n  margin-left: 420px;\n  margin-top: -117px; }\n\n.i-face {\n  display: inline-block;\n  background: url(\"assets/img/face.svg\") no-repeat center;\n  background-size: contain;\n  vertical-align: middle;\n  width: 80px;\n  height: 80px; }\n\n.i-money {\n  display: inline-block;\n  background: url(\"assets/img/money.svg\") no-repeat center;\n  background-size: contain;\n  vertical-align: middle;\n  width: 80px;\n  height: 80px; }\n\n.i-person {\n  display: inline-block;\n  background: url(\"assets/img/person.svg\") no-repeat center;\n  background-size: contain;\n  vertical-align: middle;\n  width: 80px;\n  height: 80px; }\n\n.i-refresh {\n  display: inline-block;\n  background: url(\"assets/img/refresh.svg\") no-repeat center;\n  background-size: contain;\n  vertical-align: middle;\n  width: 80px;\n  height: 80px; }\n\n::-webkit-scrollbar {\n  width: 0.5em;\n  height: 0.5em; }\n\n::-webkit-scrollbar-thumb {\n  background: #d9d9d9;\n  cursor: pointer; }\n\n::-webkit-scrollbar-track {\n  background: transparent; }\n\nbody {\n  scrollbar-face-color: #d9d9d9;\n  scrollbar-track-color: transparent; }\n\nhtml {\n  position: relative;\n  min-width: 320px; }\n\nhtml, body {\n  min-height: 100%;\n  height: 100%;\n  min-width: 320px; }\n\nmain {\n  min-height: 100%;\n  position: relative;\n  font: 14px/16px \"Roboto\", sans-serif;\n  color: #ffffff;\n  background-color: #fff !important;\n  background-color: #fff; }\n  main::before {\n    content: '';\n    position: fixed;\n    width: 100%;\n    height: 100%;\n    top: 0;\n    left: 0;\n    background: url() no-repeat center center;\n    background-size: cover;\n    will-change: transform;\n    z-index: 0; }\n  main .additional-bg {\n    display: none; }\n\n@media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) {\n  html {\n    overflow: hidden;\n    height: 100%; }\n  body {\n    overflow: auto;\n    height: 100%; } }\n\na {\n  transition: color 0.5s ease;\n  outline: 0 !important; }\n\n.body-bg {\n  display: none; }\n\n.al-header {\n  display: block;\n  height: 49px;\n  margin: 0;\n  background-repeat: repeat-x;\n  position: relative;\n  z-index: 905;\n  color: #444444; }\n\n.al-main {\n  margin-left: 180px;\n  padding: 66px 0 34px 0;\n  min-height: 500px;\n  position: relative;\n  background-color: white; }\n\n.al-footer {\n  height: 34px;\n  padding: 0 18px 0 200px;\n  width: 100%;\n  position: absolute;\n  display: block;\n  bottom: 0;\n  font-size: 13px;\n  color: #ffffff;\n  transition: padding-left 0.5s ease; }\n\n.al-footer-main {\n  float: left;\n  margin-left: 15px;\n  color: #fff; }\n\n.al-copy {\n  float: left; }\n\n.al-footer-right {\n  float: right;\n  margin-right: 12px; }\n  .al-footer-right i {\n    margin: 0 4px;\n    color: #fff;\n    font-size: 12px; }\n  .al-footer-right a {\n    margin-left: 4px;\n    color: #ffffff; }\n    .al-footer-right a:hover {\n      color: #fff; }\n\n.al-share {\n  margin: -6px 0 0 12px;\n  padding: 0;\n  list-style: none;\n  float: left; }\n  .al-share li {\n    list-style: none;\n    float: left;\n    margin-left: 16px; }\n    .al-share li i {\n      cursor: pointer;\n      transition: all 0.1s ease;\n      color: white;\n      padding: 6px;\n      box-sizing: content-box;\n      font-size: 16px; }\n      .al-share li i:hover {\n        transform: scale(1.2); }\n    .al-share li i.fa-facebook-square {\n      color: #3b5998; }\n    .al-share li i.fa-twitter-square {\n      color: #55acee; }\n    .al-share li i.fa-google-plus-square {\n      color: #dd4b39; }\n\n.al-content {\n  padding: 8px 32px 8px 40px;\n  color: black; }\n\n@media screen and (max-width: 500px) {\n  .al-content {\n    padding: 8px 20px; } }\n\n.vis-hidden {\n  visibility: hidden;\n  position: absolute;\n  top: -9999px;\n  left: -9999px; }\n\n.icon-up, .icon-down {\n  width: 5px;\n  height: 13px;\n  display: block; }\n\n.icon-up {\n  background: url(\"assets/img/arrow-green-up.svg\") no-repeat 0 0; }\n\n.icon-down {\n  background: url(\"assets/img/arrow-red-down.svg\") no-repeat 0 0; }\n\n.disable-text-selection {\n  -webkit-touch-callout: none;\n  user-select: none; }\n\n.align-right {\n  text-align: right; }\n\n.amcharts-chart-div > a {\n  font-size: 6px !important; }\n\n.content-panel {\n  padding-left: 22px;\n  padding-top: 26px; }\n\n@media (max-width: 590px) {\n  .al-footer-right {\n    float: none;\n    margin-bottom: 19px;\n    margin-right: 0; }\n  .al-footer {\n    height: 76px;\n    text-align: center; }\n  .al-main {\n    padding-bottom: 76px; }\n  .al-footer-main {\n    float: none;\n    display: inline-block; } }\n\n.full-invisible {\n  visibility: hidden !important; }\n  .full-invisible * {\n    visibility: hidden !important; }\n\n.irs-grid-text {\n  color: #ffffff; }\n\n.text-right {\n  text-align: right; }\n\n.text-left {\n  text-align: left; }\n\n.text-center {\n  text-align: center; }\n\n@font-face {\n  font-family: 'socicon';\n  src: url(\"assets/fonts/socicon.eot\");\n  src: url(\"assets/fonts/socicon.eot?#iefix\") format(\"embedded-opentype\"), url(\"assets/fonts/socicon.woff\") format(\"woff\"), url(\"assets/fonts/socicon.woff2\") format(\"woff2\"), url(\"assets/fonts/socicon.ttf\") format(\"truetype\"), url(\"assets/fonts/socicon.svg#sociconregular\") format(\"svg\");\n  font-weight: 400;\n  font-style: normal;\n  text-transform: initial; }\n\n.socicon {\n  font-family: 'socicon' !important; }\n\n.socicon {\n  position: relative;\n  top: 1px;\n  display: inline-block;\n  font-family: 'socicon';\n  font-style: normal;\n  font-weight: 400;\n  line-height: 1;\n  -webkit-font-smoothing: antialiased; }\n\n.socicon:empty {\n  width: 1em; }\n\n.socicon-twitter {\n  background-color: #55acee; }\n  .socicon-twitter:before {\n    content: \"a\"; }\n\n.socicon-facebook {\n  background-color: #3b5998; }\n  .socicon-facebook:before {\n    content: \"b\"; }\n\n.socicon-google {\n  background-color: #dd4b39; }\n  .socicon-google:before {\n    content: \"c\"; }\n\n.socicon-linkedin {\n  background-color: #0177B5; }\n  .socicon-linkedin:before {\n    content: \"j\"; }\n\n.socicon-github {\n  background-color: #6b6b6b; }\n  .socicon-github:before {\n    content: \"Q\"; }\n\n.socicon-stackoverflow {\n  background-color: #2F96E8; }\n  .socicon-stackoverflow:before {\n    content: \"(\"; }\n\n.socicon-dribble {\n  background-color: #F26798; }\n  .socicon-dribble:before {\n    content: \"D\"; }\n\n.socicon-behace {\n  background-color: #0093FA; }\n  .socicon-behace:before {\n    content: \"H\"; }\n\n.table {\n  margin-bottom: 0px; }\n  .table > thead > tr > th {\n    border-bottom: 1px solid rgba(255, 255, 255, 0.3);\n    white-space: nowrap;\n    padding: 3.75rem; }\n    .table > thead > tr > th:first-child {\n      text-align: center; }\n    .table > thead > tr > th:last-child {\n      padding-right: 16px; }\n  .table > tbody > tr > tr:first-child {\n    padding-top: 1px; }\n  .table > tbody > tr > td {\n    padding: 18px 46px;\n    line-height: 35px;\n    border-top: 1px solid rgba(255, 255, 255, 0.3); }\n    .table > tbody > tr > td:first-child {\n      text-align: center; }\n    .table > tbody > tr > td:last-child {\n      padding-right: 16px !important; }\n\n.table-id {\n  text-align: left !important;\n  width: 40px; }\n\n.table-arr {\n  width: 5px;\n  padding: 10px 8px 8px 0 !important; }\n\n.table-no-borders {\n  border: none; }\n  .table-no-borders td, .table-no-borders th, .table-no-borders tr {\n    border: none !important; }\n\n.editable-wrap .btn-group.form-control {\n  background-color: transparent; }\n\n.editable-tr-wrap .editable-wrap {\n  vertical-align: super; }\n\n.editable-tr-wrap .editable-controls input.editable-input {\n  width: 110px; }\n\n.editable-tr-wrap td {\n  width: 20%; }\n\n.editable-table-button {\n  width: 70px; }\n\n.add-row-editable-table {\n  margin-bottom: 10px; }\n\n.add-row-editable-table + table {\n  margin-bottom: 5px; }\n\n.select-page-size-wrap {\n  width: 150px; }\n\n.table .header-row th {\n  vertical-align: middle;\n  padding: 0 8px; }\n\ntr.editable-row input.form-control {\n  vertical-align: middle; }\n\n.select-td .editable-select {\n  margin-bottom: 1px; }\n\n@media screen and (max-width: 1199px) {\n  .editable-tr-wrap .editable-wrap {\n    vertical-align: middle; } }\n\n.browser-icons {\n  width: 41px; }\n\n.st-sort-ascent, .st-sort-descent {\n  position: relative; }\n\n.st-sort-ascent:after, .st-sort-descent:after {\n  width: 0;\n  height: 0;\n  border-bottom: 4px solid #ffffff;\n  border-top: 4px solid transparent;\n  border-left: 4px solid transparent;\n  border-right: 4px solid transparent;\n  margin-bottom: 2px; }\n\n.st-sort-descent:after {\n  transform: rotate(-180deg);\n  margin-bottom: -2px; }\n\n.sortable th {\n  cursor: pointer; }\n  .sortable th:after {\n    content: '';\n    display: inline-block;\n    width: 8px;\n    margin-left: 8px; }\n\na.email-link {\n  color: #ffffff; }\n  a.email-link:hover {\n    color: #f95372; }\n\ninput.search-input {\n  margin-left: -8px;\n  padding-left: 8px; }\n\n.table .pagination {\n  margin: 4px 0 -12px 0; }\n  .table .pagination a {\n    cursor: pointer; }\n\n.vertical-scroll {\n  max-height: 214px; }\n\n.pagination > li > a, .pagination > li > span {\n  background: transparent; }\n\n.pagination > li:first-child > a, .pagination > li:first-child > span {\n  border-top-left-radius: 0px;\n  border-bottom-left-radius: 0px; }\n\n.pagination > li:last-child > a, .pagination > li:last-child > span {\n  border-top-right-radius: 0px;\n  border-bottom-right-radius: 0px; }\n\n.status-button {\n  width: 60px; }\n\n.table .editable-wrap .editable-controls, .table .editable-wrap .editable-error {\n  vertical-align: sub; }\n  .table .editable-wrap .editable-controls .btn, .table .editable-wrap .editable-error .btn {\n    padding: 3px 8px; }\n    .table .editable-wrap .editable-controls .btn.dropdown-toggle, .table .editable-wrap .editable-error .btn.dropdown-toggle {\n      padding: 3px 20px;\n      margin-top: 3px; }\n  .table .editable-wrap .editable-controls input, .table .editable-wrap .editable-error input {\n    line-height: 1px;\n    height: 30px; }\n\n.form-inline button[type=\"submit\"].editable-table-button {\n  margin-left: 0; }\n\n.table > thead > tr > th {\n  border-bottom: none; }\n\n.table > tbody > tr.no-top-border:first-child > td {\n  border-top: none; }\n\n.black-muted-bg {\n  background-color: rgba(0, 0, 0, 0.1); }\n\n.table-hover > tbody > tr:hover {\n  background-color: rgba(0, 0, 0, 0.1); }\n\n.table-bordered,\n.table-bordered > thead > tr > th,\n.table-bordered > tbody > tr > th,\n.table-bordered > tfoot > tr > th,\n.table-bordered > thead > tr > td,\n.table-bordered > tbody > tr > td,\n.table-bordered > tfoot > tr > td {\n  border: 1px solid rgba(255, 255, 255, 0.3); }\n\n.table-striped > tbody > tr:nth-of-type(odd) {\n  background-color: rgba(0, 0, 0, 0.1); }\n\n.table > tbody > tr.primary > td {\n  background-color: rgba(0, 171, 255, 0.7);\n  color: #ffffff;\n  border: none; }\n  .table > tbody > tr.primary > td a.email-link {\n    color: #ffffff; }\n    .table > tbody > tr.primary > td a.email-link:hover {\n      color: #f95372; }\n\n.table > tbody > tr.success > td {\n  background-color: rgba(139, 210, 47, 0.7);\n  color: #ffffff;\n  border: none; }\n  .table > tbody > tr.success > td a.email-link {\n    color: #ffffff; }\n    .table > tbody > tr.success > td a.email-link:hover {\n      color: #f95372; }\n\n.table > tbody > tr.warning > td {\n  background-color: rgba(231, 186, 8, 0.7);\n  color: #ffffff;\n  border: none; }\n  .table > tbody > tr.warning > td a.email-link {\n    color: #ffffff; }\n    .table > tbody > tr.warning > td a.email-link:hover {\n      color: #f95372; }\n\n.table > tbody > tr.danger > td {\n  background-color: rgba(249, 83, 114, 0.7);\n  color: #ffffff;\n  border: none; }\n  .table > tbody > tr.danger > td a.email-link {\n    color: #ffffff; }\n    .table > tbody > tr.danger > td a.email-link:hover {\n      color: #f95372; }\n\n.table > tbody > tr.info > td {\n  background-color: rgba(64, 218, 241, 0.7);\n  color: #ffffff;\n  border: none; }\n  .table > tbody > tr.info > td a.email-link {\n    color: #ffffff; }\n    .table > tbody > tr.info > td a.email-link:hover {\n      color: #f95372; }\n\n.editable-click, a.editable-click {\n  color: #ffffff;\n  border-bottom: dashed 1px rgba(255, 255, 255, 0.5); }\n\nth {\n  font-weight: 400; }\n\n.editable-empty {\n  color: #d44761; }\n\n.table > tbody > tr > th {\n  border: none; }\n\n.table-striped > tbody > tr > td {\n  border: none; }\n\n.pagination > li > a,\n.pagination > li > span {\n  color: #ffffff;\n  border-color: rgba(255, 255, 255, 0.5); }\n\n.pagination > li:first-of-type > a,\n.pagination > li:first-of-type > span {\n  border-top-left-radius: 5px;\n  border-bottom-left-radius: 5px; }\n\n.pagination > li:last-of-type > a,\n.pagination > li:last-of-type > span {\n  border-top-right-radius: 5px;\n  border-bottom-right-radius: 5px; }\n\n.pagination > .active > a,\n.pagination > .active > span,\n.pagination > .active > a:hover,\n.pagination > .active > span:hover,\n.pagination > .active > a:focus,\n.pagination > .active > span:focus {\n  background-color: rgba(0, 0, 0, 0.3) !important;\n  border-color: rgba(255, 255, 255, 0.5) !important; }\n\n.pagination > li > a:hover,\n.pagination > li > span:hover,\n.pagination > li > a:focus,\n.pagination > li > span:focus {\n  background-color: rgba(0, 0, 0, 0.2);\n  color: #ffffff; }\n\n.page-item.disabled .page-link, .page-item.disabled .page-link:focus, .page-item.disabled .page-link:hover {\n  background-color: rgba(255, 255, 255, 0.1);\n  color: #ffffff; }\n\n.editable-buttons .btn-with-icon i {\n  margin-right: 0; }\n\n.table-responsive {\n  margin-top: 10px;\n  border-top: 3px solid grey; }\n\n.ng2-smart-pagination {\n  display: flex !important; }\n\n.ng2-smart-pagination-nav {\n  margin-top: 16px; }\n\n.label {\n  border-radius: 0; }\n\n.label-primary {\n  background: #00abff; }\n\n.label-info {\n  background: #4dc4ff; }\n\n.label-success {\n  background: #8bd22f; }\n\n.label-warning {\n  background: #e7ba08; }\n\n.label-danger {\n  background: #f95372; }\n\n.form-horizontal label {\n  line-height: 34px;\n  margin-bottom: 0;\n  padding-top: 0 !important; }\n\n.form-group label {\n  margin-bottom: 5px;\n  color: #ffffff;\n  font-weight: 400;\n  font-size: 17px; }\n\n.form-control {\n  color: #ffffff;\n  border: 1px solid rgba(255, 255, 255, 0.6);\n  border-radius: 5px;\n  background-color: rgba(255, 255, 255, 0.1);\n  box-shadow: none;\n  font-size: 14px; }\n  .form-control::-webkit-input-placeholder {\n    color: #ffffff;\n    opacity: 0.7; }\n  .form-control:-moz-placeholder {\n    /* Firefox 18- */\n    color: #ffffff;\n    opacity: 0.7; }\n  .form-control::-moz-placeholder {\n    /* Firefox 19+ */\n    color: #ffffff;\n    opacity: 0.7; }\n  .form-control:-ms-input-placeholder {\n    color: #ffffff;\n    opacity: 0.7; }\n  .form-control:focus {\n    color: #ffffff;\n    box-shadow: none;\n    border-color: rgba(255, 255, 255, 0.6);\n    background: rgba(255, 255, 255, 0.1); }\n\nselect.form-control {\n  padding-left: 8px; }\n\nselect.form-control:not([multiple]) option {\n  color: #7d7d7d; }\n\nselect.form-control[multiple] option {\n  color: #ffffff; }\n\ntextarea.form-control {\n  height: 96px; }\n\n.form-inline .form-group input {\n  width: 100%; }\n\n.form-inline .form-group label {\n  margin-right: 12px; }\n\n.form-inline button[type=\"submit\"] {\n  margin-left: 12px; }\n\n.switch-container {\n  display: inline-block; }\n  .switch-container.primary .bootstrap-switch.bootstrap-switch-on {\n    border-color: #00abff; }\n  .switch-container.success .bootstrap-switch.bootstrap-switch-on {\n    border-color: #8bd22f; }\n  .switch-container.warning .bootstrap-switch.bootstrap-switch-on {\n    border-color: #e7ba08; }\n  .switch-container.danger .bootstrap-switch.bootstrap-switch-on {\n    border-color: #f95372; }\n  .switch-container.info .bootstrap-switch.bootstrap-switch-on {\n    border-color: #4dc4ff; }\n\n.bootstrap-switch {\n  border-radius: 5px;\n  border: 1px solid #ffffff;\n  transition: border-color ease-in-out .7s, box-shadow ease-in-out .7s; }\n  .bootstrap-switch:focus {\n    outline: none; }\n  .bootstrap-switch.bootstrap-switch-off {\n    border-color: rgba(255, 255, 255, 0.5); }\n  .bootstrap-switch.bootstrap-switch-focused {\n    box-shadow: none; }\n    .bootstrap-switch.bootstrap-switch-focused.bootstrap-switch-off {\n      border-color: rgba(255, 255, 255, 0.5); }\n  .bootstrap-switch .bootstrap-switch-container {\n    border-radius: 0; }\n    .bootstrap-switch .bootstrap-switch-container:focus {\n      outline: none; }\n  .bootstrap-switch .bootstrap-switch-handle-on {\n    border-radius: 0; }\n    .bootstrap-switch .bootstrap-switch-handle-on.bootstrap-switch-default {\n      background: #ffffff; }\n    .bootstrap-switch .bootstrap-switch-handle-on.bootstrap-switch-success {\n      background: #8bd22f; }\n    .bootstrap-switch .bootstrap-switch-handle-on.bootstrap-switch-primary {\n      background: #00abff; }\n    .bootstrap-switch .bootstrap-switch-handle-on.bootstrap-switch-warning {\n      background: #e7ba08; }\n    .bootstrap-switch .bootstrap-switch-handle-on.bootstrap-switch-danger {\n      background: #f95372; }\n    .bootstrap-switch .bootstrap-switch-handle-on.bootstrap-switch-info {\n      background: #4dc4ff; }\n  .bootstrap-switch .bootstrap-switch-handle-off {\n    border-radius: 0; }\n  .bootstrap-switch .bootstrap-switch-label {\n    background: transparent; }\n  .bootstrap-switch.bootstrap-switch-animate .bootstrap-switch-container {\n    transition: margin-left .2s; }\n\n.switches {\n  margin-left: -12px;\n  margin-bottom: -12px; }\n  .switches .switch-container {\n    float: left;\n    margin-left: 12px;\n    margin-bottom: 12px; }\n\n.input-group {\n  width: 100%;\n  margin-bottom: 15px; }\n  .input-group > span {\n    border-radius: 0; }\n\n.nowrap {\n  white-space: nowrap; }\n\n.cut-with-dots {\n  overflow: hidden;\n  text-overflow: ellipsis;\n  display: block; }\n\nlabel.custom-radio {\n  @padding-right : 0;\n  padding-left: 0;\n  margin-bottom: 0; }\n  label.custom-radio > input {\n    height: 0;\n    z-index: -100 !important;\n    opacity: 0;\n    position: absolute; }\n    label.custom-radio > input:checked + span:before {\n      content: \"\\f00c\";\n      font-weight: 300; }\n    label.custom-radio > input:disabled + span {\n      color: rgba(255, 255, 255, 0.4);\n      cursor: not-allowed; }\n      label.custom-radio > input:disabled + span:before {\n        border-color: rgba(255, 255, 255, 0.4) !important;\n        cursor: not-allowed; }\n  label.custom-radio > span {\n    position: relative;\n    display: inline-block;\n    margin: 0;\n    line-height: 16px;\n    font-weight: 300;\n    cursor: pointer;\n    padding-left: 22px;\n    width: 100%; }\n    label.custom-radio > span:before {\n      cursor: pointer;\n      font-family: fontAwesome;\n      font-weight: 300;\n      font-size: 12px;\n      color: #ffffff;\n      content: \"\\a0\";\n      background-color: transparent;\n      border: 1px solid rgba(255, 255, 255, 0.5);\n      border-radius: 0;\n      display: inline-block;\n      text-align: center;\n      height: 16px;\n      line-height: 14px;\n      min-width: 16px;\n      margin-right: 6px;\n      position: relative;\n      top: 0;\n      margin-left: -22px;\n      float: left; }\n    label.custom-radio > span:hover:before {\n      border-color: #33bcff; }\n  label.custom-radio > input:checked + span:before {\n    content: \"\\f111\"; }\n  label.custom-radio > span:before {\n    border-radius: 16px;\n    font-size: 9px; }\n\nlabel.custom-input-primary > span:before {\n  color: #00abff; }\n\nlabel.custom-input-primary > span:hover:before {\n  border-color: #00abff; }\n\nlabel.custom-input-success > span:before {\n  color: #8bd22f; }\n\nlabel.custom-input-success > span:hover:before {\n  border-color: #8bd22f; }\n\nlabel.custom-input-warning > span:before {\n  color: #e7ba08; }\n\nlabel.custom-input-warning > span:hover:before {\n  border-color: #e7ba08; }\n\nlabel.custom-input-danger > span:before {\n  color: #f95372; }\n\nlabel.custom-input-danger > span:hover:before {\n  border-color: #f95372; }\n\n.form-horizontal .radio, .form-horizontal .radio-inline {\n  padding-top: 0; }\n\n.input-demo {\n  line-height: 25px; }\n  .input-demo ba-multi-checkbox {\n    width: 100%; }\n\n.input-group-addon {\n  line-height: inherit; }\n\n.form-control-feedback {\n  position: absolute;\n  top: 0;\n  right: 0;\n  z-index: 2;\n  display: block;\n  width: 34px;\n  height: 34px;\n  line-height: 34px;\n  text-align: center;\n  pointer-events: none; }\n\n.has-feedback .form-control {\n  padding-right: 42.5px; }\n\n.has-feedback label ~ .form-control-feedback {\n  top: 19px;\n  font-size: 18px; }\n\n.bootstrap-select .btn-default:focus {\n  color: #ffffff; }\n\n.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {\n  background-color: rgba(255, 255, 255, 0.1);\n  color: rgba(255, 255, 255, 0.4);\n  border-color: rgba(255, 255, 255, 0.49); }\n  .form-control[disabled]::-webkit-input-placeholder, .form-control[readonly]::-webkit-input-placeholder, fieldset[disabled] .form-control::-webkit-input-placeholder {\n    color: #ffffff;\n    opacity: 0.5; }\n  .form-control[disabled]:-moz-placeholder, .form-control[readonly]:-moz-placeholder, fieldset[disabled] .form-control:-moz-placeholder {\n    /* Firefox 18- */\n    color: #ffffff;\n    opacity: 0.5; }\n  .form-control[disabled]::-moz-placeholder, .form-control[readonly]::-moz-placeholder, fieldset[disabled] .form-control::-moz-placeholder {\n    /* Firefox 19+ */\n    color: #ffffff;\n    opacity: 0.5; }\n  .form-control[disabled]:-ms-input-placeholder, .form-control[readonly]:-ms-input-placeholder, fieldset[disabled] .form-control:-ms-input-placeholder {\n    color: #ffffff;\n    opacity: 0.5; }\n\n.form-control-rounded {\n  border-radius: 16px; }\n\n.help-block {\n  color: #ffffff;\n  vertical-align: sub; }\n\n.raiting-box {\n  display: flex; }\n\n.help-block.error-block {\n  display: none; }\n  .has-error .help-block.error-block.basic-block {\n    display: block; }\n\n.input-group-addon-danger {\n  background: #f95372;\n  color: #ffffff;\n  border-color: #f95372; }\n\n.input-group-addon-warning {\n  background: #e7ba08;\n  color: #ffffff;\n  border-color: #e7ba08; }\n\n.input-group-addon-success {\n  background: #8bd22f;\n  color: #ffffff;\n  border-color: #8bd22f; }\n\n.input-group-addon-primary {\n  background: #00abff;\n  color: #ffffff;\n  border-color: #00abff; }\n\n.checkbox-demo-row {\n  margin-bottom: 12px; }\n\n.dropdown-menu {\n  border-radius: 5px; }\n\n.dropdown button.btn.btn-default.dropdown-toggle {\n  color: #ffffff; }\n\n.bootstrap-select.btn-group button.btn.btn-default {\n  background: transparent;\n  color: #ffffff; }\n  .bootstrap-select.btn-group button.btn.btn-default:hover {\n    background: #ffffff;\n    box-shadow: none;\n    outline: 0 !important; }\n  .bootstrap-select.btn-group button.btn.btn-default:active {\n    background: #ffffff;\n    box-shadow: none; }\n\n.bootstrap-select.btn-group.open > .btn.btn-default.dropdown-toggle {\n  background: #ffffff;\n  box-shadow: none;\n  border-color: rgba(255, 255, 255, 0.5); }\n\n.bootstrap-select.btn-group.open > .btn {\n  border-radius: 5px 5px 0 0; }\n\n.bootstrap-select.btn-group.open .dropdown-menu.open {\n  border: 1px solid rgba(255, 255, 255, 0.3);\n  border-top: none;\n  border-radius: 0 0 5px 5px; }\n\n.bootstrap-select.btn-group.with-search.open .btn-default + .dropdown-menu .bs-searchbox .form-control {\n  background-color: #ffffff;\n  border: 1px solid rgba(255, 255, 255, 0.6); }\n\n.bootstrap-select.btn-group.with-search.open .btn-default + .dropdown-menu .no-results {\n  color: #7d7d7d; }\n\n.bootstrap-select.btn-group .notify {\n  color: #7d7d7d; }\n\n.has-success {\n  position: relative; }\n  .has-success .control-label {\n    color: #ffffff; }\n  .has-success .form-control {\n    border: 1px solid #a2db59; }\n    .has-success .form-control:focus {\n      box-shadow: none;\n      border-color: #8bd22f; }\n  .has-success label.custom-checkbox {\n    color: #a2db59; }\n    .has-success label.custom-checkbox > span:before {\n      color: #a2db59; }\n    .has-success label.custom-checkbox > span:hover:before {\n      border-color: #a2db59; }\n  .has-success .form-control-feedback {\n    color: #a2db59; }\n  .has-success .input-group-addon {\n    background-color: #a2db59;\n    color: #ffffff; }\n\n.has-warning {\n  position: relative; }\n  .has-warning .control-label {\n    color: #ffffff; }\n  .has-warning .form-control {\n    border: 1px solid #ecc839; }\n    .has-warning .form-control:focus {\n      box-shadow: none;\n      border-color: #e7ba08; }\n  .has-warning label.custom-checkbox {\n    color: #ecc839; }\n    .has-warning label.custom-checkbox > span:before {\n      color: #ecc839; }\n    .has-warning label.custom-checkbox > span:hover:before {\n      border-color: #ecc839; }\n  .has-warning .form-control-feedback {\n    color: #ecc839; }\n  .has-warning .input-group-addon {\n    background-color: #ecc839;\n    color: #ffffff; }\n\n.has-error {\n  position: relative; }\n  .has-error .control-label {\n    color: #ffffff; }\n  .has-error .form-control {\n    border: 1px solid #fa758e; }\n    .has-error .form-control:focus {\n      box-shadow: none;\n      border-color: #f95372; }\n  .has-error label.custom-checkbox {\n    color: #fa758e; }\n    .has-error label.custom-checkbox > span:before {\n      color: #fa758e; }\n    .has-error label.custom-checkbox > span:hover:before {\n      border-color: #fa758e; }\n  .has-error .form-control-feedback {\n    color: #fa758e; }\n  .has-error .input-group-addon {\n    background-color: #fa758e;\n    color: #ffffff; }\n\n.bootstrap-tagsinput {\n  color: #ffffff;\n  background-color: rgba(255, 255, 255, 0.1);\n  border: 1px solid rgba(255, 255, 255, 0.6);\n  border-radius: 5px;\n  box-shadow: none;\n  max-width: 100%;\n  font-size: 14px;\n  line-height: 26px;\n  width: 100%; }\n  .bootstrap-tagsinput.form-control {\n    display: block;\n    width: 100%; }\n  .bootstrap-tagsinput .tag {\n    border-radius: 3px;\n    font-weight: 400;\n    font-size: 11px;\n    padding: 4px 8px; }\n    .bootstrap-tagsinput .tag [data-role=\"remove\"]:hover {\n      box-shadow: none; }\n  .bootstrap-tagsinput input {\n    background-color: rgba(255, 255, 255, 0.1);\n    border: 1px solid rgba(255, 255, 255, 0.6);\n    border-radius: 5px;\n    line-height: 22px;\n    font-size: 11px;\n    min-width: 53px; }\n    .bootstrap-tagsinput input::-webkit-input-placeholder {\n      color: #ffffff;\n      opacity: 0.8; }\n    .bootstrap-tagsinput input:-moz-placeholder {\n      /* Firefox 18- */\n      color: #ffffff;\n      opacity: 0.8; }\n    .bootstrap-tagsinput input::-moz-placeholder {\n      /* Firefox 19+ */\n      color: #ffffff;\n      opacity: 0.8; }\n    .bootstrap-tagsinput input:-ms-input-placeholder {\n      color: #ffffff;\n      opacity: 0.8; }\n\n.progress {\n  background: rgba(0, 0, 0, 0.15); }\n\n.progress-bar-primary {\n  background-color: #00abff; }\n\n.progress-bar-success {\n  background-color: #aee06d; }\n\n.progress-bar-warning {\n  background-color: #e7ba08; }\n\n.progress-bar-danger {\n  background-color: #f95372; }\n\n.has-success .input-group-addon {\n  border: none; }\n\n.input-group > span.addon-left {\n  border-top-left-radius: 5px;\n  border-bottom-left-radius: 5px; }\n\n.input-group > span.addon-right {\n  border-top-right-radius: 5px;\n  border-bottom-right-radius: 5px; }\n\n.input-group-btn:not(:first-child) > .btn, .input-group-btn:not(:first-child) > .btn-group {\n  margin-left: 0; }\n\n.input-group-btn > .btn {\n  line-height: 1.56; }\n\n.with-primary-addon:focus {\n  border-color: #00abff; }\n\n.with-warning-addon:focus {\n  border-color: #e7ba08; }\n\n.with-success-addon:focus {\n  border-color: #8bd22f; }\n\n.with-danger-addon:focus {\n  border-color: #f95372; }\n\n.sub-little-text {\n  font-size: 12px; }\n\n.rating {\n  font-size: 20px; }\n\nrating-inputs span {\n  vertical-align: middle; }\n\nlabel.custom-checkbox {\n  padding-right: 0;\n  padding-left: 0;\n  margin-bottom: 0; }\n  label.custom-checkbox > input {\n    height: 0;\n    z-index: -100 !important;\n    opacity: 0;\n    position: absolute; }\n    label.custom-checkbox > input:checked + span:before {\n      content: \"\\f00c\";\n      font-weight: 300; }\n    label.custom-checkbox > input:disabled + span {\n      color: rgba(255, 255, 255, 0.4);\n      cursor: not-allowed; }\n      label.custom-checkbox > input:disabled + span:before {\n        border-color: rgba(255, 255, 255, 0.4) !important;\n        cursor: not-allowed; }\n  label.custom-checkbox > span {\n    position: relative;\n    display: inline-block;\n    margin: 0;\n    line-height: 16px;\n    font-weight: 300;\n    cursor: pointer;\n    padding-left: 22px;\n    width: 100%; }\n    label.custom-checkbox > span:before {\n      cursor: pointer;\n      font-family: fontAwesome;\n      font-weight: 300;\n      font-size: 12px;\n      color: #ffffff;\n      content: \"\\a0\";\n      background-color: transparent;\n      border: 1px solid rgba(255, 255, 255, 0.5);\n      border-radius: 0;\n      display: inline-block;\n      text-align: center;\n      height: 16px;\n      line-height: 14px;\n      min-width: 16px;\n      margin-right: 6px;\n      position: relative;\n      top: 0;\n      margin-left: -22px;\n      float: left; }\n    label.custom-checkbox > span:hover:before {\n      border-color: #33bcff; }\n\n#tree-view .tree .node-value {\n  color: white; }\n\n#tree-view .tree .folding.node-expanded::before {\n  color: white; }\n\n#tree-view .tree .folding.node-collapsed::before {\n  color: white; }\n\n#tree-view .tree .folding.node-leaf::before {\n  color: white; }\n\n#tree-view .tree .over-drop-target {\n  border: 4px solid ghostwhite; }\n\n#tree-view .tree .node-value .node-selected::after {\n  background-color: white; }\n\n#tree-view .tree .node-value:after {\n  background-color: white; }\n\n.modal-header {\n  border: none;\n  padding: 10px 15px 0 15px; }\n\n.modal-footer {\n  border: none; }\n\nbutton.close {\n  padding-bottom: 2px;\n  color: #000;\n  text-shadow: 0 1px 0 #fff;\n  opacity: .2; }\n  button.close:hover {\n    opacity: .5; }\n\n/* based on angular-toastr css https://github.com/Foxandxss/angular-toastr/blob/cb508fe6801d6b288d3afc525bb40fee1b101650/dist/angular-toastr.css */\n.toast-title {\n  font-weight: bold; }\n\n.toast-message {\n  word-wrap: break-word; }\n\n.toast-message a,\n.toast-message label {\n  color: #FFFFFF; }\n\n.toast-message a:hover {\n  color: #CCCCCC;\n  text-decoration: none; }\n\n.toast-close-button {\n  position: relative;\n  right: -0.3em;\n  top: -0.3em;\n  float: right;\n  font-size: 20px;\n  font-weight: bold;\n  color: #FFFFFF;\n  -webkit-text-shadow: 0 1px 0 #ffffff;\n  text-shadow: 0 1px 0 #ffffff;\n  opacity: 0.8; }\n\n.toast-close-button:hover,\n.toast-close-button:focus {\n  color: #000000;\n  text-decoration: none;\n  cursor: pointer;\n  opacity: 0.4; }\n\n/*Additional properties for button version\n iOS requires the button element instead of an anchor tag.\n If you want the anchor version, it requires `href=\"#\"`.*/\nbutton.toast-close-button {\n  padding: 0;\n  cursor: pointer;\n  background: transparent;\n  border: 0;\n  -webkit-appearance: none;\n  -moz-appearance: none; }\n\n.toast-center-center {\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translate(-50%, -50%);\n  transform: translate(-50%, -50%); }\n\n.toast-top-center {\n  top: 0;\n  right: 0;\n  width: 100%; }\n\n.toast-bottom-center {\n  bottom: 0;\n  right: 0;\n  width: 100%; }\n\n.toast-top-full-width {\n  top: 0;\n  right: 0;\n  width: 100%; }\n\n.toast-bottom-full-width {\n  bottom: 0;\n  right: 0;\n  width: 100%; }\n\n.toast-top-left {\n  top: 12px;\n  left: 12px; }\n\n.toast-top-right {\n  top: 12px;\n  right: 12px; }\n\n.toast-bottom-right {\n  right: 12px;\n  bottom: 12px; }\n\n.toast-bottom-left {\n  bottom: 12px;\n  left: 12px; }\n\n#toast-container {\n  pointer-events: none;\n  position: fixed;\n  z-index: 999999;\n  /*overrides*/ }\n\n#toast-container * {\n  -moz-box-sizing: border-box;\n  -webkit-box-sizing: border-box;\n  box-sizing: border-box; }\n\n#toast-container .toast {\n  position: relative;\n  overflow: hidden;\n  margin: 0 0 6px;\n  padding: 15px 15px 15px 50px;\n  width: 300px;\n  -moz-border-radius: 3px 3px 3px 3px;\n  -webkit-border-radius: 3px 3px 3px 3px;\n  border-radius: 3px 3px 3px 3px;\n  background-position: 15px center;\n  background-repeat: no-repeat;\n  -moz-box-shadow: 0 0 12px #999999;\n  -webkit-box-shadow: 0 0 12px #999999;\n  box-shadow: 0 0 12px #999999;\n  color: #FFFFFF;\n  opacity: 0.8; }\n\n#toast-container .toast:hover {\n  -moz-box-shadow: 0 0 12px #000000;\n  -webkit-box-shadow: 0 0 12px #000000;\n  box-shadow: 0 0 12px #000000;\n  opacity: 1;\n  cursor: pointer; }\n\n#toast-container .toast.toast-info {\n  background-image: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAGwSURBVEhLtZa9SgNBEMc9sUxxRcoUKSzSWIhXpFMhhYWFhaBg4yPYiWCXZxBLERsLRS3EQkEfwCKdjWJAwSKCgoKCcudv4O5YLrt7EzgXhiU3/4+b2ckmwVjJSpKkQ6wAi4gwhT+z3wRBcEz0yjSseUTrcRyfsHsXmD0AmbHOC9Ii8VImnuXBPglHpQ5wwSVM7sNnTG7Za4JwDdCjxyAiH3nyA2mtaTJufiDZ5dCaqlItILh1NHatfN5skvjx9Z38m69CgzuXmZgVrPIGE763Jx9qKsRozWYw6xOHdER+nn2KkO+Bb+UV5CBN6WC6QtBgbRVozrahAbmm6HtUsgtPC19tFdxXZYBOfkbmFJ1VaHA1VAHjd0pp70oTZzvR+EVrx2Ygfdsq6eu55BHYR8hlcki+n+kERUFG8BrA0BwjeAv2M8WLQBtcy+SD6fNsmnB3AlBLrgTtVW1c2QN4bVWLATaIS60J2Du5y1TiJgjSBvFVZgTmwCU+dAZFoPxGEEs8nyHC9Bwe2GvEJv2WXZb0vjdyFT4Cxk3e/kIqlOGoVLwwPevpYHT+00T+hWwXDf4AJAOUqWcDhbwAAAAASUVORK5CYII=\") !important; }\n\n#toast-container .toast.toast-error {\n  background-image: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAHOSURBVEhLrZa/SgNBEMZzh0WKCClSCKaIYOED+AAKeQQLG8HWztLCImBrYadgIdY+gIKNYkBFSwu7CAoqCgkkoGBI/E28PdbLZmeDLgzZzcx83/zZ2SSXC1j9fr+I1Hq93g2yxH4iwM1vkoBWAdxCmpzTxfkN2RcyZNaHFIkSo10+8kgxkXIURV5HGxTmFuc75B2RfQkpxHG8aAgaAFa0tAHqYFfQ7Iwe2yhODk8+J4C7yAoRTWI3w/4klGRgR4lO7Rpn9+gvMyWp+uxFh8+H+ARlgN1nJuJuQAYvNkEnwGFck18Er4q3egEc/oO+mhLdKgRyhdNFiacC0rlOCbhNVz4H9FnAYgDBvU3QIioZlJFLJtsoHYRDfiZoUyIxqCtRpVlANq0EU4dApjrtgezPFad5S19Wgjkc0hNVnuF4HjVA6C7QrSIbylB+oZe3aHgBsqlNqKYH48jXyJKMuAbiyVJ8KzaB3eRc0pg9VwQ4niFryI68qiOi3AbjwdsfnAtk0bCjTLJKr6mrD9g8iq/S/B81hguOMlQTnVyG40wAcjnmgsCNESDrjme7wfftP4P7SP4N3CJZdvzoNyGq2c/HWOXJGsvVg+RA/k2MC/wN6I2YA2Pt8GkAAAAASUVORK5CYII=\") !important; }\n\n#toast-container .toast.toast-success {\n  background-image: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADsSURBVEhLY2AYBfQMgf///3P8+/evAIgvA/FsIF+BavYDDWMBGroaSMMBiE8VC7AZDrIFaMFnii3AZTjUgsUUWUDA8OdAH6iQbQEhw4HyGsPEcKBXBIC4ARhex4G4BsjmweU1soIFaGg/WtoFZRIZdEvIMhxkCCjXIVsATV6gFGACs4Rsw0EGgIIH3QJYJgHSARQZDrWAB+jawzgs+Q2UO49D7jnRSRGoEFRILcdmEMWGI0cm0JJ2QpYA1RDvcmzJEWhABhD/pqrL0S0CWuABKgnRki9lLseS7g2AlqwHWQSKH4oKLrILpRGhEQCw2LiRUIa4lwAAAABJRU5ErkJggg==\") !important; }\n\n#toast-container .toast.toast-warning {\n  background-image: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAGYSURBVEhL5ZSvTsNQFMbXZGICMYGYmJhAQIJAICYQPAACiSDB8AiICQQJT4CqQEwgJvYASAQCiZiYmJhAIBATCARJy+9rTsldd8sKu1M0+dLb057v6/lbq/2rK0mS/TRNj9cWNAKPYIJII7gIxCcQ51cvqID+GIEX8ASG4B1bK5gIZFeQfoJdEXOfgX4QAQg7kH2A65yQ87lyxb27sggkAzAuFhbbg1K2kgCkB1bVwyIR9m2L7PRPIhDUIXgGtyKw575yz3lTNs6X4JXnjV+LKM/m3MydnTbtOKIjtz6VhCBq4vSm3ncdrD2lk0VgUXSVKjVDJXJzijW1RQdsU7F77He8u68koNZTz8Oz5yGa6J3H3lZ0xYgXBK2QymlWWA+RWnYhskLBv2vmE+hBMCtbA7KX5drWyRT/2JsqZ2IvfB9Y4bWDNMFbJRFmC9E74SoS0CqulwjkC0+5bpcV1CZ8NMej4pjy0U+doDQsGyo1hzVJttIjhQ7GnBtRFN1UarUlH8F3xict+HY07rEzoUGPlWcjRFRr4/gChZgc3ZL2d8oAAAAASUVORK5CYII=\") !important; }\n\n#toast-container.toast-top-center .toast,\n#toast-container.toast-bottom-center .toast {\n  width: 300px;\n  margin-left: auto;\n  margin-right: auto; }\n\n#toast-container.toast-top-full-width .toast,\n#toast-container.toast-bottom-full-width .toast {\n  width: 96%;\n  margin-left: auto;\n  margin-right: auto; }\n\n.toast {\n  background-color: #030303;\n  pointer-events: auto; }\n\n.toast-success {\n  background-color: #51A351; }\n\n.toast-error {\n  background-color: #BD362F; }\n\n.toast-info {\n  background-color: #2F96B4; }\n\n.toast-warning {\n  background-color: #F89406; }\n\n.toast-progress {\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  height: 4px;\n  background-color: #000000;\n  opacity: 0.4; }\n\n/*Responsive Design*/\n@media all and (max-width: 240px) {\n  #toast-container .toast.div {\n    padding: 8px 8px 8px 50px;\n    width: 11em; }\n  #toast-container .toast-close-button {\n    right: -0.2em;\n    top: -0.2em; } }\n\n@media all and (min-width: 241px) and (max-width: 480px) {\n  #toast-container .toast.div {\n    padding: 8px 8px 8px 50px;\n    width: 18em; }\n  #toast-container .toast-close-button {\n    right: -0.2em;\n    top: -0.2em; } }\n\n@media all and (min-width: 481px) and (max-width: 768px) {\n  #toast-container .toast.div {\n    padding: 15px 15px 15px 50px;\n    width: 25em; } }\n\n.all {\n  width: 50px;\n  height: 50px !important; }\n\n.fa {\n  color: white;\n  font-size: 20px; }\n\n.alt {\n  display: none; }\n\n.fa.fa-angle-down {\n  color: black !important; }\n\n.fa.fa-angle-up {\n  color: black !important;\n  display: inline-block; }\n\n.fa.fa-angle-right {\n  color: black !important; }\n\n.fa.fa-angle-left {\n  color: black !important;\n  margin-top: 20px;\n  display: inline-block; }\n\n.cdk-overlay-container .cdk-overlay-pane {\n  position: absolute;\n  background: #fff; }\n\n.ui-timepicker > div {\n  display: inline-block;\n  margin-left: .5em;\n  min-width: 1.5em; }\n\n.ui-widget, .ui-widget * {\n  box-sizing: border-box; }\n\n.ui-timepicker {\n  text-align: center;\n  padding: .5em 0; }\n\n.ui-widget-header {\n  border: 1px solid #d9d9d9;\n  background: #f6f7f9;\n  color: #1b1d1f;\n  font-weight: normal; }\n\n.ui-widget {\n  font-family: \"Roboto\", \"Trebuchet MS\", Arial, Helvetica, sans-serif;\n  font-size: 1em; }\n\n.ui-timepicker > .ui-separator {\n  margin-left: 0;\n  min-width: .75em; }\n\n.ui-datepicker.ui-widget .ui-datepicker-calendar td {\n  border-bottom: 1px solid rgba(213, 213, 213, 0.5);\n  padding: 0; }\n\n.ui-datepicker.ui-widget .ui-datepicker-calendar {\n  margin: 0; }\n\n.ui-datepicker table {\n  width: 100%;\n  font-size: .9em;\n  border-collapse: collapse;\n  margin: 0 0 .4em; }\n\n.ui-datepicker.ui-widget .ui-datepicker-calendar thead th {\n  background-color: #f6f8fa;\n  padding: 8px; }\n\n.fa {\n  display: inline-block;\n  font: normal normal normal 14px/1 FontAwesome; }\n\n.ui-timepicker > div {\n  display: inline-block;\n  margin-left: .5em;\n  min-width: 1.5em; }\n\n.ui-helper-clearfix {\n  zoom: 1; }\n\n.ui-separator a {\n  visibility: hidden; }\n\n.ui-widget-header {\n  border: 1px solid #d9d9d9;\n  background: #f6f7f9;\n  color: #1b1d1f;\n  font-weight: normal; }\n\n.table.user {\n  border-top: 3px solid grey; }\n"

/***/ }),
/* 497 */
/***/ (function(module, exports) {

module.exports = ".auth-main {\n  display: flex;\n  align-items: center;\n  height: 100%;\n  width: 100%;\n  position: absolute;\n  background-color: lightsteelblue; }\n\n.auth-block {\n  width: 540px;\n  margin: 0 auto;\n  border-radius: 5px;\n  background: black;\n  color: #fff;\n  padding: 32px;\n  height: 385px; }\n  .auth-block h1 {\n    font-weight: 300;\n    margin-bottom: 28px;\n    text-align: center; }\n  .auth-block p {\n    font-size: 16px;\n    margin-top: -161px; }\n  .auth-block a {\n    text-decoration: none;\n    outline: none;\n    transition: all 0.2s ease;\n    color: #00abff; }\n    .auth-block a:hover {\n      color: #0091d9; }\n  .auth-block .control-label {\n    padding-top: 11px;\n    color: #ffffff; }\n  .auth-block .form-group {\n    margin-bottom: 12px; }\n\n.auth-input {\n  width: 300px;\n  margin-bottom: 24px; }\n  .auth-input input {\n    display: block;\n    width: 100%;\n    border: none;\n    font-size: 16px;\n    padding: 4px 10px;\n    outline: none; }\n\na.forgot-pass {\n  display: block;\n  text-align: right;\n  margin-bottom: -20px;\n  float: right;\n  z-index: 2;\n  position: relative; }\n\n.auth-link {\n  display: block;\n  font-size: 16px;\n  text-align: center;\n  margin-bottom: 33px; }\n\n.auth-sep {\n  margin-top: 36px;\n  margin-bottom: 24px;\n  line-height: 20px;\n  font-size: 16px;\n  text-align: center;\n  display: block;\n  position: relative; }\n  .auth-sep > span {\n    display: table-cell;\n    width: 30%;\n    white-space: nowrap;\n    padding: 0 24px;\n    color: #ffffff; }\n    .auth-sep > span > span {\n      margin-top: -12px;\n      display: block; }\n  .auth-sep:before, .auth-sep:after {\n    border-top: solid 1px #ffffff;\n    content: \"\";\n    height: 1px;\n    width: 35%;\n    display: table-cell; }\n\n.al-share-auth {\n  text-align: center; }\n  .al-share-auth .al-share {\n    float: none;\n    margin: 0;\n    padding: 0;\n    display: inline-block; }\n    .al-share-auth .al-share li {\n      margin-left: 24px; }\n      .al-share-auth .al-share li:first-child {\n        margin-left: 0; }\n      .al-share-auth .al-share li i {\n        font-size: 24px; }\n\n.btn-auth {\n  color: #ffffff !important; }\n\n.error-message {\n  color: #ff0000;\n  float: left;\n  width: 100%;\n  margin-top: 6px; }\n\n.auth-block .form-group {\n  margin-bottom: 12px;\n  float: left;\n  width: 100%; }\n\n.multilingual-link {\n  cursor: pointer;\n  color: #54FF9F !important; }\n\n.forgotpass {\n  float: right;\n  text-decoration: none;\n  margin-left: 450px;\n  margin-top: 20px;\n  cursor: pointer; }\n"

/***/ }),
/* 498 */
/***/ (function(module, exports) {

module.exports = ".auth-main {\n  display: flex;\n  align-items: center;\n  height: 100%;\n  width: 100%;\n  position: absolute;\n  background-color: lightsteelblue; }\n\n.auth-block {\n  width: 540px;\n  margin: 0 auto;\n  border-radius: 5px;\n  background: black;\n  color: #fff;\n  padding: 32px;\n  height: 385px; }\n  .auth-block h1 {\n    font-weight: 300;\n    margin-bottom: 28px;\n    text-align: center; }\n  .auth-block p {\n    font-size: 16px;\n    margin-top: -161px; }\n  .auth-block a {\n    text-decoration: none;\n    outline: none;\n    transition: all 0.2s ease;\n    color: #00abff; }\n    .auth-block a:hover {\n      color: #0091d9; }\n  .auth-block .control-label {\n    padding-top: 11px;\n    color: #ffffff; }\n  .auth-block .form-group {\n    margin-bottom: 12px; }\n\n.auth-input {\n  width: 300px;\n  margin-bottom: 24px; }\n  .auth-input input {\n    display: block;\n    width: 100%;\n    border: none;\n    font-size: 16px;\n    padding: 4px 10px;\n    outline: none; }\n\na.forgot-pass {\n  display: block;\n  text-align: right;\n  margin-bottom: -20px;\n  float: right;\n  z-index: 2;\n  position: relative; }\n\n.auth-link {\n  display: block;\n  font-size: 16px;\n  text-align: center;\n  margin-bottom: 33px; }\n\n.auth-sep {\n  margin-top: 36px;\n  margin-bottom: 24px;\n  line-height: 20px;\n  font-size: 16px;\n  text-align: center;\n  display: block;\n  position: relative; }\n  .auth-sep > span {\n    display: table-cell;\n    width: 30%;\n    white-space: nowrap;\n    padding: 0 24px;\n    color: #ffffff; }\n    .auth-sep > span > span {\n      margin-top: -12px;\n      display: block; }\n  .auth-sep:before, .auth-sep:after {\n    border-top: solid 1px #ffffff;\n    content: \"\";\n    height: 1px;\n    width: 35%;\n    display: table-cell; }\n\n.al-share-auth {\n  text-align: center; }\n  .al-share-auth .al-share {\n    float: none;\n    margin: 0;\n    padding: 0;\n    display: inline-block; }\n    .al-share-auth .al-share li {\n      margin-left: 24px; }\n      .al-share-auth .al-share li:first-child {\n        margin-left: 0; }\n      .al-share-auth .al-share li i {\n        font-size: 24px; }\n\n.btn-auth {\n  color: #ffffff !important; }\n\n.error-message {\n  color: #ff0000;\n  float: left;\n  width: 100%;\n  margin-top: 6px; }\n\n.auth-block .form-group {\n  margin-bottom: 12px;\n  float: left;\n  width: 100%; }\n\n.multilingual-link {\n  cursor: pointer;\n  color: #54FF9F !important; }\n\n.forgot-password {\n  color: #fff !important;\n  text-decoration: none;\n  cursor: pointer; }\n\n.erm {\n  color: red;\n  margin-left: 20px; }\n\n.remember {\n  color: #fff !important;\n  float: left; }\n\n.check {\n  margin: 0 5px 0 0; }\n\n.forgot {\n  text-align: right;\n  margin-top: -40px;\n  margin-left: 256px; }\n\n.forgot a {\n  text-align: right;\n  font-size: 14px;\n  color: #434345; }\n\n.forgot a:hover {\n  color: #575757; }\n"

/***/ }),
/* 499 */
/***/ (function(module, exports) {

module.exports = ".auth-main {\n  display: flex;\n  align-items: center;\n  height: 100%;\n  width: 100%;\n  position: absolute;\n  background-color: lightsteelblue; }\n\n.auth-block {\n  width: 540px;\n  margin: 0 auto;\n  border-radius: 5px;\n  background: black;\n  color: #fff;\n  padding: 32px;\n  height: 385px; }\n  .auth-block h1 {\n    font-weight: 300;\n    margin-bottom: 28px;\n    text-align: center; }\n  .auth-block p {\n    font-size: 16px;\n    margin-top: -161px; }\n  .auth-block a {\n    text-decoration: none;\n    outline: none;\n    transition: all 0.2s ease;\n    color: #00abff; }\n    .auth-block a:hover {\n      color: #0091d9; }\n  .auth-block .control-label {\n    padding-top: 11px;\n    color: #ffffff; }\n  .auth-block .form-group {\n    margin-bottom: 12px; }\n\n.auth-input {\n  width: 300px;\n  margin-bottom: 24px; }\n  .auth-input input {\n    display: block;\n    width: 100%;\n    border: none;\n    font-size: 16px;\n    padding: 4px 10px;\n    outline: none; }\n\na.forgot-pass {\n  display: block;\n  text-align: right;\n  margin-bottom: -20px;\n  float: right;\n  z-index: 2;\n  position: relative; }\n\n.auth-link {\n  display: block;\n  font-size: 16px;\n  text-align: center;\n  margin-bottom: 33px; }\n\n.auth-sep {\n  margin-top: 36px;\n  margin-bottom: 24px;\n  line-height: 20px;\n  font-size: 16px;\n  text-align: center;\n  display: block;\n  position: relative; }\n  .auth-sep > span {\n    display: table-cell;\n    width: 30%;\n    white-space: nowrap;\n    padding: 0 24px;\n    color: #ffffff; }\n    .auth-sep > span > span {\n      margin-top: -12px;\n      display: block; }\n  .auth-sep:before, .auth-sep:after {\n    border-top: solid 1px #ffffff;\n    content: \"\";\n    height: 1px;\n    width: 35%;\n    display: table-cell; }\n\n.al-share-auth {\n  text-align: center; }\n  .al-share-auth .al-share {\n    float: none;\n    margin: 0;\n    padding: 0;\n    display: inline-block; }\n    .al-share-auth .al-share li {\n      margin-left: 24px; }\n      .al-share-auth .al-share li:first-child {\n        margin-left: 0; }\n      .al-share-auth .al-share li i {\n        font-size: 24px; }\n\n.btn-auth {\n  color: #ffffff !important; }\n\n.error-message {\n  color: #ff0000;\n  float: left;\n  width: 100%;\n  margin-top: 6px; }\n\n.auth-block .form-group {\n  margin-bottom: 12px;\n  float: left;\n  width: 100%; }\n"

/***/ }),
/* 500 */
/***/ (function(module, exports) {

module.exports = ".auth-main {\n  display: flex;\n  align-items: center;\n  height: 100%;\n  width: 100%;\n  position: absolute;\n  background-color: lightsteelblue; }\n\n.auth-block {\n  width: 540px;\n  margin: 0 auto;\n  border-radius: 5px;\n  background: black;\n  color: #fff;\n  padding: 32px;\n  height: 385px; }\n  .auth-block h1 {\n    font-weight: 300;\n    margin-bottom: 28px;\n    text-align: center; }\n  .auth-block p {\n    font-size: 16px;\n    margin-top: -161px; }\n  .auth-block a {\n    text-decoration: none;\n    outline: none;\n    transition: all 0.2s ease;\n    color: #00abff; }\n    .auth-block a:hover {\n      color: #0091d9; }\n  .auth-block .control-label {\n    padding-top: 11px;\n    color: #ffffff; }\n  .auth-block .form-group {\n    margin-bottom: 12px; }\n\n.auth-input {\n  width: 300px;\n  margin-bottom: 24px; }\n  .auth-input input {\n    display: block;\n    width: 100%;\n    border: none;\n    font-size: 16px;\n    padding: 4px 10px;\n    outline: none; }\n\na.forgot-pass {\n  display: block;\n  text-align: right;\n  margin-bottom: -20px;\n  float: right;\n  z-index: 2;\n  position: relative; }\n\n.auth-link {\n  display: block;\n  font-size: 16px;\n  text-align: center;\n  margin-bottom: 33px; }\n\n.auth-sep {\n  margin-top: 36px;\n  margin-bottom: 24px;\n  line-height: 20px;\n  font-size: 16px;\n  text-align: center;\n  display: block;\n  position: relative; }\n  .auth-sep > span {\n    display: table-cell;\n    width: 30%;\n    white-space: nowrap;\n    padding: 0 24px;\n    color: #ffffff; }\n    .auth-sep > span > span {\n      margin-top: -12px;\n      display: block; }\n  .auth-sep:before, .auth-sep:after {\n    border-top: solid 1px #ffffff;\n    content: \"\";\n    height: 1px;\n    width: 35%;\n    display: table-cell; }\n\n.al-share-auth {\n  text-align: center; }\n  .al-share-auth .al-share {\n    float: none;\n    margin: 0;\n    padding: 0;\n    display: inline-block; }\n    .al-share-auth .al-share li {\n      margin-left: 24px; }\n      .al-share-auth .al-share li:first-child {\n        margin-left: 0; }\n      .al-share-auth .al-share li i {\n        font-size: 24px; }\n\n.btn-auth {\n  color: #ffffff !important; }\n\n.error-message {\n  color: #ff0000;\n  float: left;\n  width: 100%;\n  margin-top: 6px; }\n\n.auth-block .form-group {\n  margin-bottom: 12px;\n  float: left;\n  width: 100%; }\n\n.multilingual-link {\n  cursor: pointer;\n  color: #54FF9F !important; }\n\n.logo {\n  position: absolute;\n  left: 109px;\n  width: 122px;\n  height: 99px;\n  object-fit: contain; }\n\n.Rectangle-5 {\n  position: absolute;\n  top: 301.5px;\n  left: 160.5px;\n  width: 159.9px;\n  height: 35.5px; }\n\n#txt {\n  position: relative;\n  top: 169px;\n  font-weight: bold; }\n\n.Mask {\n  position: absolute;\n  top: 142.5px;\n  left: 33px;\n  width: 408.2px;\n  height: 43.1px;\n  border-radius: 3.8px;\n  border: solid 1.1px #f0f0f0;\n  color: white;\n  background-color: black; }\n\n.Rectangle-3-Copy-2 {\n  position: absolute;\n  top: 231.5px;\n  left: 36px;\n  width: 405.2px;\n  height: 45.1px;\n  border-radius: 3.8px;\n  border: solid 1.1px #f0f0f0;\n  color: white;\n  background-color: black; }\n\n.min {\n  position: absolute;\n  top: 32.5px;\n  left: 7px;\n  color: red; }\n\n.min2 {\n  position: absolute;\n  top: 38.5px;\n  left: 7px;\n  color: red; }\n\n.notmatch {\n  position: absolute;\n  top: 281.5px;\n  left: 7px;\n  color: red; }\n\n.reset-btn {\n  margin-top: 295px;\n  margin-left: 197px; }\n\n.resetpass {\n  float: right;\n  text-decoration: none;\n  margin-left: 450px;\n  margin-top: -19px;\n  cursor: pointer; }\n\n.Mask:focus {\n  color: white; }\n\n.Rectangle-3-Copy-2:focus {\n  color: white; }\n\n.bbt {\n  margin-top: 299px;\n  margin-left: 115px;\n  width: 163px; }\n"

/***/ }),
/* 501 */
/***/ (function(module, exports) {

module.exports = ".modal-buttons .btn {\n  margin-right: 20px; }\n\n.modal-content {\n  color: #7d7d7d; }\n\n.edit-details {\n  color: black; }\n\n.edit-input {\n  color: black; }\n\n.forgotpass-backbutton {\n  color: #7d7d7d; }\n"

/***/ }),
/* 502 */
/***/ (function(module, exports) {

module.exports = ".ammapAlert {\n  display: table-cell;\n  vertical-align: middle;\n  text-align: center;\n  font-family: verdana,helvetica,arial,sans-serif;\n  font-size: 12px;\n  color: #CC0000; }\n\n.ammapDescriptionWindow {\n  font-size: 11px;\n  font-family: verdana,helvetica,arial,sans-serif;\n  background-color: #FFFFFF;\n  border-style: solid;\n  border-color: #DADADA;\n  border-width: 1px;\n  color: #000000;\n  padding: 8px;\n  -webkit-box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  box-sizing: border-box; }\n\n.ammapDescriptionTitle {\n  font-size: 12px;\n  font-weight: bold;\n  font-family: verdana,helvetica,arial,sans-serif;\n  padding-bottom: 5px; }\n\n.ammapObjectList ul {\n  padding-left: 20px;\n  list-style: square outside;\n  color: #999999;\n  font-family: verdana,helvetica,arial,sans-serif;\n  font-size: 12px; }\n\n.ammapObjectList ul ul {\n  padding-left: 14px; }\n\n.ammapObjectList a {\n  color: #000000; }\n\n.ammapObjectList a {\n  color: #000000;\n  text-decoration: none;\n  display: block;\n  padding: 2px; }\n\n.ammapObjectList a:hover {\n  color: #CC0000;\n  text-decoration: none;\n  background: #FFFFFF;\n  cursor: pointer;\n  display: block; }\n\n.ammapDescriptionText {\n  overflow: auto; }\n"

/***/ }),
/* 503 */
/***/ (function(module, exports) {

module.exports = ".ba-back-top {\n  position: fixed;\n  width: 52px;\n  height: 52px;\n  cursor: pointer;\n  z-index: 9999;\n  display: none;\n  text-decoration: none;\n  right: 40px;\n  bottom: 40px !important;\n  font-size: 45px;\n  text-align: center;\n  opacity: 0.4;\n  color: #00abff;\n  background-color: rgba(0, 0, 0, 0.75);\n  border-radius: 50%;\n  line-height: 46px; }\n  .ba-back-top:hover {\n    opacity: 0.8; }\n"

/***/ }),
/* 504 */
/***/ (function(module, exports) {

module.exports = ".ct-label {\n  fill: rgba(0, 0, 0, 0.4);\n  color: rgba(0, 0, 0, 0.4);\n  font-size: 0.75rem;\n  line-height: 1; }\n\n.ct-chart-line .ct-label,\n.ct-chart-bar .ct-label {\n  display: block;\n  display: -webkit-box;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: -webkit-flex;\n  display: flex; }\n\n.ct-chart-pie .ct-label,\n.ct-chart-donut .ct-label {\n  dominant-baseline: central; }\n\n.ct-label.ct-horizontal.ct-start {\n  -webkit-box-align: flex-end;\n  -webkit-align-items: flex-end;\n  -ms-flex-align: flex-end;\n  align-items: flex-end;\n  -webkit-box-pack: flex-start;\n  -webkit-justify-content: flex-start;\n  -ms-flex-pack: flex-start;\n  justify-content: flex-start;\n  text-align: left;\n  text-anchor: start; }\n\n.ct-label.ct-horizontal.ct-end {\n  -webkit-box-align: flex-start;\n  -webkit-align-items: flex-start;\n  -ms-flex-align: flex-start;\n  align-items: flex-start;\n  -webkit-box-pack: flex-start;\n  -webkit-justify-content: flex-start;\n  -ms-flex-pack: flex-start;\n  justify-content: flex-start;\n  text-align: left;\n  text-anchor: start; }\n\n.ct-label.ct-vertical.ct-start {\n  -webkit-box-align: flex-end;\n  -webkit-align-items: flex-end;\n  -ms-flex-align: flex-end;\n  align-items: flex-end;\n  -webkit-box-pack: flex-end;\n  -webkit-justify-content: flex-end;\n  -ms-flex-pack: flex-end;\n  justify-content: flex-end;\n  text-align: right;\n  text-anchor: end; }\n\n.ct-label.ct-vertical.ct-end {\n  -webkit-box-align: flex-end;\n  -webkit-align-items: flex-end;\n  -ms-flex-align: flex-end;\n  align-items: flex-end;\n  -webkit-box-pack: flex-start;\n  -webkit-justify-content: flex-start;\n  -ms-flex-pack: flex-start;\n  justify-content: flex-start;\n  text-align: left;\n  text-anchor: start; }\n\n.ct-chart-bar .ct-label.ct-horizontal.ct-start {\n  -webkit-box-align: flex-end;\n  -webkit-align-items: flex-end;\n  -ms-flex-align: flex-end;\n  align-items: flex-end;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n  -ms-flex-pack: center;\n  justify-content: center;\n  text-align: center;\n  text-anchor: start; }\n\n.ct-chart-bar .ct-label.ct-horizontal.ct-end {\n  -webkit-box-align: flex-start;\n  -webkit-align-items: flex-start;\n  -ms-flex-align: flex-start;\n  align-items: flex-start;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n  -ms-flex-pack: center;\n  justify-content: center;\n  text-align: center;\n  text-anchor: start; }\n\n.ct-chart-bar.ct-horizontal-bars .ct-label.ct-horizontal.ct-start {\n  -webkit-box-align: flex-end;\n  -webkit-align-items: flex-end;\n  -ms-flex-align: flex-end;\n  align-items: flex-end;\n  -webkit-box-pack: flex-start;\n  -webkit-justify-content: flex-start;\n  -ms-flex-pack: flex-start;\n  justify-content: flex-start;\n  text-align: left;\n  text-anchor: start; }\n\n.ct-chart-bar.ct-horizontal-bars .ct-label.ct-horizontal.ct-end {\n  -webkit-box-align: flex-start;\n  -webkit-align-items: flex-start;\n  -ms-flex-align: flex-start;\n  align-items: flex-start;\n  -webkit-box-pack: flex-start;\n  -webkit-justify-content: flex-start;\n  -ms-flex-pack: flex-start;\n  justify-content: flex-start;\n  text-align: left;\n  text-anchor: start; }\n\n.ct-chart-bar.ct-horizontal-bars .ct-label.ct-vertical.ct-start {\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n  -ms-flex-align: center;\n  align-items: center;\n  -webkit-box-pack: flex-end;\n  -webkit-justify-content: flex-end;\n  -ms-flex-pack: flex-end;\n  justify-content: flex-end;\n  text-align: right;\n  text-anchor: end; }\n\n.ct-chart-bar.ct-horizontal-bars .ct-label.ct-vertical.ct-end {\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n  -ms-flex-align: center;\n  align-items: center;\n  -webkit-box-pack: flex-start;\n  -webkit-justify-content: flex-start;\n  -ms-flex-pack: flex-start;\n  justify-content: flex-start;\n  text-align: left;\n  text-anchor: end; }\n\n.ct-grid {\n  stroke: rgba(0, 0, 0, 0.2);\n  stroke-width: 1px;\n  stroke-dasharray: 2px; }\n\n.ct-grid-background {\n  fill: none; }\n\n.ct-point {\n  stroke-width: 10px;\n  stroke-linecap: round; }\n\n.ct-line {\n  fill: none;\n  stroke-width: 4px; }\n\n.ct-area {\n  stroke: none;\n  fill-opacity: 0.1; }\n\n.ct-bar {\n  fill: none;\n  stroke-width: 10px; }\n\n.ct-slice-donut {\n  fill: none;\n  stroke-width: 60px; }\n\n.ct-series-a .ct-point, .ct-series-a .ct-line, .ct-series-a .ct-bar, .ct-series-a .ct-slice-donut {\n  stroke: #d70206; }\n\n.ct-series-a .ct-slice-pie, .ct-series-a .ct-area {\n  fill: #d70206; }\n\n.ct-series-b .ct-point, .ct-series-b .ct-line, .ct-series-b .ct-bar, .ct-series-b .ct-slice-donut {\n  stroke: #f05b4f; }\n\n.ct-series-b .ct-slice-pie, .ct-series-b .ct-area {\n  fill: #f05b4f; }\n\n.ct-series-c .ct-point, .ct-series-c .ct-line, .ct-series-c .ct-bar, .ct-series-c .ct-slice-donut {\n  stroke: #f4c63d; }\n\n.ct-series-c .ct-slice-pie, .ct-series-c .ct-area {\n  fill: #f4c63d; }\n\n.ct-series-d .ct-point, .ct-series-d .ct-line, .ct-series-d .ct-bar, .ct-series-d .ct-slice-donut {\n  stroke: #d17905; }\n\n.ct-series-d .ct-slice-pie, .ct-series-d .ct-area {\n  fill: #d17905; }\n\n.ct-series-e .ct-point, .ct-series-e .ct-line, .ct-series-e .ct-bar, .ct-series-e .ct-slice-donut {\n  stroke: #453d3f; }\n\n.ct-series-e .ct-slice-pie, .ct-series-e .ct-area {\n  fill: #453d3f; }\n\n.ct-series-f .ct-point, .ct-series-f .ct-line, .ct-series-f .ct-bar, .ct-series-f .ct-slice-donut {\n  stroke: #59922b; }\n\n.ct-series-f .ct-slice-pie, .ct-series-f .ct-area {\n  fill: #59922b; }\n\n.ct-series-g .ct-point, .ct-series-g .ct-line, .ct-series-g .ct-bar, .ct-series-g .ct-slice-donut {\n  stroke: #0544d3; }\n\n.ct-series-g .ct-slice-pie, .ct-series-g .ct-area {\n  fill: #0544d3; }\n\n.ct-series-h .ct-point, .ct-series-h .ct-line, .ct-series-h .ct-bar, .ct-series-h .ct-slice-donut {\n  stroke: #6b0392; }\n\n.ct-series-h .ct-slice-pie, .ct-series-h .ct-area {\n  fill: #6b0392; }\n\n.ct-series-i .ct-point, .ct-series-i .ct-line, .ct-series-i .ct-bar, .ct-series-i .ct-slice-donut {\n  stroke: #f05b4f; }\n\n.ct-series-i .ct-slice-pie, .ct-series-i .ct-area {\n  fill: #f05b4f; }\n\n.ct-series-j .ct-point, .ct-series-j .ct-line, .ct-series-j .ct-bar, .ct-series-j .ct-slice-donut {\n  stroke: #dda458; }\n\n.ct-series-j .ct-slice-pie, .ct-series-j .ct-area {\n  fill: #dda458; }\n\n.ct-series-k .ct-point, .ct-series-k .ct-line, .ct-series-k .ct-bar, .ct-series-k .ct-slice-donut {\n  stroke: #eacf7d; }\n\n.ct-series-k .ct-slice-pie, .ct-series-k .ct-area {\n  fill: #eacf7d; }\n\n.ct-series-l .ct-point, .ct-series-l .ct-line, .ct-series-l .ct-bar, .ct-series-l .ct-slice-donut {\n  stroke: #86797d; }\n\n.ct-series-l .ct-slice-pie, .ct-series-l .ct-area {\n  fill: #86797d; }\n\n.ct-series-m .ct-point, .ct-series-m .ct-line, .ct-series-m .ct-bar, .ct-series-m .ct-slice-donut {\n  stroke: #b2c326; }\n\n.ct-series-m .ct-slice-pie, .ct-series-m .ct-area {\n  fill: #b2c326; }\n\n.ct-series-n .ct-point, .ct-series-n .ct-line, .ct-series-n .ct-bar, .ct-series-n .ct-slice-donut {\n  stroke: #6188e2; }\n\n.ct-series-n .ct-slice-pie, .ct-series-n .ct-area {\n  fill: #6188e2; }\n\n.ct-series-o .ct-point, .ct-series-o .ct-line, .ct-series-o .ct-bar, .ct-series-o .ct-slice-donut {\n  stroke: #a748ca; }\n\n.ct-series-o .ct-slice-pie, .ct-series-o .ct-area {\n  fill: #a748ca; }\n\n.ct-square {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-square:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 100%; }\n\n.ct-square:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-square > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-minor-second {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-minor-second:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 93.75%; }\n\n.ct-minor-second:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-minor-second > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-major-second {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-major-second:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 88.8888888889%; }\n\n.ct-major-second:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-major-second > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-minor-third {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-minor-third:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 83.3333333333%; }\n\n.ct-minor-third:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-minor-third > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-major-third {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-major-third:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 80%; }\n\n.ct-major-third:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-major-third > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-perfect-fourth {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-perfect-fourth:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 75%; }\n\n.ct-perfect-fourth:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-perfect-fourth > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-perfect-fifth {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-perfect-fifth:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 66.6666666667%; }\n\n.ct-perfect-fifth:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-perfect-fifth > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-minor-sixth {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-minor-sixth:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 62.5%; }\n\n.ct-minor-sixth:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-minor-sixth > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-golden-section {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-golden-section:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 61.804697157%; }\n\n.ct-golden-section:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-golden-section > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-major-sixth {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-major-sixth:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 60%; }\n\n.ct-major-sixth:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-major-sixth > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-minor-seventh {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-minor-seventh:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 56.25%; }\n\n.ct-minor-seventh:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-minor-seventh > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-major-seventh {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-major-seventh:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 53.3333333333%; }\n\n.ct-major-seventh:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-major-seventh > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-octave {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-octave:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 50%; }\n\n.ct-octave:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-octave > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-major-tenth {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-major-tenth:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 40%; }\n\n.ct-major-tenth:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-major-tenth > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-major-eleventh {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-major-eleventh:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 37.5%; }\n\n.ct-major-eleventh:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-major-eleventh > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-major-twelfth {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-major-twelfth:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 33.3333333333%; }\n\n.ct-major-twelfth:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-major-twelfth > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-double-octave {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-double-octave:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 25%; }\n\n.ct-double-octave:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-double-octave > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n/*# sourceMappingURL=chartist.css.map */\n"

/***/ }),
/* 505 */
/***/ (function(module, exports) {

module.exports = ".has-success {\n  position: relative; }\n  .has-success .control-label {\n    color: #ffffff; }\n  .has-success .form-control {\n    border: 1px solid #a2db59; }\n    .has-success .form-control:focus {\n      box-shadow: none;\n      border-color: #8bd22f; }\n  .has-success label.custom-checkbox {\n    color: #a2db59; }\n    .has-success label.custom-checkbox > span:before {\n      color: #a2db59; }\n    .has-success label.custom-checkbox > span:hover:before {\n      border-color: #a2db59; }\n  .has-success .form-control-feedback {\n    color: #a2db59; }\n  .has-success .input-group-addon {\n    background-color: #a2db59;\n    color: #ffffff; }\n\n.has-warning {\n  position: relative; }\n  .has-warning .control-label {\n    color: #ffffff; }\n  .has-warning .form-control {\n    border: 1px solid #ecc839; }\n    .has-warning .form-control:focus {\n      box-shadow: none;\n      border-color: #e7ba08; }\n  .has-warning label.custom-checkbox {\n    color: #ecc839; }\n    .has-warning label.custom-checkbox > span:before {\n      color: #ecc839; }\n    .has-warning label.custom-checkbox > span:hover:before {\n      border-color: #ecc839; }\n  .has-warning .form-control-feedback {\n    color: #ecc839; }\n  .has-warning .input-group-addon {\n    background-color: #ecc839;\n    color: #ffffff; }\n\n.has-error {\n  position: relative; }\n  .has-error .control-label {\n    color: #ffffff; }\n  .has-error .form-control {\n    border: 1px solid #fa758e; }\n    .has-error .form-control:focus {\n      box-shadow: none;\n      border-color: #f95372; }\n  .has-error label.custom-checkbox {\n    color: #fa758e; }\n    .has-error label.custom-checkbox > span:before {\n      color: #fa758e; }\n    .has-error label.custom-checkbox > span:hover:before {\n      border-color: #fa758e; }\n  .has-error .form-control-feedback {\n    color: #fa758e; }\n  .has-error .input-group-addon {\n    background-color: #fa758e;\n    color: #ffffff; }\n\nlabel.custom-checkbox {\n  margin-bottom: 12px; }\n  label.custom-checkbox > span {\n    display: block;\n    margin-right: 10px; }\n"

/***/ }),
/* 506 */
/***/ (function(module, exports) {

module.exports = ".content-top {\n  padding-top: 13px;\n  padding-bottom: 27px; }\n\nh1.al-title {\n  color: black;\n  width: auto;\n  margin: 0;\n  padding: 0;\n  font-size: 24px;\n  text-transform: uppercase;\n  opacity: 0.9; }\n\n.al-breadcrumb {\n  background: none;\n  color: #ffffff;\n  padding: 0;\n  margin: 0;\n  float: right;\n  padding-top: 11px; }\n  .al-breadcrumb li {\n    font-size: 18px;\n    font-weight: 400; }\n    .al-breadcrumb li a {\n      color: #4dc4ff; }\n    .al-breadcrumb li.breadcrumb-item.active {\n      color: #ffffff; }\n\n.al-look {\n  float: right;\n  margin-right: 10px;\n  padding-top: 10px; }\n  .al-look > a {\n    font-size: 19px; }\n"

/***/ }),
/* 507 */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),
/* 508 */
/***/ (function(module, exports) {

module.exports = "/*!\n * FullCalendar v3.3.1 Stylesheet\n * Docs & License: https://fullcalendar.io/\n * (c) 2017 Adam Shaw\n */\n.fc {\n  direction: ltr;\n  text-align: left; }\n\n.fc-rtl {\n  text-align: right; }\n\nbody .fc {\n  /* extra precedence to overcome jqui */\n  font-size: 1em; }\n\n/* Colors\n--------------------------------------------------------------------------------------------------*/\n.fc-unthemed th,\n.fc-unthemed td,\n.fc-unthemed thead,\n.fc-unthemed tbody,\n.fc-unthemed .fc-divider,\n.fc-unthemed .fc-row,\n.fc-unthemed .fc-content,\n.fc-unthemed .fc-popover,\n.fc-unthemed .fc-list-view,\n.fc-unthemed .fc-list-heading td {\n  border-color: #ddd; }\n\n.fc-unthemed .fc-popover {\n  background-color: #fff; }\n\n.fc-unthemed .fc-divider,\n.fc-unthemed .fc-popover .fc-header,\n.fc-unthemed .fc-list-heading td {\n  background: #eee; }\n\n.fc-unthemed .fc-popover .fc-header .fc-close {\n  color: #666; }\n\n.fc-unthemed td.fc-today {\n  background: #fcf8e3; }\n\n.fc-highlight {\n  /* when user is selecting cells */\n  background: #bce8f1;\n  opacity: .3; }\n\n.fc-bgevent {\n  /* default look for background events */\n  background: #8fdf82;\n  opacity: .3; }\n\n.fc-nonbusiness {\n  /* default look for non-business-hours areas */\n  /* will inherit .fc-bgevent's styles */\n  background: #d7d7d7; }\n\n.fc-unthemed .fc-disabled-day {\n  background: #d7d7d7;\n  opacity: .3; }\n\n.ui-widget .fc-disabled-day {\n  /* themed */\n  background-image: none; }\n\n/* Icons (inline elements with styled text that mock arrow icons)\n--------------------------------------------------------------------------------------------------*/\n.fc-icon {\n  display: inline-block;\n  height: 1em;\n  line-height: 1em;\n  font-size: 1em;\n  text-align: center;\n  overflow: hidden;\n  font-family: \"Courier New\", Courier, monospace;\n  /* don't allow browser text-selection */\n  -webkit-touch-callout: none;\n  -webkit-user-select: none;\n  -khtml-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none; }\n\n/*\nAcceptable font-family overrides for individual icons:\n\t\"Arial\", sans-serif\n\t\"Times New Roman\", serif\n\nNOTE: use percentage font sizes or else old IE chokes\n*/\n.fc-icon:after {\n  position: relative; }\n\n.fc-icon-left-single-arrow:after {\n  content: \"\\02039\";\n  font-weight: bold;\n  font-size: 200%;\n  top: -7%; }\n\n.fc-icon-right-single-arrow:after {\n  content: \"\\0203A\";\n  font-weight: bold;\n  font-size: 200%;\n  top: -7%; }\n\n.fc-icon-left-double-arrow:after {\n  content: \"\\000AB\";\n  font-size: 160%;\n  top: -7%; }\n\n.fc-icon-right-double-arrow:after {\n  content: \"\\000BB\";\n  font-size: 160%;\n  top: -7%; }\n\n.fc-icon-left-triangle:after {\n  content: \"\\25C4\";\n  font-size: 125%;\n  top: 3%; }\n\n.fc-icon-right-triangle:after {\n  content: \"\\25BA\";\n  font-size: 125%;\n  top: 3%; }\n\n.fc-icon-down-triangle:after {\n  content: \"\\25BC\";\n  font-size: 125%;\n  top: 2%; }\n\n.fc-icon-x:after {\n  content: \"\\000D7\";\n  font-size: 200%;\n  top: 6%; }\n\n/* Buttons (styled <button> tags, normalized to work cross-browser)\n--------------------------------------------------------------------------------------------------*/\n.fc button {\n  /* force height to include the border and padding */\n  -moz-box-sizing: border-box;\n  -webkit-box-sizing: border-box;\n  box-sizing: border-box;\n  /* dimensions */\n  margin: 0;\n  height: 2.1em;\n  padding: 0 .6em;\n  /* text & cursor */\n  font-size: 1em;\n  /* normalize */\n  white-space: nowrap;\n  cursor: pointer; }\n\n/* Firefox has an annoying inner border */\n.fc button::-moz-focus-inner {\n  margin: 0;\n  padding: 0; }\n\n.fc-state-default {\n  /* non-theme */\n  border: 1px solid; }\n\n.fc-state-default.fc-corner-left {\n  /* non-theme */\n  border-top-left-radius: 4px;\n  border-bottom-left-radius: 4px; }\n\n.fc-state-default.fc-corner-right {\n  /* non-theme */\n  border-top-right-radius: 4px;\n  border-bottom-right-radius: 4px; }\n\n/* icons in buttons */\n.fc button .fc-icon {\n  /* non-theme */\n  position: relative;\n  top: -0.05em;\n  /* seems to be a good adjustment across browsers */\n  margin: 0 .2em;\n  vertical-align: middle; }\n\n/*\n  button states\n  borrowed from twitter bootstrap (http://twitter.github.com/bootstrap/)\n*/\n.fc-state-default {\n  background-color: #f5f5f5;\n  background-image: -moz-linear-gradient(top, #ffffff, #e6e6e6);\n  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ffffff), to(#e6e6e6));\n  background-image: -webkit-linear-gradient(top, #ffffff, #e6e6e6);\n  background-image: -o-linear-gradient(top, #ffffff, #e6e6e6);\n  background-image: linear-gradient(to bottom, #ffffff, #e6e6e6);\n  background-repeat: repeat-x;\n  border-color: #e6e6e6 #e6e6e6 #bfbfbf;\n  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);\n  color: #333;\n  text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);\n  box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05); }\n\n.fc-state-hover,\n.fc-state-down,\n.fc-state-active,\n.fc-state-disabled {\n  color: #333333;\n  background-color: #e6e6e6; }\n\n.fc-state-hover {\n  color: #333333;\n  text-decoration: none;\n  background-position: 0 -15px;\n  -webkit-transition: background-position 0.1s linear;\n  -moz-transition: background-position 0.1s linear;\n  -o-transition: background-position 0.1s linear;\n  transition: background-position 0.1s linear; }\n\n.fc-state-down,\n.fc-state-active {\n  background-color: #cccccc;\n  background-image: none;\n  box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px rgba(0, 0, 0, 0.05); }\n\n.fc-state-disabled {\n  cursor: default;\n  background-image: none;\n  opacity: 0.65;\n  box-shadow: none; }\n\n/* Buttons Groups\n--------------------------------------------------------------------------------------------------*/\n.fc-button-group {\n  display: inline-block; }\n\n/*\nevery button that is not first in a button group should scootch over one pixel and cover the\nprevious button's border...\n*/\n.fc .fc-button-group > * {\n  /* extra precedence b/c buttons have margin set to zero */\n  float: left;\n  margin: 0 0 0 -1px; }\n\n.fc .fc-button-group > :first-child {\n  /* same */\n  margin-left: 0; }\n\n/* Popover\n--------------------------------------------------------------------------------------------------*/\n.fc-popover {\n  position: absolute;\n  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.15); }\n\n.fc-popover .fc-header {\n  /* TODO: be more consistent with fc-head/fc-body */\n  padding: 2px 4px; }\n\n.fc-popover .fc-header .fc-title {\n  margin: 0 2px; }\n\n.fc-popover .fc-header .fc-close {\n  cursor: pointer; }\n\n.fc-ltr .fc-popover .fc-header .fc-title,\n.fc-rtl .fc-popover .fc-header .fc-close {\n  float: left; }\n\n.fc-rtl .fc-popover .fc-header .fc-title,\n.fc-ltr .fc-popover .fc-header .fc-close {\n  float: right; }\n\n/* unthemed */\n.fc-unthemed .fc-popover {\n  border-width: 1px;\n  border-style: solid; }\n\n.fc-unthemed .fc-popover .fc-header .fc-close {\n  font-size: .9em;\n  margin-top: 2px; }\n\n/* jqui themed */\n.fc-popover > .ui-widget-header + .ui-widget-content {\n  border-top: 0;\n  /* where they meet, let the header have the border */ }\n\n/* Misc Reusable Components\n--------------------------------------------------------------------------------------------------*/\n.fc-divider {\n  border-style: solid;\n  border-width: 1px; }\n\nhr.fc-divider {\n  height: 0;\n  margin: 0;\n  padding: 0 0 2px;\n  /* height is unreliable across browsers, so use padding */\n  border-width: 1px 0; }\n\n.fc-clear {\n  clear: both; }\n\n.fc-bg,\n.fc-bgevent-skeleton,\n.fc-highlight-skeleton,\n.fc-helper-skeleton {\n  /* these element should always cling to top-left/right corners */\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0; }\n\n.fc-bg {\n  bottom: 0;\n  /* strech bg to bottom edge */ }\n\n.fc-bg table {\n  height: 100%;\n  /* strech bg to bottom edge */ }\n\n/* Tables\n--------------------------------------------------------------------------------------------------*/\n.fc table {\n  width: 100%;\n  box-sizing: border-box;\n  /* fix scrollbar issue in firefox */\n  table-layout: fixed;\n  border-collapse: collapse;\n  border-spacing: 0;\n  font-size: 1em;\n  /* normalize cross-browser */ }\n\n.fc th {\n  text-align: center; }\n\n.fc th,\n.fc td {\n  border-style: solid;\n  border-width: 1px;\n  padding: 0;\n  vertical-align: top; }\n\n.fc td.fc-today {\n  border-style: double;\n  /* overcome neighboring borders */ }\n\n/* Internal Nav Links\n--------------------------------------------------------------------------------------------------*/\na[data-goto] {\n  cursor: pointer; }\n\na[data-goto]:hover {\n  text-decoration: underline; }\n\n/* Fake Table Rows\n--------------------------------------------------------------------------------------------------*/\n.fc .fc-row {\n  /* extra precedence to overcome themes w/ .ui-widget-content forcing a 1px border */\n  /* no visible border by default. but make available if need be (scrollbar width compensation) */\n  border-style: solid;\n  border-width: 0; }\n\n.fc-row table {\n  /* don't put left/right border on anything within a fake row.\n\t   the outer tbody will worry about this */\n  border-left: 0 hidden transparent;\n  border-right: 0 hidden transparent;\n  /* no bottom borders on rows */\n  border-bottom: 0 hidden transparent; }\n\n.fc-row:first-child table {\n  border-top: 0 hidden transparent;\n  /* no top border on first row */ }\n\n/* Day Row (used within the header and the DayGrid)\n--------------------------------------------------------------------------------------------------*/\n.fc-row {\n  position: relative; }\n\n.fc-row .fc-bg {\n  z-index: 1; }\n\n/* highlighting cells & background event skeleton */\n.fc-row .fc-bgevent-skeleton,\n.fc-row .fc-highlight-skeleton {\n  bottom: 0;\n  /* stretch skeleton to bottom of row */ }\n\n.fc-row .fc-bgevent-skeleton table,\n.fc-row .fc-highlight-skeleton table {\n  height: 100%;\n  /* stretch skeleton to bottom of row */ }\n\n.fc-row .fc-highlight-skeleton td,\n.fc-row .fc-bgevent-skeleton td {\n  border-color: transparent; }\n\n.fc-row .fc-bgevent-skeleton {\n  z-index: 2; }\n\n.fc-row .fc-highlight-skeleton {\n  z-index: 3; }\n\n/*\nrow content (which contains day/week numbers and events) as well as \"helper\" (which contains\ntemporary rendered events).\n*/\n.fc-row .fc-content-skeleton {\n  position: relative;\n  z-index: 4;\n  padding-bottom: 2px;\n  /* matches the space above the events */ }\n\n.fc-row .fc-helper-skeleton {\n  z-index: 5; }\n\n.fc-row .fc-content-skeleton td,\n.fc-row .fc-helper-skeleton td {\n  /* see-through to the background below */\n  background: none;\n  /* in case <td>s are globally styled */\n  border-color: transparent;\n  /* don't put a border between events and/or the day number */\n  border-bottom: 0; }\n\n.fc-row .fc-content-skeleton tbody td,\n.fc-row .fc-helper-skeleton tbody td {\n  /* don't put a border between event cells */\n  border-top: 0; }\n\n/* Scrolling Container\n--------------------------------------------------------------------------------------------------*/\n.fc-scroller {\n  -webkit-overflow-scrolling: touch; }\n\n/* TODO: move to agenda/basic */\n.fc-scroller > .fc-day-grid,\n.fc-scroller > .fc-time-grid {\n  position: relative;\n  /* re-scope all positions */\n  width: 100%;\n  /* hack to force re-sizing this inner element when scrollbars appear/disappear */ }\n\n/* Global Event Styles\n--------------------------------------------------------------------------------------------------*/\n.fc-event {\n  position: relative;\n  /* for resize handle and other inner positioning */\n  display: block;\n  /* make the <a> tag block */\n  font-size: .85em;\n  line-height: 1.3;\n  border-radius: 3px;\n  border: 1px solid #3a87ad;\n  /* default BORDER color */\n  font-weight: normal;\n  /* undo jqui's ui-widget-header bold */ }\n\n.fc-event,\n.fc-event-dot {\n  background-color: #3a87ad;\n  /* default BACKGROUND color */ }\n\n/* overpower some of bootstrap's and jqui's styles on <a> tags */\n.fc-event,\n.fc-event:hover,\n.ui-widget .fc-event {\n  color: #fff;\n  /* default TEXT color */\n  text-decoration: none;\n  /* if <a> has an href */ }\n\n.fc-event[href],\n.fc-event.fc-draggable {\n  cursor: pointer;\n  /* give events with links and draggable events a hand mouse pointer */ }\n\n.fc-not-allowed,\n.fc-not-allowed .fc-event {\n  /* to override an event's custom cursor */\n  cursor: not-allowed; }\n\n.fc-event .fc-bg {\n  /* the generic .fc-bg already does position */\n  z-index: 1;\n  background: #fff;\n  opacity: .25; }\n\n.fc-event .fc-content {\n  position: relative;\n  z-index: 2; }\n\n/* resizer (cursor AND touch devices) */\n.fc-event .fc-resizer {\n  position: absolute;\n  z-index: 4; }\n\n/* resizer (touch devices) */\n.fc-event .fc-resizer {\n  display: none; }\n\n.fc-event.fc-allow-mouse-resize .fc-resizer,\n.fc-event.fc-selected .fc-resizer {\n  /* only show when hovering or selected (with touch) */\n  display: block; }\n\n/* hit area */\n.fc-event.fc-selected .fc-resizer:before {\n  /* 40x40 touch area */\n  content: \"\";\n  position: absolute;\n  z-index: 9999;\n  /* user of this util can scope within a lower z-index */\n  top: 50%;\n  left: 50%;\n  width: 40px;\n  height: 40px;\n  margin-left: -20px;\n  margin-top: -20px; }\n\n/* Event Selection (only for touch devices)\n--------------------------------------------------------------------------------------------------*/\n.fc-event.fc-selected {\n  z-index: 9999 !important;\n  /* overcomes inline z-index */\n  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2); }\n\n.fc-event.fc-selected.fc-dragging {\n  box-shadow: 0 2px 7px rgba(0, 0, 0, 0.3); }\n\n/* Horizontal Events\n--------------------------------------------------------------------------------------------------*/\n/* bigger touch area when selected */\n.fc-h-event.fc-selected:before {\n  content: \"\";\n  position: absolute;\n  z-index: 3;\n  /* below resizers */\n  top: -10px;\n  bottom: -10px;\n  left: 0;\n  right: 0; }\n\n/* events that are continuing to/from another week. kill rounded corners and butt up against edge */\n.fc-ltr .fc-h-event.fc-not-start,\n.fc-rtl .fc-h-event.fc-not-end {\n  margin-left: 0;\n  border-left-width: 0;\n  padding-left: 1px;\n  /* replace the border with padding */\n  border-top-left-radius: 0;\n  border-bottom-left-radius: 0; }\n\n.fc-ltr .fc-h-event.fc-not-end,\n.fc-rtl .fc-h-event.fc-not-start {\n  margin-right: 0;\n  border-right-width: 0;\n  padding-right: 1px;\n  /* replace the border with padding */\n  border-top-right-radius: 0;\n  border-bottom-right-radius: 0; }\n\n/* resizer (cursor AND touch devices) */\n/* left resizer  */\n.fc-ltr .fc-h-event .fc-start-resizer,\n.fc-rtl .fc-h-event .fc-end-resizer {\n  cursor: w-resize;\n  left: -1px;\n  /* overcome border */ }\n\n/* right resizer */\n.fc-ltr .fc-h-event .fc-end-resizer,\n.fc-rtl .fc-h-event .fc-start-resizer {\n  cursor: e-resize;\n  right: -1px;\n  /* overcome border */ }\n\n/* resizer (mouse devices) */\n.fc-h-event.fc-allow-mouse-resize .fc-resizer {\n  width: 7px;\n  top: -1px;\n  /* overcome top border */\n  bottom: -1px;\n  /* overcome bottom border */ }\n\n/* resizer (touch devices) */\n.fc-h-event.fc-selected .fc-resizer {\n  /* 8x8 little dot */\n  border-radius: 4px;\n  border-width: 1px;\n  width: 6px;\n  height: 6px;\n  border-style: solid;\n  border-color: inherit;\n  background: #fff;\n  /* vertically center */\n  top: 50%;\n  margin-top: -4px; }\n\n/* left resizer  */\n.fc-ltr .fc-h-event.fc-selected .fc-start-resizer,\n.fc-rtl .fc-h-event.fc-selected .fc-end-resizer {\n  margin-left: -4px;\n  /* centers the 8x8 dot on the left edge */ }\n\n/* right resizer */\n.fc-ltr .fc-h-event.fc-selected .fc-end-resizer,\n.fc-rtl .fc-h-event.fc-selected .fc-start-resizer {\n  margin-right: -4px;\n  /* centers the 8x8 dot on the right edge */ }\n\n/* DayGrid events\n----------------------------------------------------------------------------------------------------\nWe use the full \"fc-day-grid-event\" class instead of using descendants because the event won't\nbe a descendant of the grid when it is being dragged.\n*/\n.fc-day-grid-event {\n  margin: 1px 2px 0;\n  /* spacing between events and edges */\n  padding: 0 1px; }\n\ntr:first-child > td > .fc-day-grid-event {\n  margin-top: 2px;\n  /* a little bit more space before the first event */ }\n\n.fc-day-grid-event.fc-selected:after {\n  content: \"\";\n  position: absolute;\n  z-index: 1;\n  /* same z-index as fc-bg, behind text */\n  /* overcome the borders */\n  top: -1px;\n  right: -1px;\n  bottom: -1px;\n  left: -1px;\n  /* darkening effect */\n  background: #000;\n  opacity: .25; }\n\n.fc-day-grid-event .fc-content {\n  /* force events to be one-line tall */\n  white-space: nowrap;\n  overflow: hidden; }\n\n.fc-day-grid-event .fc-time {\n  font-weight: bold; }\n\n/* resizer (cursor devices) */\n/* left resizer  */\n.fc-ltr .fc-day-grid-event.fc-allow-mouse-resize .fc-start-resizer,\n.fc-rtl .fc-day-grid-event.fc-allow-mouse-resize .fc-end-resizer {\n  margin-left: -2px;\n  /* to the day cell's edge */ }\n\n/* right resizer */\n.fc-ltr .fc-day-grid-event.fc-allow-mouse-resize .fc-end-resizer,\n.fc-rtl .fc-day-grid-event.fc-allow-mouse-resize .fc-start-resizer {\n  margin-right: -2px;\n  /* to the day cell's edge */ }\n\n/* Event Limiting\n--------------------------------------------------------------------------------------------------*/\n/* \"more\" link that represents hidden events */\na.fc-more {\n  margin: 1px 3px;\n  font-size: .85em;\n  cursor: pointer;\n  text-decoration: none; }\n\na.fc-more:hover {\n  text-decoration: underline; }\n\n.fc-limited {\n  /* rows and cells that are hidden because of a \"more\" link */\n  display: none; }\n\n/* popover that appears when \"more\" link is clicked */\n.fc-day-grid .fc-row {\n  z-index: 1;\n  /* make the \"more\" popover one higher than this */ }\n\n.fc-more-popover {\n  z-index: 2;\n  width: 220px; }\n\n.fc-more-popover .fc-event-container {\n  padding: 10px; }\n\n/* Now Indicator\n--------------------------------------------------------------------------------------------------*/\n.fc-now-indicator {\n  position: absolute;\n  border: 0 solid red; }\n\n/* Utilities\n--------------------------------------------------------------------------------------------------*/\n.fc-unselectable {\n  -webkit-user-select: none;\n  -khtml-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n  -webkit-touch-callout: none;\n  -webkit-tap-highlight-color: transparent; }\n\n/* Toolbar\n--------------------------------------------------------------------------------------------------*/\n.fc-toolbar {\n  text-align: center; }\n\n.fc-toolbar.fc-header-toolbar {\n  margin-bottom: 1em; }\n\n.fc-toolbar.fc-footer-toolbar {\n  margin-top: 1em; }\n\n.fc-toolbar .fc-left {\n  float: left; }\n\n.fc-toolbar .fc-right {\n  float: right; }\n\n.fc-toolbar .fc-center {\n  display: inline-block; }\n\n/* the things within each left/right/center section */\n.fc .fc-toolbar > * > * {\n  /* extra precedence to override button border margins */\n  float: left;\n  margin-left: .75em; }\n\n/* the first thing within each left/center/right section */\n.fc .fc-toolbar > * > :first-child {\n  /* extra precedence to override button border margins */\n  margin-left: 0; }\n\n/* title text */\n.fc-toolbar h2 {\n  margin: 0; }\n\n/* button layering (for border precedence) */\n.fc-toolbar button {\n  position: relative; }\n\n.fc-toolbar .fc-state-hover,\n.fc-toolbar .ui-state-hover {\n  z-index: 2; }\n\n.fc-toolbar .fc-state-down {\n  z-index: 3; }\n\n.fc-toolbar .fc-state-active,\n.fc-toolbar .ui-state-active {\n  z-index: 4; }\n\n.fc-toolbar button:focus {\n  z-index: 5; }\n\n/* View Structure\n--------------------------------------------------------------------------------------------------*/\n/* undo twitter bootstrap's box-sizing rules. normalizes positioning techniques */\n/* don't do this for the toolbar because we'll want bootstrap to style those buttons as some pt */\n.fc-view-container *,\n.fc-view-container *:before,\n.fc-view-container *:after {\n  -webkit-box-sizing: content-box;\n  -moz-box-sizing: content-box;\n  box-sizing: content-box; }\n\n.fc-view,\n.fc-view > table {\n  /* so dragged elements can be above the view's main element */\n  position: relative;\n  z-index: 1; }\n\n/* BasicView\n--------------------------------------------------------------------------------------------------*/\n/* day row structure */\n.fc-basicWeek-view .fc-content-skeleton,\n.fc-basicDay-view .fc-content-skeleton {\n  /* there may be week numbers in these views, so no padding-top */\n  padding-bottom: 1em;\n  /* ensure a space at bottom of cell for user selecting/clicking */ }\n\n.fc-basic-view .fc-body .fc-row {\n  min-height: 4em;\n  /* ensure that all rows are at least this tall */ }\n\n/* a \"rigid\" row will take up a constant amount of height because content-skeleton is absolute */\n.fc-row.fc-rigid {\n  overflow: hidden; }\n\n.fc-row.fc-rigid .fc-content-skeleton {\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0; }\n\n/* week and day number styling */\n.fc-day-top.fc-other-month {\n  opacity: 0.3; }\n\n.fc-basic-view .fc-week-number,\n.fc-basic-view .fc-day-number {\n  padding: 2px; }\n\n.fc-basic-view th.fc-week-number,\n.fc-basic-view th.fc-day-number {\n  padding: 0 2px;\n  /* column headers can't have as much v space */ }\n\n.fc-ltr .fc-basic-view .fc-day-top .fc-day-number {\n  float: right; }\n\n.fc-rtl .fc-basic-view .fc-day-top .fc-day-number {\n  float: left; }\n\n.fc-ltr .fc-basic-view .fc-day-top .fc-week-number {\n  float: left;\n  border-radius: 0 0 3px 0; }\n\n.fc-rtl .fc-basic-view .fc-day-top .fc-week-number {\n  float: right;\n  border-radius: 0 0 0 3px; }\n\n.fc-basic-view .fc-day-top .fc-week-number {\n  min-width: 1.5em;\n  text-align: center;\n  background-color: #f2f2f2;\n  color: #808080; }\n\n/* when week/day number have own column */\n.fc-basic-view td.fc-week-number {\n  text-align: center; }\n\n.fc-basic-view td.fc-week-number > * {\n  /* work around the way we do column resizing and ensure a minimum width */\n  display: inline-block;\n  min-width: 1.25em; }\n\n/* AgendaView all-day area\n--------------------------------------------------------------------------------------------------*/\n.fc-agenda-view .fc-day-grid {\n  position: relative;\n  z-index: 2;\n  /* so the \"more..\" popover will be over the time grid */ }\n\n.fc-agenda-view .fc-day-grid .fc-row {\n  min-height: 3em;\n  /* all-day section will never get shorter than this */ }\n\n.fc-agenda-view .fc-day-grid .fc-row .fc-content-skeleton {\n  padding-bottom: 1em;\n  /* give space underneath events for clicking/selecting days */ }\n\n/* TimeGrid axis running down the side (for both the all-day area and the slot area)\n--------------------------------------------------------------------------------------------------*/\n.fc .fc-axis {\n  /* .fc to overcome default cell styles */\n  vertical-align: middle;\n  padding: 0 4px;\n  white-space: nowrap; }\n\n.fc-ltr .fc-axis {\n  text-align: right; }\n\n.fc-rtl .fc-axis {\n  text-align: left; }\n\n.ui-widget td.fc-axis {\n  font-weight: normal;\n  /* overcome jqui theme making it bold */ }\n\n/* TimeGrid Structure\n--------------------------------------------------------------------------------------------------*/\n.fc-time-grid-container,\n.fc-time-grid {\n  /* so slats/bg/content/etc positions get scoped within here */\n  position: relative;\n  z-index: 1; }\n\n.fc-time-grid {\n  min-height: 100%;\n  /* so if height setting is 'auto', .fc-bg stretches to fill height */ }\n\n.fc-time-grid table {\n  /* don't put outer borders on slats/bg/content/etc */\n  border: 0 hidden transparent; }\n\n.fc-time-grid > .fc-bg {\n  z-index: 1; }\n\n.fc-time-grid .fc-slats,\n.fc-time-grid > hr {\n  /* the <hr> AgendaView injects when grid is shorter than scroller */\n  position: relative;\n  z-index: 2; }\n\n.fc-time-grid .fc-content-col {\n  position: relative;\n  /* because now-indicator lives directly inside */ }\n\n.fc-time-grid .fc-content-skeleton {\n  position: absolute;\n  z-index: 3;\n  top: 0;\n  left: 0;\n  right: 0; }\n\n/* divs within a cell within the fc-content-skeleton */\n.fc-time-grid .fc-business-container {\n  position: relative;\n  z-index: 1; }\n\n.fc-time-grid .fc-bgevent-container {\n  position: relative;\n  z-index: 2; }\n\n.fc-time-grid .fc-highlight-container {\n  position: relative;\n  z-index: 3; }\n\n.fc-time-grid .fc-event-container {\n  position: relative;\n  z-index: 4; }\n\n.fc-time-grid .fc-now-indicator-line {\n  z-index: 5; }\n\n.fc-time-grid .fc-helper-container {\n  /* also is fc-event-container */\n  position: relative;\n  z-index: 6; }\n\n/* TimeGrid Slats (lines that run horizontally)\n--------------------------------------------------------------------------------------------------*/\n.fc-time-grid .fc-slats td {\n  height: 1.5em;\n  border-bottom: 0;\n  /* each cell is responsible for its top border */ }\n\n.fc-time-grid .fc-slats .fc-minor td {\n  border-top-style: dotted; }\n\n.fc-time-grid .fc-slats .ui-widget-content {\n  /* for jqui theme */\n  background: none;\n  /* see through to fc-bg */ }\n\n/* TimeGrid Highlighting Slots\n--------------------------------------------------------------------------------------------------*/\n.fc-time-grid .fc-highlight-container {\n  /* a div within a cell within the fc-highlight-skeleton */\n  position: relative;\n  /* scopes the left/right of the fc-highlight to be in the column */ }\n\n.fc-time-grid .fc-highlight {\n  position: absolute;\n  left: 0;\n  right: 0;\n  /* top and bottom will be in by JS */ }\n\n/* TimeGrid Event Containment\n--------------------------------------------------------------------------------------------------*/\n.fc-ltr .fc-time-grid .fc-event-container {\n  /* space on the sides of events for LTR (default) */\n  margin: 0 2.5% 0 2px; }\n\n.fc-rtl .fc-time-grid .fc-event-container {\n  /* space on the sides of events for RTL */\n  margin: 0 2px 0 2.5%; }\n\n.fc-time-grid .fc-event,\n.fc-time-grid .fc-bgevent {\n  position: absolute;\n  z-index: 1;\n  /* scope inner z-index's */ }\n\n.fc-time-grid .fc-bgevent {\n  /* background events always span full width */\n  left: 0;\n  right: 0; }\n\n/* Generic Vertical Event\n--------------------------------------------------------------------------------------------------*/\n.fc-v-event.fc-not-start {\n  /* events that are continuing from another day */\n  /* replace space made by the top border with padding */\n  border-top-width: 0;\n  padding-top: 1px;\n  /* remove top rounded corners */\n  border-top-left-radius: 0;\n  border-top-right-radius: 0; }\n\n.fc-v-event.fc-not-end {\n  /* replace space made by the top border with padding */\n  border-bottom-width: 0;\n  padding-bottom: 1px;\n  /* remove bottom rounded corners */\n  border-bottom-left-radius: 0;\n  border-bottom-right-radius: 0; }\n\n/* TimeGrid Event Styling\n----------------------------------------------------------------------------------------------------\nWe use the full \"fc-time-grid-event\" class instead of using descendants because the event won't\nbe a descendant of the grid when it is being dragged.\n*/\n.fc-time-grid-event {\n  overflow: hidden;\n  /* don't let the bg flow over rounded corners */ }\n\n.fc-time-grid-event.fc-selected {\n  /* need to allow touch resizers to extend outside event's bounding box */\n  /* common fc-selected styles hide the fc-bg, so don't need this anyway */\n  overflow: visible; }\n\n.fc-time-grid-event.fc-selected .fc-bg {\n  display: none;\n  /* hide semi-white background, to appear darker */ }\n\n.fc-time-grid-event .fc-content {\n  overflow: hidden;\n  /* for when .fc-selected */ }\n\n.fc-time-grid-event .fc-time,\n.fc-time-grid-event .fc-title {\n  padding: 0 1px; }\n\n.fc-time-grid-event .fc-time {\n  font-size: .85em;\n  white-space: nowrap; }\n\n/* short mode, where time and title are on the same line */\n.fc-time-grid-event.fc-short .fc-content {\n  /* don't wrap to second line (now that contents will be inline) */\n  white-space: nowrap; }\n\n.fc-time-grid-event.fc-short .fc-time,\n.fc-time-grid-event.fc-short .fc-title {\n  /* put the time and title on the same line */\n  display: inline-block;\n  vertical-align: top; }\n\n.fc-time-grid-event.fc-short .fc-time span {\n  display: none;\n  /* don't display the full time text... */ }\n\n.fc-time-grid-event.fc-short .fc-time:before {\n  content: attr(data-start);\n  /* ...instead, display only the start time */ }\n\n.fc-time-grid-event.fc-short .fc-time:after {\n  content: \"\\000A0-\\000A0\";\n  /* seperate with a dash, wrapped in nbsp's */ }\n\n.fc-time-grid-event.fc-short .fc-title {\n  font-size: .85em;\n  /* make the title text the same size as the time */\n  padding: 0;\n  /* undo padding from above */ }\n\n/* resizer (cursor device) */\n.fc-time-grid-event.fc-allow-mouse-resize .fc-resizer {\n  left: 0;\n  right: 0;\n  bottom: 0;\n  height: 8px;\n  overflow: hidden;\n  line-height: 8px;\n  font-size: 11px;\n  font-family: monospace;\n  text-align: center;\n  cursor: s-resize; }\n\n.fc-time-grid-event.fc-allow-mouse-resize .fc-resizer:after {\n  content: \"=\"; }\n\n/* resizer (touch device) */\n.fc-time-grid-event.fc-selected .fc-resizer {\n  /* 10x10 dot */\n  border-radius: 5px;\n  border-width: 1px;\n  width: 8px;\n  height: 8px;\n  border-style: solid;\n  border-color: inherit;\n  background: #fff;\n  /* horizontally center */\n  left: 50%;\n  margin-left: -5px;\n  /* center on the bottom edge */\n  bottom: -5px; }\n\n/* Now Indicator\n--------------------------------------------------------------------------------------------------*/\n.fc-time-grid .fc-now-indicator-line {\n  border-top-width: 1px;\n  left: 0;\n  right: 0; }\n\n/* arrow on axis */\n.fc-time-grid .fc-now-indicator-arrow {\n  margin-top: -5px;\n  /* vertically center on top coordinate */ }\n\n.fc-ltr .fc-time-grid .fc-now-indicator-arrow {\n  left: 0;\n  /* triangle pointing right... */\n  border-width: 5px 0 5px 6px;\n  border-top-color: transparent;\n  border-bottom-color: transparent; }\n\n.fc-rtl .fc-time-grid .fc-now-indicator-arrow {\n  right: 0;\n  /* triangle pointing left... */\n  border-width: 5px 6px 5px 0;\n  border-top-color: transparent;\n  border-bottom-color: transparent; }\n\n/* List View\n--------------------------------------------------------------------------------------------------*/\n/* possibly reusable */\n.fc-event-dot {\n  display: inline-block;\n  width: 10px;\n  height: 10px;\n  border-radius: 5px; }\n\n/* view wrapper */\n.fc-rtl .fc-list-view {\n  direction: rtl;\n  /* unlike core views, leverage browser RTL */ }\n\n.fc-list-view {\n  border-width: 1px;\n  border-style: solid; }\n\n/* table resets */\n.fc .fc-list-table {\n  table-layout: auto;\n  /* for shrinkwrapping cell content */ }\n\n.fc-list-table td {\n  border-width: 1px 0 0;\n  padding: 8px 14px; }\n\n.fc-list-table tr:first-child td {\n  border-top-width: 0; }\n\n/* day headings with the list */\n.fc-list-heading {\n  border-bottom-width: 1px; }\n\n.fc-list-heading td {\n  font-weight: bold; }\n\n.fc-ltr .fc-list-heading-main {\n  float: left; }\n\n.fc-ltr .fc-list-heading-alt {\n  float: right; }\n\n.fc-rtl .fc-list-heading-main {\n  float: right; }\n\n.fc-rtl .fc-list-heading-alt {\n  float: left; }\n\n/* event list items */\n.fc-list-item.fc-has-url {\n  cursor: pointer;\n  /* whole row will be clickable */ }\n\n.fc-list-item:hover td {\n  background-color: #f5f5f5; }\n\n.fc-list-item-marker,\n.fc-list-item-time {\n  white-space: nowrap;\n  width: 1px; }\n\n/* make the dot closer to the event title */\n.fc-ltr .fc-list-item-marker {\n  padding-right: 0; }\n\n.fc-rtl .fc-list-item-marker {\n  padding-left: 0; }\n\n.fc-list-item-title a {\n  /* every event title cell has an <a> tag */\n  text-decoration: none;\n  color: inherit; }\n\n.fc-list-item-title a[href]:hover {\n  /* hover effect only on titles with hrefs */\n  text-decoration: underline; }\n\n/* message when no events */\n.fc-list-empty-wrap2 {\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0; }\n\n.fc-list-empty-wrap1 {\n  width: 100%;\n  height: 100%;\n  display: table; }\n\n.fc-list-empty {\n  display: table-cell;\n  vertical-align: middle;\n  text-align: center; }\n\n.fc-unthemed .fc-list-empty {\n  /* theme will provide own background */\n  background-color: #eee; }\n"

/***/ }),
/* 509 */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),
/* 510 */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),
/* 511 */
/***/ (function(module, exports) {

module.exports = ".container-content {\n  width: 100%;\n  display: flex; }\n  .container-content ba-checkbox {\n    width: 100%; }\n"

/***/ }),
/* 512 */
/***/ (function(module, exports) {

module.exports = ".page-top {\n  background-color: green;\n  position: fixed;\n  z-index: 904;\n  box-shadow: 2px 0 3px rgba(0, 0, 0, 0.5);\n  height: 66px;\n  width: 100%;\n  min-width: 320px;\n  padding: 0 32px 0 40px; }\n  .page-top .dropdown-toggle::after {\n    display: none; }\n\n.blur .page-top.scrolled {\n  background-color: rgba(0, 0, 0, 0.85); }\n\na.al-logo {\n  color: #ffffff;\n  display: block;\n  font-size: 24px;\n  font-family: \"Roboto\", sans-serif;\n  white-space: nowrap;\n  float: left;\n  outline: none !important;\n  line-height: 60px; }\n  a.al-logo span {\n    color: #fff; }\n\na.al-logo:hover {\n  color: #fff; }\n\n.user-profile {\n  float: right;\n  min-width: 230px;\n  margin-top: 10px; }\n\n.al-user-profile {\n  float: right;\n  margin-right: 12px;\n  transition: all .15s ease-in-out;\n  padding: 0;\n  width: 36px;\n  height: 36px;\n  border: 0;\n  opacity: 1;\n  position: relative; }\n  .al-user-profile ul.profile-dropdown:after {\n    bottom: 100%;\n    right: 0;\n    border: solid transparent;\n    content: \" \";\n    height: 0;\n    width: 0;\n    position: absolute;\n    pointer-events: none;\n    border-color: rgba(255, 255, 255, 0);\n    border-bottom-color: #fff;\n    border-width: 10px;\n    margin-right: 28px; }\n  .al-user-profile a {\n    display: block; }\n  .al-user-profile img {\n    width: 45px;\n    height: 45px;\n    border-radius: 50%; }\n\na.refresh-data {\n  color: #ffffff;\n  font-size: 13px;\n  text-decoration: none;\n  font-weight: 400;\n  float: right;\n  margin-top: 13px;\n  margin-right: 26px; }\n  a.refresh-data:hover {\n    color: #e7ba08 !important; }\n\na.collapse-menu-link {\n  font-size: 31px;\n  cursor: pointer;\n  display: block;\n  text-decoration: none;\n  line-height: 42px;\n  color: #ffffff;\n  padding: 0;\n  float: left;\n  margin: 11px 0 0 25px; }\n  a.collapse-menu-link:hover {\n    text-decoration: none;\n    color: #e7ba08; }\n\n.al-skin-dropdown {\n  float: right;\n  margin-top: 14px;\n  margin-right: 26px; }\n  .al-skin-dropdown .tpl-skin-panel {\n    max-height: 300px;\n    overflow-y: scroll;\n    overflow-x: hidden; }\n\n.icon-palette {\n  display: inline-block;\n  width: 14px;\n  height: 13px;\n  background: url(\"assets/img/theme/palette.png\");\n  background-size: cover; }\n\n.search {\n  text-shadow: none;\n  font-size: 13px;\n  line-height: 25px;\n  transition: all 0.5s ease;\n  white-space: nowrap;\n  overflow: hidden;\n  width: 162px;\n  float: left;\n  margin: 20px 0 0 30px; }\n  .search label {\n    cursor: pointer; }\n  .search i {\n    width: 16px;\n    display: inline-block;\n    cursor: pointer;\n    padding-left: 1px;\n    font-size: 16px;\n    margin-right: 13px; }\n  .search input {\n    color: #ffffff;\n    background: none;\n    border: none;\n    outline: none;\n    width: 120px;\n    padding: 0;\n    margin: 0 0 0 -3px;\n    height: 27px; }\n\n@media screen and (max-width: 660px) {\n  .search {\n    display: none; } }\n\n@media screen and (max-width: 500px) {\n  .page-top {\n    padding: 0 20px; } }\n\n@media (max-width: 435px) {\n  .user-profile {\n    min-width: 136px; }\n  a.refresh-data {\n    margin-right: 10px; }\n  a.collapse-menu-link {\n    margin-left: 10px; }\n  .al-skin-dropdown {\n    display: none; } }\n\n.profile-toggle-link {\n  cursor: pointer; }\n\n.fa.logout {\n  margin-top: 6px;\n  margin-left: 200px;\n  font-size: 30px;\n  cursor: pointer;\n  position: relative;\n  display: inline-block; }\n  .fa.logout:hover .alt {\n    display: block; }\n\n.fa.logout .tooltiptext {\n  visibility: hidden;\n  width: 47px;\n  height: 30px;\n  background-color: black;\n  color: #fff;\n  text-align: center;\n  padding: 3px 0;\n  border-radius: 1px;\n  font-size: 15px;\n  /* Position the tooltip text - see examples below! */\n  position: absolute;\n  z-index: 1; }\n\n.fa.logout:hover .tooltiptext {\n  visibility: visible; }\n\n.alt {\n  display: none; }\n"

/***/ }),
/* 513 */
/***/ (function(module, exports) {

module.exports = ".picture-group {\n  border: 1px dashed #b8b8b8;\n  width: 202px;\n  height: 202px;\n  position: relative;\n  cursor: pointer; }\n  .picture-group .picture-wrapper {\n    width: 200px;\n    height: 200px;\n    overflow: hidden;\n    display: flex;\n    justify-content: center;\n    align-items: center; }\n  .picture-group img {\n    max-width: 100%;\n    max-height: 100%; }\n  .picture-group i {\n    display: none;\n    position: absolute;\n    font-size: 32px;\n    background: #ffffff;\n    cursor: pointer;\n    color: #00abff;\n    top: -11px;\n    right: -11px;\n    height: 26px;\n    border-radius: 50%; }\n    .picture-group i:before {\n      line-height: 26px; }\n    .picture-group i:hover {\n      color: #f95372; }\n  .picture-group a.change-picture {\n    display: none;\n    width: 202px;\n    background: rgba(0, 0, 0, 0.7);\n    transition: all 200ms ease-in-out;\n    color: #ffffff;\n    text-decoration: none;\n    position: absolute;\n    bottom: -1px;\n    left: -1px;\n    line-height: 32px;\n    text-align: center; }\n  .picture-group:hover i {\n    display: block; }\n  .picture-group:hover .change-picture {\n    display: block; }\n  .picture-group .loading {\n    width: 100%;\n    height: 100%;\n    left: 0;\n    display: flex;\n    position: absolute;\n    justify-content: center;\n    align-items: center; }\n  .picture-group .spinner {\n    width: 60px;\n    height: 60px;\n    position: relative; }\n  .picture-group .double-bounce1, .picture-group .double-bounce2 {\n    width: 100%;\n    height: 100%;\n    border-radius: 50%;\n    background-color: #fff;\n    opacity: 0.6;\n    position: absolute;\n    top: 0;\n    left: 0;\n    -webkit-animation: sk-bounce 2.0s infinite ease-in-out;\n    animation: sk-bounce 2.0s infinite ease-in-out; }\n  .picture-group .double-bounce2 {\n    -webkit-animation-delay: -1.0s;\n    animation-delay: -1.0s; }\n\n@-webkit-keyframes sk-bounce {\n  0%, 100% {\n    -webkit-transform: scale(0); }\n  50% {\n    -webkit-transform: scale(1); } }\n\n@keyframes sk-bounce {\n  0%, 100% {\n    transform: scale(0);\n    -webkit-transform: scale(0); }\n  50% {\n    transform: scale(1);\n    -webkit-transform: scale(1); } }\n"

/***/ }),
/* 514 */
/***/ (function(module, exports) {

module.exports = ".al-sidebar {\n  width: 180px;\n  top: 66px;\n  left: 0;\n  z-index: 1001;\n  display: block;\n  min-height: 100%;\n  background-color: green !important;\n  height: 100%;\n  position: fixed; }\n\n.al-sidebar-list {\n  margin: 0;\n  overflow: hidden;\n  padding: 18px 0 0 0;\n  list-style: none; }\n\n.al-sidebar-sublist .subitem-submenu-list {\n  padding-left: 15px; }\n\n.subitem-submenu-link .fa {\n  top: 7px; }\n\n.al-sidebar-list-item {\n  display: block;\n  position: relative;\n  float: none;\n  padding: 0; }\n  .al-sidebar-list-item.selected:not(.with-sub-menu) {\n    background-color: #00abff; }\n    .al-sidebar-list-item.selected:not(.with-sub-menu) a.al-sidebar-list-link {\n      color: #ffffff; }\n      .al-sidebar-list-item.selected:not(.with-sub-menu) a.al-sidebar-list-link b {\n        color: #ffffff; }\n\n.ba-sidebar-item-expanded > ul.al-sidebar-sublist {\n  display: block !important; }\n\n.al-sidebar-list-item.ba-sidebar-item-expanded > .al-sidebar-list-link b, .ba-sidebar-sublist-item.ba-sidebar-item-expanded > .al-sidebar-list-link b {\n  transform: rotate(180deg); }\n\n.al-sidebar-list-item.ba-sidebar-item-expanded > .al-sidebar-sublist, .ba-sidebar-sublist-item.ba-sidebar-item-expanded > .al-sidebar-sublist {\n  display: block; }\n\na.al-sidebar-list-link {\n  display: block;\n  height: 42px;\n  padding-left: 18px;\n  text-shadow: none;\n  font-size: 13px;\n  text-decoration: none;\n  color: #ffffff;\n  line-height: 42px;\n  white-space: nowrap;\n  overflow: hidden;\n  cursor: pointer; }\n  a.al-sidebar-list-link:hover {\n    color: #00abff; }\n    a.al-sidebar-list-link:hover b {\n      color: #00abff; }\n  a.al-sidebar-list-link i {\n    margin-right: 18px;\n    width: 16px;\n    display: inline-block; }\n  a.al-sidebar-list-link b {\n    display: block;\n    opacity: 1;\n    width: 14px;\n    height: 14px;\n    line-height: 14px;\n    text-shadow: none;\n    font-size: 18px;\n    position: absolute;\n    right: 10px;\n    top: 12px;\n    padding: 0;\n    text-align: center;\n    color: #ffffff;\n    transition: transform 0.2s linear; }\n\n.slimScrollBar, .slimScrollRail {\n  border-radius: 0px !important;\n  width: 4px !important;\n  left: 176px; }\n\n.al-sidebar-sublist {\n  padding: 0;\n  list-style: none;\n  position: relative;\n  display: none; }\n  .al-sidebar-sublist.expanded {\n    display: block; }\n  .al-sidebar-sublist > ba-menu-item > li {\n    display: block;\n    float: none;\n    padding: 0;\n    border-bottom: none;\n    position: relative; }\n    .al-sidebar-sublist > ba-menu-item > li a {\n      display: block;\n      text-shadow: none;\n      font-size: 13px;\n      text-decoration: none;\n      color: #ffffff;\n      padding-left: 52px;\n      height: auto;\n      line-height: 29px; }\n      .al-sidebar-sublist > ba-menu-item > li a:hover {\n        color: #00abff; }\n    .al-sidebar-sublist > ba-menu-item > li.selected:not(.with-sub-menu) > a {\n      border: none;\n      background-color: #00abff; }\n      .al-sidebar-sublist > ba-menu-item > li.selected:not(.with-sub-menu) > a:hover {\n        color: #ffffff; }\n\n.sidebar-hover-elem {\n  width: 4px;\n  background: #00abff;\n  position: absolute;\n  top: -150px;\n  left: 176px;\n  transition: all 0.5s ease;\n  transition-property: top, height;\n  height: 42px;\n  display: block; }\n\n.sidebar-select-elem {\n  display: block;\n  top: 94px; }\n\n.menu-collapsed .slimScrollBar, .menu-collapsed .slimScrollRail {\n  display: none !important; }\n\n@media (min-width: 1200px) {\n  .menu-collapsed .al-main {\n    margin-left: 50px;\n    color: #fff; }\n  .menu-collapsed .al-footer {\n    padding-left: 83px;\n    color: #fff; } }\n\n@media (min-width: 501px) {\n  .menu-collapsed .al-sidebar {\n    width: 52px; }\n    .menu-collapsed .al-sidebar .fa-angle-down, .menu-collapsed .al-sidebar .fa-angle-up {\n      opacity: 0; }\n    .menu-collapsed .al-sidebar .al-sidebar-sublist {\n      position: absolute;\n      top: -1px;\n      left: 52px;\n      background: black;\n      width: 0;\n      display: block;\n      overflow: hidden;\n      transition: width 0.5s ease; }\n      .menu-collapsed .al-sidebar .al-sidebar-sublist.slide-right {\n        width: 135px; }\n      .menu-collapsed .al-sidebar .al-sidebar-sublist:before {\n        display: none; }\n      .menu-collapsed .al-sidebar .al-sidebar-sublist li:before {\n        display: none; }\n      .menu-collapsed .al-sidebar .al-sidebar-sublist li a {\n        padding-left: 18px;\n        padding-right: 18px;\n        min-width: 130px;\n        white-space: nowrap; }\n    .menu-collapsed .al-sidebar .sidebar-hover-elem, .menu-collapsed .al-sidebar .sidebar-select-elem {\n      left: 48px; } }\n\n@media (max-width: 1200px) and (min-width: 500px) {\n  .al-main {\n    margin-left: 50px;\n    color: #fff; }\n  .al-footer {\n    padding-left: 83px;\n    color: #fff; } }\n\n@media (max-width: 1200px) {\n  .al-sidebar {\n    width: 180px;\n    background: black;\n    transition: width 0.5s ease; }\n    .al-sidebar .fa-angle-down, .al-sidebar .fa-angle-up {\n      opacity: 1; }\n    .al-sidebar .al-sidebar-sublist {\n      padding: 0;\n      list-style: none;\n      position: relative;\n      display: none;\n      top: auto;\n      left: auto;\n      background: none;\n      width: auto;\n      overflow: visible;\n      transition: none; }\n      .al-sidebar .al-sidebar-sublist.expanded {\n        display: block; }\n      .al-sidebar .al-sidebar-sublist > ba-menu-item > li {\n        display: block;\n        float: none;\n        padding: 0;\n        border-bottom: none;\n        position: relative; }\n        .al-sidebar .al-sidebar-sublist > ba-menu-item > li a {\n          display: block;\n          text-shadow: none;\n          font-size: 13px;\n          text-decoration: none;\n          color: #ffffff;\n          padding-left: 52px;\n          height: auto;\n          line-height: 29px; }\n          .al-sidebar .al-sidebar-sublist > ba-menu-item > li a:hover {\n            color: #00abff; }\n        .al-sidebar .al-sidebar-sublist > ba-menu-item > li.selected:not(.with-sub-menu) > a {\n          border: none;\n          background-color: #00abff; }\n          .al-sidebar .al-sidebar-sublist > ba-menu-item > li.selected:not(.with-sub-menu) > a:hover {\n            color: #ffffff; }\n    .al-sidebar .sidebar-hover-elem, .al-sidebar .sidebar-select-elem {\n      left: 176px;\n      transition: left 0.5s ease; } }\n\n@media (max-width: 500px) {\n  .menu-collapsed .al-sidebar {\n    width: 0; }\n  .menu-collapsed .sidebar-hover-elem, .menu-collapsed .sidebar-select-elem {\n    display: none; }\n  .al-main {\n    margin-left: 0; }\n  .al-footer {\n    padding-left: 0; } }\n"

/***/ }),
/* 515 */
/***/ (function(module, exports) {

module.exports = "<div class=\"auth-main\">\n   <div class=\"auth-block\">\n       <button type=\"button\" class=\"forgotpass\" routerLink=\"/login\">\n          <i class=\"fa fa-arrow-left fa-2x\" style=\"color:black\" aria-hidden=\"true\"></i>\n        </button>\n      <h1>Forgot Password</h1>\n      \n      <form [formGroup]=\"form\" (ngSubmit)=\"onSubmit(form.value,$event)\" class=\"form-horizontal\">\n      <div class=\"error-message\">{{message}}</div>\n      <div class=\"form-group row\" [ngClass]=\"{'has-error': (!email.valid && email.touched), 'has-success': (email.valid && email.touched)}\">\n         <!--<label for=\"inputEmail3\" class=\"col-sm-2 control-label\">Email</label>-->\n         <div class=\"col-sm-1\"></div>\n          <div class=\"col-sm-10\">\n            <input [formControl]=\"email\" type=\"email\" class=\"form-control\" id=\"inputEmail3\" placeholder=\"Enter email\" [(ngModel)] = \"user.email\" autocomplete=\"off\">\n            <div [hidden]=\"email.valid || (email.untouched && !submitted) || !submitted\" class=\"alert bp-p-default\">\n               \n            </div>\n            <div class=\"error-message\"   *ngIf=\"form.controls.email.touched && !form.controls.email.valid\">Enter a valid email id !!\n         </div>\n          </div>\n         <!-- <div class=\"error-message alert alert-danger\" [@flyInOut]=\"state\"  *ngIf=\"form.controls['email'].errors?.required && !form.controls['email'].pristine\">Email is required !</div>\n            <div class=\"error-message alert alert-danger\" [@flyInOut]=\"state\"  *ngIf=\"form.controls['email'].errors?.invalid && !form.controls['email'].pristine\">Invalid email id !</div> -->\n        <div class=\"col-sm-1\"></div>\n        <br>\n   </div>\n   \n\n<div class=\"form-group row\">\n <div class=\"col-sm-1\"></div>\n          <div class=\"col-sm-10\">\n      <button [disabled]=\"!form.valid\" type=\"submit\" class=\"btn btn-default btn-lg btn-block\" > Submit</button>\n          </div>\n          <!--<a routerLink=\"/reset-password\"> Reset </a>-->\n          <div class=\"col-sm-1\"></div>\n</div>\n</form>\n<!-- <button type=\"submit\" class=\"btn btn-default btn-auth\" (click)=\"openToast()\">Toast click</button>\n -->\n</div>\n</div>\n\n"

/***/ }),
/* 516 */
/***/ (function(module, exports) {

module.exports = "<div class=\"auth-main\">\n   <div class=\"auth-block\">\n      <h1>Sign In</h1>\n      <form [formGroup]=\"form\" (ngSubmit)=\"onSubmit(form.value,$event)\" class=\"form-horizontal\">\n      <div class=\"error-message\">{{message}}</div>\n      <div class=\"form-group row\" [ngClass]=\"{'has-error': (email.touched&&!email.valid), 'has-success': (email.valid &&email.touched)}\">\n         \n         <div class=\"col-sm-12\">\n            <input [formControl]=\"email\"  value={{session}} type=\"email\" class=\"form-control\" id=\"inputEmail3\" placeholder=\"Email address*\" [(ngModel)] = \"user.email\" autocomplete=\"off\">\n            <div [hidden]=\"email.valid || (email.untouched && !submitted) || !submitted\" class=\"alert bp-p-default\">\n            </div>\n            <div class=\"error-message\"   *ngIf=\"form.controls.email.touched && !form.controls.email.valid\">Enter a valid email id !!\n            </div>\n         </div>\n         <div class=\"col-sm-1\"></div>\n         <br>\n      </div>\n      <div class=\"form-group row\" [ngClass]=\"{'has-error': (!password.valid && password.touched), 'has-success': (password.valid && password.touched)}\">\n         \n         <div class=\"col-sm-12\">\n            <input\n            type=\"password\"\n            class=\"form-control\"\n            placeholder=\"Password*\"\n            [(ngModel)] = \"user.password\"\n            [formControl]=\"password\"\n            autocomplete=\"off\">\n            <span class=\"error-message \"   *ngIf=\"form.controls.password.touched && !form.controls.password.valid\">Password is required !!\n            </span>\n         </div>\n         <div class=\"col-sm-1\"></div>\n      </div>\n      <!--<div class=\"form-group row \">\n          <div class=\"col-sm-12\">\n            <div class=\"checkbox\">\n              <label><input type=\"checkbox\" formControlName=\"remember\" [(ngModel)]=\"remember\" class=\"check\"> Remember me</label>\n            </div>\n          </div>\n      </div>-->\n       <div class=\"form-group row\"></div>\n  \n      <div class=\"form-group row\">\n          <div class=\"col-md-6 col-sm-6 col-xs-12\">\n            <button [disabled]=\"!form.valid\" style=\"margin-left:106px;\"type=\"submit\" class=\"btn btn-default btn-lg btn-block\">Sign in</button>\n          </div>\n              <div class=\"form-group row\"></div>\n               <div class=\"form-group row\"></div>\n            <div class=\"form-group row \">\n          <div class=\"col-sm-12\">\n            <div class=\"checkbox\">\n              <label><input type=\"checkbox\" formControlName=\"remember\" [(ngModel)]=\"remember\" class=\"check\"> Remember me</label>\n            </div>\n          </div>\n      </div>\n          <div class=\"col-md-6 col-sm-6 col-xs-12 forgot\">\n              <a routerLink=\"/forgot-password\" class=\"forgot-password\" >Forgot password?</a> \n          </div>\n\n         <!--<div class=\"erm row\">\n            <div *ngIf=\"form.valid\">{{erm}}</div>\n         </div>-->\n      </div>\n      </form>\n   </div>\n</div>"

/***/ }),
/* 517 */
/***/ (function(module, exports) {

module.exports = "<div class=\"auth-main\">\n  <div class=\"auth-block\">\n    <h1>Sign up to ng2-admin</h1>\n    <a routerLink=\"/login\" class=\"auth-link\">Already have an Cl Admin account? Sign in!</a>\n\n    <form [formGroup]=\"form\" (ngSubmit)=\"onSubmit(form.value)\" class=\"form-horizontal\">\n      <div class=\"form-group row\" [ngClass]=\"{'has-error': (!name.valid && name.touched), 'has-success': (name.valid && name.touched)}\">\n        <label for=\"inputName3\" class=\"col-sm-2 control-label\">Name</label>\n\n        <div class=\"col-sm-10\">\n          <input [formControl]=\"name\" type=\"text\" class=\"form-control\" id=\"inputName3\" placeholder=\"Full Name\">\n        </div>\n      </div>\n      <div class=\"form-group row\" [ngClass]=\"{'has-error': (!email.valid && email.touched), 'has-success': (email.valid && email.touched)}\">\n        <label for=\"inputEmail3\" class=\"col-sm-2 control-label\">Email</label>\n\n        <div class=\"col-sm-10\">\n          <input [formControl]=\"email\" type=\"email\" class=\"form-control\" id=\"inputEmail3\" placeholder=\"Email\">\n        </div>\n      </div>\n      <div class=\"form-group row\" [ngClass]=\"{'has-error': (!password.valid && password.touched), 'has-success': (password.valid && password.touched)}\">\n        <label for=\"inputPassword3\" class=\"col-sm-2 control-label\">Password</label>\n\n        <div class=\"col-sm-10\">\n          <input [formControl]=\"password\" type=\"password\" class=\"form-control\" id=\"inputPassword3\" placeholder=\"Password\">\n        </div>\n      </div>\n      <div class=\"form-group row\" [ngClass]=\"{'has-error': (!repeatPassword.valid && repeatPassword.touched), 'has-success': (repeatPassword.valid && repeatPassword.touched)}\">\n        <label for=\"inputPassword4\" class=\"col-sm-2 control-label\">Repeat</label>\n\n        <div class=\"col-sm-10\">\n          <input [formControl]=\"repeatPassword\" type=\"password\" class=\"form-control\" id=\"inputPassword4\" placeholder=\"Repeat\">\n          <span *ngIf=\"!passwords.valid && (password.touched || repeatPassword.touched)\" class=\"help-block sub-little-text\">Passwords don't match.</span>\n        </div>\n      </div>\n      <div class=\"form-group row\">\n        <div class=\"offset-sm-2 col-sm-10\">\n          <button [disabled]=\"!form.valid\" type=\"submit\" class=\"btn btn-default btn-auth\">Sign up</button>\n        </div>\n      </div>\n    </form>\n\n    <div class=\"auth-sep\"><span><span>or Sign up with one click</span></span></div>\n\n    <div class=\"al-share-auth\">\n      <ul class=\"al-share clearfix\">\n        <li><i class=\"socicon socicon-facebook\" title=\"Share on Facebook\"></i></li>\n        <li><i class=\"socicon socicon-twitter\" title=\"Share on Twitter\"></i></li>\n        <li><i class=\"socicon socicon-google\" title=\"Share on Google Plus\"></i></li>\n      </ul>\n    </div>\n  </div>\n</div>\n"

/***/ }),
/* 518 */
/***/ (function(module, exports) {

module.exports = "<!--<div class=\"auth-main\">\n    <div class=\"auth-block\">\n\n        \n        <div class=\"col-sm-10\">\n            <p class=\"Enter-your-email-bel\" id=\"txt\">Enter New Password to Reset .\n            </p>\n        </div>\n\n        <form [formGroup]=\"form\" (ngSubmit)=\"onSubmit(form.value)\" class=\"form-horizontal\">\n            <div class=\"col-sm-10\">\n                <div class=\"form-group row\" [ngClass]=\"{'has-error': (!password.valid && password.touched), 'has-success': (password.valid && password.touched)}\">\n                    <span class=\"min\" *ngIf=\"(!password.valid && password.touched)\">Min 8 characters required</span>\n                    <input [formControl]=\"password\" type=\"password\" class=\"form-control\" placeholder=\"Password\" id=\"inputPassword3\" [(ngModel)]=\"user.password\">\n                </div>\n            </div>\n            <div class=\"col-sm-10\">\n                <div class=\"form-group row\" [ngClass]=\"{'has-error': (!confirmpassword.valid && confirmpassword.touched), 'has-success': (confirmpassword.valid && confirmpassword.touched)}\">\n                    <span class=\"min2\" *ngIf=\"(!confirmpassword.valid && confirmpassword.touched)\">Min 8 characters required</span>\n                    <input [formControl]=\"confirmpassword \" type=\"password\" class=\"form-control Rectangle-3-Copy-2\" placeholder=\"Confirm Password\" id=\"inputPassword4 \" [(ngModel)]=\"user.confirmpassword \">\n                </div>\n            </div>\n            <div class=\"col-sm-10\">\n                <div class=\"form-group row \">\n                    <button [disabled]=\"!form.valid \" type=\"submit \" class=\"reset-btn\">Submit</button></div>\n                <div *ngIf=\"!passwords.valid && (password.touched || confirmpassword.touched) \" class=\"notmatch\">\n                    Passwords does not match.\n                </div>\n            </div>\n        </form>\n\n    </div>\n</div>\n<style>\n    .bp-p-default {\n        padding: 0.75rem 1.25rem;\n    }\n</style>\n-->\n<div class=\"auth-main\">\n    <div class=\"auth-block\">\n         <button type=\"button\" class=\"resetpass\" routerLink=\"/forgot-password\">\n          <i class=\"fa fa-arrow-left fa-2x\" style=\"color:#fff\" aria-hidden=\"true\"></i>\n        </button>\n       \n        <div class=\"col-sm-10\">\n            <p class=\"Enter-your-email-bel\" id=\"txt\">Enter New Password to Reset .\n            </p>\n        </div>\n       \n\n        <form [formGroup]=\"form\" (ngSubmit)=\"onSubmit(form.value)\" class=\"form-horizontal\">\n        \n            <div class=\"col-sm-10\">\n                    <div class=\"form-group row\"></div>\n                      <div class=\"form-group row\"></div>\n                       <div class=\"form-group row\"></div>\n                      <div class=\"form-group row\"></div>\n                <div class=\"form-group row\" [ngClass]=\"{'has-error': (!password.valid && password.touched), 'has-success': (password.valid && password.touched)}\">\n                    <span class=\"min\" *ngIf=\"(!password.valid && password.touched)\">Min 8 characters required</span>\n                    <input [formControl]=\"password\" type=\"password\" class=\"form-control\" placeholder=\"Password\" id=\"inputPassword3\" [(ngModel)]=\"user.password\">\n                </div>\n            </div>\n            <div class=\"form-group row\"></div>\n             <div class=\"form-group row\"></div>\n\n            <div class=\"col-sm-10\">\n                <div class=\"form-group row\" [ngClass]=\"{'has-error': (!confirmpassword.valid && confirmpassword.touched), 'has-success': (confirmpassword.valid && confirmpassword.touched)}\">\n                    <span class=\"min2\" *ngIf=\"(!confirmpassword.valid && confirmpassword.touched)\">Min 8 characters required</span>\n                    <input [formControl]=\"confirmpassword \" type=\"password\" class=\"form-control\" placeholder=\"Confirm Password\" id=\"inputPassword4 \" [(ngModel)]=\"user.confirmpassword \">\n                </div>\n            </div>\n            <br/><br/><br/>\n            <!--<div class=\"col-sm-10\">\n                <div class=\"form-group row \">\n                   \n                    </div>\n                <div *ngIf=\"!passwords.valid && (password.touched || confirmpassword.touched) \" class=\"notmatch\">\n                    Passwords does not match.\n                </div>\n            </div>-->\n            <div class=\"form-group row\"></div>\n            <!--<button type=\"button\" class=\"resetpass\" routerLink=\"/forgot-password\" style=\"margin-top:65px\">\n          <i class=\"fa fa-arrow-left fa-2x\" style=\"color:#fff\" aria-hidden=\"true\"></i>\n        </button>-->\n           <button [disabled]=\"!form.valid\" type=\"submit\" class=\"btn bbt\" > Submit</button>\n        </form>\n\n    </div>\n</div>\n<style>\n    .bp-p-default {\n        padding: 0.75rem 1.25rem;\n    }\n</style>\n\n\n\n"

/***/ }),
/* 519 */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-content\">\n  <div class=\"modal-header\">\n\n    <button class=\"close\" aria-label=\"Close\" (click)=\"closeModal()\" routerLink=\"/login\">\n      \n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n  </div>\n  <div *ngIf=\"!uploadFlag\" class=\"modal-body\">\n Password Reset Link has been sent to your email.\n \n  </div>\n  \n</div>\n"

/***/ }),
/* 520 */
/***/ (function(module, exports) {

module.exports = "<div #baAmChart class=\"ba-am-chart {{baAmChartClass || ''}}\"></div>\n"

/***/ }),
/* 521 */
/***/ (function(module, exports) {

module.exports = "<div baCardBlur class=\"animated fadeIn card {{cardType}} {{baCardClass || ''}}\" zoom-in>\n    <div *ngIf=\"title\" class=\"card-header clearfix\">\n        <h3 class=\"card-title\" translate>{{title}}</h3>\n    </div>\n    <div class=\"card-body\">\n        <ng-content></ng-content>\n    </div>\n</div>\n"

/***/ }),
/* 522 */
/***/ (function(module, exports) {

module.exports = "<div #baChartistChart class=\"ba-chartist-chart {{baChartistChartClass || ''}}\"></div>\n"

/***/ }),
/* 523 */
/***/ (function(module, exports) {

module.exports = "<div class=\"{{baCheckboxClass}}\">\n  <label class=\"checkbox-inline custom-checkbox nowrap\">\n    <input type=\"checkbox\" [checked]=state\n           (change)=\"onChange($event.target.checked)\"\n           [disabled]=\"disabled\" [value]=\"value\">\n    <span>{{label}}</span>\n  </label>\n</div>\n"

/***/ }),
/* 524 */
/***/ (function(module, exports) {

module.exports = "<div class=\"content-top clearfix\" >\n  <h1 class=\"al-title\" translate>{{ activePageTitle }}</h1>\n\n  <!--<ul class=\"breadcrumb al-breadcrumb\">\n    <!<li class=\"breadcrumb-item\">-->\n      <!--<a routerLink=\"/pages/dashboard\" translate>{{'general.home'}}</a>-->\n    <!--</li>-->\n    <!--<li class=\"breadcrumb-item active\" translate>{{ activePageTitle }}</li>-->\n  <!--</ul>-->\n</div>\n"

/***/ }),
/* 525 */
/***/ (function(module, exports) {

module.exports = "<input #fileUpload ngFileSelect type=\"file\" [options]=\"fileUploaderOptions\" (onUpload)=\"_onFileUpload($event)\" (beforeUpload)=\"beforeFileUpload($event)\" hidden=\"true\">\n<div class=\"input-group\" [ngClass]=\"{uploading: uploadFileInProgress}\">\n    <input #inputText type=\"text\" [value]=\"defaultValue\" class=\"form-control\" readonly>\n    <span class=\"input-group-btn\">\n      <button class=\"btn btn-success\" type=\"button\" (click)=\"bringFileSelector();\">Browse</button>\n  </span>\n</div>"

/***/ }),
/* 526 */
/***/ (function(module, exports) {

module.exports = "<div #baFullCalendar class=\"ba-full-calendar {{baFullCalendarClass || ''}}\"></div>\n"

/***/ }),
/* 527 */
/***/ (function(module, exports) {

module.exports = "<ul id=\"al-sidebar-list\" class=\"al-sidebar-list\" baSlimScroll [baSlimScrollOptions]=\"{height: menuHeight}\"\n    (mouseleave)=\"hoverElemTop=outOfArea\">\n  <ba-menu-item\n    [menuItem]=\"item\"\n    (itemHover)=\"hoverItem($event)\"\n    (toggleSubMenu)=\"toggleSubMenu($event)\"\n    *ngFor=\"let item of menuItems\"></ba-menu-item>\n</ul>\n<div class=\"sidebar-hover-elem\" [ngStyle]=\"{top: hoverElemTop + 'px', height: hoverElemHeight + 'px'}\"\n     [ngClass]=\"{'show-hover-elem': showHoverElem }\">\n</div>\n"

/***/ }),
/* 528 */
/***/ (function(module, exports) {

module.exports = "<li *ngIf=\"!menuItem.hidden\" [title]=\"menuItem.title | translate\" [ngClass]=\"{'al-sidebar-list-item': !child, 'ba-sidebar-sublist-item': child, 'selected': menuItem.selected && !menuItem.expanded, 'with-sub-menu': menuItem.children, 'ba-sidebar-item-expanded': menuItem.expanded}\">\n\n  <a *ngIf=\"!menuItem.children && !menuItem.url\" (mouseenter)=\"onHoverItem($event, item)\" [routerLink]=\"menuItem.route.paths\" class=\"al-sidebar-list-link\">\n    <i *ngIf=\"menuItem.icon\" class=\"{{ menuItem.icon }}\"></i><span translate>{{ menuItem.title }}</span>\n  </a>\n\n  <a *ngIf=\"!menuItem.children && menuItem.url\" (mouseenter)=\"onHoverItem($event, item)\" [href]=\"menuItem.url\" [target]=\"menuItem.target\" class=\"al-sidebar-list-link\">\n    <i *ngIf=\"menuItem.icon\" class=\"{{ menuItem.icon }}\"></i><span translate>{{ menuItem.title }}</span>\n  </a>\n\n  <a *ngIf=\"menuItem.children\" (mouseenter)=\"onHoverItem($event, item)\" href (click)=\"onToggleSubMenu($event, menuItem)\" class=\"al-sidebar-list-link\">\n    <i *ngIf=\"menuItem.icon\" class=\"{{ menuItem.icon }}\"></i><span translate>{{ menuItem.title }}</span>\n    <b class=\"fa fa-angle-down\" [ngClass]=\"{'fa-angle-up': menuItem.expanded}\"></b>\n  </a>\n\n  <ul *ngIf=\"menuItem.children\" class=\"al-sidebar-sublist\" [ngClass]=\"{'slide-right': menuItem.slideRight}\">\n    <ba-menu-item [menuItem]=\"subItem\"\n                  [child]=\"true\"\n                  (itemHover)=\"onHoverItem($event)\"\n                  (toggleSubMenu)=\"onToggleSubMenu($event, subItem)\"\n                  *ngFor=\"let subItem of menuItem.children\"></ba-menu-item>\n  </ul>\n\n</li>\n"

/***/ }),
/* 529 */
/***/ (function(module, exports) {

module.exports = "<div class=\"{{baMultiCheckboxClass}} container-content\">\n  <ba-checkbox *ngFor=\"let item of state\"\n               [(ngModel)]=\"item[propertiesMapping.model]\"\n               [baCheckboxClass]=\"getProp(item, 'baCheckboxClass')\"\n               [label]=\"getProp(item, 'label')\"\n               [disabled]=\"getProp(item, 'disabled')\"\n               [value]=\"getProp(item, 'value')\"\n               id=\"{{getProp(item, 'id')}}\">\n  </ba-checkbox>\n</div>\n"

/***/ }),
/* 530 */
/***/ (function(module, exports) {

module.exports = "<div class=\"page-top clearfix\" baScrollPosition maxHeight=\"50\" (scrollChange)=\"scrolledChanged($event)\"\n     [ngClass]=\"{scrolled: isScrolled}\">\n  <a routerLink=\"/pages/dashboard\" class=\"al-logo clearfix\"><span>RentMy Admin</span></a>\n  <a href (click)=\"toggleMenu()\" class=\"collapse-menu-link ion-navicon\"></a>\n\n  \n\n  <div class=\"user-profile clearfix\">\n    <i class=\"fa fa-sign-out logout \" (click) =\"logout()\" aria-hidden=\"true\"><span class=\"tooltiptext\">logout</span></i>\n    <div class=\"dropdown al-user-profile\">\n     \n</div>\n"

/***/ }),
/* 531 */
/***/ (function(module, exports) {

module.exports = "<div class=\"picture-group\" [ngClass]=\"{uploading: uploadInProgress}\">\n  <div class=\"picture-wrapper\" (click)=\"bringFileSelector();\">\n    <img [src]=\"picture\" *ngIf=\"picture\">\n    <img [src]=\"defaultPicture\" *ngIf=\"!!!picture && !!defaultPicture\">\n\n    <div class=\"loading\" *ngIf=\"uploadInProgress\">\n      <div class=\"spinner\">\n        <div class=\"double-bounce1\"></div>\n        <div class=\"double-bounce2\"></div>\n      </div>\n    </div>\n  </div>\n  <i class=\"ion-ios-close-outline\" (click)=\"removePicture();\" *ngIf=\"picture && canDelete\"></i>\n  <a href class=\"change-picture\" (click)=\"bringFileSelector();\">Change profile Picture</a>\n  <input #fileUpload ngFileSelect [options]=\"uploaderOptions\"\n         (onUpload)=\"_onUpload($event)\"\n         (beforeUpload)=\"beforeUpload($event)\"\n         type=\"file\" hidden=\"true\">\n</div>\n"

/***/ }),
/* 532 */
/***/ (function(module, exports) {

module.exports = "<aside class=\"al-sidebar\" sidebarResize>\n  <ba-menu [menuHeight]=\"menuHeight\"\n           [sidebarCollapsed]=\"isMenuCollapsed\"\n           (expandMenu)=\"menuExpand()\"></ba-menu>\n</aside>\n"

/***/ }),
/* 533 */,
/* 534 */,
/* 535 */,
/* 536 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(495);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(10)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../node_modules/raw-loader/index.js!../../../node_modules/sass-loader/lib/loader.js!../../../node_modules/extract-text-webpack-plugin/loader.js??ref--4-0!../../../node_modules/style-loader/index.js!../../../node_modules/css-loader/index.js!../../../node_modules/sass-loader/lib/loader.js?sourceMap!./initial.scss", function() {
			var newContent = require("!!../../../node_modules/raw-loader/index.js!../../../node_modules/sass-loader/lib/loader.js!../../../node_modules/extract-text-webpack-plugin/loader.js??ref--4-0!../../../node_modules/style-loader/index.js!../../../node_modules/css-loader/index.js!../../../node_modules/sass-loader/lib/loader.js?sourceMap!./initial.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 537 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(496);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(10)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../node_modules/raw-loader/index.js!../../node_modules/sass-loader/lib/loader.js!./app.scss", function() {
			var newContent = require("!!../../node_modules/raw-loader/index.js!../../node_modules/sass-loader/lib/loader.js!./app.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 538 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(497);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(10)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../../node_modules/raw-loader/index.js!../../../../../node_modules/sass-loader/lib/loader.js!./forgot-password.scss", function() {
			var newContent = require("!!../../../../../node_modules/raw-loader/index.js!../../../../../node_modules/sass-loader/lib/loader.js!./forgot-password.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 539 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(498);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(10)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../../node_modules/raw-loader/index.js!../../../../../node_modules/sass-loader/lib/loader.js!./login.scss", function() {
			var newContent = require("!!../../../../../node_modules/raw-loader/index.js!../../../../../node_modules/sass-loader/lib/loader.js!./login.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 540 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(499);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(10)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../../node_modules/raw-loader/index.js!../../../../../node_modules/sass-loader/lib/loader.js!./register.scss", function() {
			var newContent = require("!!../../../../../node_modules/raw-loader/index.js!../../../../../node_modules/sass-loader/lib/loader.js!./register.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 541 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(500);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(10)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../../node_modules/raw-loader/index.js!../../../../../node_modules/sass-loader/lib/loader.js!./reset.scss", function() {
			var newContent = require("!!../../../../../node_modules/raw-loader/index.js!../../../../../node_modules/sass-loader/lib/loader.js!./reset.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 542 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(502);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(10)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../../node_modules/raw-loader/index.js!../../../../../node_modules/sass-loader/lib/loader.js!./baAmChart.scss", function() {
			var newContent = require("!!../../../../../node_modules/raw-loader/index.js!../../../../../node_modules/sass-loader/lib/loader.js!./baAmChart.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 543 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(504);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(10)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../../node_modules/raw-loader/index.js!../../../../../node_modules/sass-loader/lib/loader.js!./baChartistChart.scss", function() {
			var newContent = require("!!../../../../../node_modules/raw-loader/index.js!../../../../../node_modules/sass-loader/lib/loader.js!./baChartistChart.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 544 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(508);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(10)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../../node_modules/raw-loader/index.js!../../../../../node_modules/sass-loader/lib/loader.js!./baFullCalendar.scss", function() {
			var newContent = require("!!../../../../../node_modules/raw-loader/index.js!../../../../../node_modules/sass-loader/lib/loader.js!./baFullCalendar.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 545 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(509);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(10)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../../node_modules/raw-loader/index.js!../../../../../node_modules/sass-loader/lib/loader.js!./baMenu.scss", function() {
			var newContent = require("!!../../../../../node_modules/raw-loader/index.js!../../../../../node_modules/sass-loader/lib/loader.js!./baMenu.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 546 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(510);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(10)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../../../../node_modules/raw-loader/index.js!../../../../../../../node_modules/sass-loader/lib/loader.js!./baMenuItem.scss", function() {
			var newContent = require("!!../../../../../../../node_modules/raw-loader/index.js!../../../../../../../node_modules/sass-loader/lib/loader.js!./baMenuItem.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 547 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(512);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(10)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../../node_modules/raw-loader/index.js!../../../../../node_modules/sass-loader/lib/loader.js!./baPageTop.scss", function() {
			var newContent = require("!!../../../../../node_modules/raw-loader/index.js!../../../../../node_modules/sass-loader/lib/loader.js!./baPageTop.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 548 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(514);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(10)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../../node_modules/raw-loader/index.js!../../../../../node_modules/sass-loader/lib/loader.js!./baSidebar.scss", function() {
			var newContent = require("!!../../../../../node_modules/raw-loader/index.js!../../../../../node_modules/sass-loader/lib/loader.js!./baSidebar.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 549 */,
/* 550 */,
/* 551 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(132);

/***/ }),
/* 552 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(134);

/***/ }),
/* 553 */,
/* 554 */,
/* 555 */,
/* 556 */,
/* 557 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(179);

/***/ }),
/* 558 */,
/* 559 */,
/* 560 */,
/* 561 */,
/* 562 */,
/* 563 */,
/* 564 */,
/* 565 */,
/* 566 */,
/* 567 */,
/* 568 */,
/* 569 */,
/* 570 */,
/* 571 */,
/* 572 */,
/* 573 */,
/* 574 */,
/* 575 */,
/* 576 */,
/* 577 */,
/* 578 */,
/* 579 */,
/* 580 */,
/* 581 */,
/* 582 */,
/* 583 */,
/* 584 */,
/* 585 */,
/* 586 */,
/* 587 */,
/* 588 */,
/* 589 */,
/* 590 */,
/* 591 */,
/* 592 */,
/* 593 */,
/* 594 */,
/* 595 */,
/* 596 */,
/* 597 */,
/* 598 */,
/* 599 */,
/* 600 */,
/* 601 */,
/* 602 */,
/* 603 */,
/* 604 */,
/* 605 */,
/* 606 */,
/* 607 */,
/* 608 */,
/* 609 */,
/* 610 */,
/* 611 */,
/* 612 */,
/* 613 */,
/* 614 */,
/* 615 */,
/* 616 */,
/* 617 */,
/* 618 */,
/* 619 */,
/* 620 */,
/* 621 */,
/* 622 */,
/* 623 */,
/* 624 */,
/* 625 */,
/* 626 */,
/* 627 */,
/* 628 */,
/* 629 */,
/* 630 */,
/* 631 */,
/* 632 */,
/* 633 */,
/* 634 */,
/* 635 */,
/* 636 */,
/* 637 */,
/* 638 */,
/* 639 */,
/* 640 */,
/* 641 */,
/* 642 */,
/* 643 */,
/* 644 */,
/* 645 */,
/* 646 */,
/* 647 */,
/* 648 */,
/* 649 */,
/* 650 */,
/* 651 */,
/* 652 */,
/* 653 */,
/* 654 */,
/* 655 */,
/* 656 */,
/* 657 */,
/* 658 */,
/* 659 */,
/* 660 */,
/* 661 */,
/* 662 */,
/* 663 */,
/* 664 */,
/* 665 */,
/* 666 */,
/* 667 */,
/* 668 */,
/* 669 */,
/* 670 */,
/* 671 */,
/* 672 */,
/* 673 */,
/* 674 */,
/* 675 */,
/* 676 */,
/* 677 */,
/* 678 */,
/* 679 */,
/* 680 */,
/* 681 */,
/* 682 */,
/* 683 */,
/* 684 */,
/* 685 */,
/* 686 */,
/* 687 */,
/* 688 */,
/* 689 */,
/* 690 */,
/* 691 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(409);

/***/ }),
/* 692 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(412);

/***/ }),
/* 693 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(417);

/***/ }),
/* 694 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(483);

/***/ }),
/* 695 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(491);

/***/ }),
/* 696 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(500);

/***/ }),
/* 697 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(504);

/***/ }),
/* 698 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(511);

/***/ }),
/* 699 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(512);

/***/ }),
/* 700 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(522);

/***/ }),
/* 701 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(529);

/***/ }),
/* 702 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(559);

/***/ }),
/* 703 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(588);

/***/ }),
/* 704 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(596);

/***/ }),
/* 705 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(598);

/***/ }),
/* 706 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(606);

/***/ }),
/* 707 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(617);

/***/ }),
/* 708 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(643);

/***/ }),
/* 709 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = (__webpack_require__(3))(68);

/***/ })
],[426]);
//# sourceMappingURL=main.bundle.js.map