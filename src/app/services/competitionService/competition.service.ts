import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../environment/environment'
import 'rxjs/add/operator/map';
//import { bookingDetail } from "../../pages/Booking/bookingDetail";
//import { ListResult } from "../../pages/Booking/list-result.interface";




@Injectable()
export class competition_service {
 //getting token for headers
 token=localStorage.getItem('token');
 header_token = 'Bearer '+this.token
 // console.log(header_token)
	constructor(public http: Http) {

      }

  getAllCompetitions(payload){

    console.log("payyyyyy",payload);
    let token=localStorage.getItem('token')
    var header_token = token
    let headers = new Headers({ 'Content-Type': 'application/json'});
    headers.append('Authorization',header_token);
    let options = new RequestOptions({ headers: headers });
    // let skip = (payload.currentPage - 1) * payload.limit;

    let url =  environment.APP.API_URL + environment.APP.GET_ALL_COMPETITIONS;
    
    //console.log(url);

    return this.http.post(url,options)
      .map((res: Response) => {
        console.log(res.json());
        return res.json();
      })
      .catch((error: any) => {
        console.log(error)
        try {
          return (Observable.throw(error.json()));
        } catch (jsonError) {
          var minimumViableError = {
            success: false
          };
          return (Observable.throw(minimumViableError));
        }
      })

  }
//   blockUserById(payload){
    
//       let headers = new Headers({ 'Content-Type': 'application/json', });

//      headers.append('Authorization',this.header_token);
//      let options = new RequestOptions({ headers: headers }); 
//      let url=environment.APP.API_URL+environment.APP.BLOCK_USER_BY_ID;
       
      
//      return this.http.post(url,payload,options)
//       .map((res: Response) => res.json())

//       .catch((error: any) => {
//         console.log(error)
//         try {
//           return (Observable.throw(error.json()));
//         } catch (jsonError) {
//           // If the error couldn't be parsed as JSON data
//           // then it's possible the API is down or something
//           // went wrong with the parsing of the successful
//           // response. In any case, to keep things simple,
//           // we'll just create a minimum representation of
//           // a parsed error.
//           var minimumViableError = {
//             success: false
//           };
//           return (Observable.throw(minimumViableError));
//         }
//       })
//     }

createcompetition(payload){
    let token=localStorage.getItem('token')
     var header_token = 'Bearer '+token
    let url = environment.APP.API_URL+environment.APP.CREATE_COMPETITION;
    let headers = new Headers();
      headers.append('Authorization',header_token);
    let options = new RequestOptions({ headers: headers }); 
   
    return this.http.post(url,payload,options)
      .map((res: Response) => res.json())
      .catch((error: any) => {
        console.log(error)
        try {
          return (Observable.throw(error.json()));
        } catch (jsonError) {
          // If the error couldn't be parsed as JSON data
          // then it's possible the API is down or something
          // went wrong with the parsing of the successful
          // response. In any case, to keep things simple,
          // we'll just create a minimum representation of
          // a parsed error.
          var minimumViableError = {
            success: false
          };
          return (Observable.throw(minimumViableError));
        }
      })
  }

 getAllCompetitionPagination(url){
     let token=localStorage.getItem('token')
     var header_token = 'Bearer '+token
     console.log(header_token)
   let headers = new Headers({ 'Content-Type': 'application/json','content-language':'en' });
     headers.append('Authorization',header_token);
     let options = new RequestOptions({ headers: headers });
    //let url=environment.APP.API_URL+environment.APP.GET_ALL_BOOKING+"?limit="+limit+"&skip="+skip;
    return this.http.get(url,options)
      .map((res: Response) => {
        //console.log(res.json());
        return res.json();
      })

      .catch((error: any) => {
        console.log(error)
        try {
          return (Observable.throw(error.json()));
        } catch (jsonError) {
          // If the error couldn't be parsed as JSON data
          // then it's possible the API is down or something
          // went wrong with the parsing of the successful
          // response. In any case, to keep things simple,
          // we'll just create a minimum representation of
          // a parsed error.
          var minimumViableError = {
            success: false
          };
          return (Observable.throw(minimumViableError));
        }
      })
     // }
     // else{
     //  return this.token;
     // }
 }


//  getBookingById(id){

//    let headers = new Headers({ 'Content-Type': 'application/json', 'content-language':'en'});

//      headers.append('Authorization',this.header_token);
//      let options = new RequestOptions({ headers: headers });
//      let url=environment.APP.API_URL+environment.APP.GET_BOOKING_BY_ID+id;
//     return this.http.get(url,options)
//       .map((res: Response) => res.json())

//       .catch((error: any) => {
//         console.log(error)
//         try {
//           return (Observable.throw(error.json()));
//         } catch (jsonError) {
//           // If the error couldn't be parsed as JSON data
//           // then it's possible the API is down or something
//           // went wrong with the parsing of the successful
//           // response. In any case, to keep things simple,
//           // we'll just create a minimum representation of
//           // a parsed error.
//           var minimumViableError = {
//             success: false
//           };
//           return (Observable.throw(minimumViableError));
//         }
//       })

//  }

//  cancelBooking(data){
//    data={bookingID:data._id};
//    let headers = new Headers({ 'Content-Type': 'application/json','content-language':'en' });
//      headers.append('Authorization',this.header_token);
//      let options = new RequestOptions({ headers: headers });
//      let url=environment.APP.API_URL+environment.APP.CANCEL_BOOKING_BY_ID;
//     return this.http.put(url,data,options)
//       .map((res: Response) => res.json())

//   .catch((error: any) => {
//         console.log(error)
//         try {
//           return (Observable.throw(error.json()));
//         } catch (jsonError) {
//           // If the error couldn't be parsed as JSON data
//           // then it's possible the API is down or something
//           // went wrong with the parsing of the successful
//           // response. In any case, to keep things simple,
//           // we'll just create a minimum representation of
//           // a parsed error.
//           var minimumViableError = {
//             success: false
//           };
//           return (Observable.throw(minimumViableError));
//         }
//       })


//   }
//   blockUserById(payload){
    
//       let headers = new Headers({ 'Content-Type': 'application/json', });

//      headers.append('Authorization',this.header_token);
//      let options = new RequestOptions({ headers: headers }); 
//      let url=environment.APP.API_URL+environment.APP.BLOCK_USER_BY_ID;
     
//      return this.http.put(url,payload,options)
//       .map((res: Response) => res.json())

//       .catch((error: any) => {
//         console.log(error)
//         try {
//           return (Observable.throw(error.json()));
//         } catch (jsonError) {
//           // If the error couldn't be parsed as JSON data
//           // then it's possible the API is down or something
//           // went wrong with the parsing of the successful
//           // response. In any case, to keep things simple,
//           // we'll just create a minimum representation of
//           // a parsed error.
//           var minimumViableError = {
//             success: false
//           };
//           return (Observable.throw(minimumViableError));
//         }
//       })
//     }

}
