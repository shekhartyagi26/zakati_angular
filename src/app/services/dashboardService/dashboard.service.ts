import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response,URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../environment/environment.prod'
import 'rxjs/add/operator/map';


@Injectable()
export class dashboard_service {
  
 //getting token for headers


    constructor(public http: Http) {}

    getDashBoardData(){
    let token=localStorage.getItem('token')
    var header_token = 'Bearer '+token
    let headers = new Headers({ 'Content-Type': 'application/json', });
    headers.append('Authorization',header_token);
    let options = new RequestOptions({ headers: headers });
    let url =  environment.APP.API_URL + environment.APP.GET_DASHBOARD_COUNT

    return this.http.get(url,options)
      .map((res: Response) => {
        //console.log(res.json());
        return res.json();
      })
      .catch((error: any) => {
        console.log(error)
        try {
          return (Observable.throw(error.json()));
        } catch (jsonError) {
          var minimumViableError = {
            success: false
          };
          return (Observable.throw(minimumViableError));
        }
      })
    }
    getUserReportMonthly(payload){
    let token=localStorage.getItem('token')
    var header_token = 'Bearer '+token
    let headers = new Headers({ 'Content-Type': 'application/json', });
    headers.append('Authorization',header_token);
    let options = new RequestOptions({ headers: headers });
    let url =  environment.APP.API_URL + environment.APP.GET_USER_REPORT_MONTHLY+"?month=" + payload.month + "&year=" + payload.year

    return this.http.get(url,options)
      .map((res: Response) => {
        //console.log(res.json());
        return res.json();
      })
      .catch((error: any) => {
        console.log(error)
        try {
          return (Observable.throw(error.json()));
        } catch (jsonError) {
          var minimumViableError = {
            success: false
          };
          return (Observable.throw(minimumViableError));
        }
      })
    }
    
}
