import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../environment/environment'
import 'rxjs/add/operator/map';
//import { bookingDetail } from "../../pages/Booking/bookingDetail";
//import { ListResult } from "../../pages/Booking/list-result.interface";




@Injectable()
export class booking_service {
 //getting token for headers
 token=localStorage.getItem('token');
 header_token =this.token
 // console.log(header_token)
	constructor(public http: Http) {

      }

      additem(payload){
    let token=localStorage.getItem('token')
     var header_token = token
    let url = environment.APP.API_URL+environment.APP.ADD_ITEM;
    let headers = new Headers();
      headers.append('Authorization',header_token);
    let options = new RequestOptions({ headers: headers }); 
   
    return this.http.post(url,payload,options)
      .map((res: Response) => res.json())
      .catch((error: any) => {
        console.log(error)
        try {
          return (Observable.throw(error.json()));
        } catch (jsonError) {
          // If the error couldn't be parsed as JSON data
          // then it's possible the API is down or something
          // went wrong with the parsing of the successful
          // response. In any case, to keep things simple,
          // we'll just create a minimum representation of
          // a parsed error.
          var minimumViableError = {
            success: false
          };
          return (Observable.throw(minimumViableError));
        }
      })
  }

  getAllBooking(payload){
    let token=localStorage.getItem('token')
    var header_token = token
    let headers = new Headers({ 'Content-Type': 'application/json'});
    headers.append('Authorization',header_token);
    let options = new RequestOptions({ headers: headers });
    // let skip = (payload.currentPage - 1) * payload.limit;

    let url =  environment.APP.API_URL + environment.APP.GET_ALL_BOOKING;
    
    //console.log(url);

    return this.http.post(url,options)
      .map((res: Response) => {
        console.log(res.json());
        return res.json();
      })
      .catch((error: any) => {
        console.log(error)
        try {
          return (Observable.throw(error.json()));
        } catch (jsonError) {
          var minimumViableError = {
            success: false
          };
          return (Observable.throw(minimumViableError));
        }
      })

  }

  getAllUser(payload){
    let token=localStorage.getItem('token')
    var header_token = token
    let headers = new Headers({ 'Content-Type': 'application/json'});
    headers.append('Authorization',header_token);
    let options = new RequestOptions({ headers: headers });
    // let skip = (payload.currentPage - 1) * payload.limit;

    let url =  environment.APP.API_URL + environment.APP.GET_ALL_USER;
    
    //console.log(url);

    return this.http.post(url,options)
      .map((res: Response) => {
        console.log(res.json());
        return res.json();
      })
      .catch((error: any) => {
        console.log(error)
        try {
          return (Observable.throw(error.json()));
        } catch (jsonError) {
          var minimumViableError = {
            success: false
          };
          return (Observable.throw(minimumViableError));
        }
      })

  }




 getAllBookingPagination(url){
     let token=localStorage.getItem('token')
     var header_token = 'Bearer '+token
     console.log(header_token)
   let headers = new Headers({ 'Content-Type': 'application/json','content-language':'en' });
     headers.append('Authorization',header_token);
     let options = new RequestOptions({ headers: headers });
    //let url=environment.APP.API_URL+environment.APP.GET_ALL_BOOKING+"?limit="+limit+"&skip="+skip;
    return this.http.get(url,options)
      .map((res: Response) => {
        //console.log(res.json());
        return res.json();
      })

      .catch((error: any) => {
        console.log(error)
        try {
          return (Observable.throw(error.json()));
        } catch (jsonError) {
          // If the error couldn't be parsed as JSON data
          // then it's possible the API is down or something
          // went wrong with the parsing of the successful
          // response. In any case, to keep things simple,
          // we'll just create a minimum representation of
          // a parsed error.
          var minimumViableError = {
            success: false
          };
          return (Observable.throw(minimumViableError));
        }
      })
     // }
     // else{
     //  return this.token;
     // }
 }
 


  getUserById(payload){
    
      let headers = new Headers({ 'Content-Type': 'application/json', });

     headers.append('Authorization',this.header_token);
     let options = new RequestOptions({ headers: headers }); 
     let url=environment.APP.API_URL+environment.APP.GET_USER_BY_ID+ "?userId=" + payload.userId;
     
     return this.http.get(url,options)
      .map((res: Response) => res.json())

      .catch((error: any) => {
        console.log(error)
        try {
          return (Observable.throw(error.json()));
        } catch (jsonError) {
          // If the error couldn't be parsed as JSON data
          // then it's possible the API is down or something
          // went wrong with the parsing of the successful
          // response. In any case, to keep things simple,
          // we'll just create a minimum representation of
          // a parsed error.
          var minimumViableError = {
            success: false
          };
          return (Observable.throw(minimumViableError));
        }
      })
    }
    blockUserById(payload){
    
      let headers = new Headers({ 'Content-Type': 'application/json', });

     headers.append('Authorization',this.header_token);
     let options = new RequestOptions({ headers: headers }); 
     let url=environment.APP.API_URL+environment.APP.BLOCK_USER_BY_ID;
       
      
     return this.http.post(url,payload,options)
      .map((res: Response) => res.json())

      .catch((error: any) => {
        console.log(error)
        try {
          return (Observable.throw(error.json()));
        } catch (jsonError) {
          // If the error couldn't be parsed as JSON data
          // then it's possible the API is down or something
          // went wrong with the parsing of the successful
          // response. In any case, to keep things simple,
          // we'll just create a minimum representation of
          // a parsed error.
          var minimumViableError = {
            success: false
          };
          return (Observable.throw(minimumViableError));
        }
      })
    }
    approveUserById(payload){
    
      let headers = new Headers({ 'Content-Type': 'application/json', });

     headers.append('Authorization',this.header_token);
     let options = new RequestOptions({ headers: headers }); 
     let url=environment.APP.API_URL+environment.APP.APPROVE_USER_BY_ID;
       
      
     return this.http.post(url,payload,options)
      .map((res: Response) => res.json())

      .catch((error: any) => {
        console.log(error)
        try {
          return (Observable.throw(error.json()));
        } catch (jsonError) {
          // If the error couldn't be parsed as JSON data
          // then it's possible the API is down or something
          // went wrong with the parsing of the successful
          // response. In any case, to keep things simple,
          // we'll just create a minimum representation of
          // a parsed error.
          var minimumViableError = {
            success: false
          };
          return (Observable.throw(minimumViableError));
        }
      })
    }






     deleteCustomerRecord(payload){
        console.log(payload);
        let token=localStorage.getItem('token');
        let header_token = token
        // PARAMS
        let params = new FormData();
            params.append('categoryId', payload.categoryId);
        // HEADERS
        let headers = new Headers({
          // 'content-language':'en'
        });
        headers.append('Authorization', header_token);
        let options = new RequestOptions({
          'headers' :  headers,
          'body'    :  { 'categoryId': payload.categoryId }
        });
        console.log(options);
        // URL
       // let url = 'http://54.200.235.37:3001/admin/deleteUser';
        let url = environment.APP.API_URL+environment.APP.DELETE_ITEM_BY_ID+payload.categoryId;
        // HTTP REQUEST
        return this.http.delete(url, options)
        .map((res: Response) => res.json())
        .catch((error: any) => {
          try {
            return (Observable.throw(error.json()));
          } catch (jsonError) {
            var minimumViableError = {
              success: false
            };
            return (Observable.throw(minimumViableError));
          }
        });
      };
editUser(payload){
  // console.log("ssssss",payload);
  // // let token=localStorage.getItem('token');
  // //       let header_token = token;
    
  //     let headers = new Headers({  });

  //    headers.append('Authorization',this.header_token);
  //    let options = new RequestOptions({ headers: headers }); 
  //    let url=environment.APP.API_URL+environment.APP.EDIT_USER_BY_ID;
     
  //    return this.http.put(url,options)
  //     .map((res: Response) => res.json())

  //     .catch((error: any) => {
  //       console.log(error)
  //       try {
  //         return (Observable.throw(error.json()));
  //       } catch (jsonError) {
  //         // If the error couldn't be parsed as JSON data
  //         // then it's possible the API is down or something
  //         // went wrong with the parsing of the successful
  //         // response. In any case, to keep things simple,
  //         // we'll just create a minimum representation of
  //         // a parsed error.
  //         var minimumViableError = {
  //           success: false
  //         };
  //         return (Observable.throw(minimumViableError));
  //       }
  //     })


   console.log("gfdhgsffffffshfdjhbf",payload);
        let token=localStorage.getItem('token');
        let header_token = token
        // PARAMS
        // let params = new FormData();
        //     params.append('categoryId', payload.categoryId);
        // HEADERS
        let headers = new Headers({
          // 'content-language':'en'
        });
        // alert("aaaasssss"+payload.name);
        // var fd=new FormData();
        // fd.append('id',payload.id);
        //  fd.append('name',payload.name);
        //   fd.append('email',payload.email);
        //    fd.append('photo',payload.photo);
        //     fd.append('address',payload.address);
        //      fd.append('phone',payload.phone);
        //       fd.append('password',payload.password);

        // alert(fd);
        // console.log("aaaaaa",fd);
        headers.append('Authorization', header_token);
        let options = new RequestOptions({
          'headers' :  headers
        });
        console.log(options);
        // URL
       // let url = 'http://54.200.235.37:3001/admin/deleteUser';
        // let url = environment.APP.API_URL+environment.APP.EDIT_USER_BY_ID+payload.id;
          let url = environment.APP.API_URL+environment.APP.EDIT_USER_BY_ID;
       
        // HTTP REQUEST
        return this.http.post(url,payload,options)
        .map((res: Response) => res.json())
        .catch((error: any) => {
          try {
            return (Observable.throw(error.json()));
          } catch (jsonError) {
            var minimumViableError = {
              success: false
            };
            return (Observable.throw(minimumViableError));
          }
        });
    }
  

}
