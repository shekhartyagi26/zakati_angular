import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { Effect, Actions } from '@ngrx/effects'
import { Action, Store } from '@ngrx/store'
import { Observable } from 'rxjs/Observable'
// import { booking_service } from '../services/bookingService/booking.service';
// import { customer_service } from '../services/customerService/customer.service';
// import { serviceprovider_service } from '../services/serviceproviderService/serviceprovider.service';
// import { driver_service } from '../services/driverService/driver.service';


import * as app from './app.actions'

@Injectable()
export class AppEffects {
  typeOfUser;
   constructor(
    private actions$: Actions,
    private router: Router,
    private store: Store<any>,
    // private booking_service:booking_service,
    // private customer_service:customer_service,
    // private serviceprovider_service:serviceprovider_service,
    // private driver_service:driver_service,
  ) { }

  @Effect({ dispatch: false })
  redirectDashboard: Observable<Action> = this.actions$
    .ofType(app.ActionTypes.APP_REDIRECT_DASHBOARD)
    .do(() => this.router.navigate([ '/', 'dashboard' ]))

  @Effect({ dispatch: false })
  redirectLogin: Observable<Action> = this.actions$
    .ofType(app.ActionTypes.APP_REDIRECT_LOGIN)
    .do(() => this.router.navigate([ '/', 'login' ]))

  @Effect({ dispatch: false })
  redirectRouter: Observable<Action> = this.actions$
    .ofType(app.ActionTypes.APP_REDIRECT_ROUTER)
    .do(() => this.router.navigate([ '/', 'router' ]))

  @Effect({ dispatch: false })
  appRefresh$ = this.actions$
    .ofType('APP_RELOAD')
    // FIXME: I miss UI-Routers $state.reload() - find an angular way of doing this
    .do(() => window.location.reload())


@Effect({ dispatch: false })
  getAllUsers$ = this.actions$
    .ofType('APP_GETALL_USERS')
    // FIXME: I miss UI-Routers $state.reload() - find an angular way of doing this
    .do(() => {console.log("hey users")})
@Effect({ dispatch: false })
  blockUser$: Observable<Action>= this.actions$
    .ofType('APP_BLOCK_USER_BY_ID')
    // FIXME: I miss UI-Routers $state.reload() - find an angular way of doing this
    .do(action => {console.log("block user")
//   this.customer_service.blockUserById(action.payload)
// .subscribe((result) => {
//              console.log(result)
//               if(result.message == 'Success')
//               {
//                  alert("blocked")
//               }

//               }
//             , (error) => {
//               console.log(error.message)
//             });
    })


  @Effect({ dispatch: false })
  deleteUser$: Observable<Action>= this.actions$
    .ofType('APP_DELETE_USER_BY_ID')
    // FIXME: I miss UI-Routers $state.reload() - find an angular way of doing this
    .do(action => {

//   this.customer_service.deleteUserById(action.payload)
// .subscribe((result) => {
//              console.log(result)
//               if(result.message == 'Success')
//               {
//                //this.store.dispatch(new app.AppDeleteUserByIdSuccess(result))
//                 console.log("deleted")
//               }


// }
//             , (error) => {
//               console.log(error.message)
//             });
    })






@Effect({ dispatch: false })
      registerservice: Observable<Action> = this.actions$
      .ofType('APP_REGISTER_SERVICE')
      .do((action) => {
        // this.serviceprovider_service.getServiceRegister(action.payload)
        // .subscribe((result) => {
        //           //console.log("id result...............",result)
        //           if(result.message == 'Success')
        //           {


        //             console.log("created a new service");


        //           }
        //           }
        //         , (error) => {
        //           console.log(error.message)
        //         }
        //       );
    })

@Effect({ dispatch: false })
      registercustomer: Observable<Action> = this.actions$
      .ofType('APP_REGISTER_CUSTOMER')
      .do((action) => {
        // this.customer_service.getCustomerRegister(action.payload)
        // .subscribe((result) => {
        //           //console.log("id result...............",result)
        //           if(result.message == 'Success')
        //           {


        //             console.log("created a new customer");


        //           }
        //           }
        //         , (error) => {
        //           console.log(error.message)
        //         }
        //       );
    })
     @Effect({ dispatch: false })
     bookingDetailByIdSuccess = this.actions$
    .ofType('APP_GETALL_BOOKING_BY_ID_SUCCESS')
    // FIXME: I miss UI-Routers $state.reload() - find an angular way of doing this
    .do(() => {})


     @Effect({ dispatch: false })
     getAllUser = this.actions$
    .ofType('APP_GETTALL_USER')
    // FIXME: I miss UI-Routers $state.reload() - find an angular way of doing this
    .do(() => {})

     @Effect({ dispatch: false })
      editUserByIdSuccess = this.actions$
    .ofType('APP_EDIT_USER_BY_ID_SUCCESS')
    // FIXME: I miss UI-Routers $state.reload() - find an angular way of doing this
    .do((action) => {
      // this.customer_service.editcustomer(action.payload)
      //   .subscribe((result) => {
      //             //console.log("id result...............",result)
      //             if(result.message == 'Success')
      //             {
      //               console.log("changed user")
      //             }
      //             }
      //           , (error) => {
      //             console.log(error.message)
      //           }
      //         );
    })
@Effect({ dispatch: false })
      editServiceByIdSuccess = this.actions$
    .ofType('APP_EDIT_SERVICE_BY_ID_SUCCESS')
    // FIXME: I miss UI-Routers $state.reload() - find an angular way of doing this
    .do((action) => {
      // this.serviceprovider_service.editservice(action.payload)
      //   .subscribe((result) => {
      //             // console.log("id result...............",result)
      //             if(result.message == 'Success')
      //             {
      //               console.log("changed service")
      //             }
      //             }
      //           , (error) => {
      //             console.log(error.message)
      //           }
      //         );
    })


     @Effect({ dispatch: false })
     getAllUserSuccess = this.actions$
    .ofType('APP_GETTALL_USER_SUCCESS')
    // FIXME: I miss UI-Routers $state.reload() - find an angular way of doing this
    .do((action) =>
      {
        //  console.log("payload",action.payload)
          // this.customer_service.getAllUser(action.payload)
          // .subscribe((result) => {

          //   //console.log("is result...............",result)
          //     if(result.message === 'Success'){
          //       // window.localStorage.setItem('token', JSON.stringify(action.payload))
          //               // localStorage.setItem('token',token);
          //       //header_token = 'Bearer'+ " " +this.token


          //               this.store.dispatch(new app.AppGetAllUserAfterSuccess(result))
          //     }


          //         }
          //       , (error) => {
          //         console.log(error)
          //       }
          //     );

      })

 @Effect({ dispatch: false })
     editUserById = this.actions$
    .ofType('APP_EDIT_USER_BY_ID')
    // FIXME: I miss UI-Routers $state.reload() - find an angular way of doing this
    .do((action) => {
      // this.customer_service.getAllUserById(action.payload.id)
      //     .subscribe((result) => {
      //       //console.log("is result...............",result)
      //          if(result.message === 'Success'){
      // console.log("edit")
      // this.store.dispatch(new app.AppGetAllUserByIdSuccess(result))
      // this.router.navigate(['pages/Customers/editdetails']);

      //          }
      //     }
      //           , (error) => {
      //             console.log(error)
      //           }
      //         );

    })
     @Effect({ dispatch: false })
     editServiceById = this.actions$
    .ofType('APP_EDIT_SERVICE_BY_ID')
    // FIXME: I miss UI-Routers $state.reload() - find an angular way of doing this
    .do((action) => {
      // this.customer_service.getAllUserById(action.payload.id)
      //     .subscribe((result) => {
      //       //console.log("is result...............",result)
      //          if(result.message === 'Success'){
      // console.log("edit")
      // this.store.dispatch(new app.AppGetAllUserByIdSuccess(result))
      // this.router.navigate(['pages/serviceProvider/editservice']);

      //          }
      //     }
      //           , (error) => {
      //             console.log(error)
      //           }
      //         );

    })
     @Effect({ dispatch: false })
     getAllUserAfterSuccess = this.actions$
    .ofType('APP_GETTALL_USER_AFTER_SUCCESS')
    // FIXME: I miss UI-Routers $state.reload() - find an angular way of doing this
    .do(() => {})


     @Effect({ dispatch: false })
     getAllUserById = this.actions$
    .ofType('APP_GETTALL_USER_BY_ID')
    // FIXME: I miss UI-Routers $state.reload() - find an angular way of doing this
    .do((action) => {
         // this.customer_service.getAllUserById(action.payload.id)
         //  .subscribe((result) => {
         //    //console.log("is result...............",result)
         //       if(result.message === 'Success'){
         //                  //console.log(result.data.role[0])
         //                 this.router.navigate(['pages/'+action.payload.userType+'/byIdDetails']);
         //                 this.store.dispatch(new app.AppGetAllUserByIdSuccess(result))
         //       }


         //          }
         //        , (error) => {
         //          console.log(error)
         //        }
         //      );

    })

     @Effect({ dispatch: false })
     getAllUserAfterSuccessById = this.actions$
    .ofType('APP_GETTALL_USER_BY_ID_SUCCESS')
    // FIXME: I miss UI-Routers $state.reload() - find an angular way of doing this
    .do(() => {})
    @Effect({ dispatch: false })
     deleteUserByIdSuccess = this.actions$
    .ofType('APP_DELETE_USER_BY_ID_SUCCESS')
    // FIXME: I miss UI-Routers $state.reload() - find an angular way of doing this
    .do(() => {console.log("d")})

    /*@Effect({ dispatch: false })
      cancelbookingDetailById: Observable<Action> = this.actions$
      .ofType('APP_CANCEL_BOOKING_BY_ID')
      .do((action) => {

        //this.router.navigate(['pages/Bookings/CancelBooking']);
    })*/

     /* @Effect({ dispatch: false })
      cancelbookingDetailByIdSuccess: Observable<Action> = this.actions$
      .ofType('APP_CANCEL_BOOKING_BY_ID_SUCCESS')
      .do((action) => {

        this.booking_service.cancelBooking(action.payload)
        .subscribe((result) => {
                 console.log(result)


                  }
                , (error) => {
                  console.log(error.message)
                }
              );

    })*/

    // @Effect({ dispatch: false })
    //   registerdriver: Observable<Action> = this.actions$
    //   .ofType('APP_REGISTER_DRIVER')
    //   .do((action) => {
    //     this.driver_service.getDriverRegister(action.payload)
    //     // .subscribe((result) => {
    //     //           //console.log("id result...............",result)
    //     //           if(result.message == 'Success')
    //     //           {


    //     //             console.log("created a new driver");


        //           }
        //           }
        //         , (error) => {
        //           console.log(error.message)
        //         }
  //       //       );
  //   })

  //  @Effect({ dispatch: false })
  //    editDriverById = this.actions$
  //   .ofType('APP_EDIT_DRIVER_BY_ID')
  //   // FIXME: I miss UI-Routers $state.reload() - find an angular way of doing this
  //   .do((action) => {
  //     // this.driver_service.getAllUserById(action.payload.id)
      //     .subscribe((result) => {
      //       //console.log("is result...............",result)
      //          if(result.message === 'Success'){
      // console.log("edit")
      //  this.store.dispatch(new app.AppGetAllUserByIdSuccess(result))
      // this.router.navigate(['pages/driver/editdriver']);

      //          }
      //     }
      //           , (error) => {
      //             console.log(error)
      //           }
      //         );

    // })


// @Effect({ dispatch: false })
//       editdriverByIdSuccess = this.actions$
//     .ofType('APP_EDIT_DRIVER_BY_ID_SUCCESS')
//     // FIXME: I miss UI-Routers $state.reload() - find an angular way of doing this
//     .do((action) => {this.driver_service.editdriver(action.payload)
//         // .subscribe((result) => {
//         //           // console.log("id result...............",result)
//         //           if(result.message == 'Success')
//         //           {
//         //             // this.router.navigate(['pages/driver/editdriver']);
//         //             console.log("changed DRIVER")
//         //           }
//         //           }
//         //         , (error) => {
//         //           console.log(error.message)
//         //         }
//         //       );
    //  })








}

