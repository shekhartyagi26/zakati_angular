
import { Action } from '@ngrx/store'

export const ActionTypes = {
  APP_SETTING_ADD:          'APP_SETTING_ADD',
  APP_REDIRECT_DASHBOARD:   'APP_REDIRECT_DASHBOARD',
  APP_REDIRECT_LOGIN:       'APP_REDIRECT_LOGIN',
  APP_REDIRECT_ROUTER:      'APP_REDIRECT_ROUTER',
  APP_GETALL_BOOKING:       'APP_GETALL_BOOKING',
  APP_BOOKING_DETAIL:       'APP_BOOKING_DETAIL',

  APP_GETALL_USERS:           'APP_GETALL_USERS',
  APP_BOOKING_DETAIL_SUCCESS:       'APP_BOOKING_DETAIL_SUCCESS',
  APP_GETALL_BOOKING_BY_ID:   'APP_GETALL_BOOKING_BY_ID',
  APP_GETALL_BOOKING_BY_ID_SUCCESS:   'APP_GETALL_BOOKING_BY_ID_SUCCESS',
 

  APP_CANCEL_BOOKING_BY_ID: 'APP_CANCEL_BOOKING_BY_ID',
  APP_CANCEL_BOOKING_BY_ID_SUCCESS: 'APP_CANCEL_BOOKING_BY_ID_SUCCESS',
  APP_GETTALL_USER:   'APP_GETTALL_USER',
  APP_GETTALL_USER_SUCCESS:   'APP_GETTALL_USER_SUCCESS',
  APP_GETTALL_USER_AFTER_SUCCESS:   'APP_GETTALL_USER_AFTER_SUCCESS',

  APP_GETTALL_USER_BY_ID:   'APP_GETTALL_USER_BY_ID',
  
  APP_BLOCK_USER_BY_ID:   'APP_BLOCK_USER_BY_ID',
  APP_EDIT_USER_BY_ID:'APP_EDIT_USER_BY_ID',
  APP_EDIT_USER_BY_ID_SUCCESS:'APP_EDIT_USER_BY_ID_SUCCESS',
  APP_EDIT_SERVICE_BY_ID:'APP_EDIT_SERVICE_BY_ID',
  APP_EDIT_SERVICE_BY_ID_SUCCESS:'APP_EDIT_SERVICE_BY_ID_SUCCESS',
  APP_DELETE_USER_BY_ID:   'APP_DELETE_USER_BY_ID',
   APP_DELETE_USER_BY_ID_SUCCESS:   'APP_DELETE_USER_BY_ID_SUCCESS',
  APP_GETTALL_USER_BY_ID_SUCCESS:   'APP_GETTALL_USER_BY_ID_SUCCESS',
  APP_REGISTER_SERVICE:'APP_REGISTER_SERVICE',
  APP_REGISTER_SERVICE_SUCCESS:'APP_REGISTER_SERVICE_SUCCESS',
  APP_REGISTER_CUSTOMER:'APP_REGISTER_CUSTOMER',
   APP_REGISTER_DRIVER:'APP_REGISTER_DRIVER',
  APP_REGISTER_DRIVER_SUCCESS:'APP_REGISTER_DRIVER_SUCCESS',
  APP_EDIT_DRIVER_BY_ID:'APP_EDIT_DRIVER_BY_ID',
  APP_EDIT_DRIVER_BY_ID_SUCCESS:'APP_EDIT_DRIVER_BY_ID_SUCCESS',
  



}

type credentials = {
  skip: number,
  limit:number
}

export class AppAddSettingAction implements Action {
  type = ActionTypes.APP_SETTING_ADD
  constructor() { }
}

export class AppRedirectDashboardAction implements Action {
  type = ActionTypes.APP_REDIRECT_DASHBOARD
  constructor() { }
}

export class AppRedirectLoginAction implements Action {
  type = ActionTypes.APP_REDIRECT_LOGIN
  constructor() { }
}

export class AppRedirectRouterAction implements Action {
  type = ActionTypes.APP_REDIRECT_ROUTER
  constructor() { }
}



export class AppGetAllBookingDetail implements Action {
  type = ActionTypes.APP_GETALL_BOOKING
  constructor(public payload: credentials) {}
}

 export class AppGetAllUsers implements Action {
   type = ActionTypes.APP_GETALL_USERS
   constructor() {}
 }
export class AppRegisterService implements Action {
  type = ActionTypes.APP_REGISTER_SERVICE
  constructor() {}
}
export class AppRegisterCustomer implements Action {
  type = ActionTypes.APP_REGISTER_CUSTOMER
  constructor() {console.log("asd")}
}
export class AppRegisterServiceSuccess implements Action {
  type = ActionTypes.APP_REGISTER_SERVICE_SUCCESS
  constructor() {}
}


export class AppBookingDetail implements Action {
  type = ActionTypes.APP_BOOKING_DETAIL
  constructor(public payload: credentials) {}
}

export class AppBookingDetailSuccess implements Action {
  type = ActionTypes.APP_BOOKING_DETAIL_SUCCESS
  constructor(public payload: credentials) {}
}

export class AppBookingDetailById implements Action {
  type = ActionTypes.APP_GETALL_BOOKING_BY_ID
  constructor(public payload: credentials) {}
}

export class AppBookingDetailByIdSuccess implements Action {
  type = ActionTypes.APP_GETALL_BOOKING_BY_ID_SUCCESS
  constructor(public payload: credentials) {}
}

 
export class AppGetAllUser implements Action {
  type = ActionTypes.APP_GETTALL_USER
  constructor(public payload: credentials) {}
}

export class AppGetAllUserSuccess implements Action {
  type = ActionTypes.APP_GETTALL_USER_SUCCESS
  constructor(public payload: credentials) {}
}

export class AppGetAllUserAfterSuccess implements Action {
  type = ActionTypes.APP_GETTALL_USER_AFTER_SUCCESS
  constructor(public payload: credentials) {}
}

export class AppGetAllUserById implements Action {
  type = ActionTypes.APP_GETTALL_USER_BY_ID
  constructor(public payload: credentials) {}
}
export class AppBlockUserById implements Action {
  type = ActionTypes.APP_BLOCK_USER_BY_ID
  constructor(public payload: credentials) {
    
  }
}
export class AppEditUserById implements Action {
  type = ActionTypes.APP_EDIT_USER_BY_ID
  constructor(public payload: credentials) {
    console.log("e")
    
  }
}
  export class AppEditUserByIdSuccess implements Action {
  type = ActionTypes.APP_EDIT_USER_BY_ID_SUCCESS
  constructor() {
    
  }
}

export class AppEditServiceById implements Action {
  type = ActionTypes.APP_EDIT_SERVICE_BY_ID
  constructor(public payload: credentials) {
    console.log("e")
    
  }
}
  export class AppEditServiceByIdSuccess implements Action {
  type = ActionTypes.APP_EDIT_SERVICE_BY_ID_SUCCESS
  constructor() {
    
  }
}
export class AppDeleteUserById implements Action {
  type = ActionTypes.APP_DELETE_USER_BY_ID
  constructor(public payload: credentials) {
    
  }
}
export class AppDeleteUserByIdSuccess implements Action {
  type = ActionTypes.APP_DELETE_USER_BY_ID_SUCCESS
  constructor(public payload: credentials) {
    console.log("g")
    
  }
}

export class AppGetAllUserByIdSuccess implements Action {
  type = ActionTypes.APP_GETTALL_USER_BY_ID_SUCCESS
  constructor(public payload: credentials) {}
}

export class AppCancelBookingDetail implements Action {
  type = ActionTypes.APP_CANCEL_BOOKING_BY_ID
  constructor() {}
}
export class AppCancelBookingDetailSuccess implements Action {
  type = ActionTypes.APP_CANCEL_BOOKING_BY_ID_SUCCESS
  constructor(public payload: credentials) {}
}

export class AppRegisterDriver implements Action {
  type = ActionTypes.APP_REGISTER_DRIVER
  constructor() {}
}

export class AppRegisterDriverSuccess implements Action {
  type = ActionTypes.APP_REGISTER_DRIVER_SUCCESS
  constructor() {}
}
export class AppEditDriverById implements Action {
  type = ActionTypes.APP_EDIT_DRIVER_BY_ID
  constructor(public payload: credentials) {
    console.log("e")
    
  }
}
  export class AppEditDriverByIdSuccess implements Action {
  type = ActionTypes.APP_EDIT_DRIVER_BY_ID_SUCCESS
  constructor() {
    
  }
}



export type Actions
  =  AppAddSettingAction
  | AppRedirectDashboardAction
  | AppRedirectLoginAction
  | AppRedirectRouterAction
  | AppGetAllBookingDetail
  | AppBookingDetail
  | AppBookingDetailSuccess
  | AppGetAllUsers 
  | AppBookingDetailById
  | AppBookingDetailByIdSuccess
  | AppGetAllUser
  | AppGetAllUserSuccess
  | AppGetAllUserAfterSuccess
  | AppGetAllUserById
  | AppBlockUserById
  | AppEditUserById
  | AppEditUserByIdSuccess
  | AppEditServiceById
  | AppEditServiceByIdSuccess
  | AppDeleteUserById
  | AppDeleteUserByIdSuccess
  | AppGetAllUserByIdSuccess
  | AppCancelBookingDetail
  | AppCancelBookingDetailSuccess
  | AppRegisterService
  | AppRegisterCustomer
  | AppRegisterServiceSuccess
  | AppRegisterDriver
  | AppRegisterDriverSuccess
  |AppEditDriverById
  | AppEditDriverByIdSuccess
  


