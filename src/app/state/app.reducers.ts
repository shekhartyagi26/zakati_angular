import { sortBy } from 'lodash'
import { Action, ActionReducer } from '@ngrx/store'

const initialState: any = {
  activeDomain: null,
  

  domains: [],
  settings: {},
  contentDashboard: [],
  systemDashboard: [],
  blocked:false
}

export const app: ActionReducer<any> = (state = initialState, action: Action) => {
  switch (action.type) {

    case 'APP_DEVELOPMENT_ENABLE':
      return Object.assign({}, state, { development: true })

    case 'APP_SETTING_ADD':
      const settings = state.settings
      settings[action.payload.key] = action.payload.value

      return Object.assign({}, state, { settings })

    case 'APP_CONTENT_DASHBOARD':
      return Object.assign({}, state, {
        contentDashboard: sortBy([ ...state.contentDashboard, action.payload ], ['weight'])
      })

    case 'APP_SYSTEM_DASHBOARD':
      return Object.assign({}, state, {
        systemDashboard: sortBy([ ...state.systemDashboard, action.payload ], ['weight'])
      })
      
      case 'APP_GETALL_BOOKING':
      return Object.assign({}, state,{bookingType:action.payload,userType:null })

      case 'APP_GETALL_USERS':
      return Object.assign({}, state)

     

    case 'APP_BOOKING_DETAIL':
      return Object.assign({}, state)

    case 'APP_BOOKING_DETAIL_SUCCESS':
      return Object.assign({}, state,{bookingDetails:action.payload,development: true})

    case 'APP_GETALL_BOOKING_BY_ID':
      return Object.assign({}, state)

     case 'APP_GETALL_BOOKING_BY_ID_SUCCESS':
      return Object.assign({}, state,{bookingId:action.payload})

    case 'APP_GETTALL_USER':
      return Object.assign({}, state,{userType:action.payload,bookingType:null})

     case 'APP_GETTALL_USER_SUCCESS':
      return Object.assign({}, state,{development: false})

     case 'APP_GETTALL_USER_AFTER_SUCCESS':
      return Object.assign({}, state,{userDetail:action.payload})

     case 'APP_GETTALL_USER_BY_ID':
      return Object.assign({}, state)
      case 'APP_BLOCK_USER_BY_ID':
      return Object.assign({}, state,{blocked:true})
     case 'APP_EDIT_USER_BY_ID':
      return Object.assign({}, state)
      case 'APP_EDIT_USER_BY_ID_SUCCESS':
      return Object.assign({}, state)

      case 'APP_EDIT_SERVICE_BY_ID':
      return Object.assign({}, state)
      case 'APP_EDIT_SERVICE_BY_ID_SUCCESS':
      return Object.assign({}, state)
    
    
     case 'APP_DELETE_USER_BY_ID':
      return Object.assign({}, state)
    case 'APP_DELETE_USER_BY_ID_SUCCESS':
      return Object.assign({})
    
     case 'APP_GETTALL_USER_BY_ID_SUCCESS':
      return Object.assign({}, state,{userById:action.payload})
      
      case 'APP_CANCEL_BOOKING_BY_ID':
      return Object.assign({}, state,{userById:action.payload})
      
      case 'APP_CANCEL_BOOKING_BY_ID_SUCCESS':
      return Object.assign({}, state,{userById:action.payload})
      
      case 'APP_REGISTER_SERVICE':
      return Object.assign({}, state)
          
      case 'APP_REGISTER_CUSTOMER':
      return Object.assign({}, state)
      case 'APP_REGISTER_SERVICE_SUCCESS':
      return Object.assign({}, state)
      

     case 'APP_CANCEL_BOOKING_BY_ID':
      return Object.assign({}, state)
      
     case 'APP_CANCEL_BOOKING_BY_ID_SUCCESS':
      return Object.assign({}, state)

    case 'APP_REGISTER_DRIVER':
      return Object.assign({}, state)
      case 'APP_REGISTER_DRIVER_SUCCESS':
      return Object.assign({}, state)
        case 'APP_EDIT_DRIVER_BY_ID':
      return Object.assign({}, state)
      case 'APP_EDIT_DRIVER_BY_ID_SUCCESS':
      return Object.assign({}, state)
    
      


    default:
      return state
  }
}
