import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule ,ReactiveFormsModule} from '@angular/forms'
import { RouterModule } from '@angular/router'
//import { routing }       from './auth.routes';
import { NgaModule } from '../theme/nga.module';
import { AppTranslationModule } from '../app.translation.module';
//import { ColmenaUiModule } from '@colmena/colmena-angular-ui'
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { user_service } from './service/userService/user_service';
import { AuthGuard } from './service/authGuardPublicPages/authGuardPublic.service';
import { AuthPublicPagesService } from './service/authGuardPublicPages/authPublic.service';

// import { Login } from './components/login/login.component'
// import { LogoutComponent } from './components/logout/logout.component'
// import { RecoverComponent } from './components/recover/recover.component'
// import { Register } from './components/register/register.component'
// import { ResetComponent } from './components/reset/reset.component'

@NgModule({
  
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
     NgbModalModule,
    //routing,
    NgaModule,
    AppTranslationModule
    //ColmenaUiModule,
  ],
  declarations: [
    // Login,
    // LogoutComponent,
    // RecoverComponent,
    // Register,
    // ResetComponent,
  ],
  providers: [
    user_service,
    AuthGuard,
    AuthPublicPagesService
  ]
})
export class AdminAuthModule { }
