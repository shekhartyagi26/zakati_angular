import { get } from 'lodash'
import { Injectable, VERSION } from '@angular/core'
import { Effect, Actions } from '@ngrx/effects'
import { Action, Store } from '@ngrx/store'
import { Observable } from 'rxjs/Observable'
import { user_service } from '../service/userService/user_service';
import { AuthService  } from '../../theme/services/authService/auth.service';
//import { toast_service  } from '../../services/common.service';
import {BaThemeSpinner} from '../../theme/services';
import { Router } from '@angular/router';
import { ForgotModal } from '../../publicPages/components/ui/components/modals/forgot-modal/forgot-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService , ToastrConfig} from 'ngx-toastr';
import { cloneDeep, random } from 'lodash';
const types = ['success', 'error', 'info', 'warning'];
//import { UiService } from '@colmena/colmena-angular-ui'
///import { UserApi } from '@lb-sdk'


import * as auth from './auth.actions'

@Injectable()
export class AuthEffects {
    options: ToastrConfig;
          title = '';
         // type = types[0];
          message = '';

  version = VERSION;
  private lastInserted: number[] = [];




  @Effect({ dispatch: false })
  login: Observable<Action> = this.actions$
    .ofType(auth.ActionTypes.AUTH_LOGIN)
    .do(action => {
      let number=100
     
      this.user_service.login(action.payload).subscribe((result) => {
        //console.log(result);
        if (result.message="Login successful") {
                
         
        //  console.log('Result',result);
         // console.log("logged in")
         // console.log("fcfgcfgcg",result)

          //hide loader
          // this.baThemeSpinner.hide(number);
            let m = 'Email or password  match !';
              let t = 'Authentication';
              const opt = cloneDeep(this.options);
              const inserted = this.toastrService[types[0]](m, t, opt);
              if (inserted) {
                this.lastInserted.push(inserted.toastId);
              }
              this.baThemeSpinner.hide(number);
           //this.toast_service.showSuccess();
           this.store.dispatch({ type: 'AUTH_GET_USER_ROLES', payload: result })
           this.store.dispatch(new auth.AuthLoginSuccessAction(result))
          //token store in localstorage
          console.log("gcghcd", result.response[0].accessToken);
          localStorage.setItem('token', result.response[0].accessToken)
          var tokenSession = localStorage.getItem('token')
          localStorage.setItem('tokenSession', JSON.stringify(result.response[0].accessToken))
           //console.log("api integrated succussfully")
           var loggedIn = this.authService.login()
            if (loggedIn) {
            // Get the redirect URL from our auth service
            // If no redirect has been set, use the default
            let redirect = this.authService.redirectUrl ? this.authService.redirectUrl : 'pages';
            // Redirect the user
            console.log(redirect)
            this.router.navigate([redirect]);
          }
         
        }
        else{
              

    console.log(result)
        }
      }
      , (error) => {
        this.baThemeSpinner.hide();
         // If no redirect has been set, use the default
            // let redirect = this.authService.redirectUrl ? this.authService.redirectUrl : 'pages';
            //  this.router.navigate([redirect]);
        // this.router.navigate(['pages/dashboard']);
        
            // this.store.dispatch(new auth.AuthLoginErrorAction(error))
            // console.log(error.message)
    let m = 'Email or password  donot match !';
              let t = 'Authentication';
              const opt = cloneDeep(this.options);
              const inserted = this.toastrService[types[1]](m, t, opt);
              if (inserted) {
                this.lastInserted.push(inserted.toastId);

              }

       
       
            
            
    
 

      }
    );

    })

  @Effect({ dispatch: false })
  loginError: Observable<Action> = this.actions$
    .ofType(auth.ActionTypes.AUTH_LOGIN_ERROR)
     .do((action) => {

          //  let m = 'Email or password  donot match !';
          //     let t = 'Authentication';
          //     const opt = cloneDeep(this.options);
          //     const inserted = this.toastrService[types[1]](m, t, opt);
          //     if (inserted) {
          //       this.lastInserted.push(inserted.toastId);

          //     }
       
       console.log(action);
     })

  @Effect({ dispatch: false })
  loginSuccess: Observable<Action> = this.actions$
    .ofType(auth.ActionTypes.AUTH_LOGIN_SUCCESS)
    .do((action) => {

      // window.localStorage.setItem('token', JSON.stringify(action.payload))
      //this.ui.toastSuccess('Sign in successful', `You are logged in as ${get(action, 'payload.user.email')}`)
      //return this.store.dispatch({ type: 'APP_REDIRECT_ROUTER' })
    })

  @Effect({ dispatch: false })
  register: Observable<Action> = this.actions$
    .ofType(auth.ActionTypes.AUTH_REGISTER)
    .do((action: any) => {
        console.log("registerede api call")
      // this.userApi.create(action.payload)
      //   .subscribe(
      //     (success: any) => this.store.dispatch(new auth.AuthRegisterSuccessAction({
      //       realm: action.payload.realm,
      //       email: action.payload.email,
      //       password: action.payload.password,
      //     })),
      //     (error) => this.store.dispatch(new auth.AuthRegisterErrorAction(error)),
      //   )
    })

  @Effect({ dispatch: false })
  registerSuccess: Observable<Action> = this.actions$
    .ofType(auth.ActionTypes.AUTH_REGISTER_SUCCESS)
    .do((action: any) => {

      // this.ui.toastSuccess('Successfully registered', `${action.payload.email} has been created`)
      // return this.store.dispatch(new auth.AuthLoginAction(action.payload))
    })

  @Effect({ dispatch: false })
  registerError: Observable<Action> = this.actions$
    .ofType(auth.ActionTypes.AUTH_REGISTER_ERROR)

    .do((action) => {})



  @Effect({ dispatch: false })
  logout: Observable<Action> = this.actions$
    .ofType(auth.ActionTypes.AUTH_LOGOUT)
    .do(() => {
        let number =100
        this.baThemeSpinner.show();
        this.user_service.logoutUser().subscribe((result) => {
        console.log(result);
        if(result.message='Logout successful'){

           let m = 'Logout Successfully!';
              let t = 'Authentication';
              const opt = cloneDeep(this.options);
              const inserted = this.toastrService[types[0]](m, t, opt);
              if (inserted) {
                this.lastInserted.push(inserted.toastId);
          this.baThemeSpinner.hide(number);
          //clear localstorage
          this.store.dispatch(new auth.AuthLogoutSuccessAction(result)),
          //this.authService.logout();
          window.localStorage.removeItem('token');
          window.localStorage.removeItem('tokenSession');
          window.localStorage.removeItem('language');
          window.localStorage.clear();
          this.router.navigate(['login']);
        }
      }
       (error) => {
        this.baThemeSpinner.hide(number);
        console.log(error)
       }
      });
    })
    

  @Effect({ dispatch: false })
  logoutError: Observable<Action> = this.actions$
    .ofType(auth.ActionTypes.AUTH_LOGOUT_ERROR)
    .do((action) => {

      // window.localStorage.removeItem('token')
      // this.ui.toastError(get(action, 'payload.name'), get(action, 'payload.message'))
      // return this.store.dispatch({ type: 'APP_REDIRECT_ROUTER' })

    })

  @Effect({ dispatch: false })
  logoutSuccess: Observable<Action> = this.actions$
    .ofType(auth.ActionTypes.AUTH_LOGOUT_SUCCESS)
    .do(() => {

      // window.localStorage.removeItem('token')
      // this.ui.toastSuccess('Log out successful', 'You are logged out')
      // return this.store.dispatch({ type: 'APP_REDIRECT_ROUTER' })

    })

  @Effect({ dispatch: false })
  getUserInfo$ = this.actions$
    .ofType('AUTH_GET_USER_ROLES')
    .do(action => {
       
      console.log('AUTH_GET_USER_ROLES', action.payload)

      // this.userApi.info(action.payload.userId)
      //   .subscribe(res => {
      //     console.log('set roles')
      //     window.localStorage.setItem('roles', JSON.stringify(res.roles))
      //     this.store.dispatch({ type: 'AUTH_SET_ROLES', payload: res.roles})
      //   })

    })
//     @Effect({ dispatch: false })
//   forgotpassword: Observable<Action> = this.actions$
//     .ofType(auth.ActionTypes.AUTH_FORGOT_PASSWORD)
//     .do((action) => {
//        let number =100
//        this.user_service.forgotpassword(action.payload).subscribe((result) =>{
//          console.log("password reset link sent" )
//             if(result.message='success'){

 
     
//     const activeModal = this.modalService.open(ForgotModal, {size: 'sm',
//       backdrop: 'static'});
//     activeModal.componentInstance.modalHeader = 'Static modal';
//   }  
//    this.baThemeSpinner.hide(number);


      

//     }
//        )
// })
//***********************FORGOT PASS****************************** */

@Effect({ dispatch: false })
  forgot: Observable<Action> = this.actions$
    .ofType(auth.ActionTypes.AUTH_FORGOT_PASSWORD)
    .do(action => {
      let number=100

      this.user_service.forgotPass(action.payload).subscribe((result) => {
       console.log(result)
        if (result.message) {
         // console.log("logged in")
         // console.log("fcfgcfgcg",result)

          
           //this.toast_service.showSuccess();
           //this.store.dispatch({ type: 'AUTH_GET_USER_ROLES', payload: result })
          //  this.store.dispatch(new auth.AuthForgotPassSuccessAction(result))
            // if(result.message){
              let m = result.message;
              let t = '';
              const opt = cloneDeep(this.options);
              const inserted = this.toastrService[types[0]](m, t, opt);
              if (inserted) {
                this.lastInserted.push(inserted.toastId);
              // }
            }
          //token store in localstorage
          // localStorage.setItem('token', result.data.access_token)
          // var tokenSession = localStorage.getItem('token')
          // localStorage.setItem('tokenSession', JSON.stringify(result.data.access_token))
           //console.log("api integrated succussfully")
          // var loggedIn = this.authService.forgot()
           // if (loggedIn) {
            // Get the redirect URL from our auth service
            // If no redirect has been set, use the default
           // let redirect = this.authService.redirectUrl ? this.authService.redirectUrl : 'pages';
            // Redirect the user
           // console.log(redirect)
           // this.router.navigate([redirect]);
          //}
          //navigation to homepage
          //this.router.navigate(['pages'])
          //this.login();
        }
        else
        {

            // this.baThemeSpinner.hide();
            //console.log(result.message)

            // if(result.message){
              let m = 'Email does not match !';
              let t = 'Authentication';
              const opt = cloneDeep(this.options);
              const inserted = this.toastrService[types[1]](m, t, opt);
              if (inserted) {
                this.lastInserted.push(inserted.toastId);
              // }
            }
        }
      }

    );

    })


 @Effect({ dispatch: false })
  forgotError: Observable<Action> = this.actions$
    .ofType(auth.ActionTypes.AUTH_FORGOT_PASSWORD_ERROR)
     .do((action) => {
       console.log(action);
         let m = 'Email does not match !';
              let t = 'Authentication';
              const opt = cloneDeep(this.options);
              const inserted = this.toastrService[types[1]](m, t, opt);
              if (inserted) {
                this.lastInserted.push(inserted.toastId);
             
            }
     })

  @Effect({ dispatch: false })
  forgotSuccess: Observable<Action> = this.actions$
    .ofType(auth.ActionTypes.AUTH_FORGOT_PASSWORD_SUCCESS)
    .do((action) => {

      window.localStorage.setItem('token', JSON.stringify(action.payload))
      //this.ui.toastSuccess('Sign in successful', `You are logged in as ${get(action, 'payload.user.email')}`)
      //return this.store.dispatch({ type: 'APP_REDIRECT_ROUTER' })
    })


//*********************FORGOT PASS ENDS************************** */    

//********************VERIFY PASS*************************** */


@Effect({ dispatch: false })
 forgotPasswordTokenAuthenticate: Observable<Action> = this.actions$
    .ofType(auth.ActionTypes.AUTH_FORGOT_PASS_TOKEN)
    .do((action: any) => {
     this.user_service.authenticateResetPassToken(action.payload).subscribe((result) => {
         //console.log(result);
          if (result.message) {
             
               let m = "Token verified";
            let t = 'Authentication';
            const opt = cloneDeep(this.options);
            const inserted = this.toastrService[types[0]](m, t, opt);
            if (inserted) {
              this.lastInserted.push(inserted.toastId);

          
            this.store.dispatch({
              type: auth.ActionTypes.AUTH_FORGOT_PASS_TOKEN_SUCCESS,
              payload: result
            
            })
        } 
        
        else {
            let m = "token receved";
            let t = 'Authentication';
            const opt = cloneDeep(this.options);
            const inserted = this.toastrService[types[1]](m, t, opt);
            if (inserted) {
              this.lastInserted.push(inserted.toastId);
            }
        }
          }
        })
    })

@Effect({ dispatch: false })
forgotPasswordTokenAuthenticateError: Observable<Action> = this.actions$
    .ofType(auth.ActionTypes.AUTH_RESET_PASSWORD_ERROR)
    .do((action) => {
      //Magic here
    })

@Effect({ dispatch: false })
forgotPasswordTokenAuthenticateSuccess: Observable<Action> = this.actions$
    .ofType(auth.ActionTypes.AUTH_RESET_PASSWORD_SUCCESS)
    .do((action: any) => {
      //Magic here
    })


//********************VERIFY PASS ENDS*************************** */

//********************RESET PASS*************************** */

  @Effect({ dispatch: false })
  reset: Observable<Action> = this.actions$
    .ofType(auth.ActionTypes.AUTH_RESET_PASSWORD)
    .do(action => {
      let number=100

      this.user_service.resetPassword(action.payload).subscribe((result) => {
     

        if (result.message) {

          this.baThemeSpinner.hide(number);
   
          //  this.store.dispatch({ type: 'AUTH_GET_USER_ROLES', payload: result })
          //  this.store.dispatch(new auth.AuthResetPasswordSuccessAction(result))
    
              let m = result.message;
              let t = 'Successful';
              const opt = cloneDeep(this.options);
              const inserted = this.toastrService[types[2]](m, t, opt);
              if (inserted) {
               this.lastInserted.push(inserted.toastId);
                // this.router.navigate(['login']);
               
              
            } 

 
        } else {
               this.baThemeSpinner.hide();
            // console.log(error.message)

           
              let m = 'Email Doesnot exist !';
              let t = 'Authentication';
              const opt = cloneDeep(this.options);
              const inserted = this.toastrService[types[1]](m, t, opt);
              if (inserted) {
                this.lastInserted.push(inserted.toastId);
              }
          
        
      }
    }  );

    })

  @Effect({ dispatch: false })
  resetError: Observable<Action> = this.actions$
    .ofType(auth.ActionTypes.AUTH_RESET_PASSWORD_ERROR)
     .do((action) => {
       console.log(action);
     })

  @Effect({ dispatch: false })
  resetSuccess: Observable<Action> = this.actions$
    .ofType(auth.ActionTypes.AUTH_RESET_PASSWORD_SUCCESS)
    .do((action) => {

      window.localStorage.setItem('token', JSON.stringify(action.payload))
     
    })



//********************RESET PASS ENDS*************************** */


  constructor(
    private actions$: Actions,
    private store: Store<any>,
    private user_service: user_service,
    private authService:AuthService,
    private baThemeSpinner:BaThemeSpinner,
    //private toast_service:toast_service,
     private router: Router,
      private modalService: NgbModal,
        private toastrService: ToastrService

    // private userApi: UserApi,
    // private ui: UiService,

  ) {
        this.options = this.toastrService.toastrConfig;

  }
  // openToast() {
  //   console.log("--------------------");
  //           let m = 'amar';
  //           let t = 'amar';
  //           const opt = cloneDeep(this.options);
  //           const inserted = this.toastrService[this.type](m, t, opt);
  //           if (inserted) {
  //             this.lastInserted.push(inserted.toastId);
  //           }
  //           return inserted;
  //       }

}

