import { Action } from '@ngrx/store'

export const ActionTypes = {
  AUTH_LOGIN:                 'AUTH_LOGIN',
  AUTH_LOGIN_ERROR:           'AUTH_LOGIN_ERROR',
  AUTH_LOGIN_SUCCESS:         'AUTH_LOGIN_SUCCESS',
  AUTH_LOGOUT:                'AUTH_LOGOUT',
  AUTH_LOGOUT_ERROR:          'AUTH_LOGOUT_ERROR',
  AUTH_LOGOUT_SUCCESS:        'AUTH_LOGOUT_SUCCESS',
  AUTH_PASS_REQUEST:          'AUTH_PASS_REQUEST',
  AUTH_PASS_REQUEST_ERROR:    'AUTH_PASS_REQUEST_ERROR',
  AUTH_PASS_REQUEST_SUCCESS:  'AUTH_PASS_REQUEST_SUCCESS',
  AUTH_PASS_VERIFY:           'AUTH_PASS_VERIFY',
  AUTH_PASS_VERIFY_ERROR:     'AUTH_PASS_VERIFY_ERROR',
  AUTH_PASS_VERIFY_SUCCESS:   'AUTH_PASS_VERIFY_SUCCESS',
  AUTH_REGISTER:              'AUTH_REGISTER',
  AUTH_REGISTER_ERROR:        'AUTH_REGISTER_ERROR',
  AUTH_REGISTER_SUCCESS:      'AUTH_REGISTER_SUCCESS',
  // AUTH_FORGOT_PASSWORD:      'AUTH_FORGOT_PASSWORD',
  AUTH_FORGOT_PASSWORD:        'AUTH_FORGOT_PASSWORD',
  AUTH_FORGOT_PASSWORD_SUCCESS: 'AUTH_FORGOT_PASSWORD_SUCCESS',
  AUTH_FORGOT_PASSWORD_ERROR: 'AUTH_FORGOT_PASSWORD_ERROR',
  AUTH_RESET_PASSWORD: 'AUTH_RESET_PASSWORD',
  AUTH_RESET_PASSWORD_SUCCESS: 'AUTH_RESET_PASSWORD_SUCCESS',
  AUTH_RESET_PASSWORD_ERROR: 'AUTH_RESET_PASSWORD_ERROR',
  AUTH_FORGOT_PASS_TOKEN: 'AUTH_FORGOT_PASS_TOKEN',
  AUTH_FORGOT_PASS_TOKEN_SUCCESS: 'AUTH_FORGOT_PASS_TOKEN_SUCCESS',
  AUTH_FORGOT_PASS_TOKEN_ERROR: 'AUTH_FORGOT_PASS_TOKEN_ERROR'
}
type cre={
  email:string;
}
type credentials = {
  email: string,
  password: string,
  deviceType:string
}

/** LOGIN **/
export class AuthLoginAction implements Action {
  type = ActionTypes.AUTH_LOGIN


  constructor(public payload: credentials) { console.log("action fired") }

}
export class AuthForgotPassword implements Action {
  type = ActionTypes.AUTH_FORGOT_PASSWORD


  constructor(public payload:cre) { }

}
export class AuthLoginErrorAction implements Action {
  type = ActionTypes.AUTH_LOGIN_ERROR
  constructor(public payload: any) { }
}
export class AuthLoginSuccessAction implements Action {
  type = ActionTypes.AUTH_LOGIN_SUCCESS
  constructor(public payload: any) { }
}

/** LOGOUT **/
export class AuthLogoutAction implements Action {
  type = ActionTypes.AUTH_LOGOUT
  constructor(public payload: any = {}) { }
}
export class AuthLogoutErrorAction implements Action {
  type = ActionTypes.AUTH_LOGOUT_ERROR
  constructor() { }
}
export class AuthLogoutSuccessAction implements Action {
  type = ActionTypes.AUTH_LOGOUT_SUCCESS
  constructor(public payload: any = {}) { }
}

/** PASS_REQUEST **/
export class AuthPassRequestAction implements Action {
  type = ActionTypes.AUTH_PASS_REQUEST
  constructor(public payload: any) { }
}
export class AuthPassRequestErrorAction implements Action {
  type = ActionTypes.AUTH_PASS_REQUEST_ERROR
  constructor(public payload: any) { }
}
export class AuthPassRequestSuccessAction implements Action {
  type = ActionTypes.AUTH_PASS_REQUEST_SUCCESS
  constructor(public payload: any) { }
}

/** PASS_VERIFY **/
export class AuthPassVerifyAction implements Action {
  type = ActionTypes.AUTH_PASS_VERIFY
  constructor(public payload: any) { }
}
export class AuthPassVerifyErrorAction implements Action {
  type = ActionTypes.AUTH_PASS_VERIFY_ERROR
  constructor(public payload: any) { }
}
export class AuthPassVerifySuccessAction implements Action {
  type = ActionTypes.AUTH_PASS_VERIFY_SUCCESS
  constructor(public payload: any) { }
}

/** REGISTER **/
export class AuthRegisterAction implements Action {
  type = ActionTypes.AUTH_REGISTER
  constructor(public payload: any) { }
}
export class AuthRegisterErrorAction implements Action {
  type = ActionTypes.AUTH_REGISTER_ERROR
  constructor(public payload: any) { }
}
export class AuthRegisterSuccessAction implements Action {
  type = ActionTypes.AUTH_REGISTER_SUCCESS
  constructor(public payload: any) { }
}
//*****************FORGOT PASSWORD*********************** */
export class AuthForgotAction implements Action {
  type = ActionTypes.AUTH_FORGOT_PASSWORD
  constructor(public payload: any) { }
}

export class AuthForgotPassErrorAction implements Action {
  type = ActionTypes.AUTH_FORGOT_PASSWORD_ERROR
  constructor(public payload: any) { }
}
export class AuthForgotPassSuccessAction implements Action {
  type = ActionTypes.AUTH_FORGOT_PASSWORD_SUCCESS
  constructor(public payload: any) { }
}

//*****************FORGOT PASSWORD ENDS*********************** */








//**************************RESET PASSSWORD******************** */

export class AuthResetpasswordAction implements Action {
  type = ActionTypes.AUTH_RESET_PASSWORD
  constructor(public payload: any) { }
}

export class AuthResetPasswordErrorAction implements Action {
  type = ActionTypes.AUTH_RESET_PASSWORD_ERROR
  constructor(public payload: any) { }
}
export class AuthResetPasswordSuccessAction implements Action {
  type = ActionTypes.AUTH_RESET_PASSWORD_SUCCESS
  constructor(public payload: any) { }
}

//**************************RESET PASSSWORD ENDS******************** */

//**************************VERIFY PASSSWORD ******************** */

export class AuthForgotPassTokenVerifyAction implements Action {
  type = ActionTypes.AUTH_FORGOT_PASS_TOKEN
  constructor(public payload: any) { }
}

export class AuthForgotPassTokenVerifyErrorAction implements Action {
  type = ActionTypes.AUTH_FORGOT_PASS_TOKEN_ERROR
  constructor(public payload: any) { }
}
export class AuthForgotPassTokenVerifySuccessAction implements Action {
  type = ActionTypes.AUTH_FORGOT_PASS_TOKEN_SUCCESS
  constructor(public payload: any) { }
}


//**************************VERIFY  PASSSWORD ENDS******************** */




export type Actions
  = AuthLoginAction
  | AuthLoginErrorAction
  | AuthLoginSuccessAction
  | AuthLogoutAction
  | AuthLogoutErrorAction
  | AuthLogoutSuccessAction
  | AuthPassRequestAction
  | AuthPassRequestErrorAction
  | AuthPassRequestSuccessAction
  | AuthPassVerifyAction
  | AuthPassVerifyErrorAction
  | AuthPassVerifySuccessAction
  | AuthRegisterAction
  | AuthRegisterErrorAction
  | AuthRegisterSuccessAction
  | AuthForgotPassword
  | AuthForgotAction
  | AuthForgotPassErrorAction
  | AuthForgotPassSuccessAction
  | AuthResetpasswordAction
  | AuthForgotPassTokenVerifyErrorAction
  | AuthForgotPassTokenVerifySuccessAction
  | AuthResetpasswordAction
  |AuthResetPasswordErrorAction
  |AuthResetPasswordSuccessAction
