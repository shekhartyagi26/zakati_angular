import { Action, ActionReducer } from '@ngrx/store'

const initialState: any = {
  currentUser: null,
  loggedIn: false,
  realms: [],
  roles: {
    assigned: [],
    unassigned: [],
  },
}

export const auth: ActionReducer<any> = (state = initialState, action: Action) => {
  switch (action.type) {

    case 'AUTH_LOGIN':

      //console.log("hello",state)
      return Object.assign({}, state)
      case 'AUTH_LOGIN_ERROR':

      //console.log("hello",state)
      return Object.assign({}, state,{ er:action.payload})
      

    case 'AUTH_LOGOUT_SUCCESS':
      return Object.assign({}, state, { currentUser: null, loggedIn: false })

    case 'AUTH_SET_TOKEN':
    case 'AUTH_LOGIN_SUCCESS':
      console.log('action.payload', action.payload)
      return Object.assign({}, state, action.payload, {loggedIn: true} )

    case 'AUTH_REALMS_ADD':
      return Object.assign({}, state, { realms: [ ...state.realms, action.payload ] })

    case 'AUTH_SET_ROLES':
      return Object.assign({}, state, {
        roles: {
          assigned: [...state.roles.assigned, ...action.payload.assigned ],
          unassigned: [...state.roles.unassigned, ...action.payload.unassigned ],
        },
      })

    // case 'AUTH_FORGOT_PASSWORD':
    //  {
    //   //console.log("hello",state)
    //   return Object.assign({}, state)
    //   }
    //****************FORGOT PASS************* */
    case 'AUTH_FORGOT_PASSWORD':
      return Object.assign({}, state)

       case 'AUTH_FORGOT_PASSWORD_SUCCESS':
      console.log('action.payload', action.payload)
      return Object.assign({}, state, { currentUser: action.payload, loggedIn: true })


//********************FORGOT PASS ENDS***************************** */


//********************VERYFY PASS TOKEN ***************************** */



       case 'AUTH_FORGOT_PASS_TOKEN_SUCCESS':
    
      return Object.assign({}, state, {verify_token_success_payload:action.payload },
      )



//********************VERIFY PASS ENDS***************************** */

//*************************RESET PASS******************************* */

case 'AUTH_RESET_PASSWORD':
      return Object.assign({}, state)

       case 'AUTH_RESET_PASSWORD_SUCCESS':
      console.log('action.payload', action.payload)
      return Object.assign({}, state, { currentUser: action.payload, loggedIn: true })



//*************************RESET PASS END******************************* */




    default:
      return state
  }
}
