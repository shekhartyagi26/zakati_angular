import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';



@Injectable()
export class AuthService {
  isLoggedIn: boolean = false;

  // store the URL so we can redirect after logging in
  redirectUrl: '/pages';

  
  login() {
    var token = localStorage.getItem('tokenSession')
    if(token){
      //console.log("token present")
       this.isLoggedIn = true;
       //console.log(token)
       return this.isLoggedIn
    }
    else {
          this.isLoggedIn = false;
          return this.isLoggedIn;
    }
    
      
    
    
  }

   logout(): void {
   	console.log("logout is performed")
     window.localStorage.removeItem('tokenSession');
   }
    
}
