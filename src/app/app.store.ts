import { NgModule } from '@angular/core'
import { StoreModule } from '@ngrx/store'
import { StoreDevtoolsModule } from '@ngrx/store-devtools'

import {StoreLogMonitorModule, useLogMonitor} from '@ngrx/store-log-monitor';

import { EffectsModule } from '@ngrx/effects'

import { app, AppEffects } from './state'
import { auth, AuthEffects } from './auth/state'
import { booking, BookingEffects } from './pages/Bookings/state'
import { competition, CompetitionEffects } from './pages/Competitions/state'
import { dashBoard, DashboardEffects } from './pages/dashboard/state'

@NgModule({
  imports: [
    StoreModule.provideStore({
      app,
      auth,
      booking,
      competition,
      dashBoard
    }),

    StoreDevtoolsModule.instrumentStore({
      monitor: useLogMonitor({
        visible: false,
        position: 'right'
      })
    }),

    EffectsModule.run(AppEffects),
    EffectsModule.run(AuthEffects),
    EffectsModule.run(BookingEffects),
    EffectsModule.run(CompetitionEffects),
    EffectsModule.run(DashboardEffects)
  ],
})
export class AppStoreModule {}
