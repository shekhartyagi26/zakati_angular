export const environment = {
  production: false,
  APP: {
        
        API_URL : 'http://54.202.138.156/zakati/public',
        LOGIN_API:'/api/adminLogin',
        LOGOUT_API:'/api/logout',
        FORGOT_PASSWORD:'/api/v1/user/forgotPassword',
        GET_ALL_BOOKING:'/api/getOrganizationList',
        GET_ALL_COMPETITIONS:'/api/getUserList',
        GET_USER_BY_ID:'/admin/getCustomerWithId',
        BLOCK_USER_BY_ID: '/api/blockUser',
        APPROVE_USER_BY_ID:'/api/approveOrganization',
        DELETE_USER_BY_ID:'/admin/deleteUser',
        EDIT_USER_BY_ID: '/api/adminProfile' ,
      
        GET_DASHBOARD_COUNT:'/admin/getAllUsersRegisteredToday',
          GET_USER_REPORT_MONTHLY:'/admin/geUserReportMonthly',
            CREATE_COMPETITION:'/admin/createCompetition',
            VERIFY_TOKEN:'/api/v1/user/verifyResetToken',
            RESET_API:'/api/changePassword',
             ADD_ITEM:'/api/v1/category',
             DELETE_ITEM_BY_ID:'/api/v1/category/',
             GET_ALL_USER:'/api/causeListByUser',
        
  }
};
