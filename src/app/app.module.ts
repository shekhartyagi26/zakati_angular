import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { removeNgStyles, createNewHosts, createInputTransfer } from '@angularclass/hmr';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
//import {PaginatePipe, PaginationService} from 'ng2-pagination';
//import {ToastModule} from 'ng2-toastr/ng2-toastr';
import { AdminAuthModule } from './auth/auth.module';
import { TranslateService } from '@ngx-translate/core';
import { AUTH_PROVIDERS, provideAuth ,JwtHelper} from 'angular2-jwt';
import { AuthGuard } from './theme/services/authService/authGuard.service';
import { AuthService } from './theme/services/authService/auth.service';
import { common_service } from './services/common.service';

import { competition_service} from './services/competitionService/competition.service';
import { dashboard_service } from './services/dashboardService/dashboard.service';
//import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {NgxPaginationModule} from 'ngx-pagination'; //

//import { storeuserInfoService } from './theme/services/storeService/storeUserInfo.service';

/*
 * Platform and Environment providers/directives/pipes
 */
import { ENV_PROVIDERS } from './environment';
import { routing } from './app.routing';

// App is our top level component
import { App } from './app.component';
import { AppState, InternalStateType } from './app.service';
import { AppStoreModule } from './app.store'

//redux import
// import {StoreModule} from '@ngrx/store';
// import {StoreDevtoolsModule} from '@ngrx/store-devtools';
// import {StoreLogMonitorModule, useLogMonitor} from '@ngrx/store-log-monitor';
// import {userInfo} from './theme/stores/userInfo.store';

import { GlobalState } from './global.state';
import { NgaModule } from './theme/nga.module';
import { PagesModule } from './pages/pages.module';

import { ToastrModule } from 'ngx-toastr';
import { CommonModule } from '@angular/common';
import { booking_service } from './services/bookingService/booking.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PublicPageModule } from  './publicPages/public-pages.module';
import { multilingualModule } from  './multilingual/multilingual.module';
// import { DeleteCustomerModal } from './pages/Bookings/components/delete-item-modal/delete-item-modal.component';
import { LogoutModal } from './theme/components/logout-modal/logout-modal.component';
import { ChartsModule } from 'ng2-charts/ng2-charts';


// Application wide providers
const APP_PROVIDERS = [
  AppState,
  GlobalState
];

export type StoreType = {
  state: InternalStateType,
  restoreInputValues: () => void,
  disposeOldHosts: () => void
};

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [App],
  declarations: [
    App,
   
   LogoutModal
  ],
   entryComponents: [
   
   
   LogoutModal
  ],
  imports: [ // import Angular's modules
    BrowserModule,
    //ToastModule.forRoot(),
    HttpModule,
    RouterModule,
    FormsModule,
    AdminAuthModule,
    BrowserAnimationsModule,
    CommonModule,
    ToastrModule.forRoot(), // ToastrModule added
    // StoreModule.provideStore({userInfo}),
    // StoreDevtoolsModule.instrumentStore({
    //   monitor: useLogMonitor({
    //     visible: false,
    //     position: 'right'
    //   })
    // }),
    // StoreLogMonitorModule,
    AppStoreModule,
    ReactiveFormsModule,
    NgaModule.forRoot(),
    NgbModule.forRoot(),
    
    PagesModule,
    PublicPageModule,
    NgxPaginationModule,
     ChartsModule,
    routing
  ],
  providers: [ // expose our Services and Providers into Angular's dependency injection
    ENV_PROVIDERS,
    APP_PROVIDERS,
    AuthService,
    booking_service,
    competition_service,
    common_service,
    dashboard_service,
    AuthGuard,
    JwtHelper,
    provideAuth({
      headerName: 'Authorization',
      tokenName: 'token',
      tokenGetter: (() => sessionStorage.getItem('token')),
      globalHeaders: [{'Content-Type':'application/json'}],
      noJwtError: true
    })
  ]
})

export class AppModule {

  constructor(public appRef: ApplicationRef,
              public appState: AppState) {
  }

  hmrOnInit(store: StoreType) {
    if (!store || !store.state) return;
    console.log('HMR store', JSON.stringify(store, null, 2));
    // set state
    this.appState._state = store.state;
    // set input values
    if ('restoreInputValues' in store) {
      let restoreInputValues = store.restoreInputValues;
      setTimeout(restoreInputValues);
    }
    this.appRef.tick();
    delete store.state;
    delete store.restoreInputValues;
  }

  hmrOnDestroy(store: StoreType) {
    const cmpLocation = this.appRef.components.map(cmp => cmp.location.nativeElement);
    // save state
    const state = this.appState._state;
    store.state = state;
    // recreate root elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // save input values
    store.restoreInputValues = createInputTransfer();
    // remove styles
    removeNgStyles();
  }

  hmrAfterDestroy(store: StoreType) {
    // display new elements
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }
}
