import { Component, VERSION } from '@angular/core';
import {
    FormGroup,
    AbstractControl,
    FormBuilder,
    Validators,
    FormControl
} from '@angular/forms';
import { Observable } from "rxjs/Observable";
import { user_service } from '../../../auth/service/userService/user_service';
import { BaThemeSpinner } from '../../../theme/services';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ForgotModal } from '../ui/components/modals/forgot-modal/forgot-modal.component';
import * as auth from '../../../auth/state/auth.actions'
import { EmailValidator } from '../../../theme/validators';
import { User } from '../../../auth/model/user.model'
import {
    trigger,
    state,
    style,
    animate,
    transition,
    keyframes
} from '@angular/animations';

import 'style-loader!./forgot-password.scss';

// TOAST
import { ToastrService , ToastrConfig} from 'ngx-toastr';
import { cloneDeep, random } from 'lodash';
const types = ['success', 'error', 'info', 'warning'];
////////

@Component({

    selector: 'forgot-password',
    templateUrl: './forgot-password.html',

    animations: [
        trigger('flyInOut', [
            state('in', style({
                transform: 'translateX(0)'
            })),
            transition('void => *', [
                animate(700, keyframes([
                    style({
                        opacity: 0,
                        transform: 'translateX(-100%)',
                        offset: 0
                    }),
                    style({
                        opacity: 1,
                        transform: 'translateX(15px)',
                        offset: 0.3
                    }),
                    style({
                        opacity: 1,
                        transform: 'translateX(0)',
                        offset: 1.0
                    })
                ]))
            ]),


            transition('* => void', [
                animate(700, keyframes([
                    style({
                        opacity: 1,
                        transform: 'translateX(0)',
                        offset: 0
                    }),
                    style({
                        opacity: 1,
                        transform: 'translateX(-15px)',
                        offset: 0.7
                    }),
                    style({
                        opacity: 0,
                        transform: 'translateX(100%)',
                        offset: 1.0
                    })
                ]))
            ])
        ]),

        trigger('shrinkOut', [
            state('in', style({
                height: '*'
            })),
            transition('* => void', [
                style({
                    height: '*'
                }),
                animate(2500, style({
                    height: 0
                }))
            ])
        ])
    ]

})
export class ForgotPassword{
          options: ToastrConfig;
          title = '';
          type = types[0];
          message = '';

  version = VERSION;
  private lastInserted: number[] = [];



    public form: FormGroup;
    public email: AbstractControl;
  
    public submitted: boolean = false;

    public domains: any[]
    public settings: any;
    user = new User();


    constructor(fb: FormBuilder,
        private translate: TranslateService,
        private user_service: user_service,
        private baThemeSpinner: BaThemeSpinner,
        private store: Store < any > ,
        private toastrService: ToastrService,
        private modalService: NgbModal, 
    ) {
        // TOAST
        this.options = this.toastrService.toastrConfig;
        ////////

        this.translate.use('en');



        this.form = fb.group({
            email: ['', Validators.compose([
                Validators.required,
                this.isEmail
            ])],
            
        });

        

        this.email = this.form.controls['email'];
 

        this.store
            .select('app')
            .subscribe((res: any) => {
                //console.log(res)
                //this.domains = res.domains
                this.settings = res.settings
                console.log(res);
          

               
                if (this.settings.nodeEnv === 'development') {
                    // this.credentials.email = 'admin@example.com'
                    // this.credentials.password = 'password'
                }
            })
    }

    isEmail(control: FormControl): {
        [s: string]: boolean
    } {
        console.log(control.value);
        if (control.value) {
            if (!control.value.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
                return {
                    noEmail: true
                };
            }
        }

    }



    onSubmit(values: Object) {
        event.preventDefault();
        this.submitted = true;
      
        if (this.form.valid) {
            var data = {
                emailOrPhone: this.user.email,
            }
            // var fd=new FormData();
            //     fd.append('emailorPhone',this.user.email);
            //this.baThemeSpinner.show();
            this.store.dispatch({
                type: auth.ActionTypes.AUTH_FORGOT_PASSWORD,
                payload: data
            })
        }
    }

    // TOAST
    openToast() {
        let m = 'amar';
        let t = 'amar';
        const opt = cloneDeep(this.options);
        const inserted = this.toastrService[this.type](m, t, opt);
        if (inserted) {
          this.lastInserted.push(inserted.toastId);
        }
        return inserted;
    }

    //////////

   staticModalShow() {
     console.log()
 
  }  
}
