
import {Component} from '@angular/core';
import {FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';

import { Observable } from "rxjs/Observable";
import { BaThemeSpinner } from '../../../theme/services';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';

import * as auth from '../../../auth/state/auth.actions';
// import * as lang from '../../../multilingual/state/lang.actions';

import {ActivatedRoute} from '@angular/router';

import { EqualPasswordsValidator } from '../../../theme/validators';
import { User } from '../../../auth/model/user.model';
// import { Language } from '../../../multilingual/model/lang.model.ts'

import 'style-loader!./reset.scss';
@Component({
  selector: 'reset-password',
  templateUrl: './reset.html'
})
export class ResetComponent {
  public form:FormGroup;
  public passwords:FormGroup;

  public password:AbstractControl;
  public confirmpassword:AbstractControl;

  public tokenVerified: boolean = false;
  public token: string;
  public responseMessage: String;
  public errorFlag: boolean = false;
  public successFlag: boolean = false;
  public submitted:boolean = false;
  public sub;
  public id:string;

  user = new User();

  constructor(fb:FormBuilder, private route: ActivatedRoute, private store: Store < any >) {
    this.form = fb.group({
      'passwords': fb.group({
        'password': ['', Validators.compose([Validators.required, Validators.minLength(8)])],
        'confirmpassword': ['', Validators.compose([Validators.required, Validators.minLength(8)])]
      }, {validator: EqualPasswordsValidator.validate('password', 'confirmpassword')})
    });

    this.sub = this.route.params.subscribe(params => {
            this.id = params['id']; // (+) converts string 'id' to a number
            console.log("GOT ID", this.id);
        });

    this.passwords = <FormGroup> this.form.controls['passwords'];
    
    this.password = this.passwords.controls['password'];
    this.confirmpassword = this.passwords.controls['confirmpassword'];

    // START :: STORE 
    this.store
      .select('auth')
      .subscribe((res: any) => {
        let data = res;
        console.log(res);
        
        if(data.hasOwnProperty('verify_token_success_payload')){
          
          if(data.verify_token_success_payload.status == 200){
            this.tokenVerified = true;
          }else{
            this.responseMessage = data.verify_token_success_payload.message;
          }
          delete data["verify_token_success_payload"]; 
        } else if(data.hasOwnProperty('reset_pass_success_payload')){
          if(data.reset_pass_success_payload.status == 200){
            this.errorFlag = false;
            this.successFlag = true;
            this.responseMessage = data.reset_pass_success_payload.message;
          } else {
            this.errorFlag = true;
            setTimeout(() => {
              this.errorFlag = false;
            }, 3000);
            this.successFlag = false;
            this.responseMessage = data.reset_pass_success_payload.message;
          }
          delete data["reset_pass_success_payload"]; 
        }
      });
    // END :: STORE
  }

  ngOnInit() {
    
    // this.token = this.route.snapshot.queryParams["resetToken"];
    //   this.authenticateToken(this.token);
    //   sessionStorage.setItem("resetToken", this.token);
  }

  authenticateToken(token){
  
    var data = {
      'resetToken': token,
      
     
     
    //  'password': this.user.password,
    }
    this.store.dispatch({
      type: auth.ActionTypes.AUTH_FORGOT_PASS_TOKEN,
      payload: data
    })
  }
  
     onSubmit(values: Object) {
        event.preventDefault();
        this.submitted = true;
        let token = sessionStorage.getItem('resetToken');
        if (this.form.valid) {
            var data = {
               "password": this.user.password,
               "resetToken": this.id,
              
               
            }
        
            // this.baThemeSpinner.show();
            this.store.dispatch({
                type: auth.ActionTypes.AUTH_RESET_PASSWORD,
                payload: data
            })

        }
    }
}

