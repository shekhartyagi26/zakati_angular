import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AllBookings } from '../../../../Bookings/components/AllBookings/all-bookings.component';
import { ForgotPassword } from '../../../../forgotpassword/forgot-password.component';
import {FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import { Store } from '@ngrx/store';
import * as booking from '../../../../Bookings/state/booking.actions';

@Component({
  selector: 'add-service-modal',
  styleUrls: [('./forgot-modal.component.scss')],
  templateUrl: './forgot-modal.component.html',
  providers: [ForgotPassword]
})


export class ForgotModal implements OnInit {

  public documentTypeList: string;
  public uploadFlag: boolean = false;
  public form:FormGroup;
  
   customerData=[];
   public name:AbstractControl;
    public page = 1;
  public limit = 4;
  
  // modalHeader: string;
  // modalContent: string = `Lorem ipsum dolor sit amet,
  //  consectetuer adipiscing elit, sed diam nonummy
  //  nibh euismod tincidunt ut laoreet dolore magna aliquam
  //  erat volutpat. Ut wisi enim ad minim veniam, quis
  //  nostrud exerci tation ullamcorper suscipit lobortis
  //  nisl ut aliquip ex ea commodo consequat.`;

 
    

  constructor(fb:FormBuilder,private activeModal: NgbActiveModal,private forgot:ForgotPassword,private store: Store<any>,) {
  }
//          console.log(res)
           
//           if(res.message=="Success")
//           {
//   


  //   //  fd.append('name','pratosh');
  //   //  fd.append('password','pratosh');
  //   //  fd.append('email','pratosh');
  //   //  fd.append('primaryMobile','pratosh');
     
  //    for(let prop in data){
  //     // console.log('prop-->',prop,'val--->',data[prop])
  //     fd.append(prop,data[prop]);
  //    }
    //  this.baThemeSpinner.show();
    //  //console.log(auth.ActionTypes.AUTH_LOGIN)
     
  

   ngOnInit() {
   }  

  

  
  

  
  

  closeModal() {
    this.activeModal.close();
    
  }
}
