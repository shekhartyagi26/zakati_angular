import { Component, VERSION } from '@angular/core';
import {
    FormGroup,
    AbstractControl,
    FormBuilder,
    Validators,
    FormControl
} from '@angular/forms';
import { Observable } from "rxjs/Observable";
import { user_service } from '../../../auth/service/userService/user_service';
import { BaThemeSpinner } from '../../../theme/services';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import * as auth from '../../../auth/state/auth.actions'
import { EmailValidator } from '../../../theme/validators';
import { User } from '../../../auth/model/user.model';
import { Router } from '@angular/router';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    keyframes
} from '@angular/animations';
import { LoginModal } from '../loginModal/login-modal.component';
import 'style-loader!./login.scss';

// TOAST
import { ToastrService , ToastrConfig} from 'ngx-toastr';
import { cloneDeep, random } from 'lodash';
const types = ['success', 'error', 'info', 'warning'];
////////

@Component({

    selector: 'login',
    templateUrl: './login.html',

    // animations: [
    //     trigger('flyInOut', [
    //         state('in', style({
    //             transform: 'translateX(0)'
    //         })),
    //         transition('void => *', [
    //             animate(700, keyframes([
    //                 style({
    //                     opacity: 0,
    //                     transform: 'translateX(-100%)',
    //                     offset: 0
    //                 }),
    //                 style({
    //                     opacity: 1,
    //                     transform: 'translateX(15px)',
    //                     offset: 0.3
    //                 }),
    //                 style({
    //                     opacity: 1,
    //                     transform: 'translateX(0)',
    //                     offset: 1.0
    //                 })
    //             ]))
    //         ]),


    //         transition('* => void', [
    //             animate(700, keyframes([
    //                 style({
    //                     opacity: 1,
    //                     transform: 'translateX(0)',
    //                     offset: 0
    //                 }),
    //                 style({
    //                     opacity: 1,
    //                     transform: 'translateX(-15px)',
    //                     offset: 0.7
    //                 }),
    //                 style({
    //                     opacity: 0,
    //                     transform: 'translateX(100%)',
    //                     offset: 1.0
    //                 })
    //             ]))
    //         ])
    //     ]),

    //     trigger('shrinkOut', [
    //         state('in', style({
    //             height: '*'
    //         })),
    //         transition('* => void', [
    //             style({
    //                 height: '*'
    //             }),
    //             animate(2500, style({
    //                 height: 0
    //             }))
    //         ])
    //     ])
    // ]

})
export class Login {
          options: ToastrConfig;
          title = '';
          type = types[0];
          message = '';
          erm;

  version = VERSION;
  private lastInserted: number[] = [];



    public form: FormGroup;
    public email: AbstractControl;
    public password: AbstractControl;
    public submitted: boolean = false;
    public remember:boolean=false;
    public session;
    public domains: any[]
    public settings: any;
    user = new User();


    constructor(fb: FormBuilder,
        private translate: TranslateService,
        private user_service: user_service,
        private baThemeSpinner: BaThemeSpinner,
        private store: Store < any > ,
        private toastrService: ToastrService, // TOAST
         private modalService: NgbModal,
          private router: Router,
    ) {
        // TOAST
        this.options = this.toastrService.toastrConfig;
        ////////

        this.translate.use('en');


 
        this.form = fb.group({
            email: ['', Validators.compose([
             
                this.isEmail
            ])],
            password: ['', Validators.compose([
                Validators.required, Validators.minLength(4)
                
            ])],
            remember:['']
       
        });

        
       
        this.email = this.form.controls['email'];
        this.password = this.form.controls['password'];
        this.session=sessionStorage.getItem('email1');
        this.store
            .select('auth')
            .subscribe((res: any) => {
                console.log(res)
                if(res.er)
          {
            console.log(res.er)
            this.erm=res.er.message
            console.log(this.erm)
          
          }
    //console.log(res.message)
            
                //this.domains = res.domains
                
            })
    }

    isEmail(control: FormControl): {
        [s: string]: boolean
    } {
        console.log(control.value);
        if (control.value) {
            if (!control.value.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
                return {
                    noEmail: true
                };
            }
        }

    }



    onSubmit(values: Object) {


        // this.router.navigate(['/pages/dashboard'])
        event.preventDefault();
        this.submitted = true;

        




       
       
                     
        if (this.form.valid) {
              if(this.remember)
            {

               sessionStorage.setItem('email1',this.user.email);
                 

            }
            else{

                sessionStorage.removeItem('email1');
            }
          
      
            
            var data = {
                email: this.user.email || this.session,
              
                password: this.user.password,
               
            }


           
           

            this.baThemeSpinner.show();
            this.store.dispatch({
                type: auth.ActionTypes.AUTH_LOGIN,
                payload: data
            })
 
        }

       
          
    }

    // TOAST
    openToast() {
        let m = 'amar';
        let t = 'amar';
        const opt = cloneDeep(this.options);
        const inserted = this.toastrService[this.type](m, t, opt);
        if (inserted) {
          this.lastInserted.push(inserted.toastId);
        }
        return inserted;
    }
    //////////
    
}
