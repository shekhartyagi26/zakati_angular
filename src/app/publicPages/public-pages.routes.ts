import { Routes,RouterModule } from '@angular/router'
import { ModuleWithProviders } from '@angular/core';
import { Login } from './components/login/login.component'
import { LogoutComponent } from './components/logout/logout.component'
import { RecoverComponent } from './components/recover/recover.component'
import { Register } from './components/register/register.component'
import { ForgotPassword } from './components/forgotpassword/forgot-password.component'
import { ResetComponent } from './components/reset/reset.component'
import { AuthGuard } from '../auth/service/authGuardPublicPages/authGuardPublic.service';

 export const routes: Routes = [
{
      
  
     path: 'login',
     component:Login,
     canActivate: [AuthGuard]
  },
   {
    path: 'register',
    component:Register,
    canActivate: [AuthGuard]
   },
   {
    path: 'forgot-password',
    component:ForgotPassword,
  
   },

   {
    path:'resetPassword/:id',
    component:ResetComponent,

   }


//   children: [
//   	{ path: '', redirectTo: 'login', pathMatch: 'full' },
//     { path: 'login', loadChildren: 'app/auth/components/login/login.module#LoginModule' },
//     { path: 'logout', component: LogoutComponent },
//     { path: 'password-recover', component: RecoverComponent },
//     { path: 'password-reset', component: ResetComponent },
//     { path: 'register', component: Register },
//   ],}
  ]

export const routing = RouterModule.forChild(routes);
