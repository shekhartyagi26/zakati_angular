import { Component } from '@angular/core';
import { Routes } from '@angular/router';


@Component({
  selector: 'publicpages',
  template: `
   
    
        <router-outlet></router-outlet>
     
    `
})
export class PublicPages {

  constructor() {
  }

  
}
