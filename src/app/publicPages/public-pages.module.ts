import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule ,ReactiveFormsModule} from '@angular/forms';
import { routing }       from './public-pages.routes';
import { PublicPages } from './public-pages.component';
import { AppTranslationModule } from '../app.translation.module';
import { RouterModule } from '@angular/router'
//import { ColmenaUiModule } from '@colmena/colmena-angular-ui'
import { ForgotPassword } from './components/forgotpassword/forgot-password.component'
import { Login } from './components/login/login.component'
import { LogoutComponent } from './components/logout/logout.component'
import { RecoverComponent } from './components/recover/recover.component'
import { Register } from './components/register/register.component'
import { ResetComponent } from './components/reset/reset.component'
import { ForgotModal } from './components/ui/components/modals/forgot-modal/forgot-modal.component';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { NgaModule } from '../theme/nga.module';
import { Modals } from '../ui/components/modals/modals.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    routing,
    AppTranslationModule,
     NgaModule,
  
    routing,
    NgbModalModule,
    //ColmenaUiModule,
  ],
  declarations: [
    PublicPages,
    Login,
    LogoutComponent,
    RecoverComponent,
    Register,
    
     //NgbActiveModal,
    ResetComponent,
    ForgotPassword,
    ForgotModal
  ],
  entryComponents: [
 
    ForgotModal
  ],
  
})
export class PublicPageModule { }
