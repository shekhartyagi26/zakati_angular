import { NgModule, CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA}      from '@angular/core';
import { CommonModule }  from '@angular/common';
//import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
//pipes
import {NgPipesModule} from 'ngx-pipes';
import { CreateCompetitionModal  } from './components/create-competition-modal/create-competition-modal.component';
import { Modals } from '../ui/components/modals/modals.component';
import { NgaModule } from '../../theme/nga.module';
import { FormsModule ,ReactiveFormsModule} from '@angular/forms'
import {CalendarModule} from 'primeng/primeng';
import { routing }       from './competitions.routing';
import { Competitions } from './competitions.component';
//import { PastBookings } from './components/PastBookings/past-bookings.component';
import {AllCompetitions } from './components/AllCompetitions/all-competitions.component';
import {MdNativeDateModule,MaterialModule} from '@angular/material';
import {NgxPaginationModule} from 'ngx-pagination'; 
import { DataTableModule } from "angular2-datatable";
import { DataFilterPipe }   from './components/AllCompetitions/data-filter.pipe';
import { DataFilterPipe1 }   from './components/AllCompetitions/data-filter1.pipe';
// import { TooltipModule } from 'ngx-bootstrap';






@NgModule({
  imports: [
    CommonModule,
    DataTableModule, 
    FormsModule,
    ReactiveFormsModule,
    NgaModule,
    NgxPaginationModule,
    routing,
    NgbModalModule,
    NgPipesModule,
    MdNativeDateModule,MaterialModule,
    CalendarModule
  ],
  declarations: [
    Competitions,
    AllCompetitions,
  
     CreateCompetitionModal,
     DataFilterPipe,
     DataFilterPipe1 
  ],
    schemas:[CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA],
  entryComponents: [
    
     CreateCompetitionModal,
  ],

})
export class CompetitionsModule {}
