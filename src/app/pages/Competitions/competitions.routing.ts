import { Routes, RouterModule }  from '@angular/router';

import {Competitions } from './bookings.component';
import { AllCompetitions} from './components/AllCompetitions/all-competitions.component';

const routes: Routes = [
  {
    path: '',
    component: AllCompetitions,
    children: [

       { path: 'Competition', component:AllCompetitions },
    ]
  }
];

export const routing = RouterModule.forChild(routes);
