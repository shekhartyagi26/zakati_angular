import {Component} from '@angular/core';

@Component({
  selector: 'Competitions',
  template: `<router-outlet></router-outlet>`
})
export class Competitions {

  constructor() {
  }

  ngOnInit() {
  }

}
