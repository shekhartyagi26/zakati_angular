import { get } from 'lodash'
import { Injectable } from '@angular/core'
import { Effect, Actions } from '@ngrx/effects'
import { Action, Store } from '@ngrx/store'
import { Observable } from 'rxjs/Observable'
import { competition_service} from '../../../services/competitionService/competition.service';

import { Router } from '@angular/router';
import { ToastrService , ToastrConfig} from 'ngx-toastr';
import { cloneDeep, random } from 'lodash';
const types = ['success', 'error', 'info', 'warning'];
import * as competition from './competition.actions'
import { CustomerModal } from '../../Bookings/components/customer-modal/cusotomer-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Injectable()
export class CompetitionEffects {
  private modalService: NgbModal
   options: ToastrConfig;
          title = '';
         // type = types[0];
          message = '';

  
  private lastInserted: number[] = [];



   @Effect({ dispatch: false })
  getAllCompetitions$ = this.actions$
    .ofType('APP_GETALL_COMPETITIONS')
    .do((action) => {
      this.competition_service.getAllCompetitions(action.payload).subscribe((result) => {

          if(result.message)
          {
console.log("here")
console.log(result)

   
    //         const activeModal = this.modalService.open(CustomerModal, {size: 'lg'});
    // activeModal.componentInstance.modalHeader = 'Large Modal';
            this.store.dispatch(new competition.AppCompetitionDetailSuccess(result))
            
          }
      }
        
        , (error) => {
          console.log(error)
        }
      );
    });
    @Effect({ dispatch: false })
     createcompetition = this.actions$
    .ofType('APP_CREATE_COMPETITION')
 
    .do((action) => {
      this.competition_service.createcompetition(action.payload)
          .subscribe((result) => {
            //console.log("is result...............",result)
               if(result.message === 'Success'){
                 
    //    const activeModal = this.modalService.open(CustomerModal, {size: 'lg'});
    //  activeModal.componentInstance.modalHeader = 'Large Modal';
                 
      // this.store.dispatch(new competition.AppCreateCompetitionSuccess(result))
     

               }
          }
                , (error) => {
                  console.log(error)
                }
              );

    })
  @Effect({ dispatch: false })
      competition = this.actions$
    .ofType('APP_CREATE_COMPETITION_SUCCESS')
    
    .do((action) => {
         console.log(action);
   
    })

   
     @Effect({ dispatch: false })
    competitionDetailSuccess: Observable<Action> = this.actions$
      .ofType('APP_COMPETITION_DETAIL_SUCCESS')
      .do((action) => {
    console.log("vhgv")
  });
//    @Effect({ dispatch: false })
//   blockUser$: Observable<Action>= this.actions$
//     .ofType('APP_BLOCK_USER_BY_ID')
//     // FIXME: I miss UI-Routers $state.reload() - find an angular way of doing this
//     .do(action => {console.log("block user")
//   this.competition_service.blockUserById(action.payload)
// .subscribe((result) => {
//              console.log(result)
//               if(result.message)
//                {


//                  let m = result.message;
//               let t = 'Authentication';
//               const opt = cloneDeep(this.options);
//               const inserted = this.toastrService[types[0]](m, t, opt);
//               if (inserted) {
//                 this.lastInserted.push(inserted.toastId);
//               }


//               this.store.dispatch({ type: competition.ActionTypes.APP_GETALL_COMPETITIONS})
                
//               }

//               }
//             , (error) => {
//               console.log(error.message)
//              });
//     })


    constructor(
    private actions$: Actions,
    private store: Store<any>,
    private router: Router,
    private competition_service: competition_service,
    private toastrService: ToastrService,
  ) {
  }

}

