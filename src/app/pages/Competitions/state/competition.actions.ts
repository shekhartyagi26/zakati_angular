import { Action } from '@ngrx/store'

export const ActionTypes = {

  APP_GETALL_COMPETITIONS:       'APP_GETALL_COMPETITIONS',
  APP_COMPETITION_DETAIL:       'APP_COMPETITION_DETAIL',
  APP_COMPETITION_DETAIL_SUCCESS: 'APP_COMPETITION_DETAIL_SUCCESS',
  APP_CREATE_COMPETITION:        'APP_CREATE_COMPETITION',
  APP_CREATE_COMPETITION_SUCCESS: 'APP_CREATE_COMPETITION_SUCCESS',
    APP_BLOCK_USER_BY_ID:   'APP_BLOCK_USER_BY_ID',
 
}

type credentials = {
  skip:number;
  limit:number;
}
// type cre = {
//               competitionName: string;
//               competitionMessage: string,
//               prizeValue:string;
//               termsAndConditions:string;
//               startDateTime:Date;
//               endDateTime:Date;
               
//           }

export class AppGetAllCompetitions implements Action {
  type = ActionTypes.APP_GETALL_COMPETITIONS
  constructor(public payload: any) {
    console.log("kjhj")
  }
}

export class AppCompetitionDetail implements Action {
  type = ActionTypes.APP_COMPETITION_DETAIL
  constructor(public payload: credentials) {}
}

export class AppCompetitionDetailSuccess implements Action {
  type = ActionTypes.APP_COMPETITION_DETAIL_SUCCESS
  constructor(public payload: any) {}
}
export class AppCreateCompetitionSuccess implements Action {
  type = ActionTypes.APP_CREATE_COMPETITION_SUCCESS
  constructor(public payload:any) {}
}

export class AppCreateCompetition implements Action {
  type = ActionTypes.APP_CREATE_COMPETITION
  constructor(public payload:any) {
    console.log("bhjbh")
  }
}
// export class AppBlockUserById implements Action {
//   type = ActionTypes.APP_BLOCK_USER_BY_ID
//   constructor(public payload: credentials) {
    
//   }
// }



export type Actions
  =
  | AppGetAllCompetitions
  | AppCompetitionDetail
  | AppCompetitionDetailSuccess
  | AppCreateCompetition
  | AppCreateCompetitionSuccess
  // | AppBlockUserById
 
  





