import { Action, ActionReducer } from '@ngrx/store'

const initialState: any = {

  bookings:null,
  count:0,
  currentPage:0,
  limit:0,
  active:null
    
}

export const competition: ActionReducer<any> = (state = initialState, action: Action) => {
  switch (action.type) {


     case 'APP_GETALL_COMPETITIONS':
      return Object.assign({}, state,action.payload)

     case 'APP_COMPETITION_DETAIL':
      return Object.assign({}, state)
          case 'APP_CREATE_COMPETITION':
      return Object.assign({}, state)

     case 'APP_COMPETITION_DETAIL_SUCCESS':
      return Object.assign({},state,action.payload)

     case 'APP_BOOKING_DETAIL_AFTER_SUCCESS':
      return Object.assign({}, state,{bookingDevelopment:action.payload})

     case 'APP_GETALL_BOOKING_BY_ID':
      return Object.assign({}, state,{activeBooking:action.payload})

     case 'APP_GETALL_BOOKING_BY_ID_SUCCESS':
      return Object.assign({}, state,{bookingId:action.payload})

      case 'APP_CANCEL_BOOKING_BY_ID':
      return Object.assign({}, state,{activeBooking:action.payload})

      case 'APP_CANCEL_BOOKING_BY_ID_SUCCESS':
      return Object.assign({}, state,{activeBooking:action.payload})
      case 'APP_EDIT_USER_BY_ID':
      return Object.assign({}, state)
      case 'APP_EDIT_USER_BY_ID_SUCCESS':
      return Object.assign({}, state)
  // case 'APP_BLOCK_USER_BY_ID':
  //     return Object.assign({}, state,{active:action.payload}
  //    )

    default:
      return state
  }
}
