
import {Component,OnInit} from '@angular/core';
import { Store } from '@ngrx/store';
import * as competition from '../../state/competition.actions'
import * as booking from '../../../Bookings/state/booking.actions'
import {BaThemeSpinner} from '../../../../theme/services';
import { CreateCompetitionModal } from '../create-competition-modal/create-competition-modal.component'; 
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {Http} from "@angular/http";

import 'style-loader!./all-competitions.scss';

@Component({
  selector: 'allcompetitions',
  templateUrl: './all-competitions.html',
})
export class AllCompetitions implements OnInit {

  public page = 1;
  public limit = 25;
  public count:number;
 public competitionData=[];
  public pic=true;
  public myImgUrl:string='../../../../assets/img/default.png';
  public b:boolean=false;
  public c:boolean=true;
  //new declarations
    public data:any=[];
    public data1:any=[];
    public filterQuery = "";
    public rowsOnPage = 10;
    public sortBy = "email";
    public sortOrder = "asc";
    public isBlocked=0;


  constructor(private http: Http,private store: Store<any>,private modalService: NgbModal,
             
            ) {

    this.store
      .select('competition')
       .subscribe((res: any) => {
         console.log(res)
     
          if(res.message)
          {
            console.log("competition",res);
            this.data=res.result;

            // this.data1=res.result.blockStatus;
            // if(this.data.blockStatus==0)
            // {
                 
            //   //  this.data=1;
            //   //  alert(this.data1); 

            // }

            // else

            // {
            //     // this.data=0; 
            //     // alert(this.data1); 

            // }

//           this.competitionData = res.data.users;

              
          
// console.log(this.competitionData)
          }
          
       
         
       }
       )
      //  let obj=

      //          {

      //           'blockStatus':this.data

      //          }
             
         

      //Dispatch action on load ..
      this.store.dispatch({ type: competition.ActionTypes.APP_GETALL_COMPETITIONS })

  }



     block(data1, isBlocked){
    
       if(this.isBlocked==0)
       {

         this.isBlocked=1;

        //  alert(this.isBlocked);

       }

       else{

        this.isBlocked=0;

        // alert(this.isBlocked);

       }

      //  console.log('blockedstatus',this.isBlocked);
      
    let obj = {
      'userId': data1.id,
    };

    
     
    this.store.dispatch({
      type     : booking.ActionTypes.APP_BLOCK_USER_BY_ID,
      payload  : obj
    });
    
     this.updateCustomerBlock(data1,isBlocked);
     }
      updateCustomerBlock(data1, isBlocked) {
    data1.isBlocked = data1.isBlocked
    
 }
 

  //  public sortByWordLength = (a: any) => {
  //       return a.city.length;
  //  }

  //  public sortByWordLength2 = (a: any) => {
  //       return a.country.length;
  //  }
  ngOnInit(): void {
        // this.http.get("app/pages/Competitions/components/AllCompetitions/all-data.json")
        //     .subscribe((data)=> {
        //         setTimeout(()=> {
        //             this.data = data.json();
        //         }, 1000);
        //     });
    }


//  edit(id){
//     let payload={
        
//           id:id
//     }
     
//  }
//   lgModalShow() {
//     const activeModal = this.modalService.open(CreateCompetitionModal, {size: 'lg'});
//     activeModal.componentInstance.modalHeader = 'Large Modal';
//   }  

// pageChanged(page){
//     this.page=page;
//     //this.baThemeSpinner.show();
//     // this.store.dispatch({ type:competition.ActionTypes.APP_GETALL_COMPETITIONS, payload: {currentPage:this.page,limit:this.limit,type:"all"} })

//   }
}