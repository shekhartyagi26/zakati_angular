import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import * as competition from '../../state/competition.actions'
import {
    FormGroup,
    AbstractControl,
    FormBuilder,
    Validators,
    FormControl
} from '@angular/forms';
import {User} from '../../../../auth/model/user.model'

@Component({
  selector: 'create-competition-modal',
  templateUrl: './create-competition-modal.html'
  
  
})

export class CreateCompetitionModal {

  modalHeader: string;
 public submitted: boolean = false;
  public form: FormGroup;
    public competition: AbstractControl;
    public message: AbstractControl;
    public prize: AbstractControl;
    public tc: AbstractControl;
    public brand: AbstractControl;
    public description: AbstractControl;
    public date:Date;
    public d:Date;
    public date2:Date;
      public page = 1;
  public limit = 20;
  public count:number;
    // user = new User();
  constructor(fb: FormBuilder,
    private activeModal: NgbActiveModal,
    private store: Store<any>
  )
  
  {this.form = fb.group({
      'competition': ['', [<any>Validators.required]],
      'message': ['', [<any>Validators.required,<any>Validators.minLength(5)]],
      'prize':[''],
      'tc':[''],
      'date':['',[<any>Validators.required]],
      'date2':['',[<any>Validators.required]],
      'brand':['',[<any>Validators.required]],
      'description':['',[<any>Validators.required]],

    });

           
    this.store
      .select('customer')
      .subscribe((res: any) => {
       
      });
  };
          
  onSubmit(values: Object) {
        event.preventDefault();
        this.submitted = true;
        console.log(values)
        { 
            var data = {
              competitionName: this.competition,
              competitionMessage: this.message,
              prizeValue:this.prize,
              termsAndConditions:this.tc,
              brand:this.brand,
            competitionDescription:this.description,
              startDateTime:this.date.toISOString(),
              endDateTime:this.date.toISOString()
               
          }
            console.log(data)
     var fd = new FormData();
      for(let prop in data){
    
      fd.append(prop,data[prop]);
      console.log(fd)

           
     }
  //  this.store.dispatch({
  //               type:competition.ActionTypes. APP_CREATE_COMPETITION,
  //                payload: fd
  //           })
  //      this.store.dispatch({ type: competition.ActionTypes.APP_GETALL_COMPETITIONS, payload: {currentPage:this.page,limit:this.limit} })  
          
        }
    }

  closeModal() {
    this.activeModal.close();
  }
}



