import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';

import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
//pipes
import {NgPipesModule} from 'ngx-pipes';

import { NgaModule } from '../../theme/nga.module';
import { FormsModule ,ReactiveFormsModule} from '@angular/forms'

import { routing }       from './organisation.routing';

//import { PastBookings } from './components/PastBookings/past-bookings.component';
import {AllOrganisation } from './components/AllOrganisation/allorg.component';




@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgaModule,
   
    routing,
    NgbModalModule,
    NgPipesModule
  ],
  declarations: [
    // OrganisationList,
    //PastBookings,
    AllOrganisation,
    //OnBookings,
   
  ],
 
})
export class OrganisationModule {}
