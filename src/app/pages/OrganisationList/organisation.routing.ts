import { Routes, RouterModule }  from '@angular/router';

import {OrganisationList} from './organisation.component';
//import { PastBookings } from './components/PastBookings/past-bookings.component';
import { AllOrganisation} from './components/AllOrganisation/allorg.component';
//import { OnBookings} from './components/OnBookings/on-bookings.component';
//import { ByIdDetails } from './components/byIdDetails/byId-details.component';
//import { CancelBooking } from './components/CancelBooking/cancel-booking.component';


// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component:  AllOrganisation,
    children: [
      //{ path: 'pastbookings', component:PastBookings  },
       { path: 'AllOrganisation', component: AllOrganisation },
       //{ path: 'onbookings', component:OnBookings }
    ]
  }
];

export const routing = RouterModule.forChild(routes);
