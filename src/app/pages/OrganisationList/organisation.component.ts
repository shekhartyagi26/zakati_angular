import {Component} from '@angular/core';

@Component({
  selector: 'Organisation',
  template: `<router-outlet></router-outlet>`
})
export class OrganisationList {

  constructor() {
  }

  ngOnInit() {
  }

}
