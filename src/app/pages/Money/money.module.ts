import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
//import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
//pipes
import {NgPipesModule} from 'ngx-pipes';
// import { AddItemModal  } from './components/add-item-modal/add-item-modal.component';
import { Modals } from '../ui/components/modals/modals.component';
import { NgaModule } from '../../theme/nga.module';
import { FormsModule ,ReactiveFormsModule} from '@angular/forms'

import { routing }       from './money.routing';
import { Money } from './money.component';
// import { PastBookings } from './components/PastBookings/past-bookings.component';
import { MoneyOrg } from './components/Org/org-bookings.component';
import { MoneyOther } from './components/Manage/other-bookings.component';
import {MoneyUser } from './components/User/user-bookings.component';
// import {AllBookings } from './components/AllBookings/all-bookings.component';

import {NgxPaginationModule} from 'ngx-pagination'; 

// import { CancelBookingModal } from './components/CancelBookingModal/cancel-booking-modal.component';
// import {DeleteCustomerModal} from './components/delete-item-modal/delete-item-modal.component';
// import { BookingModal } from './components/BookingModal/booking-modal.component';
// import { ChangePasswordModal } from './components/ChangePasswordModal/change-password.component';
// import {EditItemModal} from './components/edit-item-modal/edit-item-modal.component';
import { DataFilterPipe }   from './components/Org/data-filter.pipe';
import { DataTableModule } from "angular2-datatable";




@NgModule({
  imports: [
    DataTableModule,
  
   
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgaModule,
    NgxPaginationModule,
    routing,
    NgbModalModule,
    NgPipesModule
  ],
  declarations: [
      Money,
      MoneyOrg,
      MoneyOther,
     MoneyUser,
    // Bookings,
    // PastBookings,
    // AllBookings,
    // // OnBookings,
    // BookingModal,
    // CancelBookingModal,
    // DeleteCustomerModal,
    // ChangePasswordModal,

    //  AddItemModal,
    //  EditItemModal,
     DataFilterPipe
  ],
  entryComponents: [
//     CancelBookingModal,
//     BookingModal,
//     DeleteCustomerModal,
//     ChangePasswordModal,
   
//    AddItemModal,
//    EditItemModal
  ],

})
export class MoneyModule {}
