import { Routes, RouterModule }  from '@angular/router';

import { Money } from './money.component';
// import { PastBookings } from './components/PastBookings/past-bookings.component';
import {MoneyOrg} from './components/Org/org-bookings.component';
import {MoneyOther} from './components/Manage/other-bookings.component';
import {MoneyUser} from './components/User/user-bookings.component';

//import { ByIdDetails } from './components/byIdDetails/byId-details.component';
//import { CancelBooking } from './components/CancelBooking/cancel-booking.component';


// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: Money,
    children: [
      
      
       { path: 'moneyuser', component:MoneyUser},
         { path: 'moneyorg', component:MoneyOrg},
       { path: 'moneyother', component:MoneyOther}
    ]
  }
];

export const routing = RouterModule.forChild(routes);
