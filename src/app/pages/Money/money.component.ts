import {Component} from '@angular/core';

@Component({
  selector: 'money',
  template: `<router-outlet></router-outlet>`
})
export class Money {

  constructor() {
  }

  ngOnInit() {
  }

}
