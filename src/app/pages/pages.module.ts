import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';

import { routing }       from './pages.routing';
import { NgaModule } from '../theme/nga.module';
import { AppTranslationModule } from '../app.translation.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Pages } from './pages.component';
import {PastBookings} from'./Bookings/components/PastBookings/past-bookings.component';
import { ChartsModule } from 'ng2-charts/ng2-charts';
// import {OnBookings} from'./OnBookings/onbookings.component';
// import {UserBookings} from'./Bookings/components/UserBookings/user-bookings.component';

import { DataTableModule } from "angular2-datatable";
import { DataFilterPipe }   from '../pages/Bookings/components/PastBookings/data-filter.pipe';
import { FormsModule ,ReactiveFormsModule} from '@angular/forms'
@NgModule({
  imports: [CommonModule, NgaModule, AppTranslationModule,routing,NgbModule,DataTableModule,ChartsModule, FormsModule,ReactiveFormsModule  ],
  declarations: [Pages,PastBookings,DataFilterPipe]
})
export class PagesModule {
}
