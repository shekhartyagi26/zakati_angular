import {Component} from '@angular/core';

@Component({
  selector: 'Bookings',
  template: `<router-outlet></router-outlet>`
})
export class Bookings {

  constructor() {
  }

  ngOnInit() {
  }

}
