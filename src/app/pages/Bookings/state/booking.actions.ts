import { Action } from '@ngrx/store'

export const ActionTypes = {

  APP_GETALL_BOOKING:       'APP_GETALL_BOOKING',
  APP_BOOKING_DETAIL:       'APP_BOOKING_DETAIL',
  APP_BOOKING_DETAIL_SUCCESS:       'APP_BOOKING_DETAIL_SUCCESS',
  APP_BOOKING_DETAIL_AFTER_SUCCESS:   'APP_BOOKING_DETAIL_AFTER_SUCCESS',

  APP_GET_USER_BY_ID:   'APP_GET_USER_BY_ID',
  APP_GET_USER_BY_ID_SUCCESS:   'APP_USER_BY_ID_SUCCESS',


  APP_CANCEL_BOOKING_BY_ID: 'APP_CANCEL_BOOKING_BY_ID',
  APP_CANCEL_BOOKING_BY_ID_SUCCESS: 'APP_CANCEL_BOOKING_BY_ID_SUCCESS',

  APP_EDIT_USER_BY_ID:'APP_EDIT_USER_BY_ID',
  APP_EDIT_USER_BY_ID_SUCCESS:'APP_EDIT_USER_BY_ID_SUCCESS',
   APP_BLOCK_USER_BY_ID:   'APP_BLOCK_USER_BY_ID',
     APP_APPROVE_USER_BY_ID:   'APP_APPROVE_USER_BY_ID',
   APP_ADD_ITEM:        'APP_ADD_ITEM',
  APP_ADD_ITEM_SUCCESS: 'APP_ADD_ITEM_SUCCESS',
  DELETE_ITEM_RECORD: 'DELETE_ITEM_RECORD',
   DELETE_ITEM_RECORD_CONFIRM:'DELETE_ITEM_RECORD_CONFIRM',
   ADD_ID_RECORD: 'ADD_ID_RECORD',
    ADD_ID_RECORD_SUCCESS: 'ADD_ID_RECORD_SUCCESS',
     APP_GETALL_USER:'APP_GETALL_USER',
      APP_USER_DETAIL_SUCCESS:'APP_USER_DETAIL_SUCCESS',
      APP_GET_USER_ROLE:'  APP_GET_USER_ROLE'

}

type credentials = {
  skip:number;
  limit:number;
}
type cre ={}
export class AppGetAllBookingDetail implements Action {
  type = ActionTypes.APP_GETALL_BOOKING
  constructor(public payload: any) {
    console.log("kjhj")
  }
}

export class AppBookingDetail implements Action {
  type = ActionTypes.APP_BOOKING_DETAIL
  constructor(public payload: any) {}
}

export class AppBookingDetailSuccess implements Action {
  type = ActionTypes.APP_BOOKING_DETAIL_SUCCESS
  constructor(public payload: any) {}
}

export class AppGetUserById implements Action {
  
  type = ActionTypes.APP_GET_USER_BY_ID

  constructor(public payload:cre) {
    
  }
}
  export class AppGetUserByIdSuccess implements Action {
  type = ActionTypes.APP_GET_USER_BY_ID_SUCCESS
  constructor(public payload:any) {

    
  }
}
export class AppEditUserById implements Action {
  
  type = ActionTypes.APP_EDIT_USER_BY_ID

  constructor(public payload:any) {
    console.log("ffrd")
    
  }
}
  export class AppEditUserByIdSuccess implements Action {
  type = ActionTypes.APP_EDIT_USER_BY_ID_SUCCESS
  constructor(public payload:any) {

    
  }
}
export class AppBlockUserById implements Action {
  type = ActionTypes.APP_BLOCK_USER_BY_ID
  constructor(public payload: credentials) {
    
  }
}

export class AppApproveUserById implements Action {
  type = ActionTypes.APP_APPROVE_USER_BY_ID
  constructor(public payload: credentials) {
    
  }
}
  export class AppAddItemSuccess implements Action {
  type = ActionTypes.APP_ADD_ITEM_SUCCESS
  constructor(public payload: any) {}
}

export class AppAddItem implements Action {
  type = ActionTypes.APP_ADD_ITEM
  constructor(public payload: any) {
    console.log("bhjbh")
  }
  
}
export class AppDeleteItemRecord implements Action {
  type = ActionTypes.DELETE_ITEM_RECORD
  constructor() {}
  
}
export class AppDeleteItemConfirm implements Action {
  type = ActionTypes.DELETE_ITEM_RECORD_CONFIRM
  constructor() {}
}
export class AppIdRecord implements Action {
  type = ActionTypes.ADD_ID_RECORD
  constructor() {}
}
export class AppIdRecordSuccess implements Action {
  type = ActionTypes.ADD_ID_RECORD_SUCCESS
  constructor() {}
}

export class AppGetAllUserDetail implements Action {
  type = ActionTypes.APP_GETALL_USER
  constructor(public payload: any) {
    console.log("kjhj")
  }
}
export class AppUserDetailSuccess implements Action {
  type = ActionTypes.APP_USER_DETAIL_SUCCESS
  constructor(public payload: any) {}
}
export class AppGetUserDetail implements Action {
  type = ActionTypes.APP_GET_USER_ROLE
  constructor(public payload: any) {}
}

export type Actions
  =  AppGetAllBookingDetail
  | AppBookingDetail
  | AppBookingDetailSuccess
  | AppGetUserById
  | AppGetUserByIdSuccess
  | AppBlockUserById
  |AppApproveUserById
  | AppAddItemSuccess
  | AppAddItem
  |AppDeleteItemRecord
  |AppDeleteItemConfirm
  |AppEditUserById
  |AppEditUserByIdSuccess
  |AppIdRecord 
  |AppIdRecordSuccess
  |AppGetAllUserDetail
  |AppUserDetailSuccess
  |AppGetUserDetail
  





