import { get } from 'lodash'
import { Injectable } from '@angular/core'
import { Effect, Actions } from '@ngrx/effects'
import { Action, Store } from '@ngrx/store'
import { Observable } from 'rxjs/Observable'
import { booking_service } from '../../../services/bookingService/booking.service';
//import { AuthService  } from '../../theme/services/authService/auth.service';
import { Router } from '@angular/router';

import { ToastrService , ToastrConfig} from 'ngx-toastr';
import { cloneDeep, random } from 'lodash';
const types = ['success', 'error', 'info', 'warning'];
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


import * as booking from './booking.actions'
import * as competition from '../../Competitions/state/competition.actions'
import { CustomerModal } from '../../Bookings/components/customer-modal/cusotomer-modal.component';
import * as auth from '../../../auth/state/auth.actions'



@Injectable()
export class BookingEffects {
   options: ToastrConfig;
          title = '';
         // type = types[0];
          message = '';

  
  private lastInserted: number[] = [];



   @Effect({ dispatch: false })
  getAllBooking$ = this.actions$
    .ofType('APP_GETALL_BOOKING')
    .do((action) => {
        
      this.booking_service.getAllBooking(action.payload).subscribe((result) => {
        
          
     
          if(result.message)
          {
        // alert(result);
                                 
               
console.log("here")
console.log(result)
        
            //creating state payload for next action
            


            this.store.dispatch(new booking.AppBookingDetailSuccess(result))
          }
      }
        
        , (error) => {
          console.log(error)
        }
      );
    });

     @Effect({ dispatch: false })
  getAllUser$ = this.actions$
    .ofType('APP_GETALL_USER')
    .do((action) => {
        
      this.booking_service.getAllUser(action.payload).subscribe((result) => {
        
          
     
          if(result.message)
          {
        // alert(result);
                                 
               
console.log("here")
console.log(result)
        
            //creating state payload for next action
            


            this.store.dispatch(new booking.AppUserDetailSuccess(result))
          }
      }
        
        , (error) => {
          console.log(error)
        }
      );
    });
    @Effect({ dispatch: false })
     getUserById = this.actions$
    .ofType('APP_GET_USER_BY_ID')
   
    .do((action) => {
      

    })
  @Effect({ dispatch: false })
      getUserByIdSuccess = this.actions$
    .ofType('APP_GET_USER_BY_ID_SUCCESS')
  
    .do((action) => {
    
    })
      @Effect({ dispatch: false })
  editUserById$ = this.actions$
    .ofType('APP_EDIT_USER_BY_ID')
    .do((action) => {
      this.booking_service.editUser(action.payload).subscribe((result) => {

          if(result.message="Profile updated successfully")
          {
console.log("here")
console.log(result)
           let m = 'Profile Updated Successfully !';
              let t = 'Authentication';
              const opt = cloneDeep(this.options);
              const inserted = this.toastrService[types[0]](m, t, opt);
              if (inserted) {
                this.lastInserted.push(inserted.toastId);
              }
                 this.store.dispatch({ type: 'APP_GET_USER_ROLES', payload: result })
                  // this.store.dispatch({ type: booking.ActionTypes.APP_GET_USER_ROLE})

        // this.store.dispatch(new booking.AppEditUserByIdSuccess(result))  
            //  this.store.dispatch({ type: booking.ActionTypes.APP_GETALL_BOOKING, payload: {currentPage:1} })
       
          }
      }
        
        , (error) => {
          console.log(error)
        }
      );
    });


     @Effect({ dispatch: false })
  getUserInfo$ = this.actions$
    .ofType('APP_GET_USER_ROLES')
    .do(action => {
       
      console.log('APP_GET_USER_ROLES', action.payload)

      // this.userApi.info(action.payload.userId)
      //   .subscribe(res => {
      //     console.log('set roles')
      //     window.localStorage.setItem('roles', JSON.stringify(res.roles))
      //     this.store.dispatch({ type: 'AUTH_SET_ROLES', payload: res.roles})
      //   })

    })
     @Effect({ dispatch: false })
      editUserByIdSuccess = this.actions$
    .ofType('APP_EDIT_USER_BY_ID_SUCCESS')
  
    .do((action) => {
    
    })

  @Effect({ dispatch: false })
  blockUser$: Observable<Action>= this.actions$
    .ofType('APP_BLOCK_USER_BY_ID')
    // FIXME: I miss UI-Routers $state.reload() - find an angular way of doing this
    .do(action => {console.log("block user")
  this.booking_service.blockUserById(action.payload)
.subscribe((result) => {
             console.log(result)
              if(result.message)
               {


                 let m = result.message;
              let t = 'Authentication';
              const opt = cloneDeep(this.options);
              const inserted = this.toastrService[types[0]](m, t, opt);
              if (inserted) {
                this.lastInserted.push(inserted.toastId);
              }


              this.store.dispatch({ type: booking.ActionTypes.APP_GETALL_BOOKING})
               this.store.dispatch({ type: competition.ActionTypes.APP_GETALL_COMPETITIONS })
                
              }

              }
            , (error) => {
              console.log(error.message)
             });
    })
    
     @Effect({ dispatch: false })
  approveUser$: Observable<Action>= this.actions$
    .ofType('APP_APPROVE_USER_BY_ID')
    // FIXME: I miss UI-Routers $state.reload() - find an angular way of doing this
    .do(action => {console.log("APPROVE user")
  this.booking_service.approveUserById(action.payload)
.subscribe((result) => {
             console.log(result)
              if(result.message)
               {


                 let m = result.message;
              let t = 'Authentication';
              const opt = cloneDeep(this.options);
              const inserted = this.toastrService[types[0]](m, t, opt);
              if (inserted) {
                this.lastInserted.push(inserted.toastId);
              }
                
              }

              }
            , (error) => {
              console.log(error.message)
             });
    })




     @Effect({ dispatch: false })
    bookingDetailSuccess: Observable<Action> = this.actions$
      .ofType('APP_BOOKING_DETAIL_SUCCESS')
      .do((action) => {
        //console.log("Running inside APP_BOOKING_DETAIL_SUCCESS ")
        //this.store.dispatch(new booking.AppBookingDetailAfterSuccess())
    });
     @Effect({ dispatch: false })
    userDetailSuccess: Observable<Action> = this.actions$
      .ofType('APP_USER_DETAIL_SUCCESS')
      .do((action) => {
        //console.log("Running inside APP_BOOKING_DETAIL_SUCCESS ")
        //this.store.dispatch(new booking.AppBookingDetailAfterSuccess())
    });


 @Effect({ dispatch: false })
     additem = this.actions$
    .ofType('APP_ADD_ITEM')
 
    .do((action) => {
      this.booking_service.additem(action.payload)
          .subscribe((result) => {
            //console.log("is result...............",result)
               if(result.message){
                 
    //    const activeModal = this.modalService.open(CustomerModal, {size: 'lg'});
    //  activeModal.componentInstance.modalHeader = 'Large Modal';
                 
      // this.store.dispatch(new booking.AppAddItemSuccess(result))
      //  this.store.dispatch({
      //         type: booking.ActionTypes.APP_GETALL_BOOKING, 
      //       });
       this.store.dispatch({ type: booking.ActionTypes.APP_GETALL_BOOKING, payload: {currentPage:1} })  

               }
          }
                , (error) => {
                  console.log(error)
                }
              );

    })
  @Effect({ dispatch: false })
      additemsuccess = this.actions$
    .ofType('APP_ADD_ITEM_SUCCESS')
    
    .do((action) => {
         console.log(action);
   
    })


@Effect({ dispatch: false })
  deleteItemRecordConfirm$ = this.actions$
    .ofType('DELETE_ITEM_RECORD_CONFIRM')
    .do((action) => {

    });


  // SOFT DELETE CUSTOMER RECORD
  @Effect({ dispatch: false })
  deleteItemRecord$ = this.actions$
    .ofType('DELETE_ITEM_RECORD')
    // .withLatestFrom(this.store)
    .do((action) => {
      // let action = storeState[0];
      // let state = storeState[1].customer;
      this.booking_service
        .deleteCustomerRecord(action.payload)
        // console.log(action.payload);
        .subscribe((result) => {
          if (result.message) {
            // console.log("ashi....",action.payload)

              this.store.dispatch({ type: booking.ActionTypes.APP_GETALL_BOOKING, payload: {currentPage:1} })

            // this.store.dispatch({
            //   type: booking.ActionTypes.APP_GETALL_BOOKING, 
            // });
          }
        }
        , (error) => {
          console.log(error);
        });
    });
    @Effect({ dispatch: false })
      addid = this.actions$
    .ofType('ADD_ID_RECORD')
    
    .do((action) => {
         console.log(action);
          this.store.dispatch(new booking.AppIdRecordSuccess())
    })
    @Effect({ dispatch: false })
      addidsuccess = this.actions$
    .ofType('ADD_ID_RECORD_SUCCESS')
    
    .do((action) => {
         console.log(action);
   
    })

    constructor(
    private actions$: Actions,
    private store: Store<any>,
    private router: Router,
    private booking_service: booking_service,
    private toastrService: ToastrService,
    private modalService: NgbModal
  ) {
     this.options = this.toastrService.toastrConfig;
  }

}

