import { Action, ActionReducer } from '@ngrx/store'

const initialState: any = {

  bookings:null,
  count:0,
  currentPage:0,
  limit:0,
  active:null,
   activeCustomer: null,
    
}

export const booking: ActionReducer<any> = (state = initialState, action: Action) => {
  switch (action.type) {


     case 'APP_GETALL_BOOKING':
      return Object.assign({}, state,action.payload)

      case 'APP_GETALL_USER':
      return Object.assign({}, state,action.payload)


     case 'APP_BOOKING_DETAIL':
      return Object.assign({}, state,action.payload)

     case 'APP_BOOKING_DETAIL_SUCCESS':
      return Object.assign({},state,action.payload)

       case 'APP_USER_DETAIL_SUCCESS':
      return Object.assign({},state,action.payload)


     case 'APP_BOOKING_DETAIL_AFTER_SUCCESS':
      return Object.assign({}, state,{bookingDevelopment:action.payload})

      case 'APP_GET_USER_BY_ID':
      return Object.assign({}, state,{activeCustomer:action.payload})
      case 'APP_GET_USER_BY_ID_SUCCESS':
      return Object.assign({}, state,{activeCustomer:action.payload})
     
      

      case 'APP_EDIT_USER_BY_ID':
      return Object.assign({}, state,action.payload)
      case 'APP_EDIT_USER_BY_ID_SUCCESS':
      return Object.assign({},state,action.payload)

       case 'APP_BLOCK_USER_BY_ID':
      return Object.assign({}, state,{active:action.payload}
     )
     
       case 'APP_APPROVE_USER_BY_ID':
      return Object.assign({}, state,{active:action.payload}
     )


       case 'APP_ADD_ITEM':
      return Object.assign({}, state)
  case 'APP_ADD_ITEM_SUCCESS':
      return Object.assign({},action.payload)
        case 'DELETE_ITEM_RECORD_CONFIRM':
      return Object.assign({},state,  action.payload)

    case 'DELETE_ITEM_RECORD':
      return Object.assign({},state, action.payload
      )
      case 'ADD_ID_RECORD':
      return Object.assign({},state, {idrecord:action.payload}
      )
      case 'ADD_ID_RECORD_SUCCESS':
      return Object.assign({},state, action.payload
      )

       case 'APP_GET_USER_ROLES':
      return Object.assign({},state, action.payload
      )


    default:
      return state
  }
}
