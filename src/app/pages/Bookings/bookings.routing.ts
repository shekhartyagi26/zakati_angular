import { Routes, RouterModule }  from '@angular/router';

import {Bookings } from './bookings.component';
// import { PastBookings } from './components/PastBookings/past-bookings.component';
import { AllBookings} from './components/AllBookings/all-bookings.component';
import { OnBookings} from './components/OnBookings/on-bookings.component';
//import { ByIdDetails } from './components/byIdDetails/byId-details.component';
//import { CancelBooking } from './components/CancelBooking/cancel-booking.component';


// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: AllBookings,
    children: [
      // { path: 'pastbookings', component:PastBookings  },
       { path: 'Booking', component:AllBookings },
      //  { path: 'onbookings', component:OnBookings }
    ]
  }
];

export const routing = RouterModule.forChild(routes);
