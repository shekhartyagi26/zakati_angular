import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
//import { PipeTransform, Pipe } from '@angular/core';

@Component({
  selector: 'add-service-modal',
  styleUrls: [('./booking-modal.scss')],
  templateUrl: './booking-modal.html'
})

export class BookingModal implements OnInit {

  modalHeader: string;
  public activeBooking;


  constructor(private activeModal: NgbActiveModal,private store: Store<any>) {
    this.store
      .select('booking')
      .subscribe((res: any) => {
        
      });
  }

  ngOnInit() {}
  closeModal() {
    this.activeModal.close();
  }
}



