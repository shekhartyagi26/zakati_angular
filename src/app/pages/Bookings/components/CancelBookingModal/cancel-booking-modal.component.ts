import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService , ToastrConfig} from 'ngx-toastr';;
const types = ['success', 'error', 'info', 'warning'];
import {
    FormGroup,
    AbstractControl,
    FormBuilder,
    Validators,
    FormControl
} from '@angular/forms';

import * as booking from '../../state/booking.actions'

@Component({
  selector: 'cancel-booking-modal',
  styleUrls: [('./cancel-booking-modal.scss')],
  templateUrl: './cancel-booking-modal.html'
})

export class CancelBookingModal  {

  modalHeader: string;
  public activeCustomer;
  public name:AbstractControl;
  public bank:AbstractControl;
  public country:AbstractControl;
  public beneficiary:AbstractControl;
  public iban:AbstractControl;
  public submitted: boolean = false;
  public page = 1;
  public limit = 4;
  public form: FormGroup;
  public myImgUrl:string='../../../../assets/img/default.png';

  constructor(private activeModal: NgbActiveModal,fb: FormBuilder,private store: Store<any>) 
  {this.form = fb.group({
      'name': [''],
      'bank': [''],
      'country': [''],
      'beneficiary': [''],
      'iban': ['']
      });
    this.store
      .select('booking')
      .subscribe((res: any) => {
        this.activeCustomer = (res.activeCustomer)?res.activeCustomer.customer:null;
      });
  }


onSubmit(values: Object) {
        event.preventDefault();
        this.submitted = true;
        console.log(values)
       
          
         
     var fd = new FormData();
    
    
      fd.append('userName',values['name'] || this.activeCustomer.userName);
      fd.append('bankName',values['bank']||this.activeCustomer.bankDetails.bankName);
      fd.append('bankCountry',values['country']||this.activeCustomer.bankDetails.bankCountry );
      fd.append('beneficiaryName',values['beneficiary']|| this.activeCustomer.bankDetails.beneficiaryName);
      fd.append('ibanNumber',values['iban']||this.activeCustomer.bankDetails.beneficiaryName );
      fd.append('userId',this.activeCustomer._id);
console.log(fd)
      

           
     
  //  this.store.dispatch({
  //               type:booking.ActionTypes. APP_EDIT_USER_BY_ID,
  //                payload: fd
  //           })


          // this.store.dispatch({ type: booking.ActionTypes.APP_GETALL_BOOKING, payload: {currentPage:this.page,limit:this.limit} })
    this.activeModal.close();
        }
          closeModal()
           {
    this.activeModal.close();
 
}
    }



