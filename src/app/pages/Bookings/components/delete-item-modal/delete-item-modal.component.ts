import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';


import * as customer from '../../state/booking.actions';
import * as auth from '../../../../auth/state/auth.actions';

// import * as lang from '../../../../multilingual/state/lang.actions';
// import {Language} from '../../../../multilingual/model/lang.model';

import 'style-loader!./delete-item-modal.scss';

@Component({
  selector: 'delete-item-modal',
  styleUrls: [('delete-item-modal.scss')],
  template: `

  <div class="modal-content">
  <div class="modal-header">
    
    <h3>Do you want to delete this category?</h3>

    
    <button class="close" aria-label="Close" (click)="closeModal()">
      <span aria-hidden="true" style="color:white">&times;</span>
    </button>
  </div>
    
  <div class="modal-footer">
 
 
          // <i class="fa fa-trash-o " aria-hidden="true" style="color:red ; cursor:pointer" (click)="deleteCustomer(activeCustomer._id)"></i>
          <i class="fa fa-trash-o " aria-hidden="true" style="color:red ; cursor:pointer" (click)="deleteCustomer()"></i>
       
           <i class="fa fa-times" aria-hidden="true" style="color:red ; cursor:pointer" (click)="closeModal()"></i>
       
 
  </div>
  

</div>

`
})

export class DeleteCustomerModal implements OnInit {

  modalHeader: string;
  public activeCustomer:any=[];
  // language = new Language();


  constructor(private activeModal: NgbActiveModal, private store: Store<any>) {
  //  this.store
  //       .select('lang')
  //       .subscribe((res: any) => {
  //           //setting language
  //           if(res.resourceBundle != null)
  //           {
  //               for(var j=0;j<res.resourceBundle.length;j++)
  //               {
  //                   //console.log(res.resourceBundle)
  //                   this.language[res.resourceBundle[j].messageKey]=res.resourceBundle[j].customMessage

  //               }
  //           }

  //       });

    this.store
      .select('booking')
      .subscribe((res: any) => {
        //  this.activeCustomer=res.data;
         this.activeCustomer=res.idrecord;
        //  foreach(let data of active )
        // this.activeCustomer = (res.data) ? res.data : null;
       console.log("dddddd",this.activeCustomer);
    // console.log("heyyyy",this.activeCustomer._id);
      });
  }



  ngOnInit() { }
  closeModal() {
    this.activeModal.close();
  }



  deleteCustomer() {
    // this.store.dispatch(new auth.AuthLogoutAction())

    let obj = {
      'categoryId': this.activeCustomer._id

      // 'categoryId':this.activeCustomer._id
    };
    this.store.dispatch({
      type:customer.ActionTypes.DELETE_ITEM_RECORD,
      payload: obj
    });

     

    this.activeModal.close();
  }
}



