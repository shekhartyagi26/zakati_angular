import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import * as booking from '../../state/booking.actions';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ImageResult, ResizeOptions } from 'ng2-imageupload';
import {
    FormGroup,ReactiveFormsModule,
    AbstractControl,
    FormBuilder,
    Validators,
    FormControl
} from '@angular/forms';

@Component({
  selector: 'add-item-modal',
  templateUrl: './add-item-modal.html'

})

export class AddItemModal {
    public page = 1;
    public document_image: FileList;
    public image:string;
  modalHeader: string;
  public activeCustomer;


  public submitted: boolean = false;
  public form: FormGroup;
    public name: string;
     public src:string='../../../../assets/img/default.png';
   
  constructor(private modalService: NgbModal,

    private activeModal: NgbActiveModal,
    fb: FormBuilder,
    private store: Store<any>
  ) {
    this.form = fb.group({
      'name': ['', [<any>Validators.required]],
      'image':['',[<any>Validators.required]]     

    });




    this.store
      .select('booking')
      .subscribe((res: any) =>


      {
        // alert(res);
        // console.log(res.activeCustomer.customer);
        // this.activeCustomer = (res.activeCustomer) ? res.activeCustomer.customer.bankDetails : null;
      })
  };
  selected(event) {
            if (event.target.files && event.target.files[0]) {
    var reader = new FileReader();

    reader.onload = (event:any) => {
      this.src = event.target.result;
      
     
    }
     console.log(event.target.files[0]);
        this.document_image = event.target.files[0]; 

    reader.readAsDataURL(event.target.files[0]);
  }
    }

   onSubmit(values: Object) {
    
            // alert("hiiiii")
        event.preventDefault();
        this.submitted = true;
           console.log(values)
      
     
        { 
            var data = {
              name: this.name,
              image: this.document_image
               
          }
            console.log(data)
     var fd = new FormData();
      // for(let prop in data){
    
      // fd.append(prop,data[prop]);
      fd.append('name',this.name);
      fd.append('image',this.document_image);

      console.log("hellooooooo",fd)

           
    //  }
   this.store.dispatch({
                type:booking.ActionTypes. APP_ADD_ITEM,
                 payload:fd
            })
        }

        this.activeModal.close();
          
        }
    // this.activeModal.close();
  






  

  closeModal() {
    this.activeModal.close();
  }

}



