
import {Component} from '@angular/core';
import { Store } from '@ngrx/store';
import * as booking from '../../state/booking.actions'
import {BaThemeSpinner} from '../../../../theme/services';
import { BookingModal } from '../BookingModal/booking-modal.component';
import { AddItemModal } from '../add-item-modal/add-item-modal.component'; 
import { CancelBookingModal } from '../CancelBookingModal/cancel-booking-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DeleteCustomerModal } from '../delete-item-modal/delete-item-modal.component';
import { EditItemModal } from '../edit-item-modal/edit-item-modal.component';
import { ChangePasswordModal } from '../ChangePasswordModal/change-password.component';
import * as auth from '../../../../auth/state/auth.actions'
import 'style-loader!./all-bookings.scss';

@Component({
  selector: 'allbookings',
  templateUrl: './all-bookings.html',
})
export class AllBookings {

  public page = 1;
  public limit = 4;
  public count:number;
  public customerData;
  public pic=true;
  public currentPage;
  public myImgUrl:string='../../../../../assets/img/default.png';

  public c:boolean=true;
  public activeCustomer=[];
  public data:any=[];
 
  constructor(private store: Store<any>,private modalService: NgbModal,
             
            ) {

    this.store
            .select('auth')
            .subscribe((res: any) => {
                console.log("heyyyyyyyyyyyyyyyyy",res.response)
                 
                 this.data=res.response;

                 
              
    //console.log(res.message)
            
                //this.domains = res.domains
                
            })
              this.store.dispatch({ type:auth.ActionTypes.AUTH_LOGIN_SUCCESS})

      //Dispatch action on load ..
      // this.store.dispatch({ type: booking.ActionTypes.APP_GETALL_BOOKING, payload: {currentPage:this.page} })

  }


  //    block(data1, isBlocked){
  //   let obj = {
  //     'userId': data1._id,
  //     'isBlocked' : isBlocked
  //   };
  //   this.store.dispatch({
  //     type     : booking.ActionTypes.APP_BLOCK_USER_BY_ID,
  //     payload  : obj
  //   });
  //    this.updateCustomerBlock(data1, isBlocked);
  //    }
  //     updateCustomerBlock(data1, isBlocked) {
  //   data1.isBlocked = isBlocked;
    
  // }
//  getuserdetails(data)
//   { console.log("bjhh")
//   var obj={
//     customer:data
//   }
    // this.store.dispatch({ type: booking.ActionTypes.APP_GET_USER_BY_ID,payload:obj});
    //  this.lgModalShow();}
  
    
  
  lgModalShow() {
    const activeModal = this.modalService.open(AddItemModal, {size: 'lg'});
    activeModal.componentInstance.modalHeader = 'Large Modal';
  }  
  //  staticModalShow(data) {
  //    var obj={
  //   customer:data
  // }
  //   // this.store.dispatch({ type: booking.ActionTypes.APP_GET_USER_BY_ID,payload:obj});
  //   const activeModal = this.modalService.open(CancelBookingModal, {size: 'lg',
  //     backdrop: 'static'});
  //   activeModal.componentInstance.modalHeader = 'Static modal';
    

  // }     
   deleteCustomerConfirm(data) {
    console.log("Working inside deleteCustomerConfirm");
    // let obj = {
      

    //   'categoryId':uData
    // };
    // this.store.dispatch({
    //   type: booking.ActionTypes.DELETE_ITEM_RECORD,
    //   payload:obj
    // });


     console.log("FFFFFf",data);
      this.store.dispatch({
      type: booking.ActionTypes.ADD_ID_RECORD,
      payload:data
    });
    const customerActionModel = this.modalService.open(DeleteCustomerModal, { size: 'lg' });
    customerActionModel.componentInstance.modalHeader = 'Large Modal';
  }
   editProfileConfirm(data)
    { 
  //  console.log("FFFFFf",data);
  //     this.store.dispatch({
  //     type: booking.ActionTypes.ADD_ID_RECORD,
  //     payload:data
  //   });

 const customerActionModel = this.modalService.open(EditItemModal, { size: 'lg' });
    customerActionModel.componentInstance.modalHeader = 'Large Modal';
     

  }
  

  changePasswordConfirm()

  {

    const customerActionModel = this.modalService.open(ChangePasswordModal, { size: 'lg' });
    customerActionModel.componentInstance.modalHeader = 'Large Modal';


  }

}