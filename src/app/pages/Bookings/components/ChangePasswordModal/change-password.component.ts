
import {Component} from '@angular/core';
import {FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';

import { Observable } from "rxjs/Observable";
import { BaThemeSpinner } from '../../../../theme/services';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';

import * as auth from '../../../../auth/state/auth.actions';
// import * as lang from '../../../multilingual/state/lang.actions';

import {ActivatedRoute} from '@angular/router';

import { EqualPasswordsValidator } from '../../../../theme/validators';
import { User } from '../../../../auth/model/user.model';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
// import { Language } from '../../../multilingual/model/lang.model.ts'

import 'style-loader!./change-password.scss';
@Component({
  selector: 'change-password',
  templateUrl: './change-password.html',
  
  
 
})
export class ChangePasswordModal {
  public form:FormGroup;
  public passwords:FormGroup;
  public oldpassword:AbstractControl;

  public password:AbstractControl;
  public confirmpassword:AbstractControl;

//   public tokenVerified: boolean = false;
//   public token: string;
//   public responseMessage: String;
//   public errorFlag: boolean = false;
//   public successFlag: boolean = false;
//   public submitted:boolean = false;
//   public sub;
//   public id:string;

  user = new User();

  constructor(fb:FormBuilder,private activeModal: NgbActiveModal, private route: ActivatedRoute, private store: Store < any >) {
    this.form = fb.group({
      'passwords': fb.group({
        'oldpassword': ['', Validators.compose([Validators.required, Validators.minLength(8)])],
        'password': ['', Validators.compose([Validators.required, Validators.minLength(8)])],
        'confirmpassword': ['', Validators.compose([Validators.required, Validators.minLength(8)])]
      }, {validator: EqualPasswordsValidator.validate('password', 'confirmpassword')})
    });

    // this.sub = this.route.params.subscribe(params => {
    //         this.id = params['id']; // (+) converts string 'id' to a number
    //         console.log("GOT ID", this.id);
    //     });

    this.passwords = <FormGroup> this.form.controls['passwords'];
      this.oldpassword = this.passwords.controls['oldpassword'];
    this.password = this.passwords.controls['password'];
    this.confirmpassword = this.passwords.controls['confirmpassword'];

    // START :: STORE 
//     this.store
//       .select('auth')
//       .subscribe((res: any) => {
//         let data = res;
//         console.log(res);
        
//         if(data.hasOwnProperty('verify_token_success_payload')){
          
//           if(data.verify_token_success_payload.status == 200){
//             this.tokenVerified = true;
//           }else{
//             this.responseMessage = data.verify_token_success_payload.message;
//           }
//           delete data["verify_token_success_payload"]; 
//         } else if(data.hasOwnProperty('reset_pass_success_payload')){
//           if(data.reset_pass_success_payload.status == 200){
//             this.errorFlag = false;
//             this.successFlag = true;
//             this.responseMessage = data.reset_pass_success_payload.message;
//           } else {
//             this.errorFlag = true;
//             setTimeout(() => {
//               this.errorFlag = false;
//             }, 3000);
//             this.successFlag = false;
//             this.responseMessage = data.reset_pass_success_payload.message;
//           }
//           delete data["reset_pass_success_payload"]; 
//         }
//       });
//     // END :: STORE
//   }

//   ngOnInit() {
    
//     // this.token = this.route.snapshot.queryParams["resetToken"];
//     //   this.authenticateToken(this.token);
//     //   sessionStorage.setItem("resetToken", this.token);
//   }

//   authenticateToken(token){
  
//     var data = {
//       'resetToken': token,
      
     
     
//     //  'password': this.user.password,
//     }
    // this.store.dispatch({
    //   type: auth.ActionTypes.AUTH_FORGOT_PASS_TOKEN,
    //   payload: data
    // })
  }

   closeModal() {
    this.activeModal.close();
  }
  
     onSubmit(values: Object) {
        event.preventDefault();
        // this.submitted = true;
        // let token = sessionStorage.getItem('resetToken');
        if (this.form.valid) {
            var data = {
               "oldPassword": this.user.oldpassword,
               "newPassword":this.user.password
            //    "resetToken": this.id,
              
               
          }
          
          // var fd=new FormData();
          // fd.append('oldPassword',this.user.oldpassword);
          //  fd.append('newPassword',this.user.password);


        
            // this.baThemeSpinner.show();
            this.store.dispatch({
                type: auth.ActionTypes.AUTH_RESET_PASSWORD,
                payload: data

        })
    }
}

}
