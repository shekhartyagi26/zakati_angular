import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, AbstractControl, FormBuilder, Validators, FormControl } from '@angular/forms';
import { EmailValidator, EqualPasswordsValidator } from '../../../../theme/validators';
import { User } from '../../../../auth/model/user.model';

import * as booking from '../../state/booking.actions';
import * as auth from '../../../../auth/state/auth.actions'
// import * as lang from '../../../../multilingual/state/lang.actions';
// import {Language} from '../../../../multilingual/model/lang.model';



import 'style-loader!./edit-item-modal.scss';

@Component({
  selector: 'edit-item-modal',
  templateUrl: 'edit-item-model.html'
})

export class EditItemModal {


  // public activeCustomer;

  // public form: FormGroup;
  // public name: AbstractControl;


  // public mobile: AbstractControl;
  // public companyName: AbstractControl;
  // public countryCode: AbstractControl;
  // public companyAddress: AbstractControl;
  // // language = new Language();




  // public submitted: boolean = false;


  // constructor(private activeModal: NgbActiveModal, private store: Store<any>,
  //   private fb: FormBuilder) {
  //   this.form = fb.group({
  //     'name': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
  //     'mobile': ['', Validators.compose([Validators.required])],
  //     'companyName': ['', Validators.compose([Validators.required])],
  //     'countryCode': ['', Validators.compose([Validators.required])],
  //     'companyAddress': ['', Validators.compose([Validators.required])]

  //   });

  //   this.name = this.form.controls['name'];
  //   this.mobile = this.form.controls['mobile'];
  //   this.companyName = this.form.controls['companyName'];
  //   this.countryCode = this.form.controls['countryCode'];
  //   this.companyAddress = this.form.controls['companyAddress'];



  //   this.name.setValue("Lokinder");
  //   // this.store
  //   //     .select('lang')
  //   //     .subscribe((res: any) => {
  //   //         //setting language
  //   //         if(res.resourceBundle != null)
  //   //         {
  //   //             for(var j=0;j<res.resourceBundle.length;j++)
  //   //             {
  //   //                 //console.log(res.resourceBundle)
  //   //                 this.language[res.resourceBundle[j].messageKey]=res.resourceBundle[j].customMessage

  //   //             }
  //   //         }

  //   //     });


  //   this.store
  //     .select('customer')
  //     .subscribe((res: any) => {
  //       // this.activeCustomer = (res.activeCustomer) ? res.activeCustomer : null;
  //       // console.log(this.activeCustomer);
  //       // this.name.setValue(this.activeCustomer.name);
  //       // //this.email.setValue(this.activeCustomer.email);
  //       // if (this.activeCustomer.contacts && this.activeCustomer.contacts[0].countryCode)
  //       //   this.countryCode.setValue(this.activeCustomer.contacts[0].countryCode);
  //       // if (this.activeCustomer.contacts && this.activeCustomer.contacts[0].mobile)
  //       //   this.mobile.setValue(this.activeCustomer.contacts[0].mobile);
  //       // if (this.activeCustomer.contacts && this.activeCustomer.customer.companyName)
  //       //   this.companyName.setValue(this.activeCustomer.customer.companyName);

  //       // if (this.activeCustomer.contacts && this.activeCustomer.customer.companyAddress)
  //       //   this.companyAddress.setValue(this.activeCustomer.customer.companyAddress);

  //     });

  // }



  // closeModal() {
  //   this.activeModal.close();
  // }



  // onSubmit() {
  //  // insertin _id in payload
  //   // formValue._id = this.activeCustomer._id;

  //   // this.store.dispatch({
  //   //   type: customer.ActionTypes.EDIT_THIS_CUSTOMER,
  //   //   payload: formValue
  //   // });

  //   this.activeModal.close();
  // }


  modalHeader: string;
  public activeCustomer;
  public name:AbstractControl;

   public email:AbstractControl ;
    public address:AbstractControl ;
    public phone:AbstractControl;
    public password:AbstractControl;
    public id:number=1;
 
  public image:AbstractControl
  public submitted: boolean = false;
  public imageSelected: boolean = false;
  public document_image: FileList;
  public document_image1: FileList;
  public newImage:FileList;
  public page = 1;
  public limit =100;
  public form: FormGroup;
  public myImgUrl:string='../../../../assets/img/user.png';
  // public src:string;
  public src:string='../../../../assets/img/default.png';
  public data;
 

  constructor(private activeModal: NgbActiveModal,fb: FormBuilder,private store: Store<any>) 
  
  {this.form = fb.group({

      
      'name': ['', Validators.compose([
                Validators.required, 
                
            ])],
        // 'email': ['', Validators.compose([
             
        //          this.isEmail
        //     ])],

            'address': ['', Validators.compose([
                Validators.required, 
                
            ])],
            'phone': ['', Validators.compose([
                Validators.required,Validators.minLength(10),
                Validators.maxLength(10)
                
            ])],
            // 'password': ['', Validators.compose([
            //     Validators.required, Validators.minLength(4)
                
            // ])],
      
      'image': [''],
    
    });
      this.email = this.form.controls['email'];
        this.name = this.form.controls['name'];
           this.address = this.form.controls['address'];
            this.phone = this.form.controls['phone'];
              this.password = this.form.controls['password'];
                // this.id= this.form.controls['id'];
                 this.store
            .select('auth')
            .subscribe((res: any) => {
                console.log("heyyyyyyyyyyyyyyyyy in edit modal",res.response[0])
                 
                 this.data=res.response[0];

               

                 
              
    //console.log(res.message)
            
                //this.domains = res.domains
                
            })
              this.store.dispatch({ type:auth.ActionTypes.AUTH_LOGIN_SUCCESS})
            //   this.store.dispatch({ type: 'APP_GET_USER_ROLES' })
            //    this.store.dispatch({
            //     type:booking.ActionTypes. APP_EDIT_USER_BY_ID
            // })
            

      //Dispatch action on load ..
      // this.store.dispatch({ type: booking.ActionTypes.APP_GETALL_BOOKING, payload: {currentPage:this.page} })

  }
    
//     this.store
//       .select('booking')
//       .subscribe((res: any) => {
//         console.log("newwwwwwwwwwwwr",res);


//   // this.activeCustomer = (res.activeCustomer)?res.activeCustomer.customer:null;
//         //  this.imageSelected = false;
//         // this.activeCustomer=res.id;
//         // this.activeCustomer=res;
//         //  this.src=this.activeCustomer.image.thumb;
//         // console.log(this.activeCustomer);
         
//       });
    
//        this.store.dispatch({ type: booking.ActionTypes.APP_EDIT_USER_BY_ID })

//   }
  
 

 isEmail(control: FormControl): {
        [s: string]: boolean
    } {
        console.log(control.value);
        if (control.value) {
            if (!control.value.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
                return {
                    noEmail: true
                };
            }
        }

    }



  selected(event) {
  this.imageSelected = true;
            if (event.target.files && event.target.files[0]) {
    var reader = new FileReader();

    reader.onload = (event:any) => {
      this.src = event.target.result;
      
     
    }
     console.log(event.target.files[0]);
        this.document_image = event.target.files[0]; 
        console.log(this.document_image)

    reader.readAsDataURL(event.target.files[0]);
  }

 
    }
onSubmit(values: Object,$event) {
    $event.preventDefault();
        this.submitted = true;
        console.log(values)
        // alert(this.activeCustomer.name);
        // alert(this.activeCustomer._id)
        console.log("nameeeeeeeeeeeeeee",this.document_image);
    // let obj = {
    // //    id:this.activeCustomer.id ,
    //    id:this.id,
    //   name:values['name']||this.data.name,
    //   photo:this.document_image,
    //   email:this.email,
    //   address:values['address']||this.data.address,
    //   phone:values['phone']||this.data.phone,
    //   password:this.password,
      


    //   // 'categoryId': this.activeCustomer._id,
    //   // image:this.document_image

    //   // 'categoryId':this.activeCustomer._id
    // };        
     var fd = new FormData();
      fd.append('name',values['name'] || this.name);
       fd.append('address',values['name'] || this.address);
          fd.append('phone',values['phone'] || this.phone);
            // fd.append('image',this.document_image);
              fd.append('photo',this.document_image );

      if(this.imageSelected)
      {
      fd.append('photo',this.document_image );
       }
//      fd.append('categoryId', JSON.stringify(obj));
// console.log(obj)
   this.store.dispatch({
                type:booking.ActionTypes. APP_EDIT_USER_BY_ID,
                 payload:fd
            })
   

          
    this.activeModal.close();
        }
closeModal()
{
    this.activeModal.close();
}
 



}







