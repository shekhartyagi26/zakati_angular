import {Component} from '@angular/core';

@Component({
  selector: 'time',
  template: `<router-outlet></router-outlet>`
})
export class Time {

  constructor() {
  }

  ngOnInit() {
  }

}
