import { Routes, RouterModule }  from '@angular/router';

import { Time } from './time.component';
// import { PastBookings } from './components/PastBookings/past-bookings.component';
import {TimeOrg} from './components/Org/org-bookings.component';
import {TimeOther} from './components/Manage/other-bookings.component';
import {TimeUser} from './components/User/user-bookings.component';

//import { ByIdDetails } from './components/byIdDetails/byId-details.component';
//import { CancelBooking } from './components/CancelBooking/cancel-booking.component';


// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: Time,
    children: [
      
      
       { path: 'timeuser', component:TimeUser},
         { path: 'timeorg', component:TimeOrg},
       { path: 'timeother', component:TimeOther}
    ]
  }
];

export const routing = RouterModule.forChild(routes);
