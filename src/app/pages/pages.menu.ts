export const PAGES_MENU = [
  {
    path: 'pages',
    children: [
      {
        path: 'dashboard',
        data: {
          menu: {
            title: 'general.menu.dashboard',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 0

          }
        }
      },

      {
        path: 'Booking',
        data: {
          menu: {
            title: 'general.menu.Bookings',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 0
          }
        },



      },


      //  {
      //   path: 'pastbookings',
      //   data: {
      //     menu: {
      //       title: 'general.menu.pastbookings',
      //       icon: 'ion-android-home',
      //       selected: false,
      //       expanded: false,
      //       order: 0
      //     }
      //   },



      // },




      {
        path: 'Competition',
        data: {
          menu: {
            title: 'general.menu.Competition',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 100,
          }
        },


      },



      //  },
      // {
      // path: 'OrganisationList',
      // data: {
      //   menu: {
      //     title: 'general.menu.ServiceProviders',
      //     icon: 'ion-android-home',
      //     selected: false,
      //     expanded: false,
      //     order: 100,
      //   }
      // },


      {
        path: 'pastbookings',
        data: {
          menu: {
            title: 'general.menu.pastbookings',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 0
          }
        },



      },

      // // children: [
      // //           //{
      // //           //   path: 'activeservices',
      // //           //   data: {
      // //           //     menu: {
      // //           //       title: 'general.menu.activeservices',
      // //           //     }
      // //           //   }
      // //           // },
      // //            {
      // //             path: 'allservices',
      // //             data: {
      // //               menu: {
      // //                 title: 'general.menu.allservices',
      // //               }
      // //             }
      // //           },

      // //           ]

      // //         },
      // //        {
      // //         path: 'driver',
      // //         data: {
      // //           menu: {
      // //             title: 'general.menu.Drivers',
      // //             icon: 'ion-person',
      // //             selected: false,
      // //             expanded: false,
      // //             order: 100,
      // //           }
      // //         },

      // //           children: [
      // //           // {
      // //           //   path: 'activerides',
      // //           //   data: {
      // //           //     menu: {
      // //           //       title: 'general.menu.activerides',
      // //           //     }
      // //           //   }
      // //           // },

      // //             {
      // //             path: 'allrides',
      // //             data: {
      // //               menu: {
      // //                 title: 'general.menu.allrides',
      // //               }
      // //             }
      // //           }



      // //         ]


      // //   },
      {
        path: 'onbookings',
        data: {
          menu: {
            title: 'general.menu.onbookings',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 100,
          }
        },
        children: [
          {
            path: 'userbookings',
            data: {
              menu: {
                title: 'general.menu.userbookings',
              }
            }
         },

                 {
                    path: 'orgbookings',
                    data: {
                      menu: {
                        title: 'general.menu.orgbookings',
                      }
                    }
                  },
        // //            {
        // //             path: 'onbookings',
        // //             data: {
        // //               menu: {
        // //                 title: 'general.menu.onbookings',
        // //               }
        // //             }
        // //           },


        ]

      
         
       



      },
            {
        path: 'stuff',
        data: {
          menu: {
            title: 'general.menu.stuff',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 100,
          }
        },
       children: [
          {
            path: 'taskbookings',
            data: {
              menu: {
                title: 'general.menu.taskbookings',
              }
            }
         },
          {
            path: 'managementbookings',
            data: {
              menu: {
                title: 'general.menu.managementbookings',
              }
            }
         },
         
          {
            path: 'otherbookings',
            data: {
              menu: {
                title: 'general.menu.otherbookings',
              }
            }
         },




       ]

         },



          {
        path: 'time',
        data: {
          menu: {
            title: 'general.menu.time',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 100,
          }
        },
       children: [
          {
            path: 'timeuser',
            data: {
              menu: {
                title: 'general.menu.timeuser',
              }
            }
         },
          {
            path: 'timeorg',
            data: {
              menu: {
                title: 'general.menu.timeorg',
              }
            }
         },
         
          {
            path: 'timeother',
            data: {
              menu: {
                title: 'general.menu.timeother',
              }
            }
         },




       ],
      
    
  },


        {
        path: 'money',
        data: {
          menu: {
            title: 'general.menu.money',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 100,
          }
        },
       children: [
          {
            path: 'moneyuser',
            data: {
              menu: {
                title: 'general.menu.moneyuser',
              }
            }
         },
          {
            path: 'moneyorg',
            data: {
              menu: {
                title: 'general.menu.moneyorg',
              }
            }
         },
         
          {
            path: 'moneyother',
            data: {
              menu: {
                title: 'general.menu.moneyother',
              }
            }
         },




       ]


         }







    ]
  }
];
