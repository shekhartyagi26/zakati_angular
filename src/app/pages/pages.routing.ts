import { Routes, RouterModule }  from '@angular/router';
import { Pages } from './pages.component';
import { ModuleWithProviders } from '@angular/core';
import { AuthGuard }  from '../theme/services/authService/authGuard.service';
import{PastBookings} from'./Bookings/components/PastBookings/past-bookings.component'
// import{OnBookings} from'./Bookings/components/OnBookings/on-bookings.component'
import{OnBookingsModule} from'./OnBookings/onbookings.module'
// noinspection TypeScriptValidateTypes

// export function loadChildren(path) { return System.import(path); };

export const routes: Routes = [
  // {
  //   path: 'login',
  //   loadChildren: 'app/pages/login/login.module#LoginModule'
  // },
  // {
  //   path: 'register',
  //   loadChildren: 'app/pages/register/register.module#RegisterModule'
  // },
  {
    path: 'pages',
    component: Pages,
    canActivate: [AuthGuard],
    children: [
     { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
     { path: 'dashboard', loadChildren: 'app/pages/dashboard/dashboard.module#DashboardModule' },
     { path: 'Booking', loadChildren: 'app/pages/Bookings/bookings.module#BookingsModule'},
     { path: 'Competition', loadChildren: 'app/pages/Competitions/competitions.module#CompetitionsModule'}, 
      // { path: 'pastbookings', loadChildren: 'app/pages/Bookings/bookings.module#BookingsModule'},
       { path: 'pastbookings', component:PastBookings},  
        { path: 'onbookings',loadChildren:'app/pages/OnBookings/onbookings.module#OnBookingsModule'},
          // { path: 'offbookings',loadChildren:'app/pages/OffBookings/offbookings.module#OffBookingsModule'},
             { path: 'stuff',loadChildren:'app/pages/StuffManagement/stuff.module#StuffModule'},
              { path: 'time',loadChildren:'app/pages/Time/time.module#TimeModule'},
                { path: 'money',loadChildren:'app/pages/Money/money.module#MoneyModule'},
        // { path: 'onbookings', component:OnBookings,children:[{
        //     path: 'userbookings', component:UserBookings, 
        // }]},  
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
