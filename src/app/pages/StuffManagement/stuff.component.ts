import {Component} from '@angular/core';

@Component({
  selector: 'stuff',
  template: `<router-outlet></router-outlet>`
})
export class Stuff {

  constructor() {
  }

  ngOnInit() {
  }

}
