import { Routes, RouterModule }  from '@angular/router';

import { Stuff } from './stuff.component';
// import { PastBookings } from './components/PastBookings/past-bookings.component';
import {TaskBookings} from './components/Org/org-bookings.component';
import {OtherBookings} from './components/Manage/other-bookings.component';
import {ManagementBookings} from './components/User/user-bookings.component';

//import { ByIdDetails } from './components/byIdDetails/byId-details.component';
//import { CancelBooking } from './components/CancelBooking/cancel-booking.component';


// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: Stuff,
    children: [
      
      
       { path: 'taskbookings', component:TaskBookings},
         { path: 'otherbookings', component:OtherBookings},
       { path: 'managementbookings', component:ManagementBookings}
    ]
  }
];

export const routing = RouterModule.forChild(routes);
