import {Component} from '@angular/core';

@Component({
  selector: 'onbookings',
  template: `<router-outlet></router-outlet>`
})
export class OnBookings {

  constructor() {
  }

  ngOnInit() {
  }

}
