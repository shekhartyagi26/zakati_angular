import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
//import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
//pipes
import {NgPipesModule} from 'ngx-pipes';
// import { AddItemModal  } from './components/add-item-modal/add-item-modal.component';
import { Modals } from '../ui/components/modals/modals.component';
import { NgaModule } from '../../theme/nga.module';
import { FormsModule ,ReactiveFormsModule} from '@angular/forms'

import { routing }       from './onbookings.routing';
import { OnBookings } from './onbookings.component';
// import { PastBookings } from './components/PastBookings/past-bookings.component';
import { OrgBookings } from './components/TotalBookings/org-bookings.component';
import { UserBookings } from './components/UserBookings/user-bookings.component';
// import {AllBookings } from './components/AllBookings/all-bookings.component';

import {NgxPaginationModule} from 'ngx-pagination'; 

// import { CancelBookingModal } from './components/CancelBookingModal/cancel-booking-modal.component';
// import {DeleteCustomerModal} from './components/delete-item-modal/delete-item-modal.component';
// import { BookingModal } from './components/BookingModal/booking-modal.component';
// import { ChangePasswordModal } from './components/ChangePasswordModal/change-password.component';
// import {EditItemModal} from './components/edit-item-modal/edit-item-modal.component';
import { DataFilterPipe }   from './components/UserBookings/data-filter.pipe';
import { DataFilterPipe1 }   from './components/TotalBookings/data-filter.pipe';
import { DataTableModule } from "angular2-datatable";




@NgModule({
  imports: [
    DataTableModule,
  
   
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgaModule,
    NgxPaginationModule,
    routing,
    NgbModalModule,
    NgPipesModule
  ],
  declarations: [
      OnBookings,
      UserBookings,
       OrgBookings,
    // Bookings,
    // PastBookings,
    // AllBookings,
    // // OnBookings,
    // BookingModal,
    // CancelBookingModal,
    // DeleteCustomerModal,
    // ChangePasswordModal,

    //  AddItemModal,
    //  EditItemModal,
     DataFilterPipe,
     DataFilterPipe1
  ],
  entryComponents: [
//     CancelBookingModal,
//     BookingModal,
//     DeleteCustomerModal,
//     ChangePasswordModal,
   
//    AddItemModal,
//    EditItemModal
  ],

})
export class OnBookingsModule {}
