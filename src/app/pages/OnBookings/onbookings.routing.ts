import { Routes, RouterModule }  from '@angular/router';

import { OnBookings } from './onbookings.component';
// import { PastBookings } from './components/PastBookings/past-bookings.component';
import { UserBookings} from './components/UserBookings/user-bookings.component';
import { OrgBookings} from './components/TotalBookings/org-bookings.component';

//import { ByIdDetails } from './components/byIdDetails/byId-details.component';
//import { CancelBooking } from './components/CancelBooking/cancel-booking.component';


// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: OnBookings,
    children: [
      
      
       { path: 'userbookings', component:UserBookings},
       { path: 'orgbookings', component:OrgBookings }
    ]
  }
];

export const routing = RouterModule.forChild(routes);
