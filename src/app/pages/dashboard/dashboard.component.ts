import {Component,OnInit} from '@angular/core';
import {
    FormGroup,
    AbstractControl,
    FormBuilder,
    Validators,
    FormControl
} from '@angular/forms';
import { User } from '../../auth/model/user.model'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DefaultModal } from '../ui/components/modals/default-modal/default-modal.component';
import * as dashBoard from '../dashboard/state/dashboard.actions';
import { Store } from '@ngrx/store';

@Component({
  selector: 'dashboard',
  styleUrls: ['./dashboard.scss'],
  templateUrl: './dashboard.html'
})
export class Dashboard  {
   options: Object;
   options1: Object;
   
  // data=[];
  //  public form: FormGroup;
  //   public date:Date;
  //      public submitted: boolean = false;
  //     public count:number;
  constructor(private modalService: NgbModal,fb: FormBuilder,  private store: Store <any> ,) {

    //  this.options = {
    //         title : { text : 'Overall User Registeration' },
    //         series: [{
    //             data: [29.9, 71.5, 106.4, 129],

    //         }],

           
    //     };
     
    //  this.options1 = {
    //         title : { text : 'Overall Organisation Registeration' },
    //         series: [{
    //             data: [29.9, 71.5, 106.4, 129],
    //         }]



   
      //  };
   



   //this.date= this.form.controls['date'];
  //   this.form = fb.group({
  //           'date': ['',             
  //           ]
            
  //       });
  //       //alert(this.date)
  // var obj={
  //   date1:this.date}
  //     this.store
  //           .select('dashBoard')
  //            .subscribe((res: any) => {
  //        console.log(res.userreportmonthly)
  //        //this.user=res.userreportmonthly
        
  //        if(res.userreportmonthly)
  //        {
  //          console.log(res.userreportmonthly.data)
  //          this.count=res.userreportmonthly.data.count
  
  //        }

  //            })
}


public lineChartData:Array<any> = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
    {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'},
    {data: [18, 48, 77, 9, 100, 27, 40], label: 'Series C'}
  ];
  public lineChartLabels:Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions:any = {
    responsive: true
  };
  public lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';
 
  public randomize():void {
    let _lineChartData:Array<any> = new Array(this.lineChartData.length);
    for (let i = 0; i < this.lineChartData.length; i++) {
      _lineChartData[i] = {data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label};
      for (let j = 0; j < this.lineChartData[i].data.length; j++) {
        _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
      }
    }
    this.lineChartData = _lineChartData;
  }
 
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }




// private _selected: any;
//   set selected (src : any) { 
//     this._selected = src; 
//     this.selected2 = this._selected.value[0];
//   };
//   get selected(): any { return this._selected; };
//   private selected2: string = "";
//   private data: any[] = [
//     { "name": "One", "value": ["One"] },
//     { "name": "Two", "value": ["Two"] },
//     { "name": "Three", "value": ["Three"] }
//   ];


// // onChange(newValue) {
// //     console.log(newValue);
// //     this.selectedItem = newValue;  // don't forget to update the model here
// //     // ... do other stuff here ...
// // }
//   // ngOnInit()
//   // {
//   //  //this.store.dispatch({ type: dashBoard.ActionTypes.GET_DASHBOARD_COUNT}) 
//   // }
  
//   //             onSubmit(values: Object) {
//   //       event.preventDefault();
//   //       this.submitted = true;
//   //       console.log(values)
//   //       var  mm= this.date.getMonth()+1;
//   //       console.log(mm)
//   //         var  yy= this.date.getFullYear();
//   //         // alert(mm);
//   //         // alert(yy);
//   //         let payload={
        
//   //         month:mm,
//   //         year:yy
//   //   }
//   //   console.log(payload)
//   //       //  this.store.dispatch({type:dashBoard.ActionTypes.GET_USER_REPORT_MONTHLY,payload:payload})
//   //             }    

// ngOnInit()
// {
   
// }

 

  
     
 
}
