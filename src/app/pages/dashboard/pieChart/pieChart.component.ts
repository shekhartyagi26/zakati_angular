import {Component} from '@angular/core';
import {BaThemeConfigProvider, colorHelper} from '../../../theme';
import {PieChartService} from './pieChart.service';
import { Store } from '@ngrx/store';
import 'easy-pie-chart/dist/jquery.easypiechart.js';
import 'style-loader!./pieChart.scss';
import * as dashBoard from '../state/dashboard.actions'


@Component({
  selector: 'pie-chart',
  templateUrl: './pieChart.html'
})
// TODO: move easypiechart to component
export class PieChart {

  public charts: Array<Object>;
  private _init = false;

  constructor(private _pieChartService: PieChartService,private _baConfig:BaThemeConfigProvider,private store: Store<any>) {
    let pieColor = this._baConfig.get().colors.custom.dashboardPieChart;
    this.store
      .select('dashBoard')
      .subscribe((res: any) => {
         //console.log(res)
         if(res.dashBoardCount != null)
         {
             this.charts = 
                         [{
                            color: pieColor,
                            description: 'general.dashboard.totalBookingCount',
                            stats: res.dashBoardCount.data.totalBookingCount,
                            icon: 'person',
                          }, 
                          {
                            color: pieColor,
                            description: 'general.dashboard.customerCount',
                            stats: res.dashBoardCount.data.customerCount,
                            icon: 'money',
                          }, 
                          {
                            color: pieColor,
                            description: 'general.dashboard.driverCount',
                            stats: res.dashBoardCount.data.driverCount,
                            icon: 'face',
                          }, 
                          {
                            color: pieColor,
                            description: 'general.dashboard.serviceProviderCount',
                            stats: res.dashBoardCount.data.serviceProviderCount,
                            icon: 'refresh',
                          },
                          {
                            color: pieColor,
                            description: 'general.dashboard.todaysBookingRequestsCount',
                            stats: res.dashBoardCount.data.todaysBookingRequestsCount,
                            icon: 'money',
                          },
                          {
                            color: pieColor,
                            description: 'general.dashboard.todaysRevenue',
                            stats: res.dashBoardCount.data.todaysRevenue,
                            icon: 'money',
                          },
                          {
                            color: pieColor,
                            description: 'general.dashboard.totalRevenue',
                            stats: res.dashBoardCount.data.totalRevenue,
                            icon: 'money',
                          }
                          ]
         }
        //console.log(this.totalBookingCount)
      });
      //this.store.dispatch({ type: dashBoard.ActionTypes.GET_DASHBOARD_COUNT})

    
  }

  ngAfterViewInit() {
    if (!this._init) {
      this._loadPieCharts();
      this._updatePieCharts();
      this._init = true;
    }
  }

  private _loadPieCharts() {

    jQuery('.chart').each(function () {
      let chart = jQuery(this);
      chart.easyPieChart({
        easing: 'easeOutBounce',
        onStep: function (from, to, percent) {
          jQuery(this.el).find('.percent').text(Math.round(percent));
        },
        barColor: jQuery(this).attr('data-rel'),
        trackColor: 'rgba(0,0,0,0)',
        size: 84,
        scaleLength: 0,
        animation: 2000,
        lineWidth: 9,
        lineCap: 'round',
      });
    });
  }

  private _updatePieCharts() {
    let getRandomArbitrary = (min, max) => { return Math.random() * (max - min) + min; };

    jQuery('.pie-charts .chart').each(function(index, chart) {
      jQuery(chart).data('easyPieChart').update(getRandomArbitrary(55, 90));
    });
  }
}
