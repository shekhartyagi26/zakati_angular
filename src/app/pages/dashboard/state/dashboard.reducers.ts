 import { Action, ActionReducer } from '@ngrx/store'

const initialState: any = {
  dashBoardCount: null,
  userreportmonthly:null
  
}

export const dashBoard: ActionReducer<any> = (state = initialState, action: Action) => {
  switch (action.type) {

    case 'GET_DASHBOARD_COUNT':
      return Object.assign({}, state)
       
    case 'GET_DASHBOARD_COUNT_SUCCESS':
      return Object.assign({}, state,{dashBoardCount:action.payload})

    case 'GET_USER_REPORT_MONTHLY':
      return Object.assign({}, state)

        case 'GET_USER_REPORT_MONTHLY_SUCCESS':
      return Object.assign({}, state,{userreportmonthly:action.payload})


    default:
      return state
  }
}
