import { get } from 'lodash'
import { Injectable } from '@angular/core'
import { Effect, Actions } from '@ngrx/effects'
import { Action, Store } from '@ngrx/store'
import { Observable } from 'rxjs/Observable'
import { Router } from '@angular/router';
import { dashboard_service } from '../../../services/dashboardService/dashboard.service';



import * as dashBoard from './dashboard.actions'

@Injectable()
export class DashboardEffects {

 
  @Effect({ dispatch: false })
  dashboardCount: Observable<Action> = this.actions$
    .ofType(dashBoard.ActionTypes.GET_DASHBOARD_COUNT)
     .do((action) => {
          this.dashboard_service.getDashBoardData()
          .subscribe((result) => {
         
              if(result.message == 'Success')
              { 
               //this.store.dispatch(new dashBoard.AppGetDashBoardCountSuccess(result))
                
              }

            
}
            , (error) => {
              console.log(error.message)
            });
     })


  @Effect({ dispatch: false })
  dashboardCountSuccess: Observable<Action> = this.actions$
    .ofType(dashBoard.ActionTypes.GET_DASHBOARD_COUNT_SUCCESS)
     .do((action) => {
          
     })
 
  @Effect({ dispatch: false })
  userreportmonthly: Observable<Action> = this.actions$
    .ofType(dashBoard.ActionTypes.GET_USER_REPORT_MONTHLY)
     .do((action) => {
          this.dashboard_service.getUserReportMonthly(action.payload)
          .subscribe((result) => {
             console.log(result)
              if(result.message == 'Success')
              { 
               this.store.dispatch(new dashBoard.AppGetUserReportMonthlySuccess(result))
             
              
              }

            
}
            , (error) => {
              console.log(error.message)
            });
     })
 @Effect({ dispatch: false })
userreportmonthlySuccess: Observable<Action> = this.actions$
    .ofType(dashBoard.ActionTypes.GET_USER_REPORT_MONTHLY_SUCCESS)
     .do((action) => {
      
          
     })
  constructor(
    private actions$: Actions,
    private store: Store<any>,
    private router: Router,
    private dashboard_service : dashboard_service
  ) {
  }

}

