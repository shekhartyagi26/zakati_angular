 import { Action } from '@ngrx/store'
import * as dashBoard from '../state/dashboard.actions'
 export const ActionTypes = {
  GET_DASHBOARD_COUNT:                 'GET_DASHBOARD_COUNT',
  GET_DASHBOARD_COUNT_SUCCESS:         'GET_DASHBOARD_COUNT_SUCCESS',
  GET_USER_REPORT_MONTHLY:             'GET_USER_REPORT_MONTHLY',
  GET_USER_REPORT_MONTHLY_SUCCESS:      'GET_USER_REPORT_MONTHLY_SUCCESS'
  
}

type credentials = {
  month:number;
  year:number
  
}

// /** GET DASHBOARD COUNT **/
export class AppGetDashBoardCount implements Action {
  type = ActionTypes.GET_DASHBOARD_COUNT


   constructor() {  }

}
export class AppGetUserReportMonthly implements Action {
  type = ActionTypes.GET_USER_REPORT_MONTHLY
   constructor(payload:credentials) {}

}
export class AppGetUserReportMonthlySuccess implements Action {
  type = ActionTypes.GET_USER_REPORT_MONTHLY_SUCCESS
   constructor(public payload:any) { }

}
export class AppGetDashBoardCountSuccess implements Action {
  type = ActionTypes.GET_DASHBOARD_COUNT_SUCCESS


   constructor(public payload: credentials) {  }

}



export type Actions
  = AppGetDashBoardCount
  | AppGetUserReportMonthly 
  | AppGetUserReportMonthlySuccess
