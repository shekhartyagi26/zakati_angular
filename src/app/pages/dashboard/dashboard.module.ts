import { NgModule,CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule, ReactiveFormsModule, } from '@angular/forms';
import { AppTranslationModule } from '../../app.translation.module';
import { NgaModule } from '../../theme/nga.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import {MdNativeDateModule,MaterialModule} from '@angular/material';

import { Modals } from '../ui/components/modals/modals.component';


import { SmartTables } from '../tables/components/smartTables/smartTables.component';
import { SmartTablesService } from '../tables/components/smartTables/smartTables.service';



import { Dashboard } from './dashboard.component';
import { routing }       from './dashboard.routing';

import { PopularApp } from './popularApp';
import { PieChart } from './pieChart';
import { TrafficChart } from './trafficChart';
import { UsersMap } from './usersMap';
import { LineChart } from './lineChart';
import { Feed } from './feed';
import { Todo } from './todo';
import { Calendar } from './calendar';
import { CalendarService } from './calendar/calendar.service';
import { FeedService } from './feed/feed.service';
import { LineChartService } from './lineChart/lineChart.service';
import { PieChartService } from './pieChart/pieChart.service';
import { TodoService } from './todo/todo.service';
import { TrafficChartService } from './trafficChart/trafficChart.service';
import { UsersMapService } from './usersMap/usersMap.service';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { ChartModule }  from 'angular2-highcharts'; 
// import { Chart } from 'chart.js/src/chart.js';



@NgModule({
  imports: [
    CommonModule,
     ChartsModule ,
    FormsModule,
    AppTranslationModule,
    NgaModule,
   ReactiveFormsModule,
    routing,
    Ng2SmartTableModule,
    NgbModalModule,
    MdNativeDateModule,MaterialModule,
    ChartModule.forRoot(require('highcharts'))
  ],
  declarations: [
    PopularApp,
    PieChart,
    TrafficChart,
    UsersMap,
    LineChart,
    Feed,
    Todo,
    Calendar,
    Dashboard,
    
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA],
  providers: [
    CalendarService,
    FeedService,
    LineChartService,
    PieChartService,
    TodoService,
    TrafficChartService,
    UsersMapService
  ],
   
})
export class DashboardModule {}
